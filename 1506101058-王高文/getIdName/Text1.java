package getIdName;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by 15c on 2017/2/27.
 */
//可获取主机ip
public class Text1 {
    public static void main(String[] args) {
        InetAddress inetAddress=null;

        try {
            inetAddress=InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }


        System.out.println("本机名称是：" + inetAddress.getHostName());
        System.out.println("本机的ip是 ："+ inetAddress.getHostAddress());
    }
}
