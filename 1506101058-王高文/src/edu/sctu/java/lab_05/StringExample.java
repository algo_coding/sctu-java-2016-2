package src.edu.sctu.java.lab_05;

/**
 * Created by 15c on 2016/11/10.
 */
public class StringExample {
    public static void main(String[] args) {
        String s1 = "I am a student";
        char b[] = {'H', 'e', 'l', 'l', ' ', 'W', 'o', 'r', 'l', 'd'};
        String s2=new String(b);
        if(s1.equals(s2)){
            System.out.println("s1与s2相同");
        }
        else {
            System.out.print("s1与s2不同");
        }
        String s3=new String("22030219851022024");
        if(s3.startsWith("220302")){
            System.out.println("吉林省的身份证");
        }
        String s4=new String("你"),
                s5=new String("我");
        if(s4.compareTo(s5) > 0)//按着字典序s4大于s5的表达式。
        {

            System.out.println("按字典序s4大于s5");

        }
        else{
            System.out.println("按字典序s4小于s5");
            // System.out.println(s4.compareTo(s5));
        }
        int position=0;
        String path="c:\\java\\jsp\\A.java";
        position=path.lastIndexOf("\\");
        System.out.println("c:\\java\\jsp\\A.java中最后出现\\的位置:"+position);
        String fileName=path.substring(12, 18);//获取path中“A.java”子字符串。
        System.out.println("c:\\java\\jsp\\A.java中含有的文件名:"+fileName);
        String s6=new String("100"),
                s7=new String("123.678");
        int n1=Integer.parseInt(s6) ;   //将s6转化成int型数据。
        double n2=Double.parseDouble(s7);  //将s7转化成double型数据。
        float f=Float.parseFloat(s7);//将s7转化成float型数据。
        //将s7转化成double型数据。
        double m=n1+n2;
        System.out.println(f);
        System.out.println(m);
        String s8=String.valueOf(m);
        position=s8.indexOf(".");//小数点位置
        String temp=s8.substring(position+1);//将小数点后面的小数装换为字符串
        System.out.println("数字"+m+"有"+temp.length()+"位小数") ;
        // System.out.println(temp) ;
        String s9=new String("ABCDEF");
        char a[]=s9.toCharArray();//转换为字符数组；
        for(int i=a.length-1;i>=0;i--)
        {
            System.out.print(" "+a[i]);
        }
    }
}
