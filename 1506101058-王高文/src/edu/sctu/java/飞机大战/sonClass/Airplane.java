package src.edu.sctu.java.飞机大战.sonClass;

import java.util.Random;

/**
 * Created by 15c on 2017/1/6.
 */
public class Airplane extends Flyer {
   private int speed=2;//每次下落2单位
   private int score=5;//分数奖励
   public int getScore(){
        return  score;
    }


 public Airplane(){
     image=ShootGame.airplane;
     width=image.getWidth();
     height=image.getHeight();
     y=-height;
     Random r=new Random();
     x=r.nextInt(ShootGame.WIDTH-width);
 }


    @Override
    public boolean outOfBounds() {
        //敌机y坐标大于游戏界面高度，就算越界
        return y>ShootGame.HEIGHT;
}

    @Override
    public void sdept() {
    //每次向下2单位
        y+=speed;
    }

}
