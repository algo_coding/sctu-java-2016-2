package src.edu.sctu.java.飞机大战.sonClass;

import java.util.Random;

/**
 * Created by 15c on 2017/1/6.
 */
public class Hero extends Flyer {
    private int doubleFire;//双倍火力
    private int life;//生命值

    private int score;//得分

    public Hero(){
        image=ShootGame.hero0;
        width=image.getWidth();
        height=image.getHeight();
        x=140;
        y=500;
        doubleFire=0;
        life=3;
        score=0;
    }

    public  int getLife(){
        return life;
    }

    public int getScore(){
        return score;
    }

    @Override
    //实现英雄机动画效果的方法
    //让hero0与hero1之间定时切换
    public void sdept() {
        Random r=new Random();
        if(r.nextInt(2)==0){
            image=ShootGame.hero0;

        }else image=ShootGame.hero1;
    }

    @Override
    public boolean outOfBounds() {
        return false;
    }

    //英雄机随鼠标移动方法，要求传入鼠标当前x，y坐标；
    public void move(int x,int y){
        //传入鼠标位置x.y;move方法使鼠标位置与英雄机中心位置一致；
        this.x=x-width/2;
        this.y=y-height/2;

    }
    //英雄机获得分数或是奖励方法，f指向父类型变量，即同时可以指向蜜蜂又可以指向敌机
    public void getScore_Award(Flyer f){
        //先判断敌方对象类型
        if(f instanceof Airplane){//如果为敌机类型
            //获得敌机保存的分值，加到当前英雄机当前分数
            score+=((Airplane) f).getScore();
        }else {//蜜蜂类型
            //判断是否为双倍活力
            if(((Bee)f).getAwardType()==Bee.Double_fire){
                doubleFire+=40;
            }else {
                life+=1;
            }
        }

    }
    //子弹射击方法;新创建的子弹对象可以是一发，也可以是两发；所以用数组保存返回
    public Bullet[] shoot(){
        Bullet[] bullets=null;
        //如何创建双倍火力
        if (doubleFire!=0){//创建双倍火力
            bullets=new Bullet[2];
           Bullet b1=new Bullet(x+width/4,y-ShootGame.bullet.getHeight());
            Bullet b2=new Bullet(x+width*3/4,y-ShootGame.bullet.getHeight());
            bullets[0]=b1;
            bullets[1]=b2;
            //每创建双倍火力，doubleFire-2；
            doubleFire-=2;
        }else {
            //单倍火力
            //子弹x坐标：x+width/2
            //子弹y坐标：y-子弹图片高度
            bullets = new Bullet[1];
            bullets[0] = new Bullet(x + width / 2, y - ShootGame.bullet.getHeight());
        }
        return bullets;
    }
    //英雄机碰撞检测方法；f为父类型变量；可能为敌机，也可能是蜜蜂；false表明未碰撞；true碰撞；
    public boolean hit(Flyer f)    {
        //调用碰撞检测方法，检测是否发生碰撞
        boolean r=Flyer.bang(this,f);
        if(r){//如果碰撞
           life--;
            doubleFire=0;
        }
        return r;
    }

}
