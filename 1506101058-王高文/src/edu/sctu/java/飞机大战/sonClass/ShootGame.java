package src.edu.sctu.java.飞机大战.sonClass;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;
import java.util.Timer;

/**
 * Created by 15c on 2017/1/6.
 */
public class ShootGame  extends JPanel{
    //在静态块中加载所有图片；一般加载一次反复调用就行；
    //游戏界面的大小固定400*700 常量
    public static final int WIDTH=400;
    public static final int HEIGHT=700;

    //游戏启动时将所有图片加载中类存中；在启动时只需要启动一次静态块

    public static BufferedImage background;//背景图片
    public static BufferedImage start;//开始图片
    public static BufferedImage airplane;//敌机图片
    public static BufferedImage bee;//蜜蜂图片
    public static BufferedImage bullet;//子弹图片
    public static BufferedImage hero0;//英雄机0
    public static BufferedImage hero1;//英雄机1
    public static BufferedImage pause;//暂停图片
    public static BufferedImage gameover;//结束图片

    /*为游戏中角色对象定义数据结构
    * 一个英雄机对象
    * 一个储存敌人（敌机，蜜蜂）的对象的数组:并且应该用父类型保存
    * 一个储存子弹的对象数组
    * */

    public Hero hero=new Hero();
    public Flyer[] flyers={};
    public Bullet[] bullets={};


    //定义游戏状态；当前状态变量：默认是开始状态
    private int state=START;
    //定义游戏备选项常量：
    private static final int START=0;
    private static final int RUNNING=1;
    private static final int PAUSE=2;
    private static final int GAMEOVER=3;

    //静态块，尽在类首次加载到方法区时执行一次，专门加载静态资源
    static {
//        java从硬盘中记载图片到内存中：ImageIO.read方法；专门从硬盘中加载图片的静态方法；不用实例化；直接调用；
//  ShootGame.class: 获得当前类的加载器的路径
//        ShootGame.class.getResource("文件名")：从当前类所在的路径下，加载指定文件到程序中；
        try {
            background=ImageIO.read(ShootGame.class.getResource("background.jpg"));
            airplane=ImageIO.read(ShootGame.class.getResource("airplane.png"));
            bee=ImageIO.read(ShootGame.class.getResource("bee.png"));
            bullet=ImageIO.read(ShootGame.class.getResource("bullet.png"));
            hero0=ImageIO.read(ShootGame.class.getResource("hero0.png"));
            hero1=ImageIO.read(ShootGame.class.getResource("hero1.png"));
            pause=ImageIO.read(ShootGame.class.getResource("pause.png"));
            gameover= ImageIO.read(ShootGame.class.getResource("gameover.png"));
            start=ImageIO.read(ShootGame.class.getResource("start.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //绘制窗体JFrame对象——窗框
        //在窗体中绘制内容，需要嵌入背景面板——JPanel；
         JFrame frame=new JFrame("Fly");//new 对象
        frame.setSize(WIDTH,HEIGHT);//设置宽高
        frame.setAlwaysOnTop(true);//使窗体总在最顶端，不被其他窗口所挡住
        //窗体关闭则程序关闭
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);//设置窗体初始位置，其中null表示居中；
        /*在窗体中嵌入背景面板对象-JPanel对象*/
        ShootGame game=new ShootGame();//创建背景面板对象
        frame.add(game);//将面板对象加入窗体对象中
        //窗体默认不可见，需要调用setVisible方法
        frame.setVisible(true);//自动调用窗体的paint方法
            game.action();
    }
    //游戏启动时所做事情
    public void action(){
        //游戏开始开启鼠标事件监听
        //one:创建mouseAdapter匿名内部类————事件的相应程序
        MouseAdapter mouseAdapter=new MouseAdapter() {
            //重写鼠标移动事件
            @Override
            public void mouseMoved(MouseEvent e) {
              //只有在游戏RUNNING状态下，英雄机才跟着鼠标移动
                if (state==RUNNING) {
                    //随时获取鼠标坐标
                    int x = e.getX();
                    int y = e.getY();
                    //将鼠标坐标位置传递给英雄机的move方法
                 hero.move(x,y);
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (state==START){//只要处于Start状态下，单击鼠标才切换状态为Running；
                    state=RUNNING;
                }else if(state==GAMEOVER){
                    state=START;
                    //从gameover到开始状态，要初始化英雄机数据；
                     hero=new Hero();
                    flyers=new Flyer[0];
                   bullets=new Bullet[0];
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {//鼠标移出
               if(state==RUNNING){
                   state=PAUSE;
               }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if(state==PAUSE){
                    state=RUNNING;
                }
            }
        };//匿名内部类结束加分号；
        //需要相应鼠标事件必须将鼠标事件添加事件监听器；
        this.addMouseMotionListener(mouseAdapter);
       // MouseMotionListener不支持鼠标单击事件
        //想要程序相应鼠标单击事件，就必须再注册一个新的MouseListener监听器；
        this.addMouseListener(mouseAdapter);//支持鼠标单击事件

        //1.创建定时器timer
       Timer timer=new Timer();
        //2.调用schedule方法做计划
        //TimerTask()匿名内部类 run方法核心
        timer.schedule(new TimerTask() {
            //定义计时器index，记录run方法次数
            private int runTimes = 0;

            @Override
            //自动创建对象；


            public void run() {
                //除repaint方法外，其余功能在（Running）状态下执行
                if (state == RUNNING) {
                    //每执行一次run方法就++
                    runTimes++;
                    if (runTimes % 50 == 0) {
                        nextOne();
                    }
                    //遍历对象， 调用对象的sdept方法；移动对象
                    for (int i = 0; i < flyers.length; i++) {
                        flyers[i].sdept();
                    }
                    //每300毫秒创建一次子弹
                    if (runTimes % 30 == 0) {
                        shoot();
                    }

                    //遍历每个子弹对象， 调用对象的sdept方法；移动对象
                    for (int i = 0; i < bullets.length; i++) {
                        bullets[i].sdept();
                    }
                    //英雄机动画效果
                    hero.sdept();
                    //添加子弹击中敌人的碰撞检测
                    bang();
                    //敌人与英雄机碰撞检测
                    hit();
                    //添加越界销毁；
                    outOfBounds();

                    //强调：只要前面发生改变，必须调用repaint方法重绘界面；
                }
                repaint();
            }
        }, 10, 10);//每隔十毫秒变化一次
    }

    @Override
    public void paint(Graphics g) {
//        g.drawLine(10,10,100,100);
//        g.drawRect(10,10,100,100);
//        g.drawOval(10,10,100,100);
        //绘制背景图
        g.drawImage(background, 0, 0, null);
        //绘制英雄机
        paintHero(g);
        //绘制敌机
        /*测算代码
        Airplane airplane=new Airplane();
        airplane.y+=100;
        Bee bee=new Bee();
        bee.y+=50;
        flyers=new Flyer[2];
        flyers[0]=airplane;
        flyers[1]=bee;
         测试代码结束*/
        paintFlyers(g);
        paintBullet(g);
        //绘制分数与生命值
        paintScore_Life(g);
        //根据游戏状态绘制不同图片
        if(state==START){
            g.drawImage(start,40,320,null);
        }else if(state==PAUSE){
            g.drawImage(pause,40,320,null);
        }else if(state==GAMEOVER){
            g.drawImage(gameover,40,320,null);
        }
    }

//绘制英雄机对象的方法
    public void paintHero(Graphics g){
        g.drawImage(hero.image, hero.x, hero.y, null);
    }
//遍历敌人数组，批量绘制敌人对象
    public void paintFlyers(Graphics g){
        for(int i=0;i<flyers.length;i++){
            g.drawImage(flyers[i].image,flyers[i].x,flyers[i].y,null);
        }
    }
    //遍历子弹数组，批量绘制子弹对象
    public void paintBullet(Graphics g){
        for(int i=0;i<bullets.length;i++){
            g.drawImage(bullets[i].image,bullets[i].x,bullets[i].y,null);
        }
    }

    //动态随机创建敌机或者蜜蜂对象，生成一个新敌人flyers需扩容1，然后新敌人添加进数组
    public void nextOne(){
        Random r=new Random();
        Flyer f=null;
        if(r.nextInt(20)==0){//当随机数取0时 才创建一次蜜粉，否则创建敌机
            f=new Bee();
        }else {
            f=new Airplane();
        }
        //数组扩容
        flyers= Arrays.copyOf(flyers, flyers.length + 1);
        //新生成的对象添加进数组中
        flyers[flyers.length-1]=f;

    }
    //获得英雄机发射的子弹对象，并将新创建的子弹存入子弹数组；
    public void shoot(){
        Bullet[] newBullets=hero.shoot();
        //根据返回新子弹数量扩容数组
        bullets= Arrays.copyOf(bullets,bullets.length+newBullets.length);
        //从 newBullets数组中拷贝到 bullets末尾；
        System.arraycopy(newBullets,0,bullets,bullets.length-newBullets.length,newBullets.length);
    }
    //遍历子弹数组与敌机数组；进行碰撞检测；一旦发生碰撞敌人与子弹都将减少一个
    public void bang(){
        for(int i=0;i<bullets.length;i++){//取出每个子弹
            for (int j=0;j<flyers.length;j++){//当前子弹与敌机做比较
                if(Flyer.bang(bullets[i],flyers[j])){//如果发生碰撞
                    //从敌人数组中删除被击中敌机；并且子弹也将被删除；
                    //使用数组最后一个填充被删除的敌机或者是子弹；
                    hero.getScore_Award(flyers[j]);//为英雄机获得分数与奖励
                    flyers[j]=flyers[flyers.length-1];
                    //压缩数组,元素个数-1
                    flyers=Arrays.copyOf(flyers,flyers.length-1);
                    bullets[i]=bullets[bullets.length-1];
                    //压缩数组,元素个数-1
                    bullets=Arrays.copyOf(bullets,bullets.length-1);
                    i--;//每发现一次碰撞；子弹就退一个元素，再重新检测当前位置新子弹
                    //只要有敌人被击中就跳出循环
                    break;
                }
            }
        }
    }
   //在屏幕左上角写两行文字，显示分数与生命值
    public void paintScore_Life(Graphics g){
        int x=10;
        int y=15;
        Font font=new Font(Font.SANS_SERIF,Font.BOLD,14);
        g.setFont(font);
        Color myredcolor = new Color(8*25+5,0,0);
        g.setColor(myredcolor);

        //绘制分数
        g.drawString("SCORE:" + hero.getScore(), x, y);
        //绘制生命值
        y+=20;
        g.drawString("LEFT:"+hero.getLife(),x,y);
    }
    //检查飞行物越界
    public void outOfBounds(){
        //检查敌人越界
        Flyer[] Flives=new Flyer[flyers.length];
        //遍历敌人数组，将存活的敌机保存在新数组中；
        //设计Flives数组的计数器index：1：标识下一个存活对象的位置
                                    //2:统计lives数组中共有多少存活对象
        int index=0;
        for (int i=0;i<flyers.length;i++){
            if(!flyers[i].outOfBounds()){//没有越界的
                    Flives[index]=flyers[i];
                    index++;
            }
        }//遍历结束后；index保存存活个数；lives数组保存的是存活敌机对象；按照index个数对lives数组进行压缩；
        //压缩后的新数组替换回flyers变量中
        flyers=Arrays.copyOf(Flives,index);

        //检测子弹越界
        Bullet[] Blives=new Bullet[bullets.length];
        index =0;
        for (int i=0;i<bullets.length;i++){
            if(!bullets[i].outOfBounds()){//没有越界的
                Blives[index]=bullets[i];
                index++;
            }
         }
        bullets=Arrays.copyOf(Blives,index);
    }
    //遍历敌人数组是否与英雄机碰撞

    public void hit(){
        Flyer lives[]=new Flyer[flyers.length];
        //记录存活的敌人
        //压缩存活敌人数组，并替换敌人数组
        int index=0;
        for (int i=0;i<flyers.length;i++){
            if (!hero.hit(flyers[i])){
                lives[index]=flyers[i];
                index++;
            }
        }
        if(hero.getLife()<=0){
            state=GAMEOVER;
        }
        flyers=Arrays.copyOf(lives,index);
    }
}
