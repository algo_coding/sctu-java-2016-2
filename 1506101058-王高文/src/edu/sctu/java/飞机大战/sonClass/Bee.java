package src.edu.sctu.java.飞机大战.sonClass;

import java.util.Random;

/**
 * Created by 15c on 2017/1/6.
 */
public class Bee extends Flyer {
    public static final int Double_fire=0;//双倍活力奖励
    public static final int Life=1;//奖励一次生命

//构造方法
    public Bee(){
        //sdep1重主程序中获取图片对象
        image=ShootGame.bee;
        //使用图片的宽高设置对象宽高
        height=image.getHeight();
        width=image.getWidth();
        //对象开始下落高度
        y=-height;
        //开始下落的x坐标0-(界面宽度-蜜蜂宽度)之内选随机数
        Random r=new Random();
        x=r.nextInt(ShootGame.WIDTH-width);
        //在0，1中随机选取奖励
        awardType=r.nextInt(2);//左包括右不包括


    }

   private int xspeed=1;//水平速度
   private int yspeed=2;//垂直速度
    private int awardType;//奖励类型


    public int getAwardType(){
        return awardType;
    }

    @Override
    public void sdept() {
    // 每次x移动1单位，y也移动1单位
        x+=xspeed;
        y+=yspeed;
     //蜜蜂不超出边界，碰上时则xspeed*-1；相当于反方向移动
        if (x<0||x>(ShootGame.WIDTH-width)){
            xspeed*=-1;

        }

    }

    @Override
    public boolean outOfBounds() {
        //蜜粉y坐标大于游戏界面高度，就算越界
        return y>ShootGame.HEIGHT;
    }
}
