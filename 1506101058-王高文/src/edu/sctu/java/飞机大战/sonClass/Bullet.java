package src.edu.sctu.java.飞机大战.sonClass;

/**
 * Created by 15c on 2017/1/6.
 */
public class Bullet extends Flyer {
   private int speed=3;//子弹移动速度

   public Bullet(int x,int y){
     this.x=x;
      this.y=y;
      image=ShootGame.bullet;
      width=image.getWidth();
      height=image.getHeight();
   }
   @Override
   public void sdept() {
       //每次向上1单位
       y-=speed;
   }

   @Override
   public boolean outOfBounds() {
       //子弹y坐标+子弹高度小于0时
      return y+height<0;
   }
}
