package src.edu.sctu.java.飞机大战.sonClass;

import java.awt.image.BufferedImage;

/**
 * 父类
 * Created by 15c on 2017/1/6.
 */
public abstract class Flyer {
    protected int width;//飞行物宽度
    protected int height;//飞行物高度
    protected int x;//飞行物x坐标
    protected int y;//飞行物y坐标
    protected BufferedImage image ;//保存图片

    public abstract   void  sdept();//飞行物移动，子类自己实现
    public abstract  boolean outOfBounds();//判断飞行物是否出界；子类自己实现
    //判断两矩形飞行物是否碰撞；
    //与具体对象无关，定义为静态；true碰撞；false不碰撞
    public static boolean bang(Flyer f1,Flyer f2){
        //one:求出两矩形中心点
        int f1x=f1.x+f1.width/2;
        int f1y=f1.y+f1.height/2;
        int f2x=f2.x+f2.width/2;
        int f2y=f2.y+f2.height/2;
        //横向纵向检测碰撞
        boolean H=Math.abs(f1x-f2x)<(f1.width+f2.width)/2;
        boolean V=Math.abs(f1y-f2y)<(f1.height+f2.height)/2;
        //当两个方向都发生碰撞，才能说明是真的碰撞；

        return H&V;
    }
}
