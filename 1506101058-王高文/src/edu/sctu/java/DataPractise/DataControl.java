package src.edu.sctu.java.DataPractise;

import java.sql.*;

/**
 * Created by 15c on 2016/10/16.
 */
public class DataControl {
    private static String ipAddress = "localhost";
    private static int port = 3306;
    private static String databaseName = "test";
    private static String characterEncoding = "utf8";
    private static boolean useSSL = false;
    private static String url = "jdbc:mysql://" + ipAddress + ":" + port + "/" + databaseName + "?characterEncoding=" + characterEncoding + "&useSSL=" + useSSL + "&serverTimezone=UTC" + "&useUnicode=true";
    private static String user = "root";
    private static String keys = null;
    private static Connection conn;

    protected static Statement statement = null;//数据库操作语句库
    protected static ResultSet result = null;//查询结果集
    protected static ResultSetMetaData resultSetMetaData = null;
    //获得数据库连接
    public static Connection getConn() {
        try {
            Class.forName("com.mysql.jdbc.Driver");//加载驱动
            conn = DriverManager.getConnection(url, user, keys);//连接数据库
            statement = conn.createStatement();//创建sql语句容器；
            System.out.print("连接成功\n");
        } catch (ClassNotFoundException e) {
            System.out.print("数据库连接出错");
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
    //创建表
    public static void creatTable(String tableName, String[] str) {
        String sql = "CREATE TABLE if not exists " + tableName + "(";
        for (int i = 0; i < str.length; i++) {
            String sql1 = str[i] + (i == str.length - 1 ? ")" : ",");
            sql = sql.concat(sql1);
        }
        try {
            statement.executeUpdate(sql);//创建表；
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.print("请查看表是否已经建好");

        }
    }
    public static void InsertData(String tableName,String id,String Stname,String age){
        String sql = "insert into"+tableName+"id,Stname,age+ values(?,?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,id);
            pstmt.setString(2, Stname);
            pstmt.setString(3, age);
            int count = pstmt.executeUpdate();//执行并记录次数

            if(count>0){
                System.out.println("插入数据成功！影响的记录条数是"+count);
                conn.close();
            }else{
                System.out.println("插入数据失败！");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
