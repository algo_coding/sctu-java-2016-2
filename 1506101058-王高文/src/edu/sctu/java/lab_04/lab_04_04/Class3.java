package src.edu.sctu.java.lab_04.lab_04_04;

/**
 * Created by 15c on 2016/11/17.
 */
public class Class3 {
    public static void main (String args[]){
        int a[]={34,12,8,67,88,23,98,101,119,56};
        Demo m1=new Demo();
        sub_sort_Demo m2=new sub_sort_Demo();
        System.out.println("排序前的数据为：");
        for (int i=0;i<10;i++)
            System.out.print("  "+a[i]);
        //调用父类中的sort()方法
        m1.sort(a.length,a);
        System.out.println("\n按升序排列的数据为：");
        for (int i=0;i<10;i++)
            System.out.print("  "+a[i]);
        //调用子类中的sort()方法,该方法实现了对父类中方法的覆盖

        m2.sort(a.length ,a);
        System.out.println("\n按降序排列的数据为：");
        for (int i=0;i<10;i++)
            System.out.print("  "+a[i]);
    }

}
