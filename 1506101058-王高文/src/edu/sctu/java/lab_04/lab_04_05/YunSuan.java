package src.edu.sctu.java.lab_04.lab_04_05;

/**
 * Created by 15c on 2016/11/17.
 */
public class YunSuan implements JaJian {
    double a,b;

    public YunSuan(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double Jie() {
        double result=a-b;
        return result;
    }

    @Override
    public double Jian() {
        double result=a+b;
        return result;
    }
}
