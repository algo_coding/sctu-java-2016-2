package src.edu.sctu.java.lab_03;

/**
 * Created by 15c on 2016/11/3.
 */
public class Trangle {
    double sideA,sideB,sideC,area,length;
    boolean boo;

    public Trangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
        if((sideA+sideB)>sideC&&(sideA+sideC)>sideB&&(sideB+sideC)>sideA){
            boo=true;System.out.println("可以构成一个三角形");
        }else {boo=false;System.out.println("不能构成三角形");}
    }

    public double getTrangleLength()
    {
        if (boo==true){
            length=sideA+sideB+sideC;return length;
        }else System.out.println("不是一个三角形，不能计算周长");
        return 0;
    }
    public double  getArea()
    {
        if(boo==true)
        {
            double p=(sideA+sideB+sideC)/2.0;
            area=Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC)) ;
            return area;
        }
        else
        {
            System.out.println("不是一个三角形,不能计算面积");
            return 0;
        }
    }
    public void setABC(double a,double b,double c)
    {

        if((a+b)>c&&(a+c)>b&&(b+c)>a) //a,b,c构成三角形的条件表达式
        {
            boo=true; //给boo赋值。
             sideC=c;sideB=b;sideA=a;
            System.out.println("可以构成三角形");
        }
        else
        {
           boo=false;//给boo赋值。
            System.out.println("不能构成三角形");
        }
    }

}
