package src.edu.sctu.java.lab_03;

/**
 * Created by 15c on 2016/11/3.
 */
public class Circle {
    final double pi=3.1415;
    double radius,area,length;

    public Circle(double radius) {
        this.radius = radius;
    }
   public double getCircleLength(){
       length=2*pi*radius;
       return length;
   }
    public double getArea(){
        area=pi*radius*radius;
        return area;
    }
}

