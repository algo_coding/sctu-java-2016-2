package src.edu.sctu.java.Socket.tcpSocket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by 15c on 2017/2/27.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        System.out.print("请输入txt文件名及存放路径：");
        Scanner scanner=new Scanner(System.in);
        String str=scanner.nextLine();
        File file=new File(str);
        if (file.exists()==false){
            file.createNewFile();
            System.out.println("文件创建成功");
        }else System.out.println("文件已存在");
        System.out.println("等待客户端访问");
        ServerSocket serverSocket=new ServerSocket(9999);

        Socket socket=serverSocket.accept();//监听

        InputStream inputStream=socket.getInputStream();

        FileOutputStream fileOutputStream=new FileOutputStream(file);
        //读网络数据
        byte[] byteArray=new byte[1024];

        int len=0;

        while (true){
            len=inputStream.read(byteArray);
            if (len == -1) {
                break;
            }
            fileOutputStream.write(byteArray,0,len);
            fileOutputStream.write("\r\n".getBytes());

            String str1 = new String(byteArray, 0, len);
            if (str1.equals("888")) {
                System.out.println("服务器端结束！");
                serverSocket.close();
                socket.close();
                inputStream.close();
                fileOutputStream.close();



                break;
            }
        }
    }
}
