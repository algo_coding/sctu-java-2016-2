package src.edu.sctu.java.Socket.tcpSocket.tcpSocketExample;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by 15c on 2017/2/27.
 */
public class TcpClient {
    public static void main(String[] args) throws IOException {
        //1.实例化一个Socket
        Socket socket =new Socket(InetAddress.getLocalHost().getHostAddress(),8888);
        //2.获取输出流
        OutputStream outputStream=socket.getOutputStream();
        //3.输出一个byte[]数组
        byte[] byteArray="你好，这是使用TCP协议发送的数据......".getBytes();
        outputStream.write(byteArray);
        //4.释放资源
        outputStream.close();
        socket.close();
        System.out.println("发送端发送完毕！");
    }
}
