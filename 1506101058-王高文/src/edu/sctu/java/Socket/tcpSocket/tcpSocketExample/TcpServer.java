package src.edu.sctu.java.Socket.tcpSocket.tcpSocketExample;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by 15c on 2017/2/27.
 */
public class TcpServer {
    public static void main(String[] args) throws IOException {
        //1.实例化一个ServerSocket
        ServerSocket serverSocket=new ServerSocket(8888);
        //2.等待连接
        System.out.println("等待连接......");
        Socket socket=serverSocket.accept();

        System.out.println("有客户端连接成功！");
        //3.获取输入流
        InputStream inputStream=socket.getInputStream();
        //存数据的空数组
        byte[] byteArray = new byte[1024];
        //数据读入数组，返回一个长度值len；
        int len = inputStream.read(byteArray);
        //将数据转换为String
        String str = new String(byteArray,0,len);

        System.out.println("接收到信息：" + str);
        //4.释放资源
        inputStream.close();

        serverSocket.close();
    }
}
