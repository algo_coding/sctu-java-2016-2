package src.edu.sctu.java.Socket.udpSocket;

import java.io.IOException;
import java.net.*;

/**
 * Created by 15c on 2017/2/27.
 */
public class UdpClient {

    public static void main(String[] args) throws IOException {
        DatagramSocket socket=new DatagramSocket();//实例化 DatagramSocket对象
        //准备数据
        InetAddress id=InetAddress.getByName("10.131.31.34");//也可使用主机名
        int port=8888;//发送端口号
        byte[] byteArray ="你好".getBytes();//
        //准备数据包
        DatagramPacket pck=new DatagramPacket(byteArray,
                byteArray.length,
                id,
                port);

        socket.send(pck);//发送数据包

        socket.close();//释放资源
        System.out.print("信息已发出");
    }
}
