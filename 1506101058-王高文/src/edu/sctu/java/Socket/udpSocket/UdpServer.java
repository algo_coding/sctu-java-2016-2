package src.edu.sctu.java.Socket.udpSocket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created by 15c on 2017/2/27.
 */
public class UdpServer {
    public static void main(String[] args) throws IOException {
        DatagramSocket socket =new DatagramSocket(8888);//使用对应端口实例化DatagramSocket
        //准备空数据包
        byte[] byteArray=new byte[1024];
        DatagramPacket pck=new DatagramPacket(byteArray,byteArray.length);

        System.out.println("正在等待信息到达......");

        socket.receive(pck);// 程序在此处会阻塞

        System.out.println("有信息到达！");
        //读取信息
        byte[] byteInfo = pck.getData();

        String str = new String(byteInfo, 0, pck.getLength());

        String ip = pck.getAddress().getHostAddress();

        System.out.println("接收到信息：IP：" + ip + " 内容：" + str);
        //释放资源
        socket.close();
    }
}
