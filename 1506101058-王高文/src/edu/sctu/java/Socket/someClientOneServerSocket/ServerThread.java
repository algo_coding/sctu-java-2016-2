package src.edu.sctu.java.Socket.someClientOneServerSocket;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * Created by 15c on 2017/2/27.
 */
public class ServerThread extends Thread{
    public int index ;
    private Socket socket ;

    public ServerThread(int index,Socket socket) {
        this.index = index;
        this.socket=socket;
    }

    @Override
    public void run() {


        try {
        //获取输入流
            InputStream netIn= socket.getInputStream();
            FileOutputStream fileOutputStream=new FileOutputStream(index+".png");
            byte[] byteArray = new byte[1024];
            int len = 0;
            while((len = netIn.read(byteArray))  != -1){
                fileOutputStream.write(byteArray, 0, len);
            }
            fileOutputStream.close();
            netIn.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
