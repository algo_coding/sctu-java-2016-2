package src.edu.sctu.java.Socket.someClientOneServerSocket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by 15c on 2017/2/27.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(8888);
        int index = 0;
        System.out.println("服务器已就位");
        while(true){
            Socket socket = server.accept();
            //启动线程；
            new ServerThread(index++,socket).start();
            System.out.println("服务器已经接收" + index + "个");
        }

    }
}
