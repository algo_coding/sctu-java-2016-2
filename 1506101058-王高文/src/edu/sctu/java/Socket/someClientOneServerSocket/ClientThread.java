package src.edu.sctu.java.Socket.someClientOneServerSocket;

import java.io.FileInputStream;
import java.io.IOException;

import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by 15c on 2017/2/27.
 */
public class ClientThread  extends Thread {

    private int index;

    public ClientThread(int index) {
        this.index = index;
    }

    @Override
    public void run() {

        try {
        //连接服务器

            Socket socket=new Socket(InetAddress.getLocalHost().getHostAddress(),8888);

            //获得网络输出流
            OutputStream netOut= socket.getOutputStream();
            //文件输入流
            FileInputStream fileInputStream=new FileInputStream("H:\\socketExample\\"+index+".png");
            //一次读取一个数组
            byte[] byteArray = new byte[1024];

            int len=0;

            while ((len=fileInputStream.read(byteArray))!=-1){
                netOut.write(byteArray,0,len);
            }
            //释放资源
            fileInputStream.close();
            netOut.close();
            socket.close();
            System.out.println("文件：" + index + ".png 上传完毕！");

        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
