package edu.sctu.java.lab.udp;

/**
 * Created by apple on 28/12/2016.
 */
public class Config {

    public static final int SERVER_PORT = 10000;
    public static final String SERVER_HOST = "localhost";
    public static final int BUFFER_SIZE = 8096;
}
