package edu.sctu.java.lab.lab07.test02;

import java.awt.*;

public class MyCanvas extends Canvas {
    int r;
    Color c;

    public void setColor(Color c) {
        this.c = c;
    }

    public void setR(int r) {
        this.r = r;
    }

    public void paint(Graphics g) {
        g.setColor(c);
        g.fillOval(0, 0, 2 * r, 2 * r);
    }

    public int getR() {
        return r;
    }
}

