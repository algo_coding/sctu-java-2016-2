package edu.sctu.java.lab.udp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.*;

/**
 * Created by apple on 28/12/2016.
 */
public class UdpClient extends JFrame implements ActionListener {


    private DatagramSocket socket;
    private DatagramPacket packet;

    private byte[] data;

    private JButton sendBtn;
    private JTextField messageTF;


    UdpClient() throws SocketException, UnknownHostException {

        data = new byte[Config.BUFFER_SIZE];
        socket = new DatagramSocket();


        messageTF = new JTextField();
        add(messageTF, BorderLayout.NORTH);

        sendBtn = new JButton("send");
        sendBtn.addActionListener(this);
        add(sendBtn, BorderLayout.SOUTH);

        setBounds(100, 100, 400, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);

    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == sendBtn) {

            String msg = messageTF.getText();
            data = msg.getBytes();

            try {
                packet = new DatagramPacket(data, msg.length(),
                        InetAddress.getByName(Config.SERVER_HOST), Config.SERVER_PORT);
                socket.send(packet);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }


    public static void main(String[] args) throws IOException {

        new UdpClient();
    }
}
