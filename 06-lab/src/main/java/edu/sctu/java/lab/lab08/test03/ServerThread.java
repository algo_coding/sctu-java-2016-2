package edu.sctu.java.lab.lab08.test03;

import java.io.FileInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

class ServerThread extends Thread {
    private DatagramPacket packet;
    private DatagramSocket socket;

    ServerThread(DatagramPacket packet) {
        this.packet = packet;
    }


    public void run() {
        FileInputStream in;
        byte b[] = new byte[8192];
        try {
            in = new FileInputStream("/Users/apple/a.jpg");
            int n = -1;
            while ((n = in.read(b)) != -1) {
                DatagramPacket data = new DatagramPacket(b, b.length, packet.getAddress(), packet.getPort()); //创建数据包，目标地址和端口分别是
                //address和5678，其中的数据为数组b中的前n个字节
                socket.send(data);                          // mailSend发送数据data
            }
            in.close();

            byte end[] = "end".getBytes();
            DatagramPacket data = new DatagramPacket(end, end.length, packet.getAddress(), packet.getPort()); //创建数据包，目标地址和端口分别是
            //address和5678，其中的数据为数组end中的全部字节
            socket.send(data);                        // mailSend发送数据data
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
