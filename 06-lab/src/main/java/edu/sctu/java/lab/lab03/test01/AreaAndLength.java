package edu.sctu.java.lab.lab03.test01;

public class AreaAndLength {
    public static void main(String args[]) {
        double length, area;
        Circle circle = null;
        Trangle trangle;
        Lader lader;


        circle = new Circle(2);
        trangle = new Trangle(1, 2, 4);
        lader = new Lader(1, 2, 5);

        // circle调用方法返回周长并赋值给length
        length = circle.getLength();
        System.out.println("圆的周长:" + length);

        area = circle.getArea(); // circle调用方法返回面积并赋值给area
        System.out.println("圆的面积:" + area);

        length = trangle.getLength(); // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:" + length);

        area = trangle.getArea(); // trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:" + area);

        area = lader.getArea(); // lader调用方法返回面积并赋值给area
        System.out.println("梯形的面积:" + area);

        trangle.setABC(12, 34, 1); // trangle调用方法设置三个边，要求将三个边修改为12,34,1。
        area = trangle.getArea();// trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:" + area);

        length = trangle.getLength(); // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:" + length);
    }
}

