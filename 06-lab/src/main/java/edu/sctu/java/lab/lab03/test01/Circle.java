package edu.sctu.java.lab.lab03.test01;

class Circle {
    private static final double PI = 3.14;

    double radius, area;

    Circle(double r) {
        this.radius = r; //方法体
    }

    double getArea() {
        return PI * radius * radius; //方法体，要求计算出area返回
    }

    double getLength() {
        return 2 * PI * radius; //getArea方法体的代码,要求计算出length返回
    }

    void setRadius(double newRadius) {
        radius = newRadius;
    }

    double getRadius() {
        return radius;
    }
}

