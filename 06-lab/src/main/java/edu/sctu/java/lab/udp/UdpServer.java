package edu.sctu.java.lab.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Created by apple on 28/12/2016.
 */
public class UdpServer {
    public static void main(String[] args) throws IOException {


        byte[] bytes = new byte[Config.BUFFER_SIZE];
        DatagramSocket socket = new DatagramSocket(Config.SERVER_PORT);
        DatagramPacket packet = new DatagramPacket(bytes, bytes.length);

        while (true) {
            socket.receive(packet);
            String data = new String(packet.getData(), 0, packet.getLength());
            System.out.println("from client: " + data);
        }


    }
}
