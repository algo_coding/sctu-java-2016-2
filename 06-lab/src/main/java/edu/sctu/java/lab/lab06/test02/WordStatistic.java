package edu.sctu.java.lab.lab06.test02;

import java.io.*;
import java.util.Vector;

public class WordStatistic {
    Vector allWords;
    Vector noSameWords;

    WordStatistic() {
        allWords = new Vector();
        noSameWords = new Vector();
    }

    public void wordStatistic(File file) {
        try {
            System.out.println(file.getAbsolutePath());
            RandomAccessFile inOne = new RandomAccessFile(file, "r");        //创建指向文件file的inOne 的对象
            RandomAccessFile inTwo = new RandomAccessFile(file, "r");        //创建指向文件file的inTwo 的对象

            long wordStartPosition = 0;
            long wordEndPosition = 0;
            long length = inOne.length();
            int flag = 1;
            int c = -1;


            for (int k = 0; k <= length; k++) {
                c = inOne.read();       // inOne调用read()方法

                boolean isAlphabet = (c <= 'Z' && c >= 'A') || (c <= 'z' && c >= 'a');

                if (isAlphabet) {
                    if (flag == 1) {
                        wordStartPosition = inOne.getFilePointer() - 1;
                        flag = 0;
                    }
                } else {
                    if (flag == 0) {
                        if (c == -1)
                            wordEndPosition = inOne.getFilePointer();
                        else
                            wordEndPosition = inOne.getFilePointer() - 1;

                        byte cc[] = new byte[(int) wordEndPosition - (int) wordStartPosition];

                        inTwo.seek(wordStartPosition);// inTwo调用seek方法将读写位置移动到wordStarPostion
                        inTwo.readFully(cc);// inTwo调用readFully(byte a)方法，向a传递cc

                        String word = new String(cc);
                        allWords.add(word);

                        if (!(noSameWords.contains(word)))
                            noSameWords.add(word);

                    }
                    flag = 1;
                }
            }
            inOne.close();
            inTwo.close();
        } catch (Exception e) {
        }
    }

    public Vector getAllWords() {
        return allWords;
    }

    public Vector getNoSameWords() {
        return noSameWords;
    }
}

