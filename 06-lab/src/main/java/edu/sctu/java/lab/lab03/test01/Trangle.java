package edu.sctu.java.lab.lab03.test01;

class Trangle {
    double sideA, sideB, sideC, area, length;
    boolean boo;

    public Trangle(double a, double b, double c) {
        //参数a,b,c分别赋值给sideA,sideB,sideC
        sideA = a;
        sideB = b;
        sideC = c;

        //a,b,c构成三角形的条件表达式
        if ((sideA + sideB > sideC) ||
                (sideA + sideC > sideB) ||
                (sideC + sideB > sideA)) {
            boo = true; //给boo赋值。
        } else {
            boo = false;//给boo赋值。
        }
    }

    double getLength() {
        //方法体，要求计算出length的值并返回

        return sideA + sideB + sideC;
    }

    public double getArea() {
        if (boo) {
            double p = (sideA + sideB + sideC) / 2.0;
            area = Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
            return area;
        } else {
            System.out.println("不是一个三角形,不能计算面积");
            return 0;
        }
    }

    public void setABC(double a, double b, double c) {
        //参数a,b,c分别赋值给sideA,sideB,sideC
        sideA = a;
        sideB = b;
        sideC = c;

        if ((sideA + sideB > sideC) ||
                (sideA + sideC > sideB) ||
                (sideC + sideB > sideA)) {
            boo = true; //给boo赋值。
        } else {
            boo = false;//给boo赋值。
        }
    }
}

