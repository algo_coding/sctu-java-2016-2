package edu.sctu.java.lab.lab01.test02;

/**
 * Created by apple on 27/12/2016.
 */
public class Hello {
    public static void main(String[] args) {

        //命令行窗口输出"你好，只需编译我"
        System.out.println("你好，只需编译我");

        A a = new A();
        a.fA();

        B b = new B();
        b.fB();

        C c = new C();
        c.fC();
    }
}
