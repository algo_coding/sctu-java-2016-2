package edu.sctu.java.lab.lab03.test01;

class Lader {
    double above, bottom, height, area;

    Lader(double a, double b, double h) {
        //方法体，将参数a,b,c分别赋值给above,bottom,height
        this.above = a;
        this.bottom = b;
        this.height = h;
    }

    double getArea() {
        //方法体，,要求计算出area返回
        return (above + bottom) * height / 2;
    }
}

