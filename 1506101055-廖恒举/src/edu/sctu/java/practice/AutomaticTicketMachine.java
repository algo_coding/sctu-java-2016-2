package practice;

import java.util.Scanner;

/**
 * Created by hengju on 2016/12/7.
 * 自动售票机
 */
public class AutomaticTicketMachine {
    public static void main(String args[]){
        //定义变量
        int amounrt=0;
        int balance=0;
        //输入投币金额
        Scanner scan=new Scanner(System.in);
        //重复买票
        while (true) {
            System.out.print("请投币：");
            amounrt = scan.nextInt();
            balance = balance + amounrt;//利用中间变量balance解决投币金额不够问题
            if (balance >= 10) {
                //打印车票
                System.out.println("******************************");
                System.out.println("***      JAVA 城际专线     ***");
                System.out.println("***       票价：10元       ***");
                System.out.println("******************************");
                System.out.println("找零：" + (balance - 10));
                balance=0;//balance使用后初始化
                System.out.println("");
            }
        }
    }
}