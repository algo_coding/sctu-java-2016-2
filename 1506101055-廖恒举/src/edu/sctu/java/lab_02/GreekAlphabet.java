package lab_02;

/**
 * Created by hengju on 2016/10/27.
 */
public class GreekAlphabet {
    public static void main(String args[]){
    int startposition=0,endposition=0;
    char cStart='α',cEnd='ω';
    startposition=(int)cStart;
    endposition=(int)cEnd;
    System.out.println("希腊字母'α'在unicode表中的顺序位置:"+(int)cStart);
    System.out.println("希腊字母'ω'在unicode表中的顺序位置:"+(int)cEnd);
    System.out.println("打印希腊字母表：");
    for (int i=startposition;i<endposition;i++){
        cStart='\0';
        cStart=(char)i;
        System.out.print(""+cStart);
        if( (i-startposition+1)%10==0){
            System.out.println("");
        }
    }
}
}
