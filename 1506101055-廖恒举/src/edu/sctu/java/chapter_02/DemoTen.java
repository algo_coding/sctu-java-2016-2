package chapter_02;

/**
 * Created by ACER on 2016/9/21.
 * 程序输出乘法表
 */
public class DemoTen {
    public static void main(String[] args) {
        for(int i=1; i<10; i++){
            for (int j=1; j<=i; j++){
                if (j<=i){
                    int f = j * i;
                    System.out.print(j + "*" + i + "=" + f + "   ");
                }
            }
            System.out.println("");
        }
    }
}




