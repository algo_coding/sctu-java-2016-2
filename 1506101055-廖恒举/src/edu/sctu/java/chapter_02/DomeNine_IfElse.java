package chapter_02;

import java.util.Scanner;

/**
 * Created by hengju on 2016/9/21.
 * 请分别用if-else语句和switch语句编写能够实现以下功能的程序。
 *某同学某门课的成绩可能的结果为1,2,3,4,和5.当成绩为1时请输出
 *不及格；成绩为2时请输出及格；成绩为3时请输出中等；成绩为4时请
 *输出良好；成绩为5时请输出优秀。
 */
public class DomeNine_IfElse {
    public static void main(String args[]){
        System.out.println("请输入成绩：");
        Scanner scan=new Scanner(System.in);
        int x=scan.nextInt();
        if (x==1)
            System.out.println("不及格");
        else if (x==2)
            System.out.println("及格");
        else if (x==3)
            System.out.println("中等");
        else if (x==4)
            System.out.println("良好 ");
        else if (x==5)
            System.out.println("优秀");
        else System.out.println("成绩不在范围内（请输入1-5的自然数）");
    }
}
