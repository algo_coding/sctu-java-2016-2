package chapter_02;

/**
 * Created by hengju on 2016/9/21.
 * 请指出以下程序片段输出的结果是什么？
 */
public class DomeEight {
    public static void main(String args[]){
        int i=1,j=10;
        do {
            if(i++>--j)break;
        }while (i<5);
        System.out.println("i="+i+"j="+j);
    }
}
