package chapter_02;

import java.util.Scanner;

/**
 * Created by ACER on 2016/9/21.
 * 分别利用for语句、while语句以及do while 语句编写一个求和程序（sum =1+2+3+。。。+n）。
 */
public class Demo12While {
    public static void main(String[] args) {
        System.out.println("请输入：");
        Scanner in = new Scanner(System.in);
        int n, i=1,sum=0;
        n = in.nextInt();
        while(i<=n){
            sum+=i;
            i++;
        }
        System.out.println(sum);
    }
}
