package chapter_02;

import java.util.Scanner;

/**
 * Created by ACER on 2016/9/21.
 * 分别利用for语句、while语句以及do while 语句编写一个求和程序（sum =1+2+3+。。。+n）。
 */
public class Demo_12For {
    public static void main(String[] args) {
        System.out.println("请输入：");
        Scanner in = new Scanner(System.in);
        int n;
        n = in.nextInt();
        int sum=0;
        for(int i=1;i<=n;i++){
        sum+=i;
    }
        System.out.println(sum);
}}
