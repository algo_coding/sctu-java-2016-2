package chapter_02;

/**
 * Created by hengju on 2016/9/21.
 * 请指出执行完下列程序后，x，y和z的输出值是多少？请上机验证
 */
public class TointExampleSeven {
    public static void main(String args[]){
        int x,y,z;/**第一句没有加分号*/
        x=1;
        y=2;
        z=(x+y>3?x++:++y);
        System.out.println("x="+x);
        System.out.println("y="+y);
        System.out.println("z="+z);
    }
}
