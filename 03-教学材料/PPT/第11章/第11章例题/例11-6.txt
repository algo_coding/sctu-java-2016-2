import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
class Client extends JFrame implements Runnable,ActionListener
{  
   JButton connection,computer;
   JTextField inputA,inputB,inputC;
   JTextArea showResult;
   Socket socket=null;
   DataInputStream in=null; 
   DataOutputStream out=null;
   Thread thread; 
   public  Client()
   {  
      socket=new Socket();               //待连接的套接字。
      connection=new JButton("连接服务器");
      computer=new JButton("求方程的根");
      computer.setEnabled(false);        //没有和服务器连接之前,该按钮不可用。
      inputA=new JTextField("0",12);
      inputB=new JTextField("0",12);
      inputC=new JTextField("0",12);
      Box  boxV1=Box.createVerticalBox();
      boxV1.add(new JLabel("输入2次项系数"));
      boxV1.add(new JLabel("输入1次项系数"));
      boxV1.add(new JLabel("输入常数项"));
      Box boxV2=Box.createVerticalBox();
      boxV2.add(inputA);
      boxV2.add(inputB);
      boxV2.add(inputC);
      Box baseBox=Box.createHorizontalBox();
      baseBox.add(boxV1);
      baseBox.add(boxV2);
      Container con=getContentPane();
      con.setLayout(new FlowLayout());
      showResult=new JTextArea(8,18);
      con.add(connection);
      con.add(baseBox);
      con.add(computer);
      con.add(new JScrollPane(showResult));
      computer.addActionListener(this);
      connection.addActionListener(this);
      thread = new Thread(this);
      setBounds(100,100,360,310);
      setVisible(true);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
   }
   public void run()
   { 
     while(true)
       {   
           try{  
                  double root1=in.readDouble();  //堵塞状态，除非读取到信息。
                  double root2=in.readDouble(); 
                  showResult.append("\n两个根:\n"+root1+"\n"+root2);
                  showResult.setCaretPosition((showResult.getText()).length());
              }
           catch(IOException e) 
               {  
                  showResult.setText("与服务器已断开");
                  computer.setEnabled(false);
                  break;
               } 
       }
  }
  public void actionPerformed(ActionEvent e)
  { 
    if(e.getSource()==connection)
    {
        try
         {   //请求和服务器建立套接字连接：
            if(socket.isConnected())
              {
              } 
            else
              {
                InetAddress  address=InetAddress.getByName("127.0.0.1");
                InetSocketAddress socketAddress=new InetSocketAddress(address,4331);
                socket.connect(socketAddress); 
                in =new DataInputStream(socket.getInputStream());
                out = new DataOutputStream(socket.getOutputStream());
                computer.setEnabled(true);
                thread.start();
               }
         } 
        catch (IOException ee){}
    }
   if(e.getSource()==computer)
    {
        try
         {
           double a=Double.parseDouble(inputA.getText()),
                  b=Double.parseDouble(inputB.getText()),
                  c=Double.parseDouble(inputC.getText());
           double disk=b*b-4*a*c;
           if(disk>=0) 
            {
              out.writeDouble(a);
              out.writeDouble(b);
              out.writeDouble(c);
            }
           else
            { 
               inputA.setText("此2次方程无实根");
            }
         }
      catch(Exception ee)
         {
           inputA.setText("请输入数字字符");
         }
     }
  }
  public static void main(String args[])
  { 
      Client win=new  Client();
  }
}
(2) 服务器端程序
import java.io.*;
import java.net.*;
import java.util.*;
public class Server
{ 
   public static void main(String args[])
   {  
      ServerSocket server=null;
      ServerThread thread;
      Socket you=null;
      while(true) 
       { 
           try{  
                server=new ServerSocket(4331);
              }
          catch(IOException e1) 
             {  
                System.out.println("正在监听");   //ServerSocket对象不能重复创建。
             } 
          try{  
                you=server.accept();
                System.out.println("客户的地址:"+you.getInetAddress());
             }
         catch (IOException e)
             {  
                System.out.println("正在等待客户");
             }
         if(you!=null) 
             {  
                new ServerThread(you).start(); //为每个客户启动一个专门的线程。  
             }
         else 
             {  
                continue;
             }
      }
   }
}
class ServerThread extends Thread
{  
   Socket socket;
   DataOutputStream out=null;
   DataInputStream  in=null;
   String s=null;
   ServerThread(Socket t)
   {  
      socket=t;
      try  { 
             in=new DataInputStream(socket.getInputStream());
             out=new DataOutputStream(socket.getOutputStream());
          }
      catch (IOException e)
          {}
   }  
   public void run()        
   {  
      while(true)
      {  
         double a=0,b=0,c=0,root1=0,root2=0;
         try{   
                a=in.readDouble(); //堵塞状态，除非读取到信息。
                b=in.readDouble(); 
                c=in.readDouble(); 
                double disk=b*b-4*a*c;
                root1=(-b+Math.sqrt(disk))/(2*a);
                root2=(-b-Math.sqrt(disk))/(2*a);
                out.writeDouble(root1);
                out.writeDouble(root2);
             }
         catch (IOException e) 
            {  
               System.out.println("客户离开");
                break;
            }
      }
   } 
}
