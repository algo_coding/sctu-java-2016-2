package src.edu.sctu.java.lab_03.作业一;

/**
 * Created by lvjiafu on 2016/12/8.
 */
class Trangle {
    double sideA,sideB,sideC,area,length;
    boolean boo;
    public Trangle(double a,double b,double c)
    {
        sideA=a;
        sideB=b;
        sideC=c;
        if (a+b>c&&a+c>b&&b+c>a){
            boo=true;
        }
        else{
            boo=false;
        }
    }

    double getLength() {
        if (boo)
        {
            length=sideA+sideB+sideC;
            return length;
        }else
        {
            System.out.println("不是一个三角形，不能计算周长。");
            return 0;
        }
    }

    public double getArea() {
        if (boo)
        {
            double p=(sideA+sideB+sideC)/2.0;
            area=Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
            return area;
        }
        else{
            System.out.println("不是一个三角形，不能计算面积");
            return 0;
        }
    }
    public void setABC(double a,double b,double c){
        sideA=a;
        sideB=b;
        sideC=c;
        if (a+b>c&&a+c>b&&b+c>a){
            boo=true;
        }else {
            boo=false;
        }
    }
    double getSideA(double a){
        sideA=a;
        return sideA;
    }
    double getSideB(double b){
        sideB=b;
        return sideB;
    }
    double getSideC(double c){
        sideC=c;
        return sideC;
    }

}

