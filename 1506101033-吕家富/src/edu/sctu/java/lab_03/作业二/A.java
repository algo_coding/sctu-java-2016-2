package src.edu.sctu.java.lab_03.作业二;

/**
 * Created by lvjiafu on 2016/12/8.
 */
public class A {
    float a;
    static float b;
    void setA(float a){
        this.a=a;
    }
    void setB(float b){
        this.b=b;
    }
    float getA()
    {
        return a;
    }
    float getB()
    {
        return b;
    }
    void inputA()
    {
        System.out.println(a);
    }
    static void inputB()
    {
        System.out.println(b);
    }
}

