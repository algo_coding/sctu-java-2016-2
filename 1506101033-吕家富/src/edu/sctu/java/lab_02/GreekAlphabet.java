package src.edu.sctu.java.lab_02;

/**
 * Created by lvjiafu on 2016/11/3.
 */
public class GreekAlphabet {
    public static void main(String[] args){
            char cStart='α',cEnd='ω';
     //将α和ω定义为char型，并赋值给cStart和cEnd;
        for (int i=(int)cStart;i<=(int)cEnd;i++){
            System.out.print(" "+(char)i);
            if((i-(int)cStart+1)%8==0)
                System .out.print("\n");
        }

    }
}
