package edu.sctu.java.lab_03;

import javax.swing.*;
import java.util.Scanner;

/**
 * Created by DE on 2016/11/3.
 */
public class ImplClassAndInterface {

    class Trangle {
        double sideA, sideB, sideC, area, length;
        boolean boo;

        public Trangle(double a, double b, double c) {
         sideA=a;sideB=b;sideC=c;   //参数a,b,c分别赋值给sideA,sideB,sideC
            if ((sideB+sideC>sideA)&&(sideC+sideA>sideB)&&(sideA+sideB>sideC)) //a,b,c构成三角形的条件表达式
            {
              boo=true; //给boo赋值。
            }
            else
            {
               boo=false; //给boo赋值。
            }
        }

        double getLength() {
           length=sideA+sideB+sideC; //方法体，要求计算出length的值并返回
            return length;
        }

        public double getArea() {
            if (boo) {
                double p = (sideA + sideB + sideC) / 2.0;
                area = Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
                return area;
            } else {
                System.out.println("不是一个三角形,不能计算面积");
                return 0;
            }
        }

        public void setABC(double a, double b, double c) {
            sideA=a;sideB=b;sideC=c; //参数a,b,c分别赋值给sideA,sideB,sideC
            if ((sideB+sideC>sideA)&&(sideC+sideA>sideB)&&(sideA+sideB>sideC)) //a,b,c构成三角形的条件表达式
            {
                boo=true;//给boo赋值。
            }
            else
            {
                boo=false;//给boo赋值。
            }
        }
    }

    class Lader {
        double above, bottom, height, area;

        Lader(double a, double b, double h) {
           above=a;bottom=b;height=h; //方法体，将参数a,b,c分别赋值给above,bottom,height
        }

        double getArea() {
          area=(above+bottom)*height/2; //方法体，,要求计算出area返回
            return area;
        }
    }

    class Circle {
        double radius,length, area;

        Circle(double r) {
           radius=r; //方法体
        }

        double getArea() {
           area=Math.PI*radius*radius; //方法体，要求计算出area返回
            return area;
        }

        double getLength() {
           length=2*Math.PI*radius; //getArea方法体的代码,要求计算出length返回
        return length;
        }

        void setRadius(double newRadius) {
            radius = newRadius;
        }

        double getRadius() {
            return radius;
        }
    }

    public class AreaAndLength {
        public  void main(String args[]) {
            double length, area;
            Circle circle=null ;
            Trangle trangle=null;
            Lader lader = null;
            Scanner num=new Scanner(System.in);

            System.out.println("请输入圆的半径:");
           circle.radius=num.nextInt();   //创建对象circle
            System.out.println("请输入梯形的上边长：A:"); //创建对象lader
               lader.above=num.nextInt();
            System.out.println("请输入梯形的下边长：B:");
               lader.bottom=num.nextInt();
            System.out.println("请输入梯形的高：H:");
               lader.height=num.nextInt();
            System.out.println("请输入三角形的三边长:"); //创建对象trangle。
            trangle.sideA=num.nextInt();
            trangle.sideB=num.nextInt();
            trangle.sideC=num.nextInt();


            length=circle.getLength(); // circle调用方法返回周长并赋值给length
            System.out.println("圆的周长:" + length);
           area=circle.getArea();// circle调用方法返回面积并赋值给area
            System.out.println("圆的面积:" + area);
           length=trangle.getLength(); // trangle调用方法返回周长并赋值给length
            System.out.println("三角形的周长:" + length);
           area = trangle.getArea(); // trangle调用方法返回面积并赋值给area
            System.out.println("三角形的面积:" + area);
           area=lader.getArea();// lader调用方法返回面积并赋值给area
            System.out.println("梯形的面积:" + area);
            trangle.setABC(12, 34, 1);// trangle调用方法设置三个边，要求将三个边修改为12,34,1。
            area=trangle.getArea(); // trangle调用方法返回面积并赋值给area
            System.out.println("三角形的面积:" + area);
            length=trangle.getLength(); // trangle调用方法返回周长并赋值给length
            System.out.println("三角形的周长:" + length);
        }

    }
}
