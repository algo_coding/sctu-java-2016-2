package edu.sctu.java;

import javax.swing.*;

/**
 * Created by zhaoyong on 2016/9/8.
 */
//public class demo
//{
//    public static void main (String args[ ])
//    {
//        System.out.println("你好，只需编译我");   //命令行窗口输出"你好，只需编译我"
//        A a=new A();
//        a.fA();
//        B b=new B();
//        b.fB();
//    }
//}
//
//
// class A
//{
//    void fA()
//    {
//        System.out.println("I am A");   //命令行窗口输出"I am A"
//    }
//}
// class B
//{
//    void fB()
//    {
//        System.out.println("I am B");   //命令行窗口输出"I am B"
//    }
//}
// class C
//{
//    void fC()
//    {
//        System.out.println("I am C");//命令行窗口输出"I am C"
//    }
//}


//public class demo
//{
//    public static void main (String args[ ])
//    {
//        int z=(byte)128;
//        int startPosition=0,endPosition=0;
//        char cStart='α',cEnd='ω';
//        char c=0;
//        startPosition=(int)cStart;  //cStart做int型转换据运算，并将结果赋值给startPosition
//        endPosition=(int)cEnd;  //cEnd做int型转换运算，并将结果赋值给endPosition
//        System.out.println("希腊字母\'α\'在unicode表中的顺序位置:"+(int) c);
//        System.out.println("希腊字母表：");
//        System.out.println(z);
//        for(int i=startPosition;i<=endPosition;i++)
//        {
//            c = '\0';
//            c=(char)i; //i做char型转换运算，并将结果赋值给c
//            System.out.print(" "+c);
//            if((i-startPosition+1)%10==0)
//                System.out.println("");
//        }
//    }
//}


//public class demo
//{
//    public static void main(String args[])
//    {
//        int number=0,d5,d4,d3,d2,d1;
//        String str= JOptionPane.showInputDialog("输入一个1至99999之间的数");
//        number=Integer.parseInt(str);
//        if(number>=1 || number <=99999) //判断number在1至99999之间的条件
//        {
//            d5=number/10000;   //计算number的最高位（万位）d5
//            d4=number%10000/100;  //计算number的千位d4
//            d3=number%10000%1000%100;  //计算number的百位d3
//            d2=number%100/10;
//            d1=number%10;
//            if(d5>=1&&d5<=9)  //判断number是5位数的条件
//            {
//                System.out.println(number+"是5位数");
//                if(d1==d5 &&d2==d4) //判断number是回文数的条件
//                {
//                    System.out.println(number+"是回文数");
//                }
//                else
//                {
//                    System.out.println(number+"不是回文数");
//                }
//            }
//            else if(d5==0&&(d4>=1&&d5<=9))  //判断number是4位数的条件
//            {
//                System.out.println(number+"是4位数");
//                if(d1==d4&&d2==d3) //判断number是回文数的条件码
//                {
//                    System.out.println(number+"是回文数");
//                }
//                else
//                {
//                    System.out.println(number+"不是回文数");
//                }
//            }
//            else if(d5==0&&d4==0&&d3!=0)  //判断number是3位数的条件
//            {
//                System.out.println(number+"是3位数");
//                if(d1==d3) //判断number是回文数的条件
//                {
//                    System.out.println(number+"是回文数");
//                }
//                else
//                {
//                    System.out.println(number+"不是回文数");
//                }
//            }
//            else if(d2!=0)
//        {
//            System.out.println(number+"是2位数");
//            if(d1==d2)
//            {
//                System.out.println(number+"是回文数");
//            }
//            else
//            {
//                System.out.println(number+"不是回文数");
//            }
//        }
//        else if(d1!=0)
//        {
//            System.out.println(number+"是1位数");
//            System.out.println(number+"是回文数");
//        }
//        }
//        else
//        {
//            System.out.printf("\n%d不在1至99999之间",number);
//        }
//    }
//}


//import javax.swing.JOptionPane;
//public class demo
//{
//    public static void main (String args[ ])
//    {
//        System.out.println("给你一个1至100之间的整数,请猜测这个数");
//        int realNumber=(int)(Math.random()*100)+1;
//        int yourGuess=0;
//        String str=JOptionPane.showInputDialog("输入您的猜测:");
//        yourGuess=Integer.parseInt(str);
//        while(yourGuess!= realNumber) //循环条件
//        {
//            if(yourGuess>realNumber) //条件代码
//            {
//                str=JOptionPane.showInputDialog("猜大了,再输入你的猜测:");
//                yourGuess=Integer.parseInt(str);
//            }
//            else if(yourGuess<realNumber) //条件代码
//            {
//                str=JOptionPane.showInputDialog("猜小了,再输入你的猜测:");
//                yourGuess=Integer.parseInt(str);
//            }
//        }
//        System.out.println("猜对了!");
//    }
//}

class demo
{
    double sideA,sideB,sideC,area,length;
    boolean boo;
    public demo(double a, double b, double c)
    {
        sideA=a;
        sideB=b;
        sideC=c; //参数a,b,c分别赋值给sideA,sideB,sideC
        if(a+b>c&&a+c>b&&b+c>a) //a,b,c构成三角形的条件表达式
        {
            boo=true; //给boo赋值。
        }
        else
        {
            boo=false; //给boo赋值。
        }
    }
    double getLength()
    {
        ///方法体，要求计算出length的值并返回
        if(boo)
        {
            length=sideA+sideB+sideC;
            return length;
        }
        else  {
            System.out.println("不是一个三角形,不能计算周长");
            return 0;
        }
    }
    public double  getArea()
    {
        if(boo)
        {
            double p=(sideA+sideB+sideC)/2.0;
            area=Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC)) ;
            return area;
        }
        else
        {
            System.out.println("不是一个三角形,不能计算面积");
            return 0;
        }
    }
    public void setABC(double a,double b,double c)
    {
        sideA=a;
        sideB=b;
        sideC=c; //参数a,b,c分别赋值给sideA,sideB,sideC
        if(a+b>c&&a+c>b&&b+c>a) //a,b,c构成三角形的条件表达式
        {
            boo=true; //给boo赋值。
        }
        else
        {
            boo=false; //给boo赋值。
        }
    }
}
class Lader
{
    double above,bottom,height,area;
    Lader(double a,double b,double h)
    {
        above=a;
        bottom=b;
        height=h; //方法体，将参数a,b,c分别赋值给above,bottom,height
    }
    double getArea()
    {
        area=(above+bottom)/2*height;
        return area;
        //方法体，,要求计算出area返回
    }
}

class Circle
{
    double radius,area;
    Circle(double r)
    {
        radius=r; //方法体
    }
    double getArea()
    {
        return Math.PI*radius*radius;//方法体，要求计算出area返回
    }
    double getLength()
    {
        return Math.PI*2*radius;//getArea方法体的代码,要求计算出length返回
    }
    void setRadius(double newRadius)
    {
        radius=newRadius;
    }
    double getRadius()
    {
        return radius;
    }
}
class AreaAndLength
{
    public static void main(String args[])
    {
        double length,area;
        Circle circle=null;
        Trangle trangle;
        Lader lader;
        circle= new Circle(10); //创建对象circle,参数为10
        trangle=new Trangle(3,4,5); //创建对象trangle。
        lader=new Lader(3,4,10);//创建对象lader
        length=circle.getLength(); // circle调用方法返回周长并赋值给length
        System.out.println("圆的周长:"+length);
        area=circle.getArea();// circle调用方法返回面积并赋值给area
        System.out.println("圆的面积:"+area);
        length=trangle.getLength(); // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:"+length);
        area=trangle.getArea(); // trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:"+area);
        area=lader.getArea(); // lader调用方法返回面积并赋值给area
        System.out.println("梯形的面积:"+area);
        trangle.setABC(12,34,1) ;
// trangle调用方法设置三个边，要求将三个边修改为12,34,1。
        area=trangle.getArea(); // trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:"+area);
        length=trangle.getLength();// trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:"+length);
    }
}
