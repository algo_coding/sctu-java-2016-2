package edu.sctu.java.lab_06;

import java.awt.*;

class ImageCanvas extends Canvas
{  Image image=null;
    public ImageCanvas()
    {  setSize(200,200);
    }
    public void paint(Graphics g)
    {  if(image!=null)
        g.drawImage(image,0,0,this);
    }
    public void setImage(Image image)
    {  this.image=image;
    }
}


