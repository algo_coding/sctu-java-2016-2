package edu.sctu.java.lab_04;

import java.math.BigInteger;

/**
 * Created by Administrator on 2016/11/17.
 * 阶乘
 */
public class BigIntegerExample1 {
    public static void main(String args[]) {
        BigInteger n1=new BigInteger("30");
        BigInteger n2= new BigInteger("1");
        BigInteger one=new BigInteger("1");
        for(BigInteger i=one;i.compareTo(n1)<=0;i=i.add(one)){

            n2=n2.multiply(i);
}
        System.out.println(n1.toString()+"的阶乘为："+n2.toString());

        }

            }
