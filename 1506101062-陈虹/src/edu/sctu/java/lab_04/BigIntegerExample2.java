package edu.sctu.java.lab_04;

import java.math.BigInteger;

/**
 * Created by Administrator on 2016/11/17.
 * 1-999999999的和
 */
public class BigIntegerExample2 {
    public static void main(String args[]){
        BigInteger n1=new BigInteger("99999999");
        BigInteger n2=new BigInteger("0");
        BigInteger one=new BigInteger("1");
        for(BigInteger i=one;i.compareTo(n1)<=0;i=i.add(one)){
            n2=n2.add(i);
        }
        System.out.println("前"+n1.toString()+"项的和为："+n2.toString());

    }
}
