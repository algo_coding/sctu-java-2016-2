package edu.sctu.java.chapter_02.chapter_02_09;

/**
 * Created by Administrator on 2016/9/12.
 * 9、	请分别用if-else语句和switch语句编写能够实现以下功能的程序。
 某同学某门课成绩可能的结果是1,2,3,4,5。当成绩为1时输出不及格，当成绩为2时输出及格，当成绩为3时输出中等，当成绩为4时输出良好，当成绩为5时输出优秀。

 */
import java.util.Scanner;
public class IfElse {
    public static void main(String[] args) {

        System.out.println("请输入这门课的成绩（1,2,3,4,5）：");
        Scanner scan = new Scanner(System.in);
        int i=scan.nextInt();
        String str ="不及格";

        if(i==1){
            str="不及格";
        }
        else if(i==2){
            str="及格";
        }
        else if(i==3){
            str="中等";
        }
        else if(i==4){
            str="良好";
        }
        else if(i==5){
            str="优秀";
        }

        System.out.println("成绩为" + str);
    }
}
