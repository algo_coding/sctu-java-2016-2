package edu.sctu.java.chapter_02.chapter_02_12;

import java.util.Scanner;

/**
 * Created by Administrator on 2016/9/12.
 */
public class SunFor {

        public static void main(String[] args) {

            System.out.println("请输入一个数（进行递增求和）：");
            Scanner scan = new Scanner(System.in);
            int i = scan.nextInt();
            int sum = 0;


            for (int j = 1; j <= i; j++) {
                sum += j;
            }


            System.out.println("从1到"+i+"的和：sum="+sum);
        }
    }

