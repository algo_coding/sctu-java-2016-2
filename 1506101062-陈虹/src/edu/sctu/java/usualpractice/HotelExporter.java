package edu.sctu.java.usualpractice;

import java.util.List;

/**
 * Created by Administrator on 2016/10/20.
 */
public interface HotelExporter {

    public void export(List<Hotel> hotelList, String fileName) throws ClassNotFoundException, IllegalAccessException, InstantiationException;

}

/**
 *
 *
 *
 * name, position
 * 龙庭酒店，四川旅游学院
 * 阳光酒店，龙都南路
 *
 */
