package edu.sctu.java.usualpractice.exporttest;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Administrator on 2016/10/27.
 */
public class TwoReflection {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        Class clz=Class.forName("edu.sctu.java.usualpractice.exporttest.Students");
//值
        Students chen=(Students) clz.newInstance();
        chen.setName("chenhong");
        chen.setAge(20);
        System.out.println(chen.getName() + "   " + chen.getAge());

//属性名
        Field[] fileds= clz.getDeclaredFields();
        for(Field field:fileds){
            System.out.println(field.getName());
        }

//方法
        Method[] methods=clz.getMethods();
        for(Method method:methods){
            System.out.println(method.getName());}

//反射使用类
            Method method1=clz.getDeclaredMethod("study");
            method1.invoke(chen);






    }


}
