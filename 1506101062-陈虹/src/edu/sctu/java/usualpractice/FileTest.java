package edu.sctu.java.usualpractice;

import java.io.*;

/**
 * Created by Administrator on 2016/11/21.
 * 文件出入与读取
 */
public class FileTest {
    public static void main(String[] args) throws IOException {
        File file=new File("F:/zuoye","work.txt");

        FileOutputStream out=new FileOutputStream(file);
        byte[] buy="明日科技 java部".getBytes();
        out.write(buy);
        out.close();


        FileInputStream in=new FileInputStream(file);
        byte byt[]=new byte[1024];
        int len=in.read(byt);
        System.out.println("文件信息为："+new String(byt,0,len));
        in.close();




    }
}
