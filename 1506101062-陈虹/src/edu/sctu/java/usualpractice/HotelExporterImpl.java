package edu.sctu.java.usualpractice;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2016/10/20.
 */
public class HotelExporterImpl implements HotelExporter {
    @Override
    public void export(List<Hotel> hotelList, String fileName) {


        File file = new File(fileName);

        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file));

           // writer.write("hello,world!");
           for (Hotel hotel:hotelList){
               writer.write(hotel.getName()+","+hotel.getPosition());
               writer.newLine();
           }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }




    }
}
