package edu.sctu.java.chapter_02.chapter02_11;


/**
 * Created by Eoly on 2016/9/21.
 */
public class DiamondLetter {
    public static void main(String[] args) {
        char letter = 'A';
        for(int i=1,k=3;i<=8;i++){
            for(int j=1;j<=4;j++){
                System.out.print(j<=k?"  ":" "+(letter++)+"  ");
            }
            k=i<4?k-1:(i==4)?k:k+1;
            System.out.println();
        }
    }
}
