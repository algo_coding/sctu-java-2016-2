package edu.sctu.java.chapter_02.chapter02_12;

import java.util.Scanner;

/**
 * Created by Eoly on 2016/9/21.
 */
public class Sum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入累加个数：");
        int n = scanner.nextInt();
        System.out.println("for:"+getAccumulatedValue_for(n));
        System.out.println("while:"+getAccumulatedValue_while(n));
        System.out.println("do while:"+getAccumulatedValue_do_while(n));
    }
    static int getAccumulatedValue_for(int n){
        int sum=0;
        for(int i=1;i<=n;i++)sum+=i;
        return sum;
    }
    static int getAccumulatedValue_while(int n){
        int sum=0,i=1;
        while (i<=n)sum+=i++;
        return sum;
    }
    static int getAccumulatedValue_do_while(int n){
        int sum=0,i=1;
        do{
            sum+=i++;
        }
        while (i<=n);
        return sum;
    }
}
