package edu.sctu.java.chapter_02.chapter02_10;

import java.util.Scanner;

/**
 * Created by Eoly on 2016/9/21.
 */
public class MultipTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入乘法表阶数:");
        int n = scanner.nextInt();
        createMultipTable(n);
    }
    static void createMultipTable(int n){
        for(int y=1;y<=n;y++){
            for (int x=1;x<=y;x++){
                System.out.print(x+"*"+y+"="+x*y+"\t");
            }
            System.out.println();
        }
    }
}
