package edu.sctu.java.chapter_02.chapter02_09;

import java.util.Random;

/**
 * Created by Eoly on 2016/9/21.
 */
public class GradeLevelCheck {
    public static void main(String[] args) {
        Random random = new Random();
        int likelyGrade = random.nextInt(5)+1;
        System.out.println("if-else:小明的成绩为："+likelyGrade+" 等级为:"+getLevel_if(likelyGrade));
        System.out.println("switch:小明的成绩为："+likelyGrade+" 等级为:"+getLevel_switch(likelyGrade));

    }
    //if-else
    static String getLevel_if(int grade){
        String level;
        if(grade==1)level="不及格";
        else if (grade==2)level="及格";
        else if (grade==3)level="中等";
        else if (grade==4)level="良好";
        else level="优秀";
        return level;
    }
    static String getLevel_switch(int grade){
        String level;
        switch (grade){
            case 1:level="不及格";break;
            case 2:level="及格";break;
            case 3:level="中等";break;
            case 4:level="良好";break;
            case 5:level="优秀";break;
            default:level="成绩预料之外";
        }
        return level;
    }
}
