package edu.sctu.java.chapter_02;

/**
 * Created by 李大帅 on 2016/9/8.
 */
import java.util.Scanner;
public class gradeLevelIfElse {
    public static void main(String[] args) {
        System.out.println("输入的数只能为1到5");
        Scanner num = new Scanner(System.in);
        int score = num.nextInt();
        if(score==1)
        {
            System.out.println("你的成绩为不及格");
        }
        else if(score==2)
        {
            System.out.println("你的成绩及格");
        }else if(score==3){
            System.out.println("你的成绩中等");
        }else if(score==4){
            System.out.println("你的成绩良好");
        }else if(score==5){
            System.out.println("你的成绩优秀");
        }



    }
}
