package edu.sctu.java.chapter_02;

/**
 * Created by 李大帅 on 2016/9/8.
 */
import java.util.Scanner;
public class SumDoWhile {
    public static void main(String[] args) {
        System.out.println("请输入数值：");
        Scanner num = new Scanner(System.in);
        int n = num.nextInt();
        int sum = 0;
        int i=1;
        do{

            sum+=i;
            i++;
        }while (i<=n);
        System.out.println("和为："+sum);
    }

}