package edu.sctu.java.chapter_02;

/**
 * Created by 李大帅 on 2016/9/8.
 */
import java.util.Scanner;
public class gradeLevelSwitch {
    public static void main(String[] args) {
        System.out.println("输入的数只能为1到5");
        Scanner num = new Scanner(System.in);
        int score = num.nextInt();
        switch (score){
            case 1:
                System.out.println("你的成绩不合格");
                break;
            case 2:
                System.out.println("你的成绩及格");
                break;
            case 3:
                System.out.println("你的成绩中等");
                break;
            case 4:
                System.out.println("你的成绩良好");
                break;
            case 5:
                System.out.println("你的成绩优秀");
                break;
            default:
                System.out.println("你输入的数错误");
                break;
        }
    }
}
