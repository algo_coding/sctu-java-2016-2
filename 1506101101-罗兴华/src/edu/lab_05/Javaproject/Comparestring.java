package edu.lab_05.Javaproject;

/**
 * Created by 落叶的思恋 on 2016/11/17.
 * 编写一个Java应用程序，判断两个字符串是否相同，判断字符串的前缀、后缀是否和某个字符串相同
 * ，按字典顺序比较两个字符串的大小关系，检索字符串，创建字符串，将数字型字符串转换为数字，将字符串存放到数组中，
 * 用字符数组创建字符串。。
 */
public class Comparestring {
    public static void main(String[] args) {
        {
            String s1 = new String("you are a student"),
                    s2 = new String("how are you");
            if (s1.equals(s2)) // 使用equals方法判断s1与s2是否相同
            {
                System.out.println("s1与s2相同");
            } else {
                System.out.println("s1与s2不相同");
            }
            String s3 = new String("22030219851022024");
            if (s3.startsWith("220302"))   //判断s3的前缀是否是“220302”。
            {
                System.out.println("吉林省的身份证");
            }
            String s4 = new String("你"),
                    s5 = new String("我");
            if (s4.compareTo(s5) > 0)//按着字典序s4大于s5的表达式。
            {
                System.out.println("按字典序s4大于s5");
            } else {
                System.out.println("按字典序s4小于s5");
            }
            int position = 0;
            String path = "c:\\java\\jsp\\A.javadf";
            position = path.lastIndexOf("\\");
            System.out.println("c:\\java\\jsp\\A.java中最后出现\\的位置:" + position);
            String fileName = path.substring(path.indexOf('A'));
            System.out.println("c:\\java\\jsp\\A.java中含有的文件名:" + fileName);
            String s6 = new String("1234");

            String s7 = new String("123.678");
            char o = 'o';
            int num = (int) o;//字母转化为字符
            int n1 = Integer.parseInt(s6);
            double n2 = Double.parseDouble(s7); //将s7转化成double型数据。
            double m = n1 + n2;
            System.out.println(m);
            String s8 = String.valueOf(m);
            position = s8.indexOf(".");
            String temp = s8.substring(position + 1);
            System.out.println("数字" + m + "有" + temp.length() + "位小数");
            String s9 = new String("ABCDEF");
            char a[] = s9.toCharArray();//将s8存放到数组a中。
            String s = new String(a, 0, 3);
            System.out.println("我是数组a的前三个字符" + s);
            String jsp = new String(path.substring(8, 11));
            System.out.println(jsp);
            String str1 = new String("ABCABC"), str2 = null, str3 = null, str4 = null;
            str2 = str1.replaceAll("A","First"); str3 = str2.replaceAll("B","Second");
            str4 = str3.replaceAll("C","Third"); System.out.println(str1);
            System.out.println(str2);
            System.out.println(str3);
            System.out.println(str4);
            String sm=""+m;
            String ds[]=new String[2];
             ds=sm.split("\\.");
            System.out.println("整数部分是："+ds[0]);
            System.out.println("小数部分是："+ds[1]);

        }
    }

}

