package edu.lab_05.Javaproject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by 落叶的思恋 on 2016/11/23.
 */
public class Timecompare {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //输入第一个日期的时间
        System.out.println("输入第一个日期的年份");
        int yearOne = scanner.nextInt();
        System.out.println("输入第一个日期的月份（0-112）");
        int monthOne = scanner.nextInt();
        System.out.println("输入第一个日期的号数（0-31二月（0-28））");
        int dayOne = scanner.nextInt();
        System.out.println("输入第一个日期的小时数");
        int hoerOne = scanner.nextInt();
        System.out.println("输入第一个日期的分钟数");
        int minuteOne = scanner.nextInt();
        System.out.println("输入第一个日期的秒数");
        int SecondOne = scanner.nextInt();
        Calendar calendar = Calendar.getInstance();
        calendar.set(yearOne, monthOne, dayOne,hoerOne,minuteOne,SecondOne);//创建第一个日期对象
        long timeOne = calendar.getTimeInMillis();//将第一次输入时间转换成毫秒
        //输入第二个日期
        System.out.println("输入第二个日期的年份");
        int yearTwo = scanner.nextInt();
        System.out.println("输入第二个日期的月份（0-12）");
        int monthTwo = scanner.nextInt();
        System.out.println("输入第二个日期的号数（0-31二月（0-28））");
        int dayTwo = scanner.nextInt();
        System.out.println("输入第二个日期的小时数");
        int hourTwo = scanner.nextInt();
        System.out.println("输入第二个日期的分钟数");
        int minuteTwo = scanner.nextInt();
        System.out.println("输入第二个日期的秒数");
        int SecondTwo = scanner.nextInt();
        calendar.set(yearTwo,monthTwo,dayTwo,hourTwo,minuteTwo,SecondTwo);//创建一个日期对象
        long timeTwo = calendar.getTimeInMillis();//将第二次输入时间转换成毫秒
        Date date1 = new Date(timeOne);//创建两个日期
        Date date2 = new Date(timeTwo);//创建两个日期
        if (date2.equals(date1)) {
            System.out.println("两个日期的年、月、日完全相同");
        } else if (date2.after(date1)) {
            System.out.println("你输入的第二个日期大于第一个日期");
        } else if (date2.before(date1)) {
            System.out.println("你输入的第二个日期小于第一个日期");
        }
//        long days = (timeTwo - timeOne) / (long) (24 * 60 * 60 * 1000);     //计算两日期相隔天数
//        System.out.println("间隔天数"+days);
//        System.out.println("输入你的存钱金额");
//        int money=scanner.nextInt();
//        System.out.println("输入你的存储年份");
//        int year=scanner.nextInt();
//        System.out.println("输入你的存储月份");
//        int month=scanner.nextInt();
//        System.out.println("输入你的存储号数");
//        int day=scanner.nextInt();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
//        System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(year,month,day);
//
//        long time = calendar.getTimeInMillis();
//        long nowtime=new Date().getTime();
//        long days = (nowtime-time) / (long) (24 * 60 * 60 * 1000);
//        System.out.println("你的存款时间为"+days);
//
//        System.out.println("利息是"+ (money*Math.pow((1+0.1),year)-money));

    }
}
