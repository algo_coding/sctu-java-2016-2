package edu.sctu.java.socket.http;

import java.io.*;
import java.net.Socket;

/**
 * Created by Administrator on 2016-12-15.
 */
public class HttpClient {
    public static void main(String[] args) throws IOException {



        Socket socket = new Socket("www.sctu.edu.cn", 80);

        OutputStream os = socket.getOutputStream();//字节输出流
        PrintWriter pw = new PrintWriter(os);//将输出流包装成打印流
        pw.write("GET / HTTP/1.1 \r\n");
        pw.write("Host: www.sctu.edu.cn \r\n");
        pw.write("Connection: Keep-Alive\r\n");
        pw.write("\r\n");


        pw.flush();
        socket.shutdownOutput();


        InputStream is = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String info = null;
        while ((info = br.readLine()) != null) {
            System.out.println(info);
        }

        br.close();
        is.close();
        pw.close();
        os.close();
        socket.close();
    }
}
