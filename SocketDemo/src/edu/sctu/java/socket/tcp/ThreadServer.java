package edu.sctu.java.socket.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Administrator on 2016-12-8.
 */
public class ThreadServer {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(10086);
        Socket socket = null;
        int count = 0;//记录客户端的数量
        while (true) {
            socket = serverSocket.accept();
            ServerThread serverThread = new ServerThread(socket);
            serverThread.run();
            count++;
            System.out.println("客户端连接的数量：" + count);
        }
    }


    private static class ServerThread {
        Socket socket = null;


        public ServerThread(Socket socket) {
            this.socket = socket;
        }

        public void run() {
//服务器处理代码
        }
    }
}
