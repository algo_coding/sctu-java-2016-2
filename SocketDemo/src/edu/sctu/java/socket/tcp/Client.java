package edu.sctu.java.socket.tcp;

import java.io.*;
import java.net.Socket;

/**
 * Created by Administrator on 2016-12-8.
 */
public class Client {
    public static void main(String[] args) throws IOException {


        Socket socket = new Socket("localhost", 10086);

        OutputStream os = socket.getOutputStream();//字节输出流
        PrintWriter pw = new PrintWriter(os);//将输出流包装成打印流
        pw.write("用户名：admin；密码：123");
        pw.flush();
        socket.shutdownOutput();


        InputStream is = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String info = null;
        while ((info = br.readLine()) != null) {
            System.out.println("我是客户端，服务器说：" + info);
        }

        br.close();
        is.close();
        pw.close();
        os.close();
        socket.close();
    }
}
