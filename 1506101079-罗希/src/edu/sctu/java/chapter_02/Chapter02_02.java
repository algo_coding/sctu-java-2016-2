package edu.sctu.java.chapter_02;

/**
 * Created by 小可爱罗希 on 2016/9/12.
 */
import java.util.Scanner;
public class Chapter02_02 {
    public static void main(String[] args) {
        System.out.println("请输入1~5之间的数：");
        Scanner scanner=new Scanner(System.in);
        int x=scanner.nextInt();
        Chapter02_02 chapter02_02=new Chapter02_02();
        String score=chapter02_02.jundge(x);
        System.out.println(score);

    }
    String jundge(int x){
        switch (x){
            case 1:return "不合格";
            case 2:return "合格";
            case 3:return "良好";
            case 4:return "较好";
            case 5:return "优异";
            default:return "输入不正确！";
        }
    }
}
