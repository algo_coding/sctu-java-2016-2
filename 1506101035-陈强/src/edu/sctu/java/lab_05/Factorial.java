package edu.sctu.java.lab_05;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by XQ on 2016/11/21.
 */
public class Factorial {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.println("请输入你的大整数：");
        String temp=scan.next();
        BigInteger a=new BigInteger(temp);
        BigInteger b=BigInteger.ONE;
        BigInteger c=BigInteger.ONE;
        for(BigInteger i= BigInteger.ONE;i.compareTo(a)<=0;i=i.add(b)){
            c=c.multiply(i);
        }
        System.out.println(c);
        scan.close();
    }

}
