package edu.sctu.java.lab_05;

import java.math.BigInteger;

/**
 * Created by XQ on 2016/11/17.
 */
public class Sumary {
    public static void main(String[] args) {
        BigInteger max=new BigInteger("999999999"),
                temp=new BigInteger("0"),
                temp1=new BigInteger("1");
        for(BigInteger i=temp;i.compareTo(max)<0;i=i.add(temp1)){
            temp=temp.add(i);
        }
        System.out.println("1+2+3…的前999999999项的和:"+temp);
    }
}
