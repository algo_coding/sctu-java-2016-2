package edu.sctu.java.lab_04;

/**
 * Created by XQ on 2016/11/10.
 */
public class OverloadTest {
    public String sentence = "我爱java！";
    public OverloadTest(){
        System.out.println(sentence);
    }
    public OverloadTest(String a){
        sentence = a;
        System.out.println(sentence);
    }

    public static void main(String[] args) {
        OverloadTest overloadTest = new OverloadTest();
        overloadTest = new OverloadTest("我也爱java!");
    }
}
