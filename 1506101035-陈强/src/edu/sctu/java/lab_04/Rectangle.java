package edu.sctu.java.lab_04;

/**
 * Created by XQ on 2016/11/10.
 */
public class Rectangle {
    private double a;
    private double b;
    private double diagonal;

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void showDiagonal(){
        this.diagonal=Math.sqrt(this.a*this.a+this.b*this.b);
    }
}
