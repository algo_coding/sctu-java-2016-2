package edu.sctu.java.lab_04;

/**
 * Created by XQ on 2016/11/10.
 */
public class Square extends Rectangle implements EqualDiagonal {
    private double c;
    private double diagonal;

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }


    @Override
    public void getDiagonal() {
        this.diagonal=Math.sqrt(this.c*this.c);
    }
}
