package edu.sctu.java.database;

/**
 * Created by XQ on 2016/10/18.
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class RealizingCreateDatabase extends DatabaseTools {
    private String localaddress;
    private String localadatabaseName;
    Scanner scan = new Scanner(System.in);

    @Override
    public void createDatabase() {
        //开始必须有一个本地的数据库
        System.out.println("请先链接到本地数据库！");
        RealizingLoadDrive realizingLoadDrive = new RealizingLoadDrive();
        realizingLoadDrive.loadDrive();
        System.out.println("数据库连接成功！");
        Connection connection = realizingLoadDrive.connection;
        try {
            //创建新的数据库
            Statement stat = connection.createStatement();
            System.out.println("请输入要创建数据库的名称：");
            String createDatabaseName = scan.next();
            String name = "create database" + " " + createDatabaseName;
            stat.executeUpdate(name);
            System.out.println("数据库创建成功");
            //创建表
            System.out.println("请输入你准备创建的表名：");
            String tablename = scan.next();
            String createtable = "create table" + " " + tablename + "(id int, name varchar(80))";
            System.out.println("表已创建成功，且为你添加默认的属性组！");
            stat.executeUpdate(createtable);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new RealizingCreateDatabase().createDatabase();
    }
}
