package edu.sctu.java.database;

/**
 * Created by XQ on 2016/10/17.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class RealizingLoadDrive extends DatabaseTools {
    private String address;
    private String databaseName;
    private String userName;
    private String databasePassword;
    Connection connection;

    @Override
    public void loadDrive() {

        Scanner scan = new Scanner(System.in);

        System.out.println("请输入数据库的地址：");
        this.address = scan.next();

        System.out.println("请输入数据库的名字：");
        this.databaseName = scan.next();

        /*System.out.println("请输入数据库的密码：");
        System.out.println("数据库的密码是否为空？");
        String choose = scan.next();
        if (choose == "是") {
            this.databasePassword = "";
        } else {
            this.databasePassword = scan.next();

        }*/

        System.out.println("请输入用户名：");
        this.userName = scan.next();

        // 驱动
        String driver = "com.mysql.jdbc.Driver";
        // 数据库连接
        /*String url = "jdbc:mysql://"+this.address+":3306/"+databaseName+"?useUnicode=true&characterEncoding=UTF-8";*/
        String url = "jdbc:mysql://localhost:3306/" + this.databaseName + "?useUnicode=true&characterEncoding=UTF-8";
        // 用户名
        String user = this.userName;
        // 数据库密码
        String password = "";
        // 加载驱动
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //获取链接
        try {
            this.connection = DriverManager
                    .getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
