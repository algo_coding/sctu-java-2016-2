package edu.sctu.java.lab_03;

/**
 * Created by XQ on 2016/11/3.
 */
public class Trangle {
    double sideA,sideB,sideC,area,length;
    boolean boo;


    public  Trangle(double a,double b,double c)
    {
        sideA=a;
        sideB=b;
        sideC=c; //将a、b、c赋予给sideA、sideB、sideC

        if((a+b)>c && (a+c)>b && (b+c)>a ) //a,b,c构成三角形的条件表达式
        {
            boo=true; //给boo赋值。
        }
        else
        {
            boo=false; //给boo赋值。
        }
    }
    double getLength()
    {
        return sideA+sideB+sideC; //方法体，要求计算出length的值并返回
    }
    public double  getArea()
    {
        if(boo)
        {
            double p=(sideA+sideB+sideC)/2.0;
            area=Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC)) ;
            return area;
        }
        else
        {
            System.out.println("不是一个三角形,不能计算面积");
            return 0;
        }
    }
    public void setABC(double a,double b,double c)
    {
        sideA=a;
        sideB=b;
        sideC=c;       //参数a,b,c分别赋值给sideA,sideB,sideC

        if((a+b>c)&&(a+c>b)&&(c+b>a)) //a,b,c构成三角形的条件表达式
        {
            boo=true; //给boo赋值。
        }
        else
        {
            boo=false; //给boo赋值。
        }
    }
    public void returnSideA(){
        System.out.println("三角形的第一条边："+sideA);
    }
    public void returnSideB(){
        System.out.println("三角形的第一条边："+sideB);
    }
    public void returnSideC(){
        System.out.println("三角形的第一条边："+sideC);
    }
}
