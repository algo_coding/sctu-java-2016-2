package edu.sctu.java.lab_03;

/**
 * Created by XQ on 2016/11/3.
 */
public class Circle {

        double radius,area;

        Circle(double r)
        {
            radius=r; //方法体   将半径赋给radius
        }
        double getArea()
        {
            return radius*radius*3.14; //方法体，要求计算出area返回
        }
        double getLength()
        {
            return 2*radius*3.14; //getArea方法体的代码,要求计算出length返回
        }
        void setRadius(double newRadius)
        {
            radius=newRadius;
        }
        double getRadius()
        {
            return radius;
        }
}
