package src.edu.sctu.java.lab_04;

/**
 * Created by ACER on 2016/11/17.
 */
public abstract class AddSubIml implements AddSub {
    public static void main(String[] args) {
       AddSubIml addSubIml= new AddSubIml() {
           @Override
           public double add(double x, double y) {
               return x+y;
           }

           @Override
           public double sub(double x, double y) {
               return x-y;
           }
       };
        System.out.println(addSubIml.add(5,8));
        System.out.println(addSubIml.sub(9,8));
    }
}
