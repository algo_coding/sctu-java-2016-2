package src.edu.sctu.java.lab_04;
/**
 * Created by ACER on 2016/11/17.
 */
public abstract class Rectangle implements EqualDiagonal{
    @Override
    public double getDiagonal(double length, double height) {
        return Math.sqrt((height*height)+(length*length));
    }
}

