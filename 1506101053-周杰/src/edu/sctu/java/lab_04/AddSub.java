package src.edu.sctu.java.lab_04;

/**
 * Created by ACER on 2016/11/17.
 */
public interface AddSub {
    public double add(double x,double y);
    public double sub(double x,double y);
}
