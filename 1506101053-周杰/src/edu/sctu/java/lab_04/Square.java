package src.edu.sctu.java.lab_04;

/**
 * Created by ACER on 2016/11/17.
 */
public abstract class Square  extends Rectangle {

    public double getDiagonal(double length) {
        return 1.414*length;
    }

    public static void main(String[] args) {

        Square square=new Square() {
            @Override
            public double getDiagonal(double length) {
                return super.getDiagonal(1.414*length);
            }
        };
        System.out.println("正方形面积为：" +square.getDiagonal(9));
    }
}
