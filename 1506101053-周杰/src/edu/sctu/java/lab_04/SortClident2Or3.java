package src.edu.sctu.java.lab_04;

/**
 * Created by ACER on 2016/11/17.
 */
public class SortClident2Or3 {
    public static void main(String[] args) {
        Sort2Or3 sort2Or3=new Sort2Or3();
        sort2Or3.sort(5,8);
        System.out.println("两个数从小到大排列为：" + sort2Or3.max1 + "," + sort2Or3.max2);
        sort2Or3.sort(2, 5, 8);
        System.out.println("三个数从小到大排列为：" + sort2Or3.max1 + "," + sort2Or3.max2+","+sort2Or3.max3);
    }

}
