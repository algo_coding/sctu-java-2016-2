package src.edu.sctu.java.lab_03;

/**
 * Created by ACER on 2016/11/3.
 */
public class Client {
    public static void main(String args[])
    {
        double length,area;

        Circle circle=new Circle(8); //创建对象circle

        Trangle trangle=new Trangle(5,8,7); //创建对象trangle。
        Lader lader=new Lader(4,8,9); //创建对象lader
       length= circle.getLength(); // circle调用方法返回周长并赋值给length
        System.out.println("圆的周长:"+length);
        area=circle.getArea(); // circle调用方法返回面积并赋值给area
        System.out.println("圆的面积:"+area);
        area=trangle.getLength(); // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:"+length);
        area=trangle.getArea(); // trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:"+area);
        area=lader.getArea(); // lader调用方法返回面积并赋值给area
        System.out.println("梯形的面积:"+area);
        trangle.setABC(12, 34, 1); // trangle调用方法设置三个边，要求将三个边修改为12,34,1。
        area=trangle.getArea(); // trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:"+area);
        length=trangle.getLength(); // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:"+length);
        circle.setRadius(10);
        System.out.println("半径改为"+circle.radius);
        System.out.println("修改后的周长和面积分别为"+circle.getLength()+"         "+circle.getArea());
    }

}
