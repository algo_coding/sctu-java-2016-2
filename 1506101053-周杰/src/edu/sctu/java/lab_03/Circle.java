package src.edu.sctu.java.lab_03;

/**
 * Created by ACER on 2016/11/3.
 */
public class Circle {
    double radius,area;
    Circle(double r)
    {
        radius=r; //方法体
    }
    double getArea()
    {
        area=Math.PI*radius*radius; //方法体，要求计算出area返回
        return area;
    }
    double getLength()
    {
       double length=Math.PI*2*radius;//getArea方法体的代码,要求计算出length返回
        return length;
    }
    void setRadius(double newRadius)
    {
        radius=newRadius;
    }
    double getRadius()
    {
        return radius;
    }

}
