package src.edu.sctu.java.lab_03;

/**
 * Created by ACER on 2016/11/5.
 */
public class A {

        float a;   //声明一个float型实例变量a
         static  float b;         //声明一个float型类变量b,即static变量b
        void setA(float a)
        {
            this.a=a;      //将参数a的值赋值给成员变量a
        }
        void setB(float b)
        {
           this.b =b;      //将参数b的值赋值给成员变量b
        }
        float getA()
        {
            return a;
        }
        float getB()
        {
            return b;
        }
        void inputA()
        {
            System.out.println(a);
        }
        static void inputB()
        {
            System.out.println(b);
        }
    }


