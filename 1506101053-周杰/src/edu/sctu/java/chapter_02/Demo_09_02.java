package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * Created by ACER on 2016/9/12.
 */
public class Demo_09_02 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        n = in.nextInt();
        switch (n) {
            case 1:
                System.out.println("不及格");break;
            case 2:
                System.out.println("及格");break;
            case 3:
                System.out.println("中等");break;
            case 4:
                System.out.println("良好");break;
            case 5:
                System.out.println("优秀");break;
            default:
                System.out.println("error   请输入1-5的整数");
        }
    }
}