package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * Created by ACER on 2016/9/13.
 */
public class Demo_12_03 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, i=1,sum=0;
        n = in.nextInt();
        do{
            sum+=i;
            i++;
        }while (i<=n);
        System.out.println(sum);
    }
}
