package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * Created by ACER on 2016/9/13.
 */
public class Demo_12_02 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, i=1,sum=0;
        n = in.nextInt();
        while(i<=n){
            sum+=i;
            i++;
        }
        System.out.println(sum);
    }
}
