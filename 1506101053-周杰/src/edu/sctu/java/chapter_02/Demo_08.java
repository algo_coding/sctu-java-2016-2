package edu.sctu.java.chapter_02;

/**
 * Created by ACER on 2016/9/12.
 */
public class Demo_08 {
    public static void main(String[] args) {
        int i = 1, j = 10;
        do {
            if (i++ > --j) break;
        }while (i<5);
        System.out.println("i="+i+"<…>"+"j="+j);
    }
}