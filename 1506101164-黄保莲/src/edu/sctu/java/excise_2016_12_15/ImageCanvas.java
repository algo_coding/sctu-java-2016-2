package edu.sctu.java.excise_2016_12_15;

import java.awt.*;

/**
 * Created by hbl on 2016/12/28.
 */
class ImageCanvas extends Canvas
{  Image image=null;
    public ImageCanvas()
    {  setSize(200,200);
    }
    public void paint(Graphics g)
    {  if(image!=null)
        g.drawImage(image,0,0,this);
    }
    public void setImage(Image image)
    {  this.image=image;
    }
}
