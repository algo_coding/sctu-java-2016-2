package edu.sctu.java.excise_2016_12_15;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by hbl on 2016/12/28.
 */
class NetWin extends Frame implements ActionListener,Runnable{
    Button button;
    URL url;
    TextField text;
    TextArea area;
    byte b[] = new byte[118];
    Thread thread;
    NetWin(){
        text = new TextField(20);
        area = new TextArea(12,12);
        button = new Button("确定");
        button.addActionListener(this);
        thread = new Thread(this);
        Panel p = new Panel();
        p.add(new Label("输入网址:"));
        p.add(text);
        p.add(button);
        add(area, BorderLayout.CENTER);
        add(p, BorderLayout.NORTH);
        setBounds(60, 60, 360, 300);
        setVisible(true);
        validate();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (!(thread.isAlive()))
            thread = new Thread(this);
        try {
            thread.start();
        }catch (Exception ee){
            text.setText("我正在读取"+url);
        }


    }

    @Override
    public void run() {
        int n = 1;
        area.setText(null);
        String name = text.getText().trim();
        try {
            url = new URL(name);
            String hostName = url.getHost();
            int urlPortNumber = url.getPort();
            String fileName = url.getFile();
            InputStream in = url.openStream();
            area.append("\n主机:"+ hostName+"端口:"+urlPortNumber+"包含的文件名字:"+fileName);
            area.append("\n文件的内容如下:");
            while ((n = in.read(b))!=-1){
                String s = new String(b,0,n);
                area.append(s);
            }
        } catch (MalformedURLException e1) {
            text.setText(""+e1);
            return;
        } catch (IOException e1) {
            text.setText(""+e1);
            return;
        }


    }
}
