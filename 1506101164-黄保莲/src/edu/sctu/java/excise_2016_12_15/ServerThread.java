package edu.sctu.java.excise_2016_12_15;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by hbl on 2016/12/28.
 */
public   class ServerThread extends Thread
{  Socket socket;
    ObjectInputStream in=null;
    ObjectOutputStream out=null;
    String s=null;
    ServerThread(Socket t)
    {  socket=t;
        try  { out=new ObjectOutputStream(socket.getOutputStream()); //socket返回输出流。
            in=new ObjectInputStream(socket.getInputStream()); //socket返回输入流。
        }
        catch (IOException e)
        {}
    }
    public void run()
    {  TextArea text=new TextArea("你好,我是服务器",12,12);
        try{  out.writeObject(text);
        }
        catch (IOException e)
        {   System.out.println("客户离开");
        }
    }
}
