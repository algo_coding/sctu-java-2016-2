package edu.sctu.java.excise_2016_12_15;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by hbl on 2016/12/28.
 */
public class Server
{  public static void main(String args[])
{  ServerSocket server=null;
    ServerThread thread;
    Socket you=null;
    while(true)
    {  try{  server= new ServerSocket(4331);//创建在端口4331上负责监听的 ServerSocket对象
    }
    catch(IOException e1)
    {  System.out.println("正在监听");
    }
        try{  you= server.accept(); // server返回和客户端相连接的Socket对象
            System.out.println("客户的地址:"+you.getInetAddress());
        }
        catch (IOException e)
        {  System.out.println("正在等待客户");
        }
        if(you!=null)
        {  new ServerThread(you).start();
        }
        else{  continue;
        }
    }
}
}
