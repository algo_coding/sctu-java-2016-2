package edu.sctu.java.excise_2016_12_01;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by hbl on 2016/12/22.
 */
public class MoveButton extends Frame implements Runnable,ActionListener {
    Thread first,second;
    Button redButton,greenButton,starButton;
    int distance = 10;
    MoveButton(){
        first = new Thread(this);
        second = new Thread(this);
        redButton = new Button();
        greenButton = new Button();
        redButton.setBackground(Color.red);
        greenButton.setBackground(Color.green);
        starButton = new Button("start");
        starButton.addActionListener(this);
        setLayout(null);
        add(redButton);
        redButton.setBounds(10, 60, 15, 15);
        add(greenButton);
        redButton.setBounds(100, 60, 15, 15);
        add(starButton);
        starButton.setBounds(10, 100, 30, 30);
        setBounds(0, 0, 300, 200);
        setVisible(true);
        validate();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
     }

    @Override
    public void actionPerformed(ActionEvent e) {
            first.start();
            second.start();
    }

    @Override
    public void run() {
        while (true){

            if(Thread.currentThread()==first){
                moveComponent(redButton);
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (Thread.currentThread()==second){
                moveComponent(greenButton);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public synchronized void moveComponent(Component b){
        if(Thread.currentThread()==first){
            while (distance>100&&distance<=200)
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            distance = distance+1;
            b.setLocation(distance,60);
            if (distance>100){
                b.setLocation(10,60);
                notifyAll();
            }
        }
        if(Thread.currentThread()==second){
            while (distance>=10&&distance<100){
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                distance = distance+1;
                b.setLocation(distance,60);
                if(distance>200){
                    distance =10;
                    b.setLocation(100,60);
                    notifyAll();
                }
            }
        }
    }
}
