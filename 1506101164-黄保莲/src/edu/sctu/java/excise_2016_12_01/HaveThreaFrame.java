package edu.sctu.java.excise_2016_12_01;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by hbl on 2016/12/22.
 */
public class HaveThreaFrame extends Frame implements Runnable {
   Thread rotate;
    Planet earth;
    double pointX[] = new double[360],
           PointY[]  = new double[360];
    int width,height;
    int radius = 120;
    HaveThreaFrame(){
        rotate = new Thread(this);
        earth = new Planet();
        setBounds(0,0,360,400);
        width = getBounds().width;
        height = getBounds().height;
        pointX[0] = 0;
        PointY[0] = -radius;
        double angle = 1*Math.PI/180;
        for (int i = 0;i<359;i++){
            pointX[i+1] = pointX[i]*Math.cos(angle)-Math.sin(angle)*PointY[i];
            PointY[i+1] = PointY[i]*Math.cos(angle)+pointX[i]*Math.sin(angle);
        }
        for (int i = 0; i<360;i++){
            pointX[i] = pointX[i]+width/2;
            PointY[i] = PointY[i]+height/2;
        }
        setLayout(null);
        setVisible(true);
        validate();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        add(earth);
        earth.setLocation((int) pointX[0] - earth.getSize().width / 2,
                (int) PointY[0] - earth.getSize().height / 2);
        earth.start();
        rotate.start();
    }


    @Override
    public void run() {
        int i = 0;
        while (true){
            i = (i+1)%360;
            earth.setLocation((int)pointX[i]-earth.getSize().width/2,
                    (int)PointY[i]-earth.getSize().height/2);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void paint(Graphics g){
        g.setColor(Color.red);
        g.fillOval(width/2-15,height/2-15,30,30);
    }
}
