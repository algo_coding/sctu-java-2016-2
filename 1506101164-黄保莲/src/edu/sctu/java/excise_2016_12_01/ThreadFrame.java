package edu.sctu.java.excise_2016_12_01;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by hbl on 2016/12/21.
 */
public class ThreadFrame extends Frame implements ActionListener {
    Label wordLable;
    Button button;
    TextField inputText,scoreText;
    WordThread giveWord;
    int score;
    ThreadFrame(){
        wordLable = new Label(" ",Label.CENTER);
        wordLable.setFont(new Font(" ",Font.BOLD,72));
        button = new Button("开始");
        inputText = new TextField(3);
        scoreText = new TextField(5);
        scoreText.setEditable(false);
        giveWord = new WordThread(wordLable);
        button.addActionListener(this);
        inputText.addActionListener(this);
        add(button, BorderLayout.NORTH);
        add(wordLable,BorderLayout.CENTER);
        Panel southP = new Panel();
        southP.add(new Label("回车输出成绩："));
        southP.add(inputText);
        southP.add(scoreText);
        add(southP, BorderLayout.SOUTH);
        setVisible(true);
        validate();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });


    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==button) {
            if (!giveWord.isAlive()) {
                giveWord = new WordThread(wordLable);
            }
            giveWord.start();
        }
            else if(e.getSource()==inputText){
                if (inputText.getText().equals(wordLable.getText())){
                    score++;
                }
                scoreText.setText("得分:"+score);
                inputText.setText(null);
            }

        }
    }

