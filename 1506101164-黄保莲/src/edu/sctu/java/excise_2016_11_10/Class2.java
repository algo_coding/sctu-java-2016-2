package edu.sctu.java.excise_2016_11_10;

/**
 * Created by hbl on 2016/11/10.
 */
public class Class2 {
    public static void main (String args[]){
        Sort2or3 m1 = new Sort2or3();
        m1.sort(100,200);
        System.out.println("两个数从大到小为："+m1.max1+","+m1.max2);
        m1.sort(50,150,100);
        System.out.println("三个数从大到小为："+m1.max1+","+m1.max2+","+m1.max3);
    }

}
