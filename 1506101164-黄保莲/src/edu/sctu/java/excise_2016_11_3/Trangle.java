package edu.sctu.java.excise_2016_11_3;

/**
 * Created by hbl on 2016/11/3.
 */
public class Trangle {
    double sideA,sideB,sideC,area,length;
    boolean boo;

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

    public  Trangle(double a,double b,double c)
    {
        //参数a,b,c分别赋值给sideA,sideB,sideC
        this.sideA = a;
        this.sideB = b;
        this.sideC = c;
        double sum = a+b;
        double jian = a-b;
        if(sum > c && jian < c) //a,b,c构成三角形的条件表达式
        {
            boo = true;//给boo赋值。
        }
        else
        {
             boo = false;//给boo赋值。
        }
    }
     double getLength()
    {

         //方法体，要求计算出length的值并返回
        length = sideA+sideB+sideC;
        return length;
    }
    public double  getArea()
    {
        if(boo)
        {
            double p=(sideA+sideB+sideC)/2.0;
            area=Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC)) ;
            return area;
        }
        else
        {
            System.out.println("不是一个三角形,不能计算面积");
            return 0;
        }
    }
    public void setABC(double a,double b,double c)
    {
        //参数a,b,c分别赋值给sideA,sideB,sideC
        this.sideA = a;
        this.sideB = b;
        this.sideC = c;
        double sum = a+b;
        double jian = a-b;
        if(sum > c && jian < c) //a,b,c构成三角形的条件表达式
        {
            boo = true;//给boo赋值。
        }
        else
        {
            boo = false;//给boo赋值。
        }
    }

}
