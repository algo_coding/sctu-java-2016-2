package edu.sctu.java.excise_2016_11_3;

/**
 * Created by hbl on 2016/11/3.
 */
public class AreaAndLength {
    public static void main(String args[])
    {
        double length,area;
        Circle circle=null;
        Trangle trangle=null;
        Lader lader=null;
        circle = new Circle(1.5); //创建对象circle
       // trangle = new Trangle(1,2,1.5); //创建对象trangle。
//        lader = new Lader(2,3,4); //创建对象lader
//        length = circle.getLength(); // circle调用方法返回周长并赋值给length
//        System.out.println("圆的周长:"+length);
//        area = circle.getArea(); // circle调用方法返回面积并赋值给area
//        System.out.println("圆的面积:"+area);
//        length = trangle.getLength(); // trangle调用方法返回周长并赋值给length
//        System.out.println("三角形的周长:"+length);
//        area = trangle.getArea(); // trangle调用方法返回面积并赋值给area
//        System.out.println("三角形的面积:"+area);
//        area = lader.getArea(); // lader调用方法返回面积并赋值给area
//        System.out.println("梯形的面积:"+area);
//        trangle.setABC(12,34,1); // trangle调用方法设置三个边，要求将三个边修改为12,34,1。
//        area = trangle.getArea(); // trangle调用方法返回面积并赋值给area
//        System.out.println("三角形的面积:"+area);
//        length = trangle.getLength();// trangle调用方法返回周长并赋值给length
//        System.out.println("三角形的周长:"+length);
        circle.setRadius(2.3);
        length = circle.getLength(); // circle调用方法返回周长并赋值给length
        System.out.println("圆的周长:"+length);
        area = circle.getArea(); // circle调用方法返回面积并赋值给area
        System.out.println("圆的面积:"+area);
    }

}
