package edu.sctu.java.excise_2016_11_3;

/**
 * Created by hbl on 2016/11/3.
 */
public class Circle {
    double radius,area,length;
    Circle(double r)
    {
        //方法体
        this.radius = r;
    }
    double getArea()
    {
         //方法体，要求计算出area返回
        area = radius*radius*3.14;
        return area;
    }
    double getLength()
    {
        //getArea方法体的代码,要求计算出length返回
        length = 2*3.14*radius;
        return length;
    }
    void setRadius(double newRadius)
    {
        radius=newRadius;
    }
    double getRadius()
    {
        return radius;
    }


}
