package org.xuyh.demo;
import java.io.PrintStream;
/**
 * Print the char graphics like:
 * <pre>
 * A 
 * B C 
 * D E F 
 * G H I J 
 * K L M N 
 * O P Q 
 * R S 
 * T 
 *</pre>
 * @author  XuYanhang
 * @since  2016-09-15
 */
public class CharRhombus {
    private PrintStream out;
    private int n;
    private char cursor;
    public CharRhombus(int n) {
        this(System.out,n);
    }
    public CharRhombus(PrintStream out, int n) {
        this(out,n,'A');
    }
    public CharRhombus(PrintStream out, int n,char startChar) {
        super();
        this.out = out;
        this.n = n;
        cursor = startChar;
        new Thread(){
            public void run(){
                CharRhombus.this.run();
            }
        }.start();
    }
    private void run(){
        printTopHalf();
        printBottomHalf();
    }
    private void printTopHalf(){
        for (int i = 1; i <= n; i++) {
            printLine(i);
        }
    }
    private void printBottomHalf(){
        for (int i = n; i >= 1; i--) {
            printLine(i);
        }
    }
    private void printLine(int charNum){
        newSpace(n - charNum);
        for (int i = 0; i < charNum; i++) {
            printChar();
            newSpace(1);
        }
        newLine();
    }

    private void printChar(){
        out.print(cursor++);
    }

    private void newSpace(int num){
        for(int i = 0 ; i < num ; i++)
            out.print(" ");
    }
    private void newLine(){
        out.println();
    }
    /**
     * Test main method
     */
    public static void main(String[] args) {
        new CharRhombus(4);
    }
}
