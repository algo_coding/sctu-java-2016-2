package edu.sctu.java.lab_01;

/**
 * Created by Huang Suixin on 2016/9/8.
 */
public class Hello {
    public static void main(String[] args) {
        System.out.println("你好，只需编译我");
        A a = new A();
        a.fA();
        B b = new B();
        b.fB();

    }
}
class A{
    void fA(){
        System.out.println("I am A");
    }
}