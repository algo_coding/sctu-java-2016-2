package lab_07.test03;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MoveButtonFrame extends JFrame implements Runnable, ActionListener {
    public static final int BUTTON_SIZE = 30;

    private Thread first;
    private Thread second;

    private JButton redButton;
    private JButton greenButton;
    private JButton startButton;

    // 按钮横坐标
    private int axisX = 10;

    public MoveButtonFrame() {

        first = new Thread(this);
        second = new Thread(this);

        redButton = new JButton();
        redButton.setOpaque(true);
        redButton.setBackground(Color.RED);
        redButton.setBounds(10, 60, BUTTON_SIZE, BUTTON_SIZE);
        add(redButton);

        greenButton = new JButton();
        greenButton.setOpaque(true);
        greenButton.setBackground(Color.GREEN);
        greenButton.setBounds(100, 60, BUTTON_SIZE, BUTTON_SIZE);
        add(greenButton);

        startButton = new JButton("start");
        startButton.addActionListener(this);
        startButton.setBounds(10, 100, 100, 30);
        add(startButton);

        setLayout(null);
        setBounds(0, 0, 300, 200);
        setVisible(true);
        validate();

        addWindowListener(new WindowAdapter() {
                              public void windowClosing(WindowEvent e) {
                                  System.exit(0);
                              }
                          }
        );
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == startButton) {
            try {
                first.start();
                second.start();
            } catch (Exception exp) {
                exp.printStackTrace();
            }
        }
    }

    public void run() {
        while (true) {
            if (Thread.currentThread() == first) //判断当前占有CPU资源的线程是否是first
            {
                moveComponent(redButton);
                try {
                    Thread.sleep(20);
                } catch (Exception exp) {
                }
            }
            if (Thread.currentThread() == second) //判断当前占有CPU资源的线程是否是second
            {
                moveComponent(greenButton);
                try {
                    Thread.sleep(10);
                } catch (Exception exp) {
                }
            }
        }
    }

    public synchronized void moveComponent(Component b) {
        if (Thread.currentThread() == first) {
            while (axisX > 100 && axisX <= 200)
                try {
                    wait();
                } catch (Exception exp) {
                }

            axisX = axisX + 1;
            b.setLocation(axisX, 60);

            if (axisX >= 100) {
                b.setLocation(10, 60);
                notifyAll();
            }
        }

        if (Thread.currentThread() == second) {
            while (axisX >= 10 && axisX < 100)
                try {
                    wait();
                } catch (Exception exp) {
                }

            axisX = axisX + 1;
            b.setLocation(axisX, 60);

            if (axisX > 200) {
                axisX = 10;
                b.setLocation(100, 60);
                notifyAll();
            }
        }
    }

    public static void main(String[] args) {

        new MoveButtonFrame();
    }

}

