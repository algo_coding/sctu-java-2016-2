package lab_04;

/**
 * Created by Silence on 2016/11/10.
 */
public class Operation implements Addition,Subtraction {
    @Override
    public int add(int a, int b) {
        return a+b;
    }

    @Override
    public int sub(int a, int b) {
        return a-b;
    }
}
