package lab_04;

import edu.sctu.tripadvisor.taskseven.huangsuixin.demo;

/**
 * Created by Silence on 2016/11/10.
 */
public class Demo {

    public Demo() {
        System.out.println("该类已创建");
    }
    public Demo(String name){
        System.out.println(name+"  该类已创建");
    }

    public static void main(String[] args) {
        Demo demo = new Demo();
        Demo demo1 = new Demo("这是输入的字符串");
    }
}
