package lab_04;

/**
 * Created by Silence on 2016/11/10.
 */

/*矩形类*/

public class Rectangle {
   private float height;
    private float width;


    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getLength(){
        return width*2+height*2;
    }
    public float getArea(){
        return width*height;
    }
}
