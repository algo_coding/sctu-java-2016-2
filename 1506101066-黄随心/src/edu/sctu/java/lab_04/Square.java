package lab_04;

/**
 * Created by Silence on 2016/11/10.
 */

/*正方形类*/
public class Square extends Rectangle implements EqualDiagonal {
    private float side;

    public Square(float side) {
        this.side = side;
    }

    public float getSide() {
        return side;
    }

    public void setSide(float side) {
        this.side = side;
    }

    @Override
    public float getHeight() {
        return side;
    }

    @Override
    public void setHeight(float height) {
        side=height;
    }

    @Override
    public float getWidth() {
        return side;
    }

    @Override
    public void setWidth(float width) {
        side=width;
    }

    @Override
    public float getLength() {
        return side*4;
    }

    @Override
    public float getArea() {
        return side*side;
    }

    @Override
    public float getDiagonal() {
        return (float) (side*Math.sqrt(2));
    }
}
