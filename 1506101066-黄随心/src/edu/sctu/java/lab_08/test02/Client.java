package lab_08.test02;

import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;

class Client extends Frame implements Runnable, ActionListener {
    Socket socket = null;
    Button connection;
    ObjectInputStream in = null;
    ObjectOutputStream out = null;
    Thread thread;

    public Client() {
        socket = new Socket();

        connection = new Button("连接服务器,读取文本区对象");
        connection.addActionListener(this);
        add(connection, BorderLayout.NORTH);

        thread = new Thread(this);
        setBounds(100, 100, 500, 500);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
                              public void windowClosing(WindowEvent e) {
                                  System.exit(0);
                              }
                          }
        );
    }

    public void run() {
        while (true) {
            try {
                TextArea text = (TextArea) in.readObject();
                add(text, BorderLayout.CENTER);
                validate();
            } catch (Exception e) {
                break;
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == connection) {
            try {
                if (socket.isConnected()) {
                } else {

                    InetAddress address = InetAddress.getByName("127.0.0.1");

                    //创建端口为4331、地址为address的socketAddress
                    InetSocketAddress socketAddress = new InetSocketAddress(address, 4331);

                    socket = new Socket(socketAddress.getAddress(), socketAddress.getPort());  //socket建立和socketAddress的连接呼叫。


                    in = new ObjectInputStream(socket.getInputStream()); //socket返回输入流
                    out = new ObjectOutputStream(socket.getOutputStream()); //socket返回输出流
                    thread.start();
                }
            } catch (Exception ee) {
            }
        }
    }

    public static void main(String args[]) {
        Client win = new Client();
    }
}
