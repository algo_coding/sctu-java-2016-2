package lab_08.test03;

import java.net.*;
import java.io.*;

public class Server {
    byte b[] = new byte[8192];
    DatagramPacket packet;
    DatagramSocket socket;

    Server() throws SocketException {
        packet = new DatagramPacket(b, b.length);
        socket = new DatagramSocket(1234);
    }

    public void run() throws IOException {
        InetAddress address = null;

        //创建在端口1234负责收取数据包的DatagramSocket对象。

        while (true) {

            socket.receive(packet);

            if (packet != null) {
                new ServerThread(packet).start();
            }
        }

    }

    public static void main(String args[]) {

        try {
            new Server().run();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

