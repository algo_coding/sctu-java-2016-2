package lab_03;

/**
 * Created by Silence on 2016/10/29.
 */
public class AreaAndLength {
    public static void main(String args[])
    {
        double length,area;
        Circle circle=null;
        Trangle trangle;
        Lader lader;
//        【代码15】 //创建对象circle
        circle = new Circle(3);

//        【代码16】 //创建对象trangle。
        trangle = new Trangle(3,4,5);

//        【代码17】 //创建对象lader
        lader = new Lader(3,4,5);

//        【代码18】 // circle调用方法返回周长并赋值给length
        length = circle.getLength();
        System.out.println("圆的周长:"+length);

//        【代码19】 // circle调用方法返回面积并赋值给area
        area = circle.getArea();
        System.out.println("圆的面积:"+area);

//        【代码20】 // trangle调用方法返回周长并赋值给length
        length = trangle.getLength();
        System.out.println("三角形的周长:"+length);

//        【代码21】 // trangle调用方法返回面积并赋值给area
        area = trangle.getArea();
        System.out.println("三角形的面积:"+area);

//        【代码22】 // lader调用方法返回面积并赋值给area
        area = lader.getArea();
        System.out.println("梯形的面积:"+area);

//        【代码23】 // trangle调用方法设置三个边，要求将三个边修改为12,34,1。
        trangle.setSide(12,34,1);

//        【代码24】 // trangle调用方法返回面积并赋值给area
        area = trangle.getArea();
        System.out.println("三角形的面积:"+area);

//        【代码25】 // trangle调用方法返回周长并赋值给length
        length = trangle.getLength();
        System.out.println("三角形的周长:"+length);
    }

}

class Trangle{
    private double sideA,sideB,sideC,area,length;
    private boolean boo;

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public Trangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
        if ((sideA<sideB+sideC)||(sideB<sideA+sideC)||(sideC<sideA+sideB)){
            boo=true;
        }else {
            boo=false;
        }
    }

    public double getArea(){
        if (boo){
            double p = (sideA+sideB+sideC)/2;
            area = Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
            return area;
        }else {
            System.out.println("三边不能构成一个三角形，不能计算面积");
            return 0;
        }
    }
    public double getLength(){
        return sideA + sideB + sideC;
    }
    public void setSide(double a,double b,double c){
        this.sideA=a;
        this.sideB=b;
        this.sideC=c;
        if ((sideA<sideB+sideC)||(sideB<sideA+sideC)||(sideC<sideA+sideB)){
            boo=true;
        }else {
            boo=false;
        }
    }
}

class Lader{
    private double above,bottom,height,area;

    public Lader(double above, double bottom, double height) {
        this.above = above;
        this.bottom = bottom;
        this.height = height;
    }

    public double getArea() {
        area = (above+bottom)*height/2;
        return area;
    }
}

class Circle{
    private double radius,area;
    final double PI = 3.14;
    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        area = PI*radius*radius;
        return area;
    }
    public double getLength(){
        return 2*PI*radius;
    }
}