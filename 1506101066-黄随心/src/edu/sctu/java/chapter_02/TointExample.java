package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * Created by Huang Suixin on 2016/9/19.
 */
public class TointExample {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("请输入需要计算阶乘的数字：");
        int num = scan.nextInt();
        int sum=1;
        int i=1;
        do {
            sum*=i;
            i++;
        }while (i<=num);
        System.out.println(num+"的阶乘是："+sum);
    }
}
