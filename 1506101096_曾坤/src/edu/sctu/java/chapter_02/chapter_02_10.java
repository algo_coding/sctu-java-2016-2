package edu.sctu.java.chapter_02;

/**
 * 编写输入乘法口诀表的程序
 * Created by mike on 2016/9/13.
 */
public class chapter_02_10 {
    public static void main(String[] args) {
        for (int i = 1; i <= 4; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + "*" + i + "=" + (i * j) + "\t");

            }
            //换行显示
            System.out.println();
        }

    }

}


