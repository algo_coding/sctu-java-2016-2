package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * 用if else语句编写能够实现以下功能的程序。
 * 某同学某门课的成绩可能结果为1,2,3,4,5.当成绩为1是请输出不及格；成绩为2时请输出及格；成绩为3时请输出中等；
 * 成绩为4时请输出良好；成绩为5时请输出优秀。
 * Created by mike on 2016/9/13.
 * Created by mike on 2016/9/13.
 */
public class chapter_02_9_1 {

    public static void main(String[] args) {
        System.out.println("请输入你的成绩：");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n > 5 || n < 1) System.out.println("输入超出范围");

        else if (n == 1) System.out.println("不及格");

        else if (n == 2) System.out.println("及格");

        else if (n == 3) System.out.println("中等");

        else if (n == 4) System.out.println("良好");

        else System.out.println("优秀");

    }

}
