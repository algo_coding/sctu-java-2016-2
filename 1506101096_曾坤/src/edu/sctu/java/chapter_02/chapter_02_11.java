package edu.sctu.java.chapter_02;

/**
 * Created by mike on 2016/9/13.
 */
public class chapter_02_11 {

    public static void main(String[] args) {
        int i, j, k;
        char c1 = 'A';
        for (i = 1; i <= 4; i++) {
            for (j = 0; j < 4 - i; j++)
                System.out.print(" ");
            for (k = 0; k < i; k++)
                System.out.print(c1++ + "" + "  ");
            System.out.println("");
        }
        for (i = 4; i > 0; i--) {
            for (j = 0; j < 4 - i; j++)
                System.out.print(" ");
            for (k = 0; k < i; k++)
                System.out.print(c1++ + "" + "  ");
            System.out.println("");
        }
    }
}



