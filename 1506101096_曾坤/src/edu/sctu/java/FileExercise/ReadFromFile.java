package edu.sctu.java.FileExercise;

import java.io.*;


/**
 * Created by mike on 2016/10/20.
 */
public class ReadFromFile {
    public static void readFileByBytes(String filename) {
        File file = new File(filename);

        InputStream in = null;
        System.out.println("以字节为单位读取文件内容，一次读取一个字节");
        //一次读一个字节
        try {
            in = new FileInputStream(file);
            int tempbyte;
            try {
                while ((tempbyte = in.read()) != -1) {


                 System.out.write(tempbyte);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }


        try {
            System.out.println("以字节为单位读取文件内容，一次读多个字节：");
            // 一次读多个字节
            byte[] tempbytes = new byte[100];
            int byteread = 0;
            in = new FileInputStream(filename);
            ReadFromFile.showAvailableBytes(in);
            // 读入多个字节到字节数组中，byteread为一次读入的字节数
            while ((byteread = in.read(tempbytes)) != -1) {
                System.out.write(tempbytes, 0, byteread);

            /*单来说，write(byte[] b, int off, int len)就是将数组 b 中的 len 个字节按顺序写入输出流。所以如果 b 为 null，则抛出 NullPointerException。如果 off 为负，或 len 为负，又或者 off+len 大于数组 b 的长度，则抛出 IndexOutOfBoundsException。如果 len 为零，则不写入字节。否则，首先写入字节 b[off]，然后写入字节 b[off+1]，依此类推；最后一个写入字节是 b[off+len-1]。
                楼主的问题是off+len>b.length了，就是写入的输出字节数超出了要写入的字节的长度；就好像我要只有4个字节的文字，却要输出到第5个字节，那当然会报错啦~~
                System.out.write(tempbytes, 0, byteread);*/

            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e1) {
                }
            }
        }

    }

    /**
     * 以字符为单位读取文本，常用于读文本，数字等类型的文件
     */
    public static void readFileByChars(String filename) {
        File file = new File(filename);

        Reader reader = null;

        System.out.println("以字符为单位读取文件内容，一次读一个字节");
        //一次读一个字符
        try {
            reader = new InputStreamReader(new FileInputStream(file));
            int tempchar;
            try {
                while ((tempchar = reader.read()) != -1) {
                    //对于windoows下，\r\n两个字符在一起，表示一个换行
                    //但如果两个字符分开显示，会换两次行
                    //因此，屏蔽掉\r,或者屏蔽\n,否则会多出很多空行
                    if ((char) tempchar != '\r') {             //if里的条件一直为真，只不过强制转换了一下\r
                        System.out.print((char) tempchar);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("以字符为单位读取文件内容，一次读多个字节");
        char[] tempchars = new char[100];
        int charread = 0;
        try {
            reader = new InputStreamReader(new FileInputStream(filename));
            //读入多个字符到字符数组，charread为一次读取字符数
            try {
                while ((charread = reader.read(tempchars)) != -1) {
                    //同样屏蔽掉\r不显示
                    if ((charread == tempchars.length) && (tempchars[tempchars.length - 1] != '\r')) {
                        System.out.println();
                        System.out.print(tempchars);
                    } else {

                        for (int i = 0; i < charread; i++) {
                            if (tempchars[i] == '\r') {
                                continue;
                            } else {
                                System.out.print(tempchars[i]);
                            }

                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 以行为单位读取文件，常用于读面向行的格式化文件
     */
    public static void readFileByLines(String filename) {
        File file = new File(filename);

        BufferedReader reader = null;
        System.out.println("以行为单位读取文件内容，一次读一整行");
        try {
            reader = new BufferedReader(new FileReader(file));

            String tempString = null;
            int line = 1;
            //一次读一行，直到读到null文件位置

            try {
                while ((tempString = reader.readLine()) != null) {
                    //显示行号
                    System.out.println("line" + line + ":" + tempString);

                    line++;
                }
                reader.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    /**
     * 随机读取文件内容
     */

    public static void readfileByRandomAccess(String filename) {

        RandomAccessFile randomFile = null;
        System.out.println("随机读取一段文件内容");
        //打开随机访问文件流，按只读方式
        try {
            randomFile = new RandomAccessFile(filename, "r");

            /*这里的r:如果模式为只读 r，不会创建文件。会去读取一个已存在文件，如果该文件不存在，则会出现异常。
            如果模式为读写 rw，操作的文件不存在，会自动创建，如果存在则不会覆盖该文件。*/

            //文件长度,字节数
            long fileLength = 0;
            try {
                fileLength = randomFile.length();
                //读取文件的起始位置
                int beginIndex = (fileLength > 4) ? 4 : 0;
                //将读文件的位置移到beginIndex的位置。
                randomFile.seek(beginIndex);
                byte[] bytes = new byte[10];
                int byteread = 0;
                //一次读10个字节,如果文件内容不足10个字节,则读剩下的字节
                //将一次读取的字节赋值给byteread
                while ((byteread = randomFile.read(bytes)) != -1) {
                    System.out.write(bytes, 0, byteread);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (randomFile != null) {
                try {
                    randomFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
    /**
     * 显示字节输入流还剩的字节数
     *
     */
        private static  void showAvailableBytes(InputStream in){
            try {
                System.out.println("当前字节输入流的字节数为:"+ in.available());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    public static void main(String[] args) {
        String filename = "D:\\作业\\01.txt";
    //    ReadFromFile.readFileByBytes(filename);


      //  ReadFromFile.readFileByChars(filename);
     // ReadFromFile.readFileByLines(filename);
       ReadFromFile.readfileByRandomAccess(filename);
    }

}

