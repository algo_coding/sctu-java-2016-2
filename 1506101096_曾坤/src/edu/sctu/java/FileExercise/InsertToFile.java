package edu.sctu.java.FileExercise;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.*;

/**
 * Created by mike on 2016/10/23.
 */
public class InsertToFile {

    public InsertToFile(){

    }
    public static void main(String[] args) {

        FileOutputStream out = null;

        FileOutputStream outSTr = null;

        BufferedOutputStream Buff=null;

        FileWriter fw = null;

        int count=1000;//写文件行数

        try {

            out = new FileOutputStream(new File("D:\\作业\\02.txt"));

            long begin = System.currentTimeMillis();

            for (int i = 0; i < count; i++) {

                out.write("测试java 文件操作\r\n".getBytes());

            }

            out.close();

            long end = System.currentTimeMillis();

            System.out.println("FileOutputStream执行耗时:" + (end - begin) + "豪秒");

            outSTr = new FileOutputStream(new File("D:\\作业\\03.txt"));

            Buff=new BufferedOutputStream(outSTr);

            long begin0 = System.currentTimeMillis();

            for (int i = 0; i < count; i++) {

                Buff.write("测试java 文件操作\r\n".getBytes());

            }

            Buff.flush();

            Buff.close();

            long end0 = System.currentTimeMillis();

            System.out.println("BufferedOutputStream执行耗时:" + (end0 - begin0) + "豪秒");

            fw = new FileWriter("D:\\作业\\04.txt");

            long begin3 = System.currentTimeMillis();

            for (int i = 0; i < count; i++) {

                fw.write("测试java 文件操作\r\n");

            }

            fw.close();

            long end3 = System.currentTimeMillis();

            System.out.println("FileWriter执行耗时:" + (end3 - begin3) + "豪秒");

        } catch (Exception e) {

            e.printStackTrace();

        }

        finally {

            try {

                fw.close();

                Buff.close();

                outSTr.close();

                out.close();

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }
}
