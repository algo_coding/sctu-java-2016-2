package edu.sctu.java.FileExercise;

/**
 * Created by mike on 2016/10/24.
 */

import java.util.ArrayList;
import java.util.List;

public class ObjectAry {

    public static void main(String[] args) {

        List<Object[]> list = new ArrayList<Object[]>();

        list.add(new Object[]{"Sun Jianjing", "Xiandaiyinxiang", "Renminyoudian", "2010-05-01"});
        list.add(new Object[]{"Wang Aiping", "Ruanjianceshi", "Qinghuadaxue", "2010-05-11"});
        list.add(new Object[]{"Zhang Yihe", "51DanPianJi", "Renminyoudian", "2010-06-13"});

        String time = "2010-05-01";

//starts


        for (int i = 0, len = list.size(); i < len; i++) {
            Object[] objs = list.get(i);

            StringBuffer sb = new StringBuffer();

            for (int m = 0; m < objs.length; m++) {
                sb.append(((String) objs[m]) + ", ");

            }
            String result = sb.toString().trim();
            result = result.substring(0, result.length() - 1);
            System.out.println(result);


        }


    }

    //ends


}
