package edu.sctu.java.FileExercise;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by mike on 2016/10/23.
 */
public class AddFileContent {

    public static void appendMethodA(String filename, String content) {   //方法一使用RandomAccessFile

        try {
            //打开随机访问文件流,按读写方式
            RandomAccessFile randomFile = new RandomAccessFile(filename, "rw");
            //文件长度 ,字节数
            long fileLength = 0;
            try {
                fileLength = randomFile.length();
                randomFile.seek(fileLength);
                randomFile.writeBytes(content);
                randomFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //将文件指针移到文件尾

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static void appendMethodB(String filename,String content){

        //打开一个文件器，构造函数中的第二个参数true 表示以追加形式写文件

        FileWriter writer = null;
        try {
            writer = new FileWriter(filename,true);
            writer.write(content);

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
