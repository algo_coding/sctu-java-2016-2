package edu.sctu.java.ownbymyself;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Scanner;

/**
 * This program uses reflection to print all features of a class
 * Created by mike on 2016/10/27.
 */
public class Reflection {


    public static void main(String[] args) {

        //read class name from command lines args or user input
      //阅读类的名字从命令行参数或用户输入
        String name;
        if (args.length > 0) name = args[0];
        else {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter class name(e.g.java.util.Date)");
            name = in.next();
        }

        try {
            //print class name add superclass name(if !=Object)
            Class cl = Class.forName(name);

            Class supercl = cl.getSuperclass();

            String modifiers = Modifier.toString(cl.getModifiers());

            if (modifiers.length() > 0) System.out.println(modifiers + "");

            System.out.println("class " + name);

            if (supercl != null && supercl != Object.class) {
                System.out.println("extends " + supercl.getName());
            }


      /*      System.out.print("\n{\n");
            printConstructors(cl);
            System.out.println();
            printMethods(cl);
            System.out.println();
            printFields(cl);
            System.out.println("}");*/


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }


    //print all constructors of a class
    public static void printConstructors(Class cl) {

        Constructor[] constructors = cl.getConstructors();

        for (Constructor c : constructors) {
            String name = c.getName();
            System.out.print("    ");
            String modifiers = Modifier.toString(c.getModifiers());
            if (modifiers.length() > 0) System.out.println(modifiers + " ");
            System.out.println(name + "(");
            //print parameter types
            Class[] paramTypes = c.getParameterTypes();
            for (int j = 0; j < paramTypes.length; j++) {
                if (j > 0) System.out.print(paramTypes[j].getName());
            }
            System.out.println(");");
        }

    }




    //              print all methods  of a class

    public static void printMethods(Class cl) {
        Method[] methods = cl.getDeclaredMethods();   //返回类包含的全内部类
        for (Method m : methods) {                   //对成员内部类进行发反射
            Class retType = m.getReturnType();
            String name = m.getName();
            System.out.print("   ");
            //print modifiers, return type and method name
            String modifiers = Modifier.toString(m.getModifiers());
            if (modifiers.length() > 0) System.out.println(modifiers + " ");
            System.out.print(retType.getName() + "   " + "(");


            //print parameter
            Class[] paramTypes = m.getParameterTypes();

            for (int j = 0; j < paramTypes.length; j++) {
                if (j > 0) System.out.println(",   ");
                System.out.print(paramTypes[j].getName());

            }
            System.out.println(");");

        }

    }

    /**
     * Prints all fields of a class     (域)成员变量
     */
    public static void printFields(Class cl) {

        Field[] fields = cl.getFields();
//getDeclaredFields()返回Class中所有的字段，包括私有字段        /
//getFields  只返回公共字段，即有public修饰的字段/
        for (Field f : fields) {

            Class type = f.getType();//获取属性声明是类型对象（返回class对象）

            String name = f.getName();// 获取属性声明的名字   /
            System.out.println("   ");
            String modifiers = Modifier.toString(f.getModifiers());
            //public static String toString(int mod)可以输出该整数对应的所有的修饰符
            // getModifiers()方法返回int 类型值表示该字段的修饰符  特定的字节码

            if (modifiers.length() > 0) {
                System.out.print(modifiers + " ");
                System.out.println(type.getName() + name + ";");
            }
                        /* getType() 和 getGenericType()的区别 ：*/
        }               /* */
                        /* 1.首先是返回的类型不一样，一个是Class对象一个是Type接口*/
    }                   /* */
                        /* 2.如果属性是一个泛型，从getType（）只能得到这个属性的接口类型。但从getGenericType（）还能得到这个泛型的参数类型。*/
}
