package edu.sctu.java.ownbymyself;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by mike on 2016/10/10.
 */
public class GetsumPageandNextUrl {

    String Url = "";
    int a = 0;


    //获取一个地区景点的所有页面数 返回值为整数
    public static int getsumurl(String Url) {

        Document document = null;
        try {
            document = Jsoup.connect(Url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Element content = document.body();
        Element links6 = content.select(".pageNum.taLnk").last();
        if(links6==null) {//对只有一个页面处理
            return 200;
        }
        else {
            int n = Integer.valueOf(links6.text()).intValue();
            return n;
        }
    }

//返回去余下的Url地址
    public ArrayList<String> NextgetUrl(String Url) {

        ArrayList<String> arrayList = new ArrayList();//创建Array list数组存储
        if ( GetsumPageandNextUrl.getsumurl(Url)==200){
            arrayList.add(Url);
         return   arrayList;
        }
        else {


            StringBuilder str = new StringBuilder(Url);
            arrayList.add(str.toString());

            for (int i = 0; i <( GetsumPageandNextUrl.getsumurl(Url)-1); i++) {


                a = a + 30;
                str.insert(56, "-oa" + a);
                arrayList.add(str.toString());//添加余下页面的URl
                str = new StringBuilder(Url);

            }

            return arrayList;
        }
    }
}