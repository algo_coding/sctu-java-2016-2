package edu.sctu.java.ownbymyself;

/**
 * Created by mike on 2016/10/25.
 */
public class Manager extends Employee {


    public Manager(String n, double s, int year, int month, int day) {
        super(n, s, year, month, day);
    }
//这里的super是    “调用超类Employee中含有n,s,year,month和day参数的构造器”

//由于Manger类的构造器不能访问Employee类的私有域,所以必须利用Employee类的构造器对这部分私有域进行初始化
    //关键字this的两个用途:一是调用该类其他的构造器，二是引用隐式参数

    //super 也是调用超类的构造器和超类的方法
    private double bonus;

    public void setBonus(double b) {
        bonus = b;
    }


    public double getSalary() {

        double baseSalary = super.getSalary();
        return baseSalary + bonus;
    }



    public static void main(String[] args) {
        Manager boss = new Manager("Carl Cracker", 80000.0, 1987, 12, 15);
        boss.setBonus(50000);

        Employee[] staff = new Employee[3];
        staff[0] = boss;
        staff[1] = new Employee("Harry Hacker", 5000, 1989, 10, 1);

        staff[2] = new Employee("Tony Tester", 40000, 1990, 3, 15);
        for (Employee e : staff) {
            System.out.println(e.getName() + e.getSalary());
        }

    }


}
