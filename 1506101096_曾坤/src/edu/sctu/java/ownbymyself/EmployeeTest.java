package edu.sctu.java.ownbymyself;

import java.util.Date;

/**
 * Created by mike on 2016/10/25.
 */
public class EmployeeTest {


    public static void main(String[] args) {

        Employee[] staff = new Employee[3];
        staff[0] = new Employee("Carl Cracker", 75000.0, 1987, 12, 15);
        staff[1] = new Employee("Harry Hacker", 50000.0, 1989, 10, 1);
        staff[2] = new Employee("Tony Tester", 40000.0, 1990, 3, 15);

/*
        for(Employee e:staff){
            e.raiseSalary(5.0);
        }
        for(Employee e :staff){
            System.out.println("name="+ e.getName() + ", salary="+e.getSalary()+",hireDay="+e.getHireDay());
        }*/

        Class cl = staff.getClass();
        //  System.out.println(cl.getName() + " " + staff[2].getName());


        Date d = new Date();
        Class cl2 = d.getClass();
        String name = cl2.getName();//java.util.Date


        String className = "java.util.Date";//这个方法 只有是className是类名或者接口才能执行  否则抛出异常
        try {
            Class cl3 = Class.forName(className);//通过类名获得对应的Class对象
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        //获得Class类对象的第三种方法：   T 是任意的java类型 T.class将表示任意的类对象

        Class cl4 = Date.class;//if you import java.util.*

        Class cl5 = int.class;//返回int


        Class Cl6 = Double[].class;//返回[Ljava.lang.Double;  奇怪的名字

        System.out.println(Cl6 + "    " + cl5);

        //一个class对象实际上表示的是一个类型，而这个类型未必是一种类


        try {

            Object m = staff[0].getClass().newInstance();//使用newInstance来快速创建一个类的实例



            String l = "java.util.Date";
            try {

                Object n = Class.forName(l).newInstance();//forName与newInstance配合

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }




        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // Class类中的 Field Method Constructor 分别为描述类的  域  方法  构造器数组





    /*
    Employee harry  = new Employee("Carl Cracker",75000.0,1987,12,15);
        Date d = harry.getHireDay();
        double tenYearsInMilliSeconds =10 * 365.25 * 24 *60 *60 *1000;
    d.setTime(d.getTime()-(Long)tenYearsInMilliSeconds);
    }*/  // d 和 harry.hireDay应用同一个对象 出错原因微妙

        //在构造函数应该：

      /*  class Employee{
            public Date getHireDay(){
                return getHireDay().clone();
            }

        }
*/




/*    private static int nextId =1;*/


  /*  private int id;



    public void getNextId() {

        id = nextId;
        nextId++;

    }*/


    }
}




