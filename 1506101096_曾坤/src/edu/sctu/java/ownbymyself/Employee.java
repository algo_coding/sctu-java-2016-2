package edu.sctu.java.ownbymyself;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by mike on 2016/10/25.
 */
public class Employee {

    private String name;

    private double salary;

    private Date hireDay;

    public Employee(String n, double s, int year, int month, int day) {
        name = n;
        salary = s;
        GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
        hireDay = calendar.getTime();

    }


    public String getName() { //只读域

        return name;

    }

   /* public Employee(double salary, Date hireDay) { //可以多个构造函数  只要参数个数与参数类型不同即可
        this.salary = salary;
        this.hireDay = hireDay;
    }
*/

    public double getSalary(){
        return salary;
    }

    public Date getHireDay(){  //返回了引用可变对象(Date类对象)的访问器方法,

        return hireDay;

    }
    public void raiseSalary(Double byPercent) {//只能通过此方法修改salary

        double raise = salary * byPercent / 100;
        salary += raise;
    }
   /*
    String firstname ;
    String lastname ;
    return firstname+ "" + lastname
*/
}

