package edu.sctu.java.ownbymyself;

/**
 * Created by mike on 2016/10/25.
 */

//public enum Size{SMALL,MEDIUM,LARGE,EXTRA_LARGE}
public enum Size {

    SMALL("S"),MEDIUM("M"),LARGE("L"),EXTRA_LARGE("XL");
    private String abbreviation;
    private Size(String abbreviation){this.abbreviation=abbreviation;}
    public String getAbbreviation(){return abbreviation;}
     //   Size.SMALL.toString()  将返回字符串“SMALL”
    //其逆方法：是静态方法value方法,将返回一个包含全部枚举值的数组
    Size[] values = Size.values();


}
