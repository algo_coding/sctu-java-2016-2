package edu.sctu.java.listtest;


import java.util.List;

/**
 * Created by AD on 2016/10/20.
 */
public interface HotelExporter  {

    public void export(List<Hotel> hotelList,String fileName);
}
