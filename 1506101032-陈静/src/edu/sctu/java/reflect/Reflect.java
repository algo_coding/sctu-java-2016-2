package edu.sctu.java.reflect;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by AD on 2016/10/27.
 */
public class Reflect {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {


        try {
            Class a=Class.forName("edu.sctu.java.reflect.Student");
            Student chen= (Student) a.newInstance();
            Method[] methods=a.getMethods();
            for (Method method:methods){
                System.out.println(method.getName());
            }
            Field[] fields=a.getDeclaredFields();
            for (Field field:fields){
                System.out.println(field.getName());
            }

               Method method=a.getDeclaredMethod("eat");
                method.invoke(chen);



        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
