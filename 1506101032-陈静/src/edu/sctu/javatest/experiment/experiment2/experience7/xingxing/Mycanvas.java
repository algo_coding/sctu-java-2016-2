package edu.sctu.javatest.experiment.experiment2.experience7.xingxing;

import java.awt.*;

/**
 * Created by AD on 2016/12/22.
 */
public class Mycanvas extends Canvas
{  int r;
    Color c;
    public void setColor(Color c)
    { this.c=c;
    }
    public void setR(int r)
    {  this.r=r;
    }
    public void paint(Graphics g)
    { g.setColor(c);
        g.fillOval(0,0,2*r,2*r);
    }
    public int getR()
    {  return r;
    }
}
