package edu.sctu.javatest.experiment.experiment2;

/**
 * Created by AD on 2016/10/27.
 */
public class GreekAlphabet {
    public static void main (String args[ ])
    {
        int startPosition=0,endPosition=0 ;
        char cStart='α',cEnd='ω';
        startPosition=(int) cStart;
           //cStart做int型转换据运算，并将结果赋值给startPosition
        endPosition=(int) cEnd;
          //cEnd做int型转换运算，并将结果赋值给endPosition
        System.out.println("希腊字母表：");
        for(int i=startPosition;i<=endPosition;i++)
        {
            char c;
            c=(char) i;
              //i做char型转换运算，并将结果赋值给c
            System.out.print(" "+c);
            if((i-startPosition+1)%10==0)
                System.out.println("");
        }
    }

}
