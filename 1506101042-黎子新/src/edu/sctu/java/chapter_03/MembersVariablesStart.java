package edu.sctu.java.chapter_03;

/**
 * Created by 黎子新 on 2016/10/18.
 * 成员变量可自动初始化，其中数值型为0，逻辑型为false，引用型为null。
 * 成员变量的赋值可以和在声明时进行，也可以在方法中实现，但不能在声明和
 * 方法之间赋值。所以这里写来测试一下；
 */
class A {
    int x;
    float y;

    // int x=12;
    //float y=12.56f;
    float sumXY() {
        x = 12;
        y = 12.56f;
        return x + y;
    }
}

public class MembersVariablesStart {
    public static void main(String[] args) {
        A a = new A();
        // a.x=12;
        //a.y=12.56f;
        System.out.println("x+y=" + a.sumXY());


    }
}
