package edu.sctu.java.lab_01;

/**
 * Created by 黎子新 on 2016/11/24.
 */
public class MainClass
{
    public static void main (String args[ ])
    {
        System.out.println("你好，只需编译我");    //命令行窗口输出"你好，只需编译我"
        A a=new A();
        a.fA();
        B b=new B();
        b.fB();
    }
    public static class A
    {
        void fA()
        {
            System.out.println("I am A");    //命令行窗口输出"I am A"
        }
    }

    public static class B
    {
        void fB()
        {
            System.out.println("I am B");    //命令行窗口输出"I am B"
        }
    }

    public class C
    {
        void fC()
        {
            System.out.println("I am C");    //命令行窗口输出"I am C"
        }
    }

}
