package edu.sctu.java.lab_01;

/**
 * Created by 黎子新 on 2016/11/24.
 */
public class Hello {
    public static void main(String args[]) {
        System.out.println("你好，很高心学习java");    //命令行窗口输出"你好，很高兴学习Java"
        A a = new A();
        a.fA();
    }
}

class A {
    void fA() {
        System.out.println("We are student");    //命令行窗口输出"We are students"
    }
}
