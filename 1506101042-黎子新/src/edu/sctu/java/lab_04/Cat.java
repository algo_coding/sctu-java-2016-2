package edu.sctu.java.lab_04;

/**
 * Created by 黎子新 on 2016/11/14.
 * 创建一个猫类，来实现类的方法的重载
 */
public class  Cat {
    String color;
    String sex;
    String age;

    Cat() {
        System.out.println("这是一个无参数的构造函数------我是一只猫");
    }

    Cat(String color, String sex, String age) {
        this.color = color;
        this.sex = sex;
        this.age = age;
        System.out.println("这是方法的重载，是一个有参数的构造函数-----");
        System.out.println(color + " " + sex + " " + age);

    }

    public static void main(String[] args) {
        Cat oneCat = new Cat();
        Cat twoCat = new Cat("黑色", "母猫", "2岁");


    }

}
