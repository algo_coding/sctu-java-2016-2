package edu.sctu.java.lab_04;

/**
 * Created by 黎子新 on 2016/11/14.
 * 定义一个矩形类，再定义接口EqualDiagonal，其中包含方法getDiagonal()；
 * 由矩形类派生出一个正方形类，自行扩充成员变量和方法，并实现此接口EqualDiagonal。
 */
public class  Rectangular {
    int edgeUpDown;
    int edgeLeftRight;
    int getArea(int a, int b) {
        edgeLeftRight = a;
        edgeUpDown = b;
        int area;
        area = a * b;
        return area;

    }

}
