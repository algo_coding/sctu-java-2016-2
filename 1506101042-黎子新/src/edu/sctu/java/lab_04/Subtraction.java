package edu.sctu.java.lab_04;

/**
 * Created by 黎子新 on 2016/11/14.
 * 运行下面的程序，理解成员变量的继承与隐藏。
 */
public interface Subtraction {
    public  int sub(int num1, int num2);

}
