package edu.sctu.java.lab_04;

/**
 * Created by 黎子新 on 2016/11/14.
 * 创建一个主类在实现书店由大到小排序
 */
public class  Class2 {

    public static void main(String args[]) {
        Sort2or3 m1 = new Sort2or3();
        m1.sort(100, 200);
        System.out.println("两个数从大到小为：" + m1.max1 + "," + m1.max2);
        m1.sort(50, 150, 100);
        System.out.println("三个数从大到小为：" + m1.max1 + "," + m1.max2 + "," + m1.max3);

    }
}

