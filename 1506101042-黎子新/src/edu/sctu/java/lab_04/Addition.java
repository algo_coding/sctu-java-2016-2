package edu.sctu.java.lab_04;

/**
 * Created by 黎子新 on 2016/11/14.
 * 创建一个加法运算的接口
 */
public interface Addition {

  public int sum(int a, int b);

}
