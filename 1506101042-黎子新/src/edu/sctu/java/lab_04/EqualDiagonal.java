package edu.sctu.java.lab_04;

/**
 * Created by 黎子新 on 2016/11/14.
 * 创建一个可以得到对边边长的接口
 */
public interface  EqualDiagonal {

    public int getDiagonal();

}
