package edu.sctu.java.lab_04;

/**
 * Created by 黎子新 on 2016/11/14.
 * 运行下面的程序，理解成员变量的继承与隐藏。
 */
public class  TestAdditionAndSubtraction implements Addition,Subtraction {

    @Override
    public int sum(int a, int b) {
        int sums=a+b;
        System.out.println("实现加法接口的运算结果"+a+"+"+b+"="+sums);
        return  sums;
    }

    @Override
    public int sub(int num1, int num2) {
        int subs= num1-num2;
        System.out.println("实现减法接口的运算结果是"+num1+"-"+num2+"="+subs);
        return subs;
    }

    public static void main(String[] args) {
        TestAdditionAndSubtraction user=new TestAdditionAndSubtraction();
        user.sum(10,29);
        user.sub(38,13);
    }

}
