package edu.sctu.java.lab_04;

import java.lang.StrictMath;

/**
 * Created by 黎子新 on 2016/11/14.
 * 定义一个矩形类，再定义接口EqualDiagonal，其中包含方法getDiagonal()；
 * 由矩形类派生出一个正方形类，自行扩充成员变量和方法，并实现此接口EqualDiagonal。
 */
public class  Square extends Rectangular implements EqualDiagonal {
    @Override
    public int getDiagonal() {
        double square = edgeLeftRight * edgeLeftRight + edgeUpDown * edgeUpDown;
        int diagonal = (int) Math.sqrt(square);
        return diagonal;
    }

    int circle;
    int getCircle(int edge) {

        circle = 4 * edge;
        return circle;

    }

    public static void main(String[] args) {
        Square oneSquare = new Square();
        System.out.println("这个矩形的面积是：" + oneSquare.getArea(7, 8));
        System.out.println("这个矩形的对边边长是：" + oneSquare.getDiagonal());
        System.out.println("这儿正方形的边长是：" + oneSquare.getCircle(8));
    }
}
