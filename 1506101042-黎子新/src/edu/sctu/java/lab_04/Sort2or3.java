package edu.sctu.java.lab_04;

/**
 * Created by 黎子新 on 2016/11/14.
 * 运行下面的程序，理解方法的重载。
 */
public class  Sort2or3 {
    double max1, max2, max3;
    Sort2or3() {
        max1 = -1;
        max2 = -1;
        max3 = -1;
    }

    void sort(double i, double j) {
        double s;
        max1 = i;
        max2 = j;
        if (max1 < max2) {
            s = max1;
            max1 = max2;
            max2 = s;
        }

    }
    void sort(double i, double j, double k) {
        double s;
        max1 = i;
        max2 = j;
        max3 = k;
        if (max1 < max2) {
            s = max1;
            max1 = max2;
            max2 = s;
        }
        if (max1 < max3) {
            s = max1;
            max1 = max3;
            max3 = s;
        }
        if (max2 < max3) {
            s = max2;
            max2 = max3;
            max3 = s;
        }
    }
}


