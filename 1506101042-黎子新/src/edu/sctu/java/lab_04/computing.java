package edu.sctu.java.lab_04;

/**
 * Created by 黎子新 on 2016/11/14.，
 * 创建一个主类来理解成员变量的继承与隐藏。
 */
public class  computing {
    public static void main(String arg[]) {
        Subsum3 m1 = new Subsum3();
        m1.sum(100, 200, 300);
        System.out.println("sum=" + m1.num1 + "+" + m1.num2 + "+" + m1.num3 + "=" + m1.sum);
    }

}
