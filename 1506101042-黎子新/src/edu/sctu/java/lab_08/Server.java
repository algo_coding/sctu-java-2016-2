package edu.sctu.java.lab_08;

/**
 * Created by 黎子新 on 2016/12/21.
 */

import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.*;

public class Server {
    public static void main(String args[]) {
        ServerSocket server = null;
        ServerThread thread;
        Socket you = null;
        while (true) {
            try {
                server = new ServerSocket(4331); //创建在端口4331上负责监听的 ServerSocket对象
            } catch (IOException e1) {
                System.out.println("正在监听");
            }
            try {
                you = server.accept(); // server返回和客户端相连接的Socket对象
                System.out.println("客户的地址:" + you.getInetAddress());
            } catch (IOException e) {
                System.out.println("正在等待客户");
            }
            if (you != null) {
                new ServerThread(you).start();
            } else {
                continue;
            }
        }
    }
}

class ServerThread extends Thread {
    Socket socket;
    ObjectInputStream in = null;
    ObjectOutputStream out = null;
    String s = null;

    ServerThread(Socket t) {
        socket = t;
        try {
            out = new ObjectOutputStream(socket.getOutputStream()); //socket返回输出流。
            in = new ObjectInputStream(socket.getInputStream()); //socket返回输入流。
        } catch (IOException e) {
        }
    }

    public void run() {
        TextArea text = new TextArea("你好,我是服务器", 12, 12);
        try {
            out.writeObject(text);
        } catch (IOException e) {
            System.out.println("客户离开");
        }
    }
}

