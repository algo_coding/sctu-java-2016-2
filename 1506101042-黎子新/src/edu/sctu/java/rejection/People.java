package edu.sctu.java.rejection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by 黎子新 on 2016/10/27.
 */
public class People {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
//用反射的方法来实例化对象
        Class clz = Class.forName("edu.sctu.java.rejection.Student");
        Student wang = null;
        try {
            wang = (Student) clz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        wang.setName("王然");

        wang.setAge(18);

        wang.setSex('女');

        System.out.println(wang.getName() + "\t" + wang.getSex() + "\t" + wang.getAge());

//用反射的方法来获得对象的属性和方法
        Field[] fileds = clz.getDeclaredFields();
        for (Field field : fileds) {

            System.out.println(field.getName());
        }

        Method[] methods = clz.getMethods();
        for (Method method : methods) {

            System.out.println(method.getName());
        }
        Method method = clz.getDeclaredMethod("study");

        method.invoke(wang);


// 按正常方法new对象
        // Student wang=new Student();

        // wang.setAge(18);

        // System.out.println(wang.getAge());


    }
}
