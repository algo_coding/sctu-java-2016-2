package edu.sctu.java.rejection;

/**
 * Created by 黎子新 on 2016/10/27.
 */
public class Student {
    int age;
    String name;
    char sex;

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public char getSex() {
        return sex;
    }

    public void study(){
        System.out.println(" We can play game");
    }
}
