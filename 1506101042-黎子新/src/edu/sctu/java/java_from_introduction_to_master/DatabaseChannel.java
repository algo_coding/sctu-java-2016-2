package edu.sctu.java.java_from_introduction_to_master;

/**
 * Created by 黎子新 on 2016/10/25.
 */

import java.io.*;
import java.sql.*;

public class DatabaseChannel {
    public static void main(String[] args) {
        //声明Connection对象
        Connection con;
        //驱动程序名
        String driver = "com.mysql.jdbc.Driver";
        //URL指向要访问的数据库名mydata
        String url = "jdbc:mysql://172.16.10.37/elite";
        //MySQL配置时的用户名
        String user = "elite";
        //MySQL配置时的密码
        String password = "elite";
        File file = new File("D:/javawenjianliu", "ScenicSpots.txt");
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, user, password);
            String sq = "select * from ScenicSpots";
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery(sq);
            FileWriter fw = new FileWriter(file);
            BufferedWriter bufw = new BufferedWriter(fw);
            String id;
            String ScenicName, URL, Rank, Allassess;
            System.out.println("id\tScenicName\tURL\tRank\tAllassess");
            while (rs.next()) {

                id = rs.getString("id");

                ScenicName = rs.getString("ScenicName");

                URL = rs.getString("URL");

                Rank = rs.getString("Rank");

                Allassess = rs.getString("Allassess");
                if (rs.getString("Allassess") == null) {
                    Allassess = "null";
                } else {
                    Allassess = rs.getString("Allassess");
                }

                System.out.println(id + "\t" + ScenicName + "\t" + URL + "\t" + Allassess);
                bufw.write(id);
                bufw.write("\r\n");
                bufw.write(ScenicName);
                bufw.write("\r\n");
                bufw.write(URL);
                bufw.write("\r\n");
                bufw.write(Rank);
                bufw.write("\r\n");
                bufw.write(Allassess);
                bufw.write("\r\n");

            }

            bufw.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        } catch (SQLException e) {

            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getString(String allassess) {
        return allassess;
    }
}
