package edu.sctu.java.java_from_introduction_to_master;

import java.io.*;

/**
 * Created by 黎子新 on 2016/10/20.
 * 通过Fileoutputstream写文件，以及通过Fileintputstream
 * 把文件内容显示到控制台上
 */
public class FileInputAndOutputStream {
    public static void main(String[] args) {
        File file=new File("D:/javawenjianliu","work.txt");
        try {
            FileOutputStream out=new FileOutputStream(file);
            byte buy[]="今天很残酷，明天更残酷，后天很美好，但大多数人都死在明天的晚上看不到初生的太阳".getBytes();
            out.write(buy);
            out.close();
            System.out.println("写好了亲");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
        try {
            FileInputStream in=new FileInputStream(file);
            byte[] byt=new byte[1024];
            try {
                int len=in.read(byt);
                System.out.println("文件中的信息是："+new String(byt,0,len));
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}
