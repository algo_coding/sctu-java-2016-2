package edu.sctu.java.java_from_introduction_to_master;

import java.io.*;

/**
 * Created by 黎子新 on 2016/10/21.
 */
public class StudentFile {
    public static void main(String[] args) {
        String content[] = {"学号", "姓名", "出生年月"};
        File file = new File("D:/javawenjianliu", "student.txt");
        try {
            FileWriter fw = new FileWriter(file);
            BufferedWriter buffer = new BufferedWriter(fw);
            for (int k = 0; k < content.length; k++) {
                buffer.write(content[k]);
                buffer.newLine();
            }
            buffer.close();
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileReader fi = new FileReader(file);
            BufferedReader bff = new BufferedReader(fi);
            String s = null;
            int n = 0;
            while ((s = bff.readLine()) != null) {
                n++;
                System.out.println("第" + n + "行:" + s);
            }
            bff.close();
            fi.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
