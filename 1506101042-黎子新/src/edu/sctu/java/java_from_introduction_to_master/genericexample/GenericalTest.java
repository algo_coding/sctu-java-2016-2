package edu.sctu.java.java_from_introduction_to_master.genericexample;

/**
 * Created by 黎子新 on 2016/10/31.
 * 终于明白泛型是怎么回事了
 */
public class GenericalTest {
    public static void main(String[] args) {
        GenFX1<Integer> intOb = new GenFX1<Integer>(100);

        intOb.showType();

        int i = intOb.getOb();

        System.out.println("value=" + i);

        System.out.println("************");

        GenFX1<String> strOb = new GenFX1<String>("Hello Gen!");

        strOb.showType();

        String s = strOb.getOb();

        System.out.println("value=" + s);
    }
}
class GenFX1<T> {
    private T ob;

    public GenFX1(T ob) {
        this.ob = ob;
    }

    public T getOb() {
        return ob;
    }

    public void setOb() {

        this.ob = ob;
    }

    public void showType() {
        System.out.println("T的实际类型是：" + ob.getClass().getName());
    }

}
