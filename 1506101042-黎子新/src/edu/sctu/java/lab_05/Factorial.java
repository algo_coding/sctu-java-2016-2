package edu.sctu.java.lab_05;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by 黎子新 on 2016/11/28.
 */
public class Factorial {
    public static void main(String[] args) {
        System.out.println("请输入数据");
        Scanner cin = new Scanner(System.in);
        int x = cin.nextInt();
        BigInteger a = BigInteger.valueOf(x);
        BigInteger b = BigInteger.ONE;
        BigInteger s = BigInteger.ONE;
        for (BigInteger i = BigInteger.ONE; i.compareTo(a) <= 0; i = i.add(b)) {
            s = s.multiply(i);
        }
        System.out.println("此数的阶乘是" + s);
        cin.close();
    }
}
