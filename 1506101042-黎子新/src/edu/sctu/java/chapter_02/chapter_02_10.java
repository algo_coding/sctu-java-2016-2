package edu.sctu.java.chapter_02;

/**
 * Created by home on 2016/9/11.
 * 请编写输出乘法口诀表的程序
 */
public class chapter_02_10 {
    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + "*" + i + "=" + i * j + "\t");
            }
            System.out.println();
        }
    }
}
