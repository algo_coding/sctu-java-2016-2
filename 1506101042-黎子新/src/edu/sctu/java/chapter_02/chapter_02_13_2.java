package edu.sctu.java.chapter_02;

/**
 * Created by home on 2016/9/12.
 * 复习break和continue语句，请调试本章中设计这两个语句的程序
 */
public class chapter_02_13_2 {
    public static void main(String[] args) {
        int sum = 0, i, j;
        for (i = 1; i <= 10; i++) {
            if (i % 3 != 0) {
                continue;
            } else {
            }
            sum = sum + i;
        }
        System.out.println("sum=" + sum);
    }
}
