package edu.sctu.java.chapter_02;

/**
 * Created by home on 2016/9/12.
 * 编写程序实现如下效果图
 * A
 * B     C
 * D     E     F
 * G    H     I     J
 * K    L     M     N
 * O     P     Q
 * R     S
 * T
 */

public class chapter_02_11 {
    public static void main(String[] args) {
        int i, j, m, num;
        char ch;
        num = 'A' - 1;
        for (i = 1; i <= 4; i++) {
            for (m = 1; m <= 4 - i; m++)
                System.out.print(" ");
            for (j = 1; j <= i; j++) {
                num = num + 1;
                ch = (char) num;
                System.out.print(ch + " ");
            }
            System.out.println();
        }
        for (i = 1; i <= 4; i++) {
            for (m = 1; m <= i - 1; m++)
                System.out.print(" ");
            for (j = 1; j <= 5 - i; j++) {
                num = num + 1;
                ch = (char) num;
                System.out.print(ch + " ");
            }
            System.out.println();
        }


    }
}
