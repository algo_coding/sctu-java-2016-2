package edu.sctu.java.chapter_02;

/**
 * Created by home on 2016/10/12.
 */
public class Test<T> {
    private T name;

    public T getName() {
        return name;
    }

    public void setName(T name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Test<String> stringTest = new Test<String>();
        stringTest.setName("aaa");
        System.out.println(stringTest.getName());
        Test<Integer> integerTest = new Test<Integer>();
        integerTest.setName(111);
        System.out.println(integerTest.getName());
    }
}