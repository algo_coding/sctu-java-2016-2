package edu.sctu.java.chapter_02;

/**
 * Created by home on 2016/10/12.
 */
interface Info<T>{
    public T getVar();
}
class Infolmpl implements Info<String>{
    private String var;
    public Infolmpl(String var){
        this.setVar(var);
    }
    public void setVar(String var){
        this.var=var;
    }
    public String getVar(){
        return this.var;
    }
}

public class dsadas {
    public static void main(String args[]){
        Info<String> i=null;
        i=new Infolmpl("lixinghua");
        System.out.println("content:"+i.getVar());
    }
}
