package edu.sctu.java.chapter_02;

/**
 * Created by home on 2016/9/12.
 * 上机调试本章的输入输出语句
 */
public class chapter_02_14 {
    public static void main(String[] args) {
        int x = 3;
        System.out.println("Hello");
        System.out.println(" " + x);
        System.out.println("Hello" + x + "!");
    }
}
