package edu.sctu.java.chapter_02;

/**
 * Created by home on 2016/9/12.
 * 复习break和continue语句，请调试本章中设计这两个语句的程序
 */
public class chapter_02_13_1 {
    public static void main(String[] args) {
        int i, j = 0;
        outer:
        for (i = 0; i < 5; i++)
            for (j = 0; j < 5; j++) {
                System.out.println(i + " " + j);
                if (j == 1)
                    break outer;
            }
        System.out.println("最终值：" + i + " " + j);
    }
}
