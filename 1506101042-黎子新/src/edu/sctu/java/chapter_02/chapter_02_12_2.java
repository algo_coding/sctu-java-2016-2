package edu.sctu.java.chapter_02;

/**
 * Created by home on 2016/9/12.
 * 分别利用for语句、while语句以及do while 语句编写一个求和程序
 * 即（sum=1+2+3+....n）
 */

import java.util.*;

public class chapter_02_12_2 {
    public static void main(String[] args) {
        System.out.println("1+2+3+....n,请输入n的值");
        Scanner num = new Scanner(System.in);
        int nums = num.nextInt();
        int i = 1, sum = 0;
        while (i <= nums) {
            sum = sum + i;
            i++;
        }
        System.out.println("用while语句输出的结果 sum=" + sum);
    }
}
