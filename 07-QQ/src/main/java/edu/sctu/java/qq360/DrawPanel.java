package edu.sctu.java.qq360;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

class DrawPanel extends JPanel {

    DatagramSocket ds;
    DatagramPacket dp;
    DatagramSocket ds1;
    DatagramPacket dp1;

    Thread thread;

    TextArea textArea1 = new TextArea(10, 50);
    TextArea textArea2 = new TextArea(10, 50);
    JTextField textField = new JTextField(50);

    static int cat1, cat2;
    static String IP;

    public void setCat1(int cat) {
        cat1 = cat;
    }

    public void setCat2(int cat) {
        cat2 = cat;
    }

    public void setIP(String ip) {
        IP = ip;
    }
    //重置端口号

    public void ls() {
        try {
            ds = new DatagramSocket(cat1);
            ds1 = new DatagramSocket(cat2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        thread = new Thread(new Runnable() {

            public void run() {
                byte buf[] = new byte[1024];//聊天每次发送文字不能超过1024b
                DatagramPacket dp = new DatagramPacket(buf, buf.length);

                while (true) {
                    try {
                        ds.receive(dp);
                        textArea1.setText(textArea1.getText() + new String(buf, 0, dp.getLength()) + "\r\n");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }

    public DrawPanel() {

        setLayout(new BorderLayout());
        JTabbedPane card = new JTabbedPane();

        textArea1.setEditable(false);
        card.add("聊天", textArea1);
        add(card, BorderLayout.NORTH);

        JPanel center = new JPanel();
        center.setLayout(new FlowLayout());
        center.add(new JLabel("昵称："));

        center.add(textField);
        add(center, BorderLayout.CENTER);

        JPanel south = new JPanel();
        south.setLayout(new BorderLayout());


        south.add(textArea2, BorderLayout.NORTH);

        JPanel south2 = new JPanel();
        south2.setLayout(new FlowLayout());
        JButton sent = new JButton("传送文件");
        JButton receive = new JButton("接收文件");
        //receive.setEnabled(false);
        JButton sentMessage = new JButton("发送");
        south2.add(sent);
        south2.add(receive);

        south2.add(sentMessage);
        add(south, BorderLayout.SOUTH);
        south.add(south2, BorderLayout.SOUTH);
        //发送信息的按钮的监听器:
        sentMessage.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                textArea1.append(textField.getText() + ":" + textArea2.getText() + "\n");
                String s;
                try {
                    s = textField.getText() + ":" + textArea2.getText();
                    byte[] buf = s.getBytes();
                    dp = new DatagramPacket(buf, buf.length, InetAddress.getByName(IP), cat1);
                    ds.send(dp);
                    textArea2.setText("");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        //发送文件的按钮的监听器
        sent.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                JFrame f = new JFrame("打开");
                f.setSize(400, 300);
                JFileChooser chooser = new JFileChooser();
                f.add(chooser);
                int returnVal = chooser.showOpenDialog(f);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    textArea1.append("正在传送文件:" + chooser.getCurrentDirectory() + "\\"
                            + chooser.getSelectedFile().getName() + "\n");
                    try {
                        byte[] b = new byte[10248];
                        FileInputStream fis = new FileInputStream(chooser.getSelectedFile());
                        DataInputStream dos = new DataInputStream(fis);
                        dos.read(b);
                        dp1 = new DatagramPacket(b, b.length, InetAddress.getByName(IP), cat2);
                        ds1.send(dp1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        //接收文件的按钮的监听器:
        receive.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                JFrame f1 = new JFrame("保存");
                f1.setSize(400, 300);
                byte buf[] = new byte[10248];
                DatagramPacket dp1 = new DatagramPacket(buf, buf.length);
                JFileChooser ch = new JFileChooser();
                f1.add(ch);
                int a = ch.showSaveDialog(f1);
                if (a == JFileChooser.APPROVE_OPTION) {
                    String fileName = ch.getSelectedFile().getPath();
                    try {
                        ds1.receive(dp1);
                        FileOutputStream fos = new FileOutputStream(fileName);
                        fos.write(buf);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}