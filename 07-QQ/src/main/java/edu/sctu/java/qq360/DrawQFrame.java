package edu.sctu.java.qq360;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

//面板类
class DrawQFrame extends JFrame {

    public static final int DEFAULT_WIDTH = 200;
    public static final int DEFAULT_HEIGHT = 400;

    //框架类
    public DrawQFrame() {
        this.setLocation(100, 100);
        this.setTitle("360QQ");
        this.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        this.setLayout(new BorderLayout());

        DrawQPanel panel = new DrawQPanel();

        Border etched = BorderFactory.createEtchedBorder();
        Border title = BorderFactory.createTitledBorder(etched, "360QQ");//加入边框
        panel.setBorder(title);

        Border lineborder = BorderFactory.createLineBorder(Color.yellow);
        panel.setBorder(lineborder);

        this.add(panel, BorderLayout.NORTH);

    }
}