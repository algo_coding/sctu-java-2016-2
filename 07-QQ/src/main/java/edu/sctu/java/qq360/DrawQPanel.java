package edu.sctu.java.qq360;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

//面板类
class DrawQPanel extends JPanel {


    public ArrayList getFriendList() {
        return this.friendList;
    }

    public ArrayList getbuttonList() {
        return this.buttonList;
    }

    private static ArrayList<MyFriend> friendList = new ArrayList<MyFriend>(10);
    MyFriend bestFriend = new MyFriend("李兵", "火", "Never Give up!");
    MyFriend bestFriend2 = new MyFriend("实验学院", "honor school", "Never Give up!");
    private static ArrayList<JButton> buttonList = new ArrayList<JButton>(10);



    public DrawQPanel() {
        this.setSize(100, 300);
        this.setLayout(new BorderLayout());


        JButton button = new JButton("与神秘陌生人聊天");
        button.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                DrawFrame frame = new DrawFrame();
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frame.setVisible(true);
            }
        });
        this.add(button, new BorderLayout().CENTER);

        JTextField textField = new JTextField();
        textField.setText("好友昵称或Q号");
        this.add(textField);

        JButton searchButton = new JButton("搜索好友");
        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        this.add(searchButton);


        friendList.add(bestFriend);
        friendList.add(bestFriend2);

        for (int i = 0; i < friendList.size(); i++) {
            buttonList.add(
                    new JButton(
                            friendList.get(i).getName() + "(" + friendList.get(i).getNickname() + ")" + friendList.get(i).getAutograph()));
            this.add(buttonList.get(i));
            final String tempName = friendList.get(i).getName();
            final String tempNickname = friendList.get(i).getNickname();
            final String tempAutograph = friendList.get(i).getAutograph();
            buttonList.get(i).addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    DrawFrame talkFrame = new DrawFrame();

                    talkFrame.setTitle(tempName + "(" + tempNickname + ")" + tempAutograph);
                    talkFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    talkFrame.setVisible(true);

                }
            });
        }

        JButton addFriendButton = new JButton("添加好友");
        addFriendButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                AddFriendFrame addFriendFrame1 = new AddFriendFrame();
                addFriendFrame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                AddFriendPanel panel = new AddFriendPanel();
                addFriendFrame1.add(panel);

                final JTextField friendName = new JTextField(10);
                final JTextField friendNickname = new JTextField(10);
                final JTextField friendAutograph = new JTextField(10);
                JLabel nameLabel = new JLabel("好友姓名");
                JLabel nicknameLabel = new JLabel("好友昵称");
                JLabel autographLabel = new JLabel("好友签名");
                JButton button1 = new JButton("保存");
                button1.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        friendList.remove(friendList.size() - 1);
                        friendList.remove(friendList.size() - 1);
                        buttonList.clear();
                        DrawQPanel.friendList.add(
                                new MyFriend(friendName.getText(), friendNickname.getText(), friendAutograph.getText()));


                        DrawQFrame frame = new DrawQFrame();
                        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        frame.setVisible(true);
                    }
                });
                panel.add(friendName);
                panel.add(nameLabel);
                panel.add(friendNickname);
                panel.add(nicknameLabel);
                panel.add(friendAutograph);
                panel.add(autographLabel);

                panel.add(button1);
                addFriendFrame1.setSize(200, 180);
                addFriendFrame1.setLocation(500, 400);
                addFriendFrame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                addFriendFrame1.setVisible(true);
            }
        });
        this.add(addFriendButton);

    }

}
