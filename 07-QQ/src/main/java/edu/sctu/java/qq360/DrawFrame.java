package edu.sctu.java.qq360;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class DrawFrame extends JFrame {

    public DrawFrame() {

        setLocation(100, 100);
        setTitle("360QQ");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        //添加菜单项……
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu menu = new JMenu("菜单");
        menuBar.add(menu);
        JMenuItem settingsItem = new JMenuItem("设置");
        JMenuItem exitItem = new JMenuItem("退出");
        exitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });
        menu.add(settingsItem);
        menu.addSeparator();
        menu.add(exitItem);

        JMenu video = new JMenu("视频");
        menuBar.add(video);
        JMenuItem startVideo = new JMenuItem("发起视频通话", new ImageIcon("computer.ico"));
        JMenuItem mutiVideo = new JMenuItem("多人视频聊天");
        JMenuItem setVideo = new JMenuItem("视频设置");
        JMenuItem testVideo = new JMenuItem("视频测试向导");
        video.add(startVideo);
        video.addSeparator();
        video.add(mutiVideo);
        video.addSeparator();
        video.add(setVideo);
        video.addSeparator();
        video.add(testVideo);


        startVideo.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JFrame littleFrame = new JFrame();
                JOptionPane.showMessageDialog(littleFrame, "正在连接，请稍后……");
                littleFrame.dispose();
            }
        });
        class MyListener implements ActionListener {

            public void actionPerformed(ActionEvent e) {
                JFrame littleFrame = new JFrame();
                JOptionPane.showMessageDialog(littleFrame, "正在连接，请稍后……");
                littleFrame.dispose();
            }
        }
        mutiVideo.addActionListener(new MyListener());
        setVideo.addActionListener(new MyListener());
        testVideo.addActionListener(new MyListener());


        JMenu voice = new JMenu("语音");
        menuBar.add(voice);
        JMenuItem startVoice = new JMenuItem("发起语音通话");
        JMenuItem mutiVoice = new JMenuItem("多人语音通话");
        JMenuItem setVoice = new JMenuItem("语音设置");
        JMenuItem testVoice = new JMenuItem("语音测试向导");
        voice.add(startVoice);
        voice.addSeparator();
        voice.add(mutiVoice);
        voice.addSeparator();
        voice.add(setVoice);
        voice.addSeparator();
        voice.add(testVoice);
        startVoice.addActionListener(new MyListener());
        mutiVoice.addActionListener(new MyListener());
        setVoice.addActionListener(new MyListener());
        testVoice.addActionListener(new MyListener());

        JMenu file = new JMenu("文件");
        menuBar.add(file);
        JMenuItem sendFile = new JMenuItem("发送文件");
        JMenuItem sendMutiFile = new JMenuItem("发送文件夹");
        JMenuItem sendOfflineFile = new JMenuItem("发送离线文件");
        JMenuItem setFile = new JMenuItem("发送设置");
        file.add(sendFile);
        file.addSeparator();
        file.add(sendMutiFile);
        file.addSeparator();
        file.add(sendOfflineFile);
        file.addSeparator();
        file.add(setFile);
        sendFile.addActionListener(new MyListener());
        sendMutiFile.addActionListener(new MyListener());
        sendOfflineFile.addActionListener(new MyListener());
        setFile.addActionListener(new MyListener());

        JMenu SMS = new JMenu("短信");
        menuBar.add(SMS);
        JMenuItem sendSMS = new JMenuItem("给该好友发短信");
        JMenuItem sendMutiSMS = new JMenuItem("群发短信");
        JMenuItem sendOfflineSMS = new JMenuItem("发送离线短信");
        JMenuItem setSMS = new JMenuItem("短信设置");
        SMS.add(sendSMS);
        SMS.addSeparator();
        SMS.add(sendMutiSMS);
        SMS.addSeparator();
        SMS.add(sendOfflineSMS);
        SMS.addSeparator();
        SMS.add(setSMS);
        sendSMS.addActionListener(new MyListener());
        sendMutiSMS.addActionListener(new MyListener());
        sendOfflineSMS.addActionListener(new MyListener());
        setSMS.addActionListener(new MyListener());

        JMenu sue = new JMenu("举报");
        menuBar.add(sue);
        JMenuItem sueFriend = new JMenuItem("举报该好友");
        JMenuItem deleteFriend = new JMenuItem("删除该好友");
        JMenuItem blackListFriend = new JMenuItem("将该好友移至黑名单");
        sue.add(sueFriend);
        sue.addSeparator();
        sue.add(deleteFriend);
        sue.addSeparator();
        sue.add(blackListFriend);
        sueFriend.addActionListener(new MyListener());
        deleteFriend.addActionListener(new MyListener());
        blackListFriend.addActionListener(new MyListener());

        JMenu game = new JMenu("休闲娱乐");
        menuBar.add(game);
        JMenuItem QQLive = new JMenuItem("QQLive网络电视");
        JMenuItem cellphoneQQ = new JMenuItem("手机聊QQ");
        JMenuItem WebGame = new JMenuItem("网络游戏");
        JMenuItem cleverGame = new JMenuItem("益智游戏");
        JMenuItem adorn = new JMenuItem("装扮");
        game.add(QQLive);
        game.addSeparator();
        game.add(cellphoneQQ);
        game.addSeparator();
        game.add(WebGame);
        game.addSeparator();
        game.add(cleverGame);
        game.addSeparator();
        game.add(adorn);
        QQLive.addActionListener(new MyListener());
        cellphoneQQ.addActionListener(new MyListener());
        WebGame.addActionListener(new MyListener());
        cleverGame.addActionListener(new MyListener());
        adorn.addActionListener(new MyListener());


        final DrawPanel pan = new DrawPanel();
        //添加设置对话框
        settingsItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                final JFrame jf = new JFrame("设置");
                jf.setSize(300, 150);
                jf.setLocation(200, 200);
                jf.setResizable(false);
                jf.setVisible(true);

                JPanel settingsPanel = new JPanel();
                final JTextField portTextField = new JTextField(20);
                final JTextField hostTextField = new JTextField(20);
                final JTextField filePortTextField = new JTextField(20);

                settingsPanel.add(new JLabel("聊天端口:"));
                jf.add(settingsPanel);
                settingsPanel.add(portTextField);
                settingsPanel.add(new JLabel("目标地址:"));
                settingsPanel.add(hostTextField);
                settingsPanel.add(new JLabel("文件端口:"));
                settingsPanel.add(filePortTextField);
                JButton okBtn = new JButton("确定");
                JButton cancelBtn = new JButton("取消");
                settingsPanel.add(okBtn);
                settingsPanel.add(cancelBtn);

                okBtn.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent event) {
                        try {
                            int cat1 = Integer.parseInt(portTextField.getText());
                            pan.setCat1(cat1);//设置对话端口号
                            int cat2 = Integer.parseInt(filePortTextField.getText());
                            pan.setCat2(cat2);//设置文件传输端口号
                            String s = hostTextField.getText();
                            pan.setIP(s);//设置IP地址
                            pan.ls();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        jf.dispose();//关闭对话框
                    }
                });
                cancelBtn.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent event) {
                        jf.dispose();
                    }
                });
            }
        });

        add(pan);
        pack();
    }

    public static final int DEFAULT_WIDTH = 400;
    public static final int DEFAULT_HEIGHT = 400;
}