package edu.sctu.java.qq360;

public class MyFriend {
    private String name;
    private String nickname;
    private String autograph;


    public MyFriend(String name, String nickname, String autograph) {
        this.name = name;
        this.nickname = nickname;
        this.autograph = autograph;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAutograph() {
        return autograph;
    }

    public void setAutograph(String autograph) {
        this.autograph = autograph;
    }
}

