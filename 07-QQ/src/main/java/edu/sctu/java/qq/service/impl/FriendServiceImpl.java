package edu.sctu.java.qq.service.impl;

import edu.sctu.java.qq.bean.Friend;
import edu.sctu.java.qq.service.FriendService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 27/12/2016.
 */
public class FriendServiceImpl implements FriendService {
    @Override
    public List<Friend> getAllFriends(int userId) {


        List<Friend> list = new ArrayList<Friend>();

        list.add(new Friend("chen"));
        list.add(new Friend("zhen"));

        return list;
    }
}
