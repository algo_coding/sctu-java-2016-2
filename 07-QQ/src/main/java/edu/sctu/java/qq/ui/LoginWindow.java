package edu.sctu.java.qq.ui;

import edu.sctu.java.qq.service.UserService;
import edu.sctu.java.qq.service.impl.UserServiceImpl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by apple on 27/12/2016.
 */
public class LoginWindow extends JFrame implements ActionListener {

    private JLabel titleLbl;
    private JTextField usernameTF;
    private JTextField passwordTF;
    private JButton loginBtn;
    private JButton registerBtn;

    public LoginWindow() {

        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        titleLbl = new JLabel("欢迎进入Java课程学习平台!");
        getContentPane().add(titleLbl);

        usernameTF = new JTextField();
        usernameTF.setText("用户名");
        getContentPane().add(usernameTF);

        passwordTF = new JTextField();
        passwordTF.setText("密码");
        getContentPane().add(passwordTF);

        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));
        getContentPane().add(btnPanel);

        loginBtn = new JButton("登录");
        loginBtn.addActionListener(this);
        btnPanel.add(loginBtn);

        registerBtn = new JButton("注册");
        registerBtn.addActionListener(this);
        btnPanel.add(registerBtn);

        setBounds(100, 100, 500, 150);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == loginBtn) {

            String username = usernameTF.getText();
            String password = passwordTF.getText();

            UserService userService = new UserServiceImpl();
            if (userService.login(username, password)) {
                dispose();
                new MainWindow();
            } else {
                JOptionPane.showMessageDialog(null, "用户名或密码错误!", "信息", JOptionPane.ERROR_MESSAGE);
            }
        }

        if (e.getSource() == registerBtn) {
            new RegisterWindow();
        }
    }


}
