package edu.sctu.java.qq.service;

import edu.sctu.java.qq.bean.Friend;

import java.util.List;

/**
 * Created by apple on 27/12/2016.
 */
public interface FriendService {

    public List<Friend> getAllFriends(int userId);

}
