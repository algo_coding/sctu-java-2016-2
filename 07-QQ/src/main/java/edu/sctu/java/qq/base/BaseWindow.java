package edu.sctu.java.qq.base;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by apple on 27/12/2016.
 */
public class BaseWindow extends JFrame implements ActionListener {



    public BaseWindow() {


        setBounds(100, 100, 500, 500);
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setVisible(true);

    }


    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
