package edu.sctu.java.qq.bean;

/**
 * Created by apple on 27/12/2016.
 */
public class Friend {
    private String nickname;

    public Friend(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
