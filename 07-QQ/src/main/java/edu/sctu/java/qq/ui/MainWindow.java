package edu.sctu.java.qq.ui;

import edu.sctu.java.qq.base.BaseWindow;
import edu.sctu.java.qq.service.impl.FriendServiceImpl;
import edu.sctu.java.qq.ui.panel.FriendTableModel;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by apple on 27/12/2016.
 */
public class MainWindow extends BaseWindow {
    public MainWindow() {
        super();

        setTitle("主界面");
        setBounds(100, 100, 300, 500);

        initJMenuBar();


        TableModel dm = new FriendTableModel(new FriendServiceImpl().getAllFriends(0));
        JTable table = new JTable(dm);
        table.setTableHeader(null);
        table.setRowHeight(40);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
    }

    private void initJMenuBar() {


        JMenuBar bar = new JMenuBar();
        setJMenuBar(bar);

        initSettingsMenu(bar);
        initHelpMenu(bar);

    }


    private void initHelpMenu(JMenuBar bar) {

        JMenu helpMenu = new JMenu("帮助");
        bar.add(helpMenu);

        JMenuItem aboutItem = new JMenuItem("关于");
        helpMenu.add(aboutItem);

        aboutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JOptionPane.showMessageDialog(null, "这是EliteQQ 第一版,感谢你的使用!", "信息", JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }

    private void initSettingsMenu(JMenuBar bar) {
        JMenu settingsMenu = new JMenu("设置");
        bar.add(settingsMenu);
        JMenuItem exitItem = new JMenuItem("退出");
        settingsMenu.add(exitItem);
        exitItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

}
