package edu.sctu.java.qq.ui.panel;

import edu.sctu.java.qq.bean.Friend;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;

/**
 * Created by apple on 27/12/2016.
 */
public class FriendTableModel implements TableModel {
    private List<Friend> friendList;

    public FriendTableModel(List<Friend> friendList) {
        this.friendList = friendList;
    }

    @Override
    public int getRowCount() {
        return friendList.size();
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return "昵称";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Friend friend = friendList.get(rowIndex);

        if (columnIndex == 0) {
            return friend.getNickname();
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {

    }

    @Override
    public void removeTableModelListener(TableModelListener l) {

    }
}
