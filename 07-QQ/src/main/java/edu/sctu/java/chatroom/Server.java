package edu.sctu.java.chatroom;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by apple on 27/12/2016.
 */
public class Server {

    public static final int PORT = 18080;

    private ServerSocket serverSocket;
    private List<Socket> clientSockets;

    public Server() throws IOException {

        serverSocket = new ServerSocket(PORT);
        clientSockets = new ArrayList<Socket>();

    }

    public void run() throws IOException {

        while (true) {

            Socket clientSocket = serverSocket.accept();
            if (clientSocket != null) {

                System.out.println("new client: " + clientSocket.getInetAddress());
                clientSockets.add(clientSocket);
                new ServerThread(clientSocket).run();
            }

        }

    }

    public static void main(String[] args) {

        try {
            new Server().run();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class ServerThread implements Runnable {

        private Socket socket;
        private BufferedReader reader;

        public ServerThread(Socket socket) throws IOException {
            this.socket = socket;
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }

        @Override
        public void run() {

            try {
                while (true) {
                    String line = reader.readLine();

                    sendToAll(line);

                }


            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        private void sendToAll(String message) throws IOException {

            for (Socket s : clientSockets) {

                PrintWriter writer = new PrintWriter(s.getOutputStream());
                writer.println(String.format("[%s] %s", new Date(), message));
                writer.flush();
            }

        }
    }
}
