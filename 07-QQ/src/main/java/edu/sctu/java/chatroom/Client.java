package edu.sctu.java.chatroom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;

/**
 * Created by apple on 27/12/2016.
 */
public class Client extends JFrame implements ActionListener {


    private JTextArea contentArea;
    private JTextField messageField;
    private JButton sendBtn;

    private BufferedReader reader;
    private PrintWriter writer;
    private Socket socket;

    public Client() throws IOException {

        initSocket();
        initWidget();

        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);

        new ReaderThread().run();
    }

    private void initSocket() throws IOException {
        socket = new Socket("127.0.0.1", Server.PORT);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

    }

    private void initWidget() {

        contentArea = new JTextArea(20, 40);
        contentArea.setEditable(false);
        contentArea.setFont(new Font("宋体", Font.BOLD, 24));
        JScrollPane scrollPane = new JScrollPane(contentArea);
        add(scrollPane, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel();
        add(bottomPanel, BorderLayout.SOUTH);

        sendBtn = new JButton("发送");
        sendBtn.addActionListener(this);
        messageField = new JTextField(28);
        bottomPanel.add(messageField);
        bottomPanel.add(sendBtn);

    }

    public static void main(String[] args) {

        try {
            new Client();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == sendBtn) {

            String message = messageField.getText();
            writer.println(message);
            writer.flush();

        }
    }

    public class ReaderThread implements Runnable {

        @Override
        public void run() {

            while (true) {
                try {
                    String msg = reader.readLine();
                    contentArea.append(msg + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
