package edu.sctu.java.lab_08.lianxitwo;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by dell on 2016/12/26.
 */
public class Server {
    public static void main(String[] args) {
        ServerSocket server = null;
        ServerThread thread;
        Socket you = null;
        while (true) {
            try {
                server = new ServerSocket(4331);
            } catch (IOException el) {
                System.out.println("正在监听");
            }
            try {
                you = server.accept();
                System.out.println("客户的地址：" + you.getInetAddress());
            } catch (IOException e) {
                System.out.println("正在等待客户");
            }
            if (you != null) {
                new ServerThread(you).start();
            } else {
                continue;
            }
        }
    }
}

class ServerThread extends Thread {
    Socket socket;
    ObjectInputStream in = null;
    ObjectOutputStream out = null;
    String s = null;

    ServerThread(Socket t) {
        socket = t;
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
        }
    }

    public void run() {
        TextArea text = new TextArea("你好，我是服务器", 12, 12);
        try {
            out.writeObject(text);
        } catch (IOException e) {
            System.out.println("客户离开");
        }
    }
}
