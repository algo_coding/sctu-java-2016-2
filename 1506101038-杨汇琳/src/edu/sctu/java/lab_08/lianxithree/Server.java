package edu.sctu.java.lab_08.lianxithree;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by dell on 2016/12/26.
 */

public class Server{
    public static void main(String args[])
    {
        DatagramPacket pack=null;
        DatagramSocket mailReceive=null;
        ServerThread thread;
        byte b[]=new byte[8192];
        InetAddress address=null;
        pack=new DatagramPacket(b,b.length);
        while(true)
        {   try{
            mailReceive=new DatagramSocket(1234);//创建在端口1234负责收取数据包的
            //DatagramSocket对象。
        }
        catch(IOException e1)
        {
            System.out.println("正在等待");
        }
            try{
                mailReceive.receive(pack);
                address=pack.getAddress(); //pack返回InetAddress对象。
                System.out.println("客户的地址:"+address);
            }
            catch (IOException e) { }
            if(address!=null)
            {
                new ServerThread(address).start();
            }
            else
            {   continue;
            }
        }
    }
}
class ServerThread extends Thread
{
    InetAddress address;
    DataOutputStream out=null;
    DataInputStream in=null;
    String s=null;
    ServerThread(InetAddress address)
    {  this.address=address;
    }
    public void run()
    {
        FileInputStream in;
        byte b[]=new byte[8192];
        try{
            in=new  FileInputStream ("a.jpg");
            int n=-1;
            while((n=in.read(b))!=-1)
            {  DatagramPacket data=new DatagramPacket(b,n,address,5678);//创建数据包，目标地址和端口分别是address和5678，其中的数据为数组b中的前n个字节
                DatagramSocket mailSend=new DatagramSocket(); //创建发送数据的DatagramSocket对象
                mailSend.send(data);                   // mailSend发送数据data
            }
            in.close();
            byte end[]="end".getBytes();
            DatagramPacket data=new DatagramPacket(end,end.length,address,5678); //创建数据包，目标地址和端口分别是address和5678，其中的数据为数组end中的全部字节
            DatagramSocket mailSend=new DatagramSocket();//创建负责发送数据的DatagramSocket对象
            mailSend.send(data);                     // mailSend发送数据data
        }
        catch(Exception e){}
    }
}
