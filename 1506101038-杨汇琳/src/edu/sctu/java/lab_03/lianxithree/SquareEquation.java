package edu.sctu.java.lab_03.lianxithree;

import java.util.Scanner;

/**
 * Created by yanghuilin on 2016/11/4.
 */
public class SquareEquation {
    double a, b, c;
    double root1, root2;
    boolean t;

    public void SquareEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
        if (a != 0) {
            t = true;
        } else {
            t = false;
        }
    }

    public void getRoots() {
        if (t) {
            System.out.println("是一元二次方程");
            double disk = b * b - 4 * a * c;
            if (disk >= 0) {
                root1 = (-b + Math.sqrt(disk)) / (2 * a);
                root2 = (-b - Math.sqrt(disk)) / (2 * a);
                System.out.println("方程的根为：" + root1 + "," + root2);
            } else {
                System.out.println("方程没有实根。");
            }
        } else {
            System.out.println("不是一元二次方程");
        }
    }


    public static void main(String[] args) {
        System.out.println("依次输入一元二次方程的3个系数a、b、c：");
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();
        SquareEquation squareEquation = new SquareEquation();
        squareEquation.SquareEquation(a, b, c);
        squareEquation.getRoots();

    }
}

