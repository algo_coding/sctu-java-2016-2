package edu.sctu.java.lab_03.lianxione;

import java.util.Scanner;

/**
 * Created by yanghuilin on 2016/11/3.
 */
public class Main {
    public static void main(String[] args) {
//        三角形
        System.out.println("输入三角形的三边：");
        Scanner scanner = new Scanner(System.in);
        double sideA = scanner.nextDouble();
        double sideB = scanner.nextDouble();
        double sideC = scanner.nextDouble();
        Trangle trangle = new Trangle();
        trangle.Panduan(sideA, sideB, sideC);
//        梯形
        System.out.println("输入梯形的上底、下底和高：");
        double topline = scanner.nextDouble();
        double baseline = scanner.nextDouble();
        double height = scanner.nextDouble();
        Leder leder = new Leder();
        leder.LederLederArea(topline, baseline, height);
//        圆
        System.out.println("输入圆的半径：");
        double radius= scanner.nextDouble();
        Circle circle=new Circle();
        circle.CircleLengthAndArea(radius);
    }
}
