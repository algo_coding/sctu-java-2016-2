package edu.sctu.java.lab_03.lianxione;

import java.util.Scanner;

/**
 * Created by yanghuilin on 2016/11/3.
 */
public class Trangle {
    double sideA, sideB, sideC;

    public double getSideC() {
        return sideC;
    }
    public double getSideB() {
        return sideB;
    }
    public double getSideA() {
        return sideA;
    }
    protected double LengthAndArea(double A, double B, double C) {
        double area, length;
        length = A + B + C;
        System.out.println("三角形的周长为：" + length);
        area = Math.sqrt((A + B + C) * (A + B - C) * (A + C - B) * (B + C - A)) / 4;
        System.out.println("三角形的面积为：" + area);
        return area;
    }


    protected boolean Panduan(double sideA, double sideB, double sideC) {
        Scanner scanner = new Scanner(System.in);
        boolean ss = true;
        while (sideA + sideB <= sideC || sideA + sideC <= sideB || sideC + sideB <= sideA) {
            ss = false;
            System.out.println("不能构成三角形，请重新输入三边：");
            sideA = scanner.nextDouble();
            sideB = scanner.nextDouble();
            sideC = scanner.nextDouble();
        }
        LengthAndArea(sideA, sideB, sideC);
        return ss;
    }
}
