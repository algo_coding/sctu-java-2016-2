package edu.sctu.java.lab_03.lianxitwo;

/**
 * Created by yanghuilin on 2016/11/4.
 */
public class Example {
    public static void main(String[] args) {
        new A().b = 100;
        new A().inputB();
//        A testa = new A();
//        testa.b = 100;
//        testa.inputB();
        A cat = new A();
        A dog = new A();
        cat.setA(200);
        cat.setB(400);
        dog.setA(150);
        dog.setB(300);
        cat.inputA();
        cat.inputB();
        dog.inputA();
        dog.inputB();
    }
}

class A {
    float a;
    static float b;

    void setA(float a) {
        this.a = a;
    }

    void setB(float b) {
        this.b = b;
    }

    float getA() {
        return a;
    }

    float getB() {
        return b;
    }

    void inputA() {
        System.out.println(a);
//        System.out.println(a +b);
    }

    static void inputB() {
        System.out.println(b);
//        System.out.println(a+b);
    }
}