package edu.sctu.java.lab_05.lianxithree;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by yanghuilin on 2016/11/22.
 * 计算1+2+3…的前999999999项的和
 */
public class Sum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("求1-n的和，输入数字n：");
        BigInteger n = scanner.nextBigInteger();
        BigInteger a = new BigInteger("1");
        BigInteger sum = new BigInteger("0");
        for (BigInteger i = a; i.compareTo(n) <= 0; i = i.add(a)) {
            sum = sum.add(i);
        }
        System.out.println("1+2+3+...+"+n+"的和为："+sum);
    }
}
