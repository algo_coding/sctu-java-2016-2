package edu.sctu.java.lab_05.lianxithree;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by yanghuilin on 2016/11/22.
 * 计算大整数的阶乘
 */
public class Factorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入数字，以计算该数字的阶乘：");
        BigInteger num = scanner.nextBigInteger();
        BigInteger a = new BigInteger("0");
        BigInteger b = new BigInteger("1");
        BigInteger re = new BigInteger("1");
        for (BigInteger i = num; i.compareTo(a) > 0; i = i.subtract(b)) {
            re = re.multiply(i);
        }
        System.out.println(re);
    }
}
