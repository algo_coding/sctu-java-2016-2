package edu.sctu.java.lab_05.lianxithree;

import java.math.BigInteger;

/**
 * Created by yanghuilin on 2016/11/17.
 * 编写一个Java应用程序，计算两个大整数的和、差、积和商，
 * 并计算一个大整数的因子个数（因子中不包括1和大整数本身）
 */
public class HandleLargeInteger {
    public static void main(String[] args) {
        BigInteger n1 = new BigInteger("1357908642468097531");
        BigInteger n2 = new BigInteger("1234567890987654321");
        BigInteger result;
        result = n1.add(n2);
        System.out.println("和：" + result.toString());
        result = n1.subtract(n2);
        System.out.println("差：" + result.toString());
        result = n1.multiply(n2);
        System.out.println("积：" + result.toString());
        result = n1.remainder(n2);
        System.out.println("商：" + result.toString());
        BigInteger m = new BigInteger("1968957");
        BigInteger COUNT = new BigInteger("0");
        BigInteger ONE = new BigInteger("1");
        BigInteger TWO = new BigInteger("2");
        System.out.println(m.toString() + "的因子有:");
        for (BigInteger i = TWO; i.compareTo(m) < 0; i = i.add(ONE)) {
            if ((n1.remainder(i).compareTo(BigInteger.ZERO)) == 0) {
                COUNT = COUNT.add(ONE);
                System.out.print("  " + i.toString());
            }
        }
        System.out.println("");
        System.out.println(m.toString() + "一共有" + COUNT.toString() + "个因子");
    }
}
