package edu.sctu.java.lab_05.lianxione;

import java.util.Scanner;

/**
 * Created by yanghuiilin on 2016/11/17.
 * 编写一个Java应用程序，判断两个字符串是否相同，判断字符串的前缀、后缀是否和某个字符串相同，
 * 按字典顺序比较两个字符串的大小关系，检索字符串，创建字符串，
 * 将数字型字符串转换为数字，将字符串存放到数组中，用字符数组创建字符串。
 */
public class ComparisonString {
    public static void main(String[] args) {

//        判断字符串是否相同

        String s1 = new String("You are a student.");
        String s2 = new String("You are a girl.");
        if (s1.equals(s2)) {
            System.out.println("s1与s2相同");
        } else
            System.out.println("s1与s2不同");

//        判断前缀

        String s3 = new String("22030219851022024");
        if (s3.startsWith("220302")) {
            System.out.println("吉林省的身份证");
        } else
            System.out.println("不是吉林省的身份证");

//        字典中的排序比较

        String s4 = new String("你");
        String s5 = new String("我");
        if (s4.compareTo(s5) > 0) {
            System.out.println("在字典中，“" + s4 + "”比“" + s5 + "”靠后。");
        } else
            System.out.println("在字典中，“" + s4 + "”比“" + s5 + "”靠前。");

//        获取字符串的位置

        int position = 0;
        String path = "f:\\java\\jsp\\A.java";
        position = path.lastIndexOf("\\") + 1;
        System.out.println("f:\\java\\jsp\\A.java中最后出现斜线的位置是：" + position);
        String filename = path.substring(path.lastIndexOf("\\") + 1);
        System.out.println("f:\\java\\jsp\\A.java中含有的文件名：" + filename);

//        1.3

//        String filename2 = path.substring(path.indexOf("\\js")+1,path.indexOf("\\A"));
//        System.out.println(filename2);

//        转换字符类型

//        String s6 = new String("1a12b");
        String s6 = new String("100");
        String s7 = new String("123.456");

        int n1 = Integer.valueOf(s6);
        double n2 = Double.parseDouble(s7);
        double m = n1 + n2;
        System.out.println(n1 + "+" + n2 + "=" + m);
        String s8 = String.valueOf(m);
        position = s8.indexOf(".");
        String temp = s8.substring(position + 1);
        System.out.println("数字" + m + "有" + temp.length() + "为小数。");
//        1.6
        String ID[] = s8.split("\\.");
        System.out.println("整数部分为"+ID[0]);
        System.out.println("小数部分为"+ID[1]);


        String s9 = new String("ABCDEF");
        char a[] = s9.toCharArray();
        for (int i = a.length - 1; i >= 0; i--) {
            System.out.print(" " + a[i]);
        }

//        1.2
//
//        String b = "";
//        for (int j = 0; j <= 2; j++) {
//            b = b + a[j];
//        }
//        System.out.println(b);



        String str1 = new String("ABCABC"),
                str2 = null,
                str3 = null,
                str4 = null;
        str2 = str1.replaceAll("A", "First");
        str3 = str2.replaceAll("B", "Second");
        str4 = str3.replaceAll("C", "Third");
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
        System.out.println(str4);

//        进制的转换
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入需要进行进制转换的数字i和转化的p进制：");
        int i = scanner.nextInt();
        int p = scanner.nextInt();
        String er=Long.toBinaryString(Long.parseLong(String.valueOf(i)));
        String ba=Long.toOctalString(Long.parseLong(String.valueOf(i)));
        String shiliu=Long.toHexString(Long.parseLong(String.valueOf(i)));
        String fi = Long.toString(Long.parseLong(String.valueOf(i)), p);
        System.out.println(i+"由十进制转化为2进制："+er);
        System.out.println(i+"由十进制转化为8进制："+ba);
        System.out.println(i+"由十进制转化为16进制："+shiliu);
        System.out.println(i+"由十进制转化为"+p+"进制："+fi);

    }
}
