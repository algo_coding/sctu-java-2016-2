package edu.sctu.java.lab_05.lianxitwo;

import javax.swing.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by yanghuilin on 2016/11/17.
 * 编写一个Java应用程序，用户从输入对话框输入了两个日期，
 * 程序将判断两个日期的大小关系，以及两个日期之间的间隔天数。
 */
public class ComparisonDate {
    public static void main(String[] args) {
        String str=JOptionPane.showInputDialog("输入第一个日期的年份:");
        int yearOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份:");
        int monthOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期:");

        str=JOptionPane.showInputDialog("请输入该天的小时");
        int hourOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("请输入该小时的分钟");
        int mumiteOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("请输该分钟的秒");
        int secondOne=Integer.parseInt(str);

        int dayOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入第二个日期的年份:");
        int yearTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份:");
        int monthTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期:");
        int dayTwo=Integer.parseInt(str);

        str=JOptionPane.showInputDialog("请输入该天的小时");
        int hourTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("请输入该小时的分钟");
        int mumiteTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("请输该分钟的秒");
        int secondTwo=Integer.parseInt(str);

        Calendar calendar=Calendar.getInstance();
//        calendar.set(yearOne, monthOne, dayOne);
        calendar.set(yearOne, monthOne, dayOne,hourOne,monthOne,secondOne);
        long timeOne=calendar.getTimeInMillis();
        calendar.set(yearTwo, monthTwo, dayTwo);
        long timeTwo=calendar.getTimeInMillis();
        Date date1=new Date(timeOne);
        Date date2=new Date(timeTwo);
        if(date2.equals(date1))
        {
            System.out.println("两个日期的年、月、日完全相同");
        }
        else if(date2.after(date1))
        {
            System.out.println("您输入的第二个日期大于第一个日期");
        }
        else if(date2.before(date1))
        {
            System.out.println("您输入的第二个日期小于第一个日期");
        }
        long days=(date2.getTime()-date1.getTime())/(1000*60*60*24);
        System.out.println(yearOne+"年"+monthOne+"月"+dayOne+"日和"
                +yearTwo+"年"+monthTwo+"月"+dayTwo+"相隔"+days+"天");
        System.out.println("请输入本金：");
        Scanner scanner = new Scanner(System.in);
        double principal = scanner.nextDouble();
        double interest = (date2.getTime()-date1.getTime())/(1000*60*60*24)/365*principal*0.005;
        System.out.println("本金为"+principal+"时，利息有"+interest);
    }
}
