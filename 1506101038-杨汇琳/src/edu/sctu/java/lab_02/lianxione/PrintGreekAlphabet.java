package edu.sctu.java.lab_02.lianxione;

/**
 * Created by yanghuilin on 2016/10/31.
 */
public class PrintGreekAlphabet {
    public static void main(String[] args) {
        int firstPosition = 0;
        int lastPosition = 0;
        char chfirst = 'α';
        char chlast = 'ω';
        System.out.print(" " + chfirst);
        while (chfirst != chlast) {
            chfirst++;
            System.out.print(" " + chfirst);
        }
    }
}
