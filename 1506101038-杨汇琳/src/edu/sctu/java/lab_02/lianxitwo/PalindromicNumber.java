package edu.sctu.java.lab_02.lianxitwo;

import java.util.Scanner;

/**
 * Created by yanghuilin on 2016/10/31.
 */
public class PalindromicNumber {
    public static void main(String[] args) {
        System.out.println("Please enter 1-99999 number:");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        char ch[] = s.toCharArray();
        for (int i = 0; i < s.length() / 2; i++) {
            if (ch[i] != ch[ch.length - (i + 1)]) {
                System.out.println("The number is not palindromic number.");
                break;
            }
            if (i >= s.length() / 2 - 1) {
                System.out.println("The number is palindromic number.");
            }
        }
    }
}
