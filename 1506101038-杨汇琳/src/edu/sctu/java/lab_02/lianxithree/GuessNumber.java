package edu.sctu.java.lab_02.lianxithree;


import java.util.Random;
import java.util.Scanner;

/**
 * Created by yanghuilin on 2016/10/31.
 */
public class GuessNumber {
    public static void main(String[] args) {
        System.out.println("Game go：");
        try {
            System.out.println("Being generated in the data ......");
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int realNnumber = new Random().nextInt(100);
        System.out.println("Please enter your number:");
        Scanner scanner = new Scanner(System.in);
        int Yournum = scanner.nextInt();
        boolean s = true;
        while (s) {
            if (Yournum < realNnumber) {
                System.out.println("It's smaller.Please enter your number again:");
                Yournum = scanner.nextInt();
            } else if (Yournum > realNnumber) {
                System.out.println("It's bigger.Please enter your number again:");
                Yournum = scanner.nextInt();
            } else {
                System.out.println("Congratulate!It's right.");
                s = false;
            }
        }
    }
}
