package edu.sctu.java.lab_06.lianxithree;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Created by yanghuilin on 2016/11/30.
 */
public class ReadZipFile {
//    public static void main(String[] args) {
//        File f = new File("F:\\大二上学期\\JAVA\\实验五\\book.zip");
//        File dir = new File("F:\\大二上学期\\JAVA\\实验五\\Book");
//        byte b[] = new byte[100];
//        dir.mkdir();
//        try {
//            ZipInputStream in = new ZipInputStream(new FileInputStream(f));
//            ZipEntry zipEntry = null;
//            while ((zipEntry = in.getNextEntry())!= null) {
//                File file = new File(dir, zipEntry.getName());
//                FileOutputStream out = new FileOutputStream(file);
//                int n = -1;
//                System.out.println(file.getAbsolutePath() + "的内容");
//                while ((n = in.read(b, 0, 100)) != -1) {
//                    String str = new String(b, 0, n);
//                    System.out.println(str);
//                    out.write(b, 0, n);
//                }
//                out.close();
//            }
//            in.close();
//        } catch (Exception ee) {
//            System.out.println(ee);
//        }
//    }

    public static void main(String[] args) throws Exception {
        try {
            readZipFile("G:\\dev\\sctu-java-2016-2\\Book.zip");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void readZipFile(String file) throws Exception {
        ZipFile zf = new ZipFile(file);
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        ZipInputStream zin = new ZipInputStream(in);
        ZipEntry ze;
        while ((ze = zin.getNextEntry()) != null) {
            if (ze.isDirectory()) {
            } else {
                System.err.println("file - " + ze.getName() + " : "
                        + ze.getSize() + " bytes");
                long size = ze.getSize();
                if (size > 0) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(zf.getInputStream(ze)));
                    String line;
                    while ((line = br.readLine()) != null) {
                        System.out.println(line);
                    }
                    br.close();
                }
                System.out.println();
            }
        }
        zin.closeEntry();
    }
}

