package edu.sctu.java.lab_06.lianxitwo;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.Vector;

/**
 * Created by yanghuilin on 2016/11/30.
 */
public class WordStatistic {
    Vector allWords, noSameWord;

    WordStatistic() {
        allWords = new Vector();
        noSameWord = new Vector();
    }

    public void wordStatistic(File file) {
        try {
            RandomAccessFile inOne = new RandomAccessFile(file, "rw");//创建指向文件file的inOne 的对象
            RandomAccessFile inTwo = new RandomAccessFile(file, "rw");//创建指向文件file的inTwo 的对象
            long wordStarPosition = 0, wordEndPosition = 0;
            long length = inOne.length();
            int flag = 1;
            int c = -1;
            for (int k = 0; k <= length; k++) {
                c = inOne.read();       // inOne调用read()方法
                boolean boo = (c <= 'Z' && c >= 'A') || (c <= 'z' && c >= 'a');
                if (boo) {
                    if (flag == 1) {
                        wordStarPosition = inOne.getFilePointer() - 1;
                        flag = 0;
                    }
                } else {
                    if (flag == 0) {
                        if (c == -1)
                            wordEndPosition = inOne.getFilePointer();
                        else
                            wordEndPosition = inOne.getFilePointer() - 1;
                        inTwo.seek(wordStarPosition); // inTwo调用seek方法将读写位置移动到wordStarPostion
                        byte cc[] = new byte[(int) wordEndPosition - (int) wordStarPosition];// inTwo调用readFully(byte a)方法，向a传递cc
                        String word = new String(cc);
                        allWords.add(word);
                        if (!(noSameWord.contains(word)))
                            noSameWord.add(word);

                    }
                    flag = 1;
                }
            }
            inOne.close();
            inTwo.close();
        } catch (Exception e) {
        }
    }

    public Vector getAllWords() {
        return allWords;
    }

    public Vector getNoSameWord() {
        return noSameWord;
    }

}
