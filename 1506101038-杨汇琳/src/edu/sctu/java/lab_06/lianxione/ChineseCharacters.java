package edu.sctu.java.lab_06.lianxione;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

/**
 * Created by yanghuilin on 2016/11/30.
 */
public class ChineseCharacters {
    public StringBuffer getChineseCharacters(File file) {
        StringBuffer hanzi = new StringBuffer();
        try {
            FileReader inOne = new FileReader(file);//创建指向文件f的inOne 的对象
            BufferedReader inTwo = new BufferedReader(inOne);//创建指向文件inOne的inTwo 的对象
            String s = null;
            int i = 0;
            while ((s = inTwo.readLine()) != null) { //inTwo读取一行
                StringTokenizer tokenizer = new StringTokenizer(s, ",'\n'");
                while (tokenizer.hasMoreTokens()) {
                    hanzi.append(tokenizer.nextToken());
                }
            }
        } catch (Exception e) {
        }
        return hanzi;
    }

    public StringBuffer getChinesecharacters(File file) {
        return null;
    }
}
