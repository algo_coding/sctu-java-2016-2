package edu.sctu.java.chapter_08;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by yanghuilin on 2017/1/7.
 *编写一个程序实现一下功能：从键盘输入一行文字写入到一个文件中。
 */
public class firstQuestion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println(text);
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("D:\\inputText.txt"));
            bw.write(text);
            bw.flush();
            bw.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
