package edu.sctu.java.chapter_08;

import java.io.*;

/**
 * Created by yanghuilin on 2017/1/7.
 * 编写一个程序实现一下功能：将1~100内的奇数写入二进制文件，
 * 然后从该二进制文件中逐一读取奇数并以每行10个数的方式输出到显示屏。
 */
public class thirdQuestion {
    public static void main(String[] args) {
        String fname = "binary.txt";
        try {
            int i;
            FileOutputStream out = new FileOutputStream(fname);
            DataOutputStream wf = new DataOutputStream(out);
            for (i = 1; i <= 100; i++) {
                if (i % 2 != 0) {
                    wf.writeInt(i);
                }
            }
            wf.close();     //关闭输出流
            FileInputStream input = new FileInputStream(fname);
            DataInputStream rf = new DataInputStream(input);
            i = 1;
            while (i <= 10) {
                i = i + 1;
                System.out.print(rf.readInt() + "  ");
            }
            System.out.println();
            i = 11;
            while (i <= 20) {
                i = i + 1;
                System.out.print(rf.readInt() + "  ");
            }
            System.out.println();
            i = 21;
            while (i <= 30) {
                i = i + 1;
                System.out.print(rf.readInt() + "  ");
            }
            System.out.println();
            i = 31;
            while (i <= 40) {
                i = i + 1;
                System.out.print(rf.readInt() + "  ");
            }
            System.out.println();
            i = 41;
            while (i <= 50) {
                i = i + 1;
                System.out.print(rf.readInt() + "  ");
            }
            rf.close();     //关闭输入流
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
