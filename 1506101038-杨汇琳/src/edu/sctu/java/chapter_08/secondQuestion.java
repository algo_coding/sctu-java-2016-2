package edu.sctu.java.chapter_08;

import java.io.*;

/**
 * Created by yanghuilin on 2017/1/7.
 * 编写一个程序实现一下功能：读取文本文件中的内容并输出到显示屏
 */
public class secondQuestion {
    public static void main(String[] args) {
        File filePath = new File("D:\\inputText.txt");
        BufferedReader br;
        String s = null;
        try{
            br = new BufferedReader(new FileReader(filePath));
            try {
                while ((s = br.readLine()) != null) {
                System.out.println(s + "\r\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            br.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}