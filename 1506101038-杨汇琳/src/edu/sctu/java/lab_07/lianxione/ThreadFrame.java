package edu.sctu.java.lab_07.lianxione;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.nio.file.WatchEvent;

/**
 * Created by yanghuilin on 2016/12/15.
 */
public class ThreadFrame extends Frame implements ActionListener {

    Label wordLable;
    Button button;
    TextField inputText, scoreText;
    WordThread giveWord;
    int score = 0;

    ThreadFrame() {
        wordLable = new Label(" ", Label.CENTER);
        wordLable.setFont(new Font("", Font.BOLD, 72));
        button = new Button("开始");
        inputText = new TextField(3);
        scoreText = new TextField(5);
        scoreText.setEditable(false);
        giveWord = new WordThread(wordLable);
        button.addActionListener(this);
        inputText.addActionListener(this);
        add(button, BorderLayout.NORTH);
        add(wordLable, BorderLayout.CENTER);
        Panel southP = new Panel();
        southP.add(new Label("输入标签所显示的汉字后回车:"));
        southP.add(inputText);
        southP.add(scoreText);
        add(southP, BorderLayout.SOUTH);
        setBounds(100, 100, 350, 180);
        setVisible(true);
        validate();
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WatchEvent e) {
                System.exit(0);
            }
        });
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button) {
            if (!(giveWord.isAlive())) {
                giveWord = new WordThread(wordLable);
            }
            try {
                giveWord.start();
            }
            catch (Exception exe){}
        }
        else if (e.getSource() == inputText){
            if (inputText.getText().equals(wordLable.getText())){
                score++;
            }
            scoreText.setText("得分"+score);
            inputText.setText(null);
        }
    }
}
