package edu.sctu.java.chapter_02.chapter2_12;

/**
 * Created by dell on 2016/9/18.
 */
public class WhileSum {
    public static void main(String[] args) {
        int n=1;
        int sum=0;
        while (n<=10){
            sum=sum+n;
            n++;
        }
        System.out.println("sum="+sum);
    }
}
