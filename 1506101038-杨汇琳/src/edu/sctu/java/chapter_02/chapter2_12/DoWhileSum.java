package edu.sctu.java.chapter_02.chapter2_12;

/**
 * Created by dell on 2016/9/18.
 */
public class DoWhileSum {
    public static void main(String[] args) {
        int n=1;
        int sum=0;
        do {
            sum=sum+n;
        }
        while (n<=10);
        System.out.println("sum="+sum);
    }
}
