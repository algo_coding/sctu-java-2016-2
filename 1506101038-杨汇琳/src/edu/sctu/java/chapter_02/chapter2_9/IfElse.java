package edu.sctu.java.chapter_02.chapter2_9;

import java.util.Scanner;

/**
 * Created by dell on 2016/9/18.
 */
public class IfElse {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.println("input a grade:");
        int x=scan.nextInt();
        if (x==1)
            System.out.println("不及格！");
        else {
            if (x==2)
                System.out.println("及格！");
            else {
                if (x==3)
                    System.out.println("中等！");
                else {
                    if(x==4)
                        System.out.println("良好！");
                    else {
                        if (x==5)
                            System.out.println("优秀！");
                        else
                            System.out.println("输入范围有问题！");
                    }
                }
            }
        }
    }
}
