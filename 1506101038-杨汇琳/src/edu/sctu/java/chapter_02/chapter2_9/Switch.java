package edu.sctu.java.chapter_02.chapter2_9;

import java.util.Scanner;

/**
 * Created by dell on 2016/9/18.
 */
public class Switch {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.println("input a grade:");
        int x=scan.nextInt();
        String str="";
        switch (x){
            case 1:str="不及格！";
                break;
            case 2:str="及格！";
                break;
            case 3:str="中等！";
                break;
            case 4:str="良好！";
                break;
            case 5:str="优秀！";
                break;
            default:str="输入范围错误！";
        }
        System.out.println(""+str);
    }
}
