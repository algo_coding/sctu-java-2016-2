package edu.sctu.java.lab_04.lianxifour;

/**
 * Created by yanghuilin on 2016/11/15.
 * 运行下面的程序，理解方法的覆盖。
 */
public class MethodCoverage {
    public static void main(String[] args) {
        int a[]={34,12,8,67,88,23,98,101,119,56};
        sort_Demo m1 = new sort_Demo();
        System.out.print("排序前的数分别是：");
        for (int i=0;i<10;i++)
            System.out.print("  "+a[i]);
        m1.sort(a.length, a);
        System.out.print("\n按升序排列，数字依次为：");
        for (int i=0;i<10;i++)
            System.out.print("  "+a[i]);
        m1.sort(a.length,a);
        System.out.print("\n按降序排列，数字依次为：");
        for (int i=0;i<10;i++)
            System.out.print("  "+a[i]);
    }
}

class sort_Demo {
    int i, j, k, swap;

    sort_Demo() {
        i = j = k = swap = 0;
    }

    void sort(int t1, int t2[]) {
        for (i = 0; i < t1 - 1; i++) {
            k = i;
            for (j = i + 1; j < t1; j++) {
                if (t2[j] < t2[k])
                    k = j;
                if (k != i) {
                    swap = t2[i];
                    t2[i] = t2[k];
                    t2[k] = swap;
                }
            }
        }
    }

    class sub_Demo extends sort_Demo {
        void sort(int t1, int t2[]) {
            for (i = 0; i < t1 - 1; i++) {
                k = i;
                for (j = i + 1; j < t1; j++) {
                    if (t2[j] > t2[k])
                        k = j;
                    if (k!=i){
                        swap=t2[i];
                        t2[i]=t2[k];
                        t2[k]=swap;
                    }
                }
            }
        }
    }
}
