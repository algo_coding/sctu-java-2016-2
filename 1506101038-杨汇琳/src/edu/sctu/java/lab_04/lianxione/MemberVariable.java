package edu.sctu.java.lab_04.lianxione;

/**
 * Created by yanghuilin on 2016/11/15.
 * 运行下面的程序，理解成员变量的继承与隐藏。
 */
public class MemberVariable {
    public static void main(String[] args) {
        sub_sum3 m1 = new sub_sum3();
        m1.sum(100,200,300);
        System.out.println("sum="+m1.n1+"+"+m1.n2+"+"+m1.n3+"="+m1.sum);
    }
}

class sum_3 {
    int sum, n1, n2;
    static int n3;

    sum_3() {
        n1 = 0;
        n2 = 0;
        n3 = 0;
        sum = 0;
    }
}

class sub_sum3 extends sum_3 {
    int sum, n1, n2;
    static int n3;

    void sum(int i, int j, int k) {
        n1 = i;
        n2 = j;
        n3 = k;
        sum = n1 + n2 + n3;
    }
}

