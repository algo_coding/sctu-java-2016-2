package edu.sctu.java.lab_04.lianxifive;

/**
 * Created by yanghuilin on 2016/11/15.
 * 创建一个类，声明一个无参数的构造函数，打印类已创建的信息；
 * 再重载一个具有String参数的构造函数，打印参数信息；并创建主类验证之
 */
public class Printf {
    public static void main(String[] args) {
        Printf p=new Printf();
        p.Having();
        String s = null;
        p.Having(s);
    }
    void Having(){
        System.out.println("我是一个无参的构造函数。");
    }
    void Having(String name){
        System.out.println("我是一个有参的构造函数。");
    }
}
