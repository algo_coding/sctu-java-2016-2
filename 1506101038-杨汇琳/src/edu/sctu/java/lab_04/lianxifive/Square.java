package edu.sctu.java.lab_04.lianxifive;

import java.util.Scanner;

/**
 * Created by dell on 2016/11/15.
 * 定义一个矩形类，再定义接口EqualDiagonal，其中包含方法getDiagonal()；
 * 由矩形类派生出一个正方形类，自行扩充成员变量和方法，并实现此接口EqualDiagonal。
 */
public class Square extends Rectangle implements EqualDiagonal {
    double side;

    @Override
    public double getDiagonal() {

        side = length / 4;
        return side;
    }

    double Sarea;

    double getSarea() {
        Sarea = side * side;
        return Sarea;
    }

    public static void main(String[] args) {
        Square s = new Square();
        Scanner scanner = new Scanner(System.in);
        System.out.println("矩形边长分别为：");
        double ra = scanner.nextDouble();
        double rb = scanner.nextDouble();
        new Rectangle().getLength(ra, rb);
        System.out.println("正方形边长为：" + s.getDiagonal());
        System.out.println("正方形面积为：" + s.getSarea());
    }
}
