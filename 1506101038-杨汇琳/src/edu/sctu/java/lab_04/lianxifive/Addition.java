package edu.sctu.java.lab_04.lianxifive;

/**
 * Created by yanghuilin on 2016/11/15.
 */
public interface Addition {
    int add(int a, int b);
}
