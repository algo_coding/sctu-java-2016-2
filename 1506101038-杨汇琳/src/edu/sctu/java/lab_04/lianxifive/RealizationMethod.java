package edu.sctu.java.lab_04.lianxifive;

/**
 * Created by yanghuilin on 2016/11/15.
 * 定义两个接口，其方法协议分别完成两个数的加法和减法操作，然后创建一个类实现这两个接口的方法。
 */
public class RealizationMethod implements Addition, Subtraction {
    @Override
    public int add(int a, int b) {
        int sum;
        sum = a + b;
        System.out.println("两数之和为：" + a + " + " + b + " = " + sum);
        return sum;
    }

    @Override
    public int sub(int i, int j) {
        int dif;
        dif = i - j;
        System.out.println("两数之差为：" + i + " - " + j + " = " + dif);
        return dif;
    }

    public static void main(String[] args) {
        RealizationMethod sumdif = new RealizationMethod();
        sumdif.add(1, 2);
        sumdif.sub(5, 3);
    }
}
