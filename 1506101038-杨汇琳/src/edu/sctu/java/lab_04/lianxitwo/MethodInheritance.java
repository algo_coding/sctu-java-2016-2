package edu.sctu.java.lab_04.lianxitwo;

/**
 * Created by yanghuilin on 2016/11/15.
 * 运行下面的程序，理解方法的继承。
 */
public class MethodInheritance {
    public static void main(String[] args) {
        sub_Sorts m1=new sub_Sorts();
        m1.subsort(1,2,3);
        System.out.println("三个数字由大到小依次为："+m1.max1+"   "+m1.max2+"   "+m1.max3);
    }
}

class Sort3 {
    double max1, max2, max3;

    Sort3() {
        max1 = -1;
        max2 = -1;
        max3 = -1;
    }

    void sort() {
        double s;
        if (max1 < max2) {
            s = max1;
            max1 = max2;
            max2 = s;
        }
        if (max1 < max3) {
            s = max1;
            max1 = max3;
            max3 = s;
        }
        if (max2 < max3) {
            s = max2;
            max2 = max3;
            max3 = s;
        }
    }
}

class sub_Sorts extends Sort3 {
    void subsort(double i, double j, double k) {
        max1 = i;
        max2 = j;
        max3 = k;
        sort();
    }
}
