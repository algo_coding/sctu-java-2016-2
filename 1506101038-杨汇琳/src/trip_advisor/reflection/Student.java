package trip_advisor.reflection;

/**
 * Created by dell on 2016/10/27.
 */
public class Student {
    private String sno;
    private String sname;
    private int sage;
    private String ssex;

    protected void study() {
        System.out.println("I am a student,and now,I study hard");
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public int getSage() {
        return sage;
    }

    public void setSage(int sage) {
        this.sage = sage;
    }

    public String getSsex() {
        return ssex;
    }

    public void setSsex(String ssex) {
        this.ssex = ssex;
    }
}

