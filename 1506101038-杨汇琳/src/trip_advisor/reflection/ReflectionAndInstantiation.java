package trip_advisor.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by dell on 2016/10/27.
 */
public class ReflectionAndInstantiation {
    private static Object yang;

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {

//        一般实例化对象的方法

        Student yang = new Student();

        yang.setSname("杨汇琳");
        yang.setSno("1506101038");
        yang.setSage(19);
        yang.setSsex("female");
        System.out.println(yang.getSno() + "," + yang.getSname() + "," + yang.getSage() + "," + yang.getSsex());

//        通过反射来实现实例化
        Class student = Class.forName("trip_advisor.reflection.Student");
        Method methods = student.getDeclaredMethod("study");
        System.out.println(methods.invoke(yang));

        Method[] method = student.getDeclaredMethods();
        for( Method method1:method)
        System.out.println(method1.getName());
    }
}
