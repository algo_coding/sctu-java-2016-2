package edu.sctu.java.lab_03;

/**
 * Created by 001 on 2016/11/3.
 */
public class AreaAndLength {
    public static void main(String args[]) {
        double length, area,boder;
        Circle circle = new Circle(5);//创建对象circle
        Trangle trangle = new Trangle(3, 4, 5); //创建对象trangle。
        Lader lader = new Lader(4, 5, 7); //创建对象lader
        length = circle.getLength(); // circle调用方法返回周长并赋值给length
        System.out.println("圆的周长:" + length);
        area = circle.getArea(); // circle调用方法返回面积并赋值给area
        System.out.println("圆的面积:" + area);
        length = trangle.getLength(); // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:" + length);
        area = trangle.getArea();// trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:" + area);
        area = lader.getArea(); // lader调用方法返回面积并赋值给area
        System.out.println("梯形的面积:" + area);
        trangle = new Trangle(12, 34, 1);// trangle调用方法设置三个边，要求将三个边修改为12,34,1。
        area = trangle.getArea(); // trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:" + area);
        length = trangle.getLength(); // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:" + length);
        boder=trangle.getSideA();
        System.out.println("三角形a的边长:" + boder);
        boder=trangle.getSideB();
        System.out.println("三角形b的边长:" + boder);
        boder=trangle.getSideC();
        System.out.println("三角形c的边长:" + boder);
        circle=new Circle(10);// circle调用方法设置半径，要求将半径修改为10
        double radius = circle.getRadius();
        System.out.println("圆的半径:" + radius);
        length = circle.getLength(); // circle调用方法返回周长并赋值给length
        System.out.println("圆的周长:" + length);
        area = circle.getArea(); // circle调用方法返回面积并赋值给area
        System.out.println("圆的面积:" + area);

    }
}
