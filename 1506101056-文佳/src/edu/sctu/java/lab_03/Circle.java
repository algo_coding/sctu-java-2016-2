package edu.sctu.java.lab_03;

import com.sun.corba.se.impl.interceptors.PICurrent;

/**
 * Created by 001 on 2016/11/3.
 */
public class Circle {
    double radius,area,length;
    Circle(double r)
    {
        radius=r; //方法体
    }
    double getArea()
    {
        area=radius*radius*3.14;
         return area;//方法体，要求计算出area返回
    }
    double getLength()
    {
        length=2*radius*3.14;
        return length;//getArea方法体的代码,要求计算出length返回
    }
    void setRadius(double newRadius)
    {
        radius=newRadius;
    }
    double getRadius()
    {
        return radius;
    }
}
