package edu.sctu.java.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by Zero on 2016/10/20.
 */
public class HotCityExporterImpl implements HotCityExporter {
    @Override
    public void export(List<HotCity> hotCityList, String fileName) {


        File file = new File(fileName);

        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file));



            for (HotCity hotCity : hotCityList) {


                writer.write(hotCity.getRank() + "," + hotCity.getName()+","+ hotCity.getUrl());
                writer.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
