package edu.sctu.java.export;

/**
 * Created by Zero on 2016/10/20.
 */
public class HotCity {
    private int rank;
    private String name;
    private String url;

    public HotCity(int rank, String name, String url) {
        this.rank = rank;
        this.name = name;
        this.url = url;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
