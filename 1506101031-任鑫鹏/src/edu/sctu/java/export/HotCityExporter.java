package edu.sctu.java.export;

import java.util.List;

/**
 * Created by Zero on 2016/10/20.
 */
public interface HotCityExporter {

    public void export(List<HotCity> hotCityList, String fileName);

}

/**
 *
 *
 *
 * name, position
 * 龙庭酒店，四川旅游学院
 * 阳光酒店，龙都南路
 *
 */
