package edu.sctu.java.lab_03;

/**
 * Created by Zero on 2016/11/3.
 * 三角形、梯形和圆形的类封装
 */


class Trangle
{
    double sideA,sideB,sideC,area,length;
    boolean boo;


    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public  Trangle(double a,double b,double c)
    {
        sideA=a;
        sideB=b;
        sideC=c; //将a、b、c赋予给sideA、sideB、sideC

        if((a+b)>c && Math.abs(a-b)<c) //a,b,c构成三角形的条件表达式
        {
            boo=true; //给boo赋值。
        }
        else
        {
            boo=false; //给boo赋值。
        }
    }
    double getLength()
    {
        return sideA+sideB+sideC; //方法体，要求计算出length的值并返回
    }
    public double  getArea()
    {
        if(boo)
        {
            double p=(sideA+sideB+sideC)/2.0;
            area=Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC)) ;
            return area;
        }
        else
        {
            System.out.println("不是一个三角形,不能计算面积");
            return 0;
        }
    }
    public void setABC(double a,double b,double c)
    {
        sideA=a;
        sideB=b;
        sideC=c;       //参数a,b,c分别赋值给sideA,sideB,sideC

        if((a+b)>c && Math.abs(a-b)<c) //a,b,c构成三角形的条件表达式
        {
            boo=true; //给boo赋值。
        }
        else
        {
            boo=false; //给boo赋值。
        }
    }
}
class Lader
{
    double above,bottom,height,area;
    Lader(double a,double b,double h)
    {
        above=a;
        bottom=b;
        height=h;//方法体，将参数a,b,c分别赋值给above,bottom,height
    }
    double getArea()
    {
       return (above+bottom)/2*height; //方法体，,要求计算出area返回
    }
}

class Circle
{
    double radius,area;

    Circle(double r)
    {
        radius=r; //方法体   将半径赋给radius
    }
    double getArea()
    {
        return radius*radius*3.14; //方法体，要求计算出area返回
    }
    double getLength()
    {
         return 2*radius*3.14; //getArea方法体的代码,要求计算出length返回
    }
    void setRadius(double newRadius)
    {
        radius=newRadius;
    }
    double getRadius()
    {
        return radius;
    }
}
public class AreaAndLength
{
    public static void main(String args[])
    {
        double length,area;
        Circle circle=null;
        Trangle trangle;
        Lader lader;
        circle= new Circle(5);       //创建对象circle
        trangle=new Trangle(3,4,5);    //创建对象trangle。
        lader=new Lader(3,4,5);     //创建对象lader

        length=circle.getLength();      // circle调用方法返回周长并赋值给length
        System.out.println("圆的周长:"+length);

        area=circle.getArea();         // circle调用方法返回面积并赋值给area
        System.out.println("圆的面积:"+area);

        length=trangle.getLength();        // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:"+length);

        area=trangle.getArea();       // trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:"+area);

        area=lader.getArea();      // lader调用方法返回面积并赋值给area
        System.out.println("梯形的面积:" + area);

        trangle.setABC(12, 34, 1); // trangle调用方法设置三个边，要求将三个边修改为12,34,1。

        area=trangle.getArea(); // trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:"+area);

        length=trangle.getLength(); // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:" + length);


        System.out.println("..................................");

        circle.setRadius(6);
        System.out.println("新圆的半径:" + circle.getRadius());
        System.out.println("新圆的面积:" + circle.getArea());
        System.out.println("新圆的周长:" + circle.getLength());

    }
}

