package edu.sctu.java.lab_05;

/**
 * Created by Zero on 2016/11/17.
 * 实验五第一题
 */
public class StringExample
{   public static void main(String args[])
{   String s1=new String("you are a student"),
        s2=new String("how are you");
    if(s1.equals(s2)) // 使用equals方法判断s1与s2是否相同
    {
        System.out.println("s1与s2相同");
    }
    else
    {
        System.out.println("s1与s2不相同");
    }
    String s3=new String("22030219851022024");
    if(s3.startsWith("220302"))   //判断s3的前缀是否是“220302”。
    {
        System.out.println("吉林省的身份证");
    }
    String s4=new String("你"),
            s5=new String("我");
    if(s4.compareTo(s5)>0)//按着字典序s4大于s5的表达式。
    {
        System.out.println("按字典序s4大于s5");
    }
    else
    {
        System.out.println("按字典序s4小于s5");
    }
    int position=0;
    String path="c:\\java\\jsp\\A.java";
    position=path.lastIndexOf("\\"); //获取path中最后出现目录分隔符号的位置
    System.out.println("c:\\java\\jsp\\A.java中最后出现\\的位置:"+position);
    String fileName=path.substring(path.indexOf("A")); //获取path中“A.java”子字符串。
    System.out.println("c:\\java\\jsp\\A.java中含有的文件名:"+fileName);
    String s6=new String("100"), s7=new String("123.678");
    int n1=Integer.parseInt(s6);     //将s6转化成int型数据。
    double n2=Double.parseDouble(s7);  //将s7转化成double型数据。
    double m=n1+n2;
    System.out.println(m);
    String s8=String.valueOf(m); //String调用valuOf(int n)方法将m转化为字符串对象
    position=s8.indexOf(".");
    String temp=s8.substring(position+1);
    System.out.println("数字"+m+"有"+temp.length()+"位小数") ;
    String s9=new String("ABCDEF");
    char a[]=s8.toCharArray();   //将s8存放到数组a中。
    for(int i=a.length-1;i>=0;i--)
    {
        System.out.print(" " + a[i]);
    }


//2.数组a的前3个单元创建一个字符串并输出该串
    String s=new String(a,0,2);
    System.out.println("输出结果为："+s);

//3. 请给出获取path中“jsp”子字符串的代码。

    String getJsp=path.substring(8,11);
    System.out.println("c:\\java\\jsp\\A.java中含有的文件名："+getJsp);
//4. 在程序的适当位置增加如下代码，注意输出的结果。

    String str1=new String("ABCABC"),
            str2=null,
            str3=null,
            str4=null;
    str2=str1.replaceAll("A","First");
    str3=str2.replaceAll("B","Second");
    str4=str3.replaceAll("C","Third");
    System.out.println(str1);
    System.out.println(str2);
    System.out.println(str3);
    System.out.println(str4);

//5. 可以使用Long类中的下列static方法得到整数各种进制的字符串表示：
    Long I=new Long(12345);
    System.out.println("12345的二进制表示："+I.toBinaryString(I));
    System.out.println("12345的八进制表示："+I.toOctalString(I));
    System.out.println("12345的十六进制表示："+I.toHexString(I));

    Long number=new Long(12345);
    System.out.println(number+"二进制表示："+Long.toString(number,2));
    System.out.println(number+"的八进制表示："+Long.toString(number, 8));
    System.out.println(number+"的十六进制表示："+Long.toString(number, 16));

//6. 在适当位置添加代码，分别输出数字m的整数部分和小数部分。

    String str=String.valueOf(m);
    String dec=str.substring((str.indexOf(".") + 1));
    String inte=str.substring(0,str.indexOf(".")-1);

    System.out.println("输出整数部分:"+Integer.parseInt(inte));
    System.out.println("输出小数部分"+Integer.parseInt(dec));


}
}

