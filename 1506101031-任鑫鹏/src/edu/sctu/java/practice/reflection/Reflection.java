package edu.sctu.java.practice.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Zero on 2016/10/27.
 */
public class Reflection {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {



        Student student=new Student();
        student.setName("Ren");
        student.setAge(20);



        Class clz=Class.forName("edu.sctu.java.practice.reflection.Student");
        Student ren= (Student) clz.newInstance();
        ren.setName("ren");
        ren.setAge(20);


        System.out.println("--------------------反射获得姓名和年龄--------------------");
        System.out.println("name:" + ren.getName() + "  age:" + ren.getAge());

        System.out.println("--------------------反射获得属性名--------------------");

        Field[] fields=clz.getDeclaredFields();
        for(Field field:fields){

            System.out.println(field.getName());
        }


        System.out.println("--------------------反射获得方法--------------------");


        Method[] methods=clz.getMethods();

        for(Method method:methods){

            System.out.println(method.getName());

        }


        System.out.println("--------------------反射使用类的方法--------------------");

        Method method=clz.getDeclaredMethod("study");
        method.invoke(ren);

    }
}
