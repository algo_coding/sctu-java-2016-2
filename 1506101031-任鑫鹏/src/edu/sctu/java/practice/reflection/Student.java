package edu.sctu.java.practice.reflection;

/**
 * Created by Zero on 2016/10/27.
 */
public class Student {


    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public void study(){

        System.out.println("study hard！！");
    }
}
