package edu.sctu.java.lab_04;

/**
 * Created by Zero on 2016/11/10.
 * 加法和减法的接口
 */
public interface AddAndSubtractImpl {

    int add1(int a,int b);
    int subtract(int a,int b);
}

