package edu.sctu.java.lab_04;

/**
 * Created by Zero on 2016/11/10.
 * 矩形接口，获得对角线方法。
 */
public interface EqualDiagonal {

    double getDiagonal();
}
