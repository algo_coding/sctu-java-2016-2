package edu.sctu.java.lab_04;

/**
 * Created by Zero on 2016/11/10.
 * 矩形类
 */
public class Rectangle implements EqualDiagonal {


    double diagonal;


    public void setDiagonal(int width, int height){


        this.diagonal=Math.sqrt((width*width)+(height*height));

    }

    @Override
    public double getDiagonal() {
        return diagonal;
    }
}
