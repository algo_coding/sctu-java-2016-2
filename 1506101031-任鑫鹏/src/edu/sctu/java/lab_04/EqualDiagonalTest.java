package edu.sctu.java.lab_04;

/**
 * Created by Zero on 2016/11/10.
 * 实现接口。
 */
public class EqualDiagonalTest {

    public static void main(String[] args) {



        Rectangle rectangle=new Rectangle();
        rectangle.setDiagonal(3, 4);
        System.out.println("矩形的对角线：" + rectangle.getDiagonal());

        Square square=new Square();
        square.setDiagonal(4);
        System.out.println("正方形的对角线："+square.getDiagonal());

    }
}
