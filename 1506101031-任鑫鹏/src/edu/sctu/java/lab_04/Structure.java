package edu.sctu.java.lab_04;

/**
 * Created by Zero on 2016/11/10.
 * 构造函数
 */
public class Structure {


    public Structure() {

        System.out.println("这是一个无参数构造函数");
    }

    public Structure(String name){

        System.out.println("这是一个"+name+"构造函数");
    }
}

