package edu.sctu.java.lab_04;

/**
 * Created by Zero on 2016/11/10.
 * 接口的方法
 */
public class AddAndSubtract implements AddAndSubtractImpl {

    @Override
    public int add1(int a, int b) {
        return a+b;
    }

    @Override
    public int subtract(int a, int b) {
        return a-b;
    }

}

