package edu.sctu.java.lianxi;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by asus on 2016/12/8.
 */
public class TCPsever {
    public static void main(String[] args) throws IOException {
        ServerSocket listen = new ServerSocket(5050);
        System.out.println("服务器启动成功!");
        Socket server  = listen.accept();
        System.out.println("连接成功!");
        InputStream in = server.getInputStream();

        OutputStream out = server.getOutputStream();
        char c = (char)in.read();
        System.out.println("收到:" + c);
        out.write('s');

        out.close();
        in.close();
        server.close();
        listen.close();
    }
}
