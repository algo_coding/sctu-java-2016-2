package edu.sctu.java.lab_05;

import javax.swing.*;
import java.sql.Date;
import java.util.Calendar;

/**
 * Created by asus on 2016/11/23.
 * 根据本程序中的一些知识，编写一个计算利息（按天计算）的程序。从输入对话框输入存款的数目和起止时间。
 */
public class Money {
    public static void main(String[] args) {
        String str = JOptionPane.showInputDialog("请输入存款数目：");
        int deposit=Integer.parseInt(str);
        str = JOptionPane.showInputDialog("输入存款开始日期的年份:");
        int yearOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份:");
        int monthOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期:");
        int dayOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入存款结束日期的年份:");
        int yearTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份:");
        int monthTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期:");
        int dayTwo=Integer.parseInt(str);

        Calendar calendar = Calendar.getInstance();  //初始化日历对象
        calendar.set(yearOne, monthOne, dayOne);                 //将calendar的时间设置为yearOne年monthOne月dayOne日

        long timeOne = calendar.getTimeInMillis();     //calendar表示的时间转换成毫秒
        calendar.set(yearTwo,monthTwo,dayTwo);                 //将calendar的时间设置为yearTwo年monthTwo月dayTwo日
        long timeTwo=calendar.getTimeInMillis();    //calendar表示的时间转换成毫秒。
        Date date1= new Date(timeOne);       // 用timeOne做参数构造date1
        Date date2=new Date(timeTwo);      // 用timeTwo做参数构造date2
        System.out.println("存款日期为：" + date1);
        System.out.println("取款日期为：" + date2);
        System.out.println("利率为：1.5%");
        long days=(date2.getTime()-date1.getTime())/(1000*60*60*24);

        double interest = deposit*0.015*days;
        System.out.println(interest);
    }

}
