package edu.sctu.java.lab_05;

import java.math.BigInteger;

/**
 * Created by asus on 2016/11/23.
 * 编写一个Java应用程序，计算两个大整数的和、差、积和商，并计算一个大整数的因子个数（因子中不包括1和大整数本身）。
 */
public class BigintegerExample {
    public static void main(String args[])
    {
//        BigInteger n1=new BigInteger("987654321987654321987654321"),
//            n2=new BigInteger("123456789123456789123456789"),
//            result=null;
//        result= n1.add(n2); //n1和n2做加法运算
//        System.out.println("和:"+result.toString());
//        result= n1.subtract(n2);//n1和n2做减法运算
//        System.out.println("差:"+result.toString());
//        result=n1.multiply(n2);//n1和n2做乘法运算
//        System.out.println("积:"+result.toString());
//        result= n1.remainder(n2); //n1和n2做除法运算
//        System.out.println("商:"+result.toString());
//        BigInteger m=new BigInteger("1968957"),
//                COUNT=new BigInteger("0"),
//                ONE=new BigInteger("1"),
//                TWO=new BigInteger("2");
//        System.out.println(m.toString()+"的因子有:");
//        for(BigInteger i=TWO;i.compareTo(m)<0;i=i.add(ONE)) {
//            if((n1.remainder(i).compareTo(BigInteger.ZERO))==0) {
//            COUNT=COUNT.add(ONE);
//            System.out.print("  "+i.toString());
//             }
//        }
//        System.out.println("");
//        System.out.println(m.toString()+"一共有"+COUNT.toString()+"个因子");


        //计算大整数的阶乘
//        BigInteger n1 = new BigInteger("50");   BigInteger n2 = new BigInteger("1");
//        BigInteger one = new BigInteger("1");
//        for(BigInteger i=one;i.compareTo(n1) <=0;i = i.add(one))    {
//            n2=n2.multiply(i);
//        }
//        System.out.println(n1.toString() + "的阶乘为：" + n2.toString());

        //计算1+2+3…的前999999999项的和。
        BigInteger n1 = new BigInteger("99999999");
        BigInteger n2 = new BigInteger("0");   BigInteger one = new BigInteger("1");
        for(BigInteger i=one;i.compareTo(n1) <=0;i = i.add(one))    {
            n2=n2.add(i);
        }
        System.out.println("前" + n1.toString() + "项的和为：" + n2.toString());



    }

}
