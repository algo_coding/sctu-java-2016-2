package edu.sctu.java.lab_05;

import javax.swing.*;

import java.sql.Date;
import java.util.Calendar;
import javax.swing.JOptionPane;

/**
 * Created by asus on 2016/11/17.
 *编写一个Java应用程序，用户从输入对话框输入了两个日期，程序将判断两个日期的大小关系，以及两个日期之间的间隔天数。
 */
public class DateExample {
    public static void main(String args[ ])
    {
        String str = JOptionPane.showInputDialog("输入第一个日期的年份:");
        int yearOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份:");
        int monthOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期:");
        int dayOne=Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入该日期的小时");
        int hourOne = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入该小时的分数");
        int minOne = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入该分钟的秒数");
        int secOne = Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入第二个日期的年份:");
        int yearTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份:");
        int monthTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期:");
        int dayTwo=Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入该日期的小时");
        int hourTwo = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入该小时的分数");
        int minTwo = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入该分钟的秒数");
        int secTwo = Integer.parseInt(str);
        Calendar calendar = Calendar.getInstance();  //初始化日历对象
        calendar.set(yearOne, monthOne, dayOne,hourOne,minOne,secOne);                 //将calendar的时间设置为yearOne年monthOne月dayOne日

        long timeOne = calendar.getTimeInMillis();     //calendar表示的时间转换成毫秒
        calendar.set(yearTwo,monthTwo,dayTwo,hourTwo,minTwo,secTwo);                 //将calendar的时间设置为yearTwo年monthTwo月dayTwo日
        long timeTwo=calendar.getTimeInMillis();    //calendar表示的时间转换成毫秒。
        Date date1= new Date(timeOne);       // 用timeOne做参数构造date1
        Date date2=new Date(timeTwo);      // 用timeTwo做参数构造date2
        System.out.println("第一个日期为：" + date1);
        System.out.println("第二个日期为：" + date2);
        if(date2.equals(date1))
        {
            System.out.println("两个日期的年、月、日完全相同");
        }
        else if(date2.after(date1))
        {
            System.out.println("您输入的第二个日期大于第一个日期");
        }
        else if(date2.before(date1))
        {
            System.out.println("您输入的第二个日期小于第一个日期");
        }
        long days=(date2.getTime()-date1.getTime())/(1000*60*60*24);//计算两个日期相隔天数
        System.out.println(yearOne+"年"+monthOne+"月"+dayOne+"日和"
                +yearTwo+"年"+monthTwo+"月"+dayTwo+"相隔"+days+"天");
    }

}
