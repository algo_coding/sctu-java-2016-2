package edu.sctu.java.lab_04;

/**
 * Created by asus on 2016/11/10.
 */
public interface Count {
    int addNumber(int a, int b);
    int reduceNumber(int a,int b);
}
