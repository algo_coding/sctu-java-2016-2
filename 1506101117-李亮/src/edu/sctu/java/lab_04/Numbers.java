package edu.sctu.java.lab_04;

/**
 * Created by asus on 2016/11/10.
 */

public class Numbers implements Count {
    @Override
    public int addNumber(int a, int b) {
        int c = a + b;
        System.out.println("a + b的值为："+ c);
        return 0;
    }

    @Override
    public int reduceNumber(int a, int b) {
        int c = a - b;
        System.out.println("a - b的值为："+ c);
        return 0;
    }

    public static void main(String[] args) {
        Numbers numbers = new Numbers();
        int c = numbers.addNumber(1,2);
        int d = numbers.reduceNumber(3,2);
    }
}
