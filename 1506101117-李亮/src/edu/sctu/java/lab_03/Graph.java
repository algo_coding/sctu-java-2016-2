package edu.sctu.java.lab_03;

/**
 * Created by asus on 2016/11/3.
 * 编写一个Java应用程序，该程序中有3个类：Trangle、Leder和Circle，分别用来刻画“三角形”、“梯形”和“圆形”。具体要求如下：
 a)	Trangle类具有类型为double的三个边，以及周长、面积属性，Trangle类具有返回周长、面积以及修改三个边的功能。
 另外，Trangle类还具有一个boolean型的属性，该属性用来判断三个属能否构成一个三角形。
 b)	Lader类具有类型double的上底、下底、高、面积属性，具有返回面积的功能。
 c)	Circle类具有类型为double的半径、周长和面积属性，具有返回周长、面积的功能。

 */
 class Trangle {
    double sideA,sideB,sideC,area,length;
    boolean boo;
    public  Trangle(double a,double b,double c)
    {
        sideA = a;
        sideB = b;
        sideC = c; //参数a,b,c分别赋值给sideA,sideB,sideC
        if(sideA + sideB > sideC && sideA + sideC > sideB && sideB + sideC > sideA) //a,b,c构成三角形的条件表达式
        {
            boo = true; //给boo赋值。
        }
        else
        {
            boo = false; //给boo赋值。
        }
    }
    double getLength()
    {

        length = sideA + sideB + sideC;
        return length; //方法体，要求计算出length的值并返回
    }
    public double  getArea()
    {
        if(boo)
        {
            double p=(sideA+sideB+sideC)/2.0;
            area=Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC)) ;
            return area;
        }
        else
        {
            System.out.println("不是一个三角形,不能计算面积");
            return 0;
        }
    }
    public void setABC(double a,double b,double c)
    {
        sideA = a;
        sideB = b;
        sideC = c; //参数a,b,c分别赋值给sideA,sideB,sideC
        if(sideA + sideB > sideC && sideA + sideC > sideB && sideB + sideC > sideA) //a,b,c构成三角形的条件表达式
        {
            boo = true; //给boo赋值。
        }
        else
        {
            boo = false; //给boo赋值。
        }
    }
}
class Lader
{
    double above,bottom,height,area;
    Lader(double a,double b,double c)
    {
        above = a;
        bottom = b;
        height = c;  //方法体，将参数a,b,c分别赋值给above,bottom,height
    }
    double getArea()
    {
        return (above+bottom)*height/2; //方法体，,要求计算出area返回
    }
}

class Circle
{
    double radius,area,length;
    Circle(double r)
    {
        radius = r;     //方法体
    }
    double getArea()
    {
       area = 3.14 * radius * radius;
        return area;
       //方法体，要求计算出area返回
    }
    double getLength()
    {
        length = 2 * 3.14 *radius;
        return length;  //getArea方法体的代码,要求计算出length返回
    }
    void setRadius(double newRadius)
    {
        radius=newRadius;
    }
    double getRadius()
    {
        return radius;
    }
}
class AreaAndLength {
    public static void main(String args[]) {
        double length, area;
        Circle circle = new Circle(1.5); //创建对象circle
        Trangle trangle = new Trangle(2.0,3.1,2.5); //创建对象trangle。
        Lader lader = new Lader(2.0,3.1,2.5); //创建对象lader

        length = circle.getLength();  // circle调用方法返回周长并赋值给length
        System.out.println("圆的周长:" + length);

        area = circle.getArea();  // circle调用方法返回面积并赋值给area
        System.out.println("圆的面积:" + area);

        length = circle.getLength(); // trangle调用方法返回周长并赋值给length
        System.out.println("三角形的周长:" + length);

        area = circle.getArea(); // trangle调用方法返回面积并赋值给area
        System.out.println("三角形的面积:" + area);

        area = lader.getArea(); // lader调用方法返回面积并赋值给area
        System.out.println("梯形的面积:" + area);

        trangle.setABC(12, 34, 1); // trangle调用方法设置三个边，要求将三个边修改为12,34,1。

        area = trangle.getArea(); // trangle调用方法返回面积并赋值给area

        System.out.println("三角形的面积:" + area);

        length = trangle.getLength(); // trangle调用方法返回周长并赋值给length

        System.out.println("三角形的周长:" + length);
    }
}




