package edu.sctu.tripadvisor.taskseven.nieying;

/**
 * Created by Administrator on 2016/10/12 0012.
 */
public class Exam030405 {
    public static void main(String[] args) {
        F f1=new F();
        F f2=new F();
        System.out.println("通过a1来设置a1和a2的值");
        f1.setx(1);
        f1.showx();
        f2.showx();

        f1.sety(2);
        f1.showy();
        f2.showy();

        f2.setx(3);
        f1.showx();
        f2.showx();

        f2.sety(4);
        f1.showy();
        f2.showy();

    }
}
class F {
    static int x;
    int y;

    void setx(int x0) {
        x = x0;
    }

    void sety(int y0) {
        y = y0;
    }

    void showx() {
        System.out.println("x=" + x);
    }

    void showy() {
        System.out.println("Y=" + y);
    }
}

