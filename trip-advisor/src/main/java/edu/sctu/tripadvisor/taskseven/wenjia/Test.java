package edu.sctu.tripadvisor.taskseven.wenjia;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.lang.model.element.Element;
import java.io.IOException;

/**
 * Created by 001 on 2016/9/22.
 */
public class Test {
    public static void main(String[] args) {
        try {
            Document document = Jsoup.connect("http://www.tripadvisor.cn/members/zhengjieyao").get();
            //昵称
            Elements nickName=document.select(".nameText").select("span");
            System.out.println("昵称："+nickName.text());
            //注册时间
            Elements regisTime=document.select(".since").select("p");
            System.out.println("注册时间："+regisTime.text());
            //头像
            Elements headPicture=document.select(".avatarUrl");
            System.out.println("头像："+headPicture.attr("img"));
            //地点
            Elements homeTown=document.select(".homeTown").select("p");
            System.out.println("地点："+homeTown.text());
            //总分
            Elements cause=document.select(".points");
            System.out.println("总分："+cause.text());
            //点评数量
            Elements times=document.select(".active").select("a");
            System.out.println("点评数量："+times.text());




        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
