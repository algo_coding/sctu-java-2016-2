package edu.sctu.tripadvisor.taskseven.nieying;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by Administrator on 2016/9/26 0026.
 */
public class Test2 {
    public static void main(String[] args){
        String name = "http://www.tripadvisor.cn/members/RosieZhang";
        try{

            Document document = Jsoup.connect("http://www.tripadvisor.cn/members/RosieZhang").get();

            Elements username=document.select(".nameText");

            System.out.println(username.text());

            Elements registration=document.select(".since");

            System.out.println(registration.text());

            Elements head=document.select(".avatarUrl");

            System.out.println(head.attr("src"));

            Elements address=document.select(".hometown").select("p");

            System.out.println(address.text());

            Element zong=document.select("div.label").first();

            System.out.println(zong.text());

            Element score=document.select("div.points").first();

            System.out.println(score.text());

            Elements rank=document.select(".name").select(".totalBadges");

            System.out.println(rank.text());

            Elements Number=document.select(".active").select("a");

            System.out.println(Number.text());

            Elements condition=document.select(".cs-filter-bar").select("p");

            System.out.println(condition.text());

            Elements Unlimited=document.select(".active").select("li");

            System.out.println(Unlimited.text());

        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
