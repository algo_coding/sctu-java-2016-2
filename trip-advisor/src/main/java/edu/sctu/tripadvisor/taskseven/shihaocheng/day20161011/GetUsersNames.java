package edu.sctu.tripadvisor.taskseven.shihaocheng.day20161011;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2016/10/11.
 */
public class GetUsersNames implements Getdatas {


    @Override
    public List<String> getNames(String tableName) {


        try {
            //调用Class.forName()方法加载驱动程序
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("成功加载MySQL驱动！");
        } catch (ClassNotFoundException e1) {
            System.out.println("找不到MySQL驱动!");
            e1.printStackTrace();
        }

//      String url="jdbc:mysql://localhost:3306/test?useUnicode=true&amp;characterEncoding=UTF-8";    //JDBC的URL
        String url = "jdbc:mysql://172.16.10.37/elite";
        String user = "elite";
        String psw = "elite";
        //调用DriverManager对象的getConnection()方法，获得一个Connection对象
        Connection conn = null;
        ResultSet rr = null;
        List<String> list = new ArrayList<String>();
        try {
            conn = DriverManager.getConnection(url, user, psw);
            //创建一个Statement对象
            Statement stmt = conn.createStatement(); //创建Statement对象
            ResultSet rs = stmt.executeQuery("select * from tableName");
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
//            while (rs.next()) {
//                System.out.println(rs.getString("city_type"));
//                        + "\t"
//                        + rs.getString("first_name") + "\t" + "\t"
//                        + rs.getString("last_name") + "\t"
//                        + rs.getString("last_update"));

            while (rs.next()) {
                list.add(rs.getString(2));
            }


            stmt.close();


        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("插入数据失败");
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        String users[] = (String[]) list.toArray(new String[list.size()]);
        return list;
    }




}
