package edu.sctu.tripadvisor.taskseven;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;


/**
 * Created by hengju on 2016/9/22.
 */
public class TheUserCenter {
    public static void main(String args[]){
        try {

            Document doc= Jsoup.connect("http://www.tripadvisor.cn/members/我不是天一").get();
            Elements username=doc.select(".nameText");
            System.out.println("昵称：" + username.text());

            Elements usertime=doc.select(".since");
            System.out.println("注册时间："+usertime.text());

            Elements useraddress=doc.select(".hometown");
            System.out.println("地址:" + useraddress.text());

            Elements userreview=doc.select(".content-info");
            System.out.println("点评内容:"+userreview.text());

            Elements userphoto=doc.select(".avatarUrl");
            System.out.println("头像:"+userphoto.attr("src"));

            Elements usergrade=doc.select(".points");
            System.out.println("总分:"+usergrade.text());

            Elements userlevel=doc.select(".tripcollectiveinfo").select("span");
            System.out.println("等级:"+userlevel.text());

            Elements useractive=doc.select(".cs-filter-bar").select("ul").select("li[data-filter=\"REVIEWS_HOTELS\"]");
            System.out.println("点评数量:"+useractive.text());

            Document document= Jsoup.connect("http://www.tripadvisor.cn/members-photos/我不是天一").get();
            Elements userpho=document.select(".cs-filter-bar").select("ul").select("li[data-filter=\"PHOTOS_HOTELS\"],li[data-filter=\"PHOTOS_RESTAURANTS\"]");
            System.out.println("照片数量:"+userpho.text());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
