package edu.sctu.tripadvisor.export;

/**
 * Created by Administrator on 2016/10/20.
 */
public class Hotel {
    private String name;
    private String position;


    public Hotel(String name, String position) {
        this.name = name;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
