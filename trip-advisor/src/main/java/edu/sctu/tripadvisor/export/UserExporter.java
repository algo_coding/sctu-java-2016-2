package edu.sctu.tripadvisor.export;

import java.util.List;

/**
 * Created by Administrator on 2016/10/20.
 */
public interface UserExporter {

public void export(List<User> userList, String fileName);
        }
