package edu.sctu.tripadvisor.export;

import java.io.*;
import java.util.List;

/**
 * Created by Administrator on 2016/10/20.
 */
public class HotelExporterImpl implements HotelExporter {
    @Override
    public void export(List<Hotel> hotelList, String fileName) {


        File file = new File(fileName);

        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file));

//            writer.write("hello,world!");

            for (Hotel hotel : hotelList) {
                // name, position
                writer.write(hotel.getName() + "," + hotel.getPosition());
                writer.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
