package edu.sctu.tripadvisor.export;

import java.util.List;

/**
 * Created by Administrator on 2016/10/27.
 */
public interface TestExport<T> {

    public void export(T list, String fileName);
}
