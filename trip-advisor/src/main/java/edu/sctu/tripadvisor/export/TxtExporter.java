package edu.sctu.tripadvisor.export;

import java.util.List;

/**
 * Created by Administrator on 2016/10/20.
 */
public interface TxtExporter {

    public void export(List list, String fileName);
}
