package edu.sctu.tripadvisor.exception;

/**
 * Created by apple on 03/11/2016.
 */
public interface UserService {
    public boolean login(String username, String password);
}
