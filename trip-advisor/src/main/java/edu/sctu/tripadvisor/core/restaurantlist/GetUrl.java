package edu.sctu.tripadvisor.core.restaurantlist;

import edu.sctu.tripadvisor.core.citytypes.CityTypeDataInformation;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/10/9.
 */
public class GetUrl implements Url<ArrayList> {
    private String http;
    private String http1 = "http://www.tripadvisor.cn/RestaurantSearch?Action=PAGE&geo=";
    private String num;
    private String http2 = "&ajax=1&itags=10591&sortOrder=popularity&o=a";
    private String http3 = "&availSearchEnabled=false";
    int j;
    private int max;


    public ArrayList getUrl() {

        ArrayList<String> arrayList = new ArrayList<String>();
        CityTypeDataInformation cityTypeDataInfor = new CityTypeDataInformation();

        //get url from  tasktwo ;
        List<String> list = cityTypeDataInfor.readUrlFromDatabase("餐厅");

        for (String str : list) {

            Document document ;

            try {

                document = Jsoup.connect(str).get();
                Element div = document.select(".pageNum").select(".taLnk").last();

                if (div != null) {
                    num = str.substring(39, 46);

                    if (num.equals("297463-")) {
                        this.max = 97;
                    } else if (num.equals("608466-")) {
                        this.max = 3;
                    } else if (num.equals("1411417")) {
                        this.max = 2;
                    }

                    for (int i = 1; i <= this.max; i++) {

                        j = 30 * (i - 1);
                        this.http = http1 + num + http2 + j + http3;
                        arrayList.add(http);

                    }
                    } else {

                    num = str.substring(39, 46);
                    j = 0;
                    this.http = http1 + num + http2 + j + http3;
                    arrayList.add(http);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return arrayList;
    }
}

