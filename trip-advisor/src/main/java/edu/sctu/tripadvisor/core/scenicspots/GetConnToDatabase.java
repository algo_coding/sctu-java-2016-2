package edu.sctu.tripadvisor.core.scenicspots;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mike on 2016/10/24.
 */
public class GetConnToDatabase {

    private static String databaseip = "172.16.10.37";

    private static String port = "3306";

    private static String databasename = "elite";

    private static String username = "elite";

    private static String password = "elite";

    private static String url = "jdbc:mysql://" + databaseip + ":" + port + "/" + databasename + "?characterEncoding=" + "utf-8" + "&useSSL=" + "false" + "&serverTimezone=UTC" + "&useUnicode=true" + "&autoReconnect=true&failOverReadOnly=false";

    private static Connection conn;

    public static String tablename = "ScenicSpots";

    public static Connection getConn() {   //获得数据库连接对象


        try {
            Class.forName("com.mysql.jdbc.Driver");
            try {
                conn = DriverManager.getConnection(url, username, password);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();


        }

        return conn;
    }


    public static List<Object[]> getalldata() {  //返回数据库查询结果集 用Object数组返回

        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select * from " + tablename;
        List list = new ArrayList();
        int count = 0;
        try {
            stmt = GetConnToDatabase.getConn().createStatement();

            rs = stmt.executeQuery(sql);

            ResultSetMetaData rsMetaData = rs.getMetaData();

            int columnCount = rsMetaData.getColumnCount();

            while (rs.next()) {
                Object[] result = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    result[i - 1] = rs.getObject(i);
                }
                list.add(count, result);
                count++;
            }
            stmt.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public static List<String> getAlldatabystrlist() {    //获得的数据用String类型的数组处理


        List<Object[]> list = GetConnToDatabase.getalldata();

        List<String> strlist = new ArrayList<String>();

        for (int i = 0, len = list.size(); i < len; i++) {

            Object[] objs = list.get(i);

            StringBuffer sb = new StringBuffer();

            for (int m = 1; m < objs.length; m++) {

                sb.append(((String) objs[m]) + ", ");

            }

            String result = sb.toString().trim();

            result = result.substring(0, result.length() - 1);

            strlist.add(result);

        }
        return strlist;
    }


    public static void setTablename(String tablename) {
        GetConnToDatabase.tablename = tablename;
    }


}
