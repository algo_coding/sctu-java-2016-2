package edu.sctu.tripadvisor.core.citytypes;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.io.IOException;

/**
 * Created by Administrator on 2016/9/29.
 */
public abstract class AbstractPageParser<T> implements PageParser<T> {
    private String url;
    private Document document;
    public String getUrl() {
        return url;
    }

    public AbstractPageParser(String url) {
        this.url = url;
    }

    @Override
    public Document getDocument() throws IOException {
        this.document =  Jsoup.connect(url).get();
        return document;
    }

    @Override
    public abstract T parse();
}
