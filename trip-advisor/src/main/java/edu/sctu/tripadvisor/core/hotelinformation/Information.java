package edu.sctu.tripadvisor.core.hotelinformation;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by cc on 2016/10/8.
 */
//胡周
public class Information {
    List<String> hotelName=new ArrayList<String>();
    List<String> hotelRank=new ArrayList<String>();
    List<String> hotelComment=new ArrayList<String>();
    public List<String> name(){
        try {PageForwarding pageUrl=new PageForwarding();
            List<String> list = pageUrl.getpageforwarding();
            Iterator<String> li = list.iterator();
            while (li.hasNext()) {
                String hotelUrl = li.next();
                Document document = Jsoup.connect(hotelUrl).get();
                for (Element element : document.select(".property_title ")) {
                    hotelName.add(element.text());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hotelName;
    }
    public List<String> ranking(){
        try {PageForwarding pageUrl=new PageForwarding();
            List<String> list = pageUrl.getpageforwarding();
            Iterator<String> li = list.iterator();
            while (li.hasNext()) {
                String hotelUrl = li.next();
                Document document = Jsoup.connect(hotelUrl).get();
                for (Element element : document.select(".slim_ranking ")) {
                    hotelRank.add(element.text());
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return hotelRank;
    }

    public List<String> comment(){
        try {PageForwarding pageUrl=new PageForwarding();
            List<String> list = pageUrl.getpageforwarding();
            Iterator<String> li = list.iterator();
            while (li.hasNext()) {
                String hotelUrl = li.next();
                Document document = Jsoup.connect(hotelUrl).get();
                for (Element elenment : document.select(".more")) {
                    hotelComment.add(elenment.text());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hotelComment;
    }


    }
