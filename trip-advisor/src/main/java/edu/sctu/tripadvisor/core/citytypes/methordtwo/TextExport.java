package edu.sctu.tripadvisor.core.citytypes.methordtwo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by 小可爱罗希 on 2016/10/27.
 */
public class TextExport <T>{
    public void export(List<T> list,String filname) throws InvocationTargetException, IllegalAccessException {
        for( T t :list){
            Class <T> Gclass =(Class<T>) t.getClass();
            Method[] methods=Gclass.getMethods();
            for (Method method:methods){
               if(method.getName().substring(0,3).equals("get")) {
                   if (!(method.getName().equals("getClass"))) {
                       System.out.println(method.invoke(t));
                   }
               }

            }
        }

    }


}
