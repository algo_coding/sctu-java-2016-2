package edu.sctu.tripadvisor.core.pageparser;

import edu.sctu.tripadvisor.core.bean.TripAdvisorUser;
import edu.sctu.tripadvisor.core.pageparser.AbstractPageParser;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by Administrator on 2016/9/29.
 */
public  class HomePagePageParser extends AbstractPageParser<TripAdvisorUser> {

    public HomePagePageParser(String url) {
        super(url);
    }

    @Override
    public Document getDocument() throws IOException {
        return null;
    }

    @Override
    public TripAdvisorUser parse() {

        return null;
    }

}
