package edu.sctu.tripadvisor.core.citytypes;

import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by Administrator on 2016/9/29.
 */
public interface PageParser<T> {


    public Document getDocument() throws IOException;

    public T parse();
}
