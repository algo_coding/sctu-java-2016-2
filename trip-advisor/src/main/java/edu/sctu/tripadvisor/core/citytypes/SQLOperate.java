package edu.sctu.tripadvisor.core.citytypes;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eoly on 2016/6/11.
 */
public class SQLOperate {
    private static String ipAddress = "118.178.86.71";
    private static int port = 3306;
    private static String databaseName="elite";
    private static String characterEncoding = "utf8";
    private static boolean useSSL = false;
    private static String url = "jdbc:mysql://"+ipAddress+":"+port+"/"+databaseName+"?characterEncoding="+characterEncoding+"&useSSL="+useSSL+"&serverTimezone=UTC"+"&useUnicode=true";
    private static String user = "elite";
    private static String keys = "elite";
    private static Connection conn;
    private static int connectTimes=0;

    //获得数据库连接
    public static Connection getConn() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, keys);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connectTimes++;
        return conn;
    }

    //对数据库进行删除操作
    //key：表主关键字；TableName：表名；
    public static void delete(String key,String TableName) throws SQLException {
        String sql = "DELETE FROM "+TableName+" WHERE KeyToRE=" + addQuote(key);
        SQLexecute(sql);
    }

    //对数据库进行更新操作（value为String型）
    public static void update(String TableName, String name, String key,String keyfield ,String value) throws SQLException {
        String sql = "UPDATE "+TableName+" SET " +
                name + "=" + addQuote(value) +
                " WHERE "+keyfield+"=" + addQuote(key);
        SQLexecute(sql);
    }

    //对数据库进行更新操作（value为int型）
    public static void update(String TableName,String name, String key,String keyfield, int value) throws SQLException {
        String sql = "UPDATE "+TableName+" SET " +
                name + "=" + value +
                " WHERE "+keyfield+"=" + addQuote(key);
        SQLexecute(sql);
    }

    //对数据库进行插入操作，并返回是否操作成功
    //TableName：表名；name：每列关键字；value：对应关键字插入值；
    public static boolean insert(String TableName, String[] name, String[] value) throws SQLException {
        if (name.length == value.length) {
            String sql = "INSERT INTO " + TableName + " (";
            for (int i = 0; i < name.length; i++) {
                sql = sql + name[i] + (i == name.length - 1 ? ") VALUE(" : ",");
            }
            for (int i = 0; i < name.length; i++) {
                sql = sql + addQuote(value[i]) + (i == value.length - 1 ? ") " : ",");
            }
            SQLexecute(sql);
            return true;
        } else return false;
    }

    //对数据库进行插入操作（注明key的位置），并返回是否操作成功
    //TableName：表名；name：每列关键字；value：对应关键字插入值；key：表的主关键字在name的索引号位置；
    public static boolean insert(String TableName, String[] name, String[] value, int keyindex) throws SQLException {
        String sql;
        if (!isNullTable(TableName)) {
            sql = "select " + name[keyindex] + " from " + TableName;
            Statement stmt = getStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if(rs.getString(name[keyindex]).equals(value[keyindex])) {
                    return false;
                }
            }
            stmt.close();
        }
        if (insert(TableName,name,value))
            return true;
        else return false;
    }

    //对数据库进行查找操作，并返回一个从服务端获得的List集合
    //TableName：表名；fieldname：每列关键字；value：对应关键字插入值；isLike：是否为模糊查找；
    public static List<Object[]> select (String TableName,String[] fieldname,String[] value,boolean isLike) throws SQLException {
        List list = new ArrayList();
        String sql;
        int count=0;
        if (!fieldname[0].equals("")) {
            sql = "SELECT *FROM " + TableName + " WHERE ";
            for (int i = 0; i < fieldname.length; i++) {
                sql = sql + fieldname[i] + (isLike ? " LIKE " : " =") +
                        addPercentSigns(value[i], isLike) + (i == fieldname.length - 1 ? "" : " or ");
            }
        } else {
            sql = "SELECT *FROM " + TableName;
        }
        Statement stmt = getStatement();
        ResultSet rs = stmt.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();
        while (rs.next()) {
            Object[] result = new Object[columnCount];
            for (int i = 1; i <= columnCount; i++) {
                result[i - 1] = rs.getObject(i);
            }
            list.add(count,result);
            count++;
        }
        stmt.close();
        return list;
        }


    //创建表
    public static void createTable(String TableName, String[] field) throws SQLException {
        String sql = "CREATE TABLE if not exists " + TableName + " ( ";
        for (int i = 0; i < field.length; i++) {
            sql = sql + field[i] + (i == field.length - 1 ? ") " : ",");
        }
        SQLexecute(sql);
    }

    //实现sql语句在数据库的实现，仅限增删改
    private static void SQLexecute(String sql) throws SQLException {
        Statement stmt = getStatement();
        stmt.executeUpdate(sql);
        stmt.close();
    }

    //获得数据库连接的Statement
    private static Statement getStatement() throws SQLException {
        while (conn == null){
            if(connectTimes<4) {
                getConn();
            }else {
                System.out.println("连接出错");
                System.exit(-1);
            }
        }
        Statement stmt = conn.createStatement();
        return stmt;
    }

    //判断数据库对应表是否为空表
    public static boolean isNullTable(String TableName) throws SQLException {
        String sql = "select count(*) from "+TableName;
        Statement stmt = getStatement();
        ResultSet rs = stmt.executeQuery(sql);
        rs.next();
        if(rs.getInt(1)==0)return true;
        else return false;
    }

    public static boolean checkValueExist(String tableName,String fieldName,String value){
        String[] name= {fieldName};
        String[] findValue= {value};
        List<Object[]> objList;
        try {
            objList=select(tableName,name,findValue,false);
        } catch (SQLException e) {
            e.printStackTrace();
            return true;
        }
        for (Object[] objects:objList){
            if(objects[0]!=null)return true;
        }
        return false;
    }

    //对一个字符串增加百分号和单引号，如：'%string%'
    private static String addPercentSigns(String str,boolean isLike){
        return isLike?addQuote("%"+str+"%"):addQuote(str);
    }

    //对一个字符串单引号，如：'string'
    private static String addQuote(String str) {
        return "'" + str + "'";
    }

}