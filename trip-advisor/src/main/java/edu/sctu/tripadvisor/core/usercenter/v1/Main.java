package edu.sctu.tripadvisor.core.usercenter.v1;

import java.util.ArrayList;

/**
 * Created by Huang Suixin on 2016/9/29.
 */
public class Main {
    public static void main(String[] args){
//        String url = "jdbc:mysql://172.16.10.37:3306/elite?useUnicode=true&amp;characterEncoding=UTF-8";
//        String user = "elite";
//        String password = "elite";
        String url = "jdbc:mysql://localhost:3306/test?serverTimezone=UTC&useSSL=false";
        String user = "root";
        String password = "";
        String table="hotelcommentlist";
        String column="peopleNamelist";


//        目标服务器数据库
        String url1 = "jdbc:mysql://118.178.86.71/elite?useUnicode=true&amp;characterEncoding=UTF-8";
        String user1 = "elite";
        String password1 = "elite";

        DBConnection dbConnection = new DBConnection(url1,user1,password1);
        DBConnection db = new DBConnection(url1,user1,password1);


        HomePagePageParser homePagePageParser;
        ArrayList<String> userNames = dbConnection.getData(table, column);
        System.out.println();
        System.out.println(userNames.size());
        System.out.println(userNames);
        if (userNames.size()==0){
            System.out.println("链接为空");
            return;
        }else
        for (int i = 0;i<userNames.size();i++){
            homePagePageParser = new HomePagePageParser(userNames.get(i));
            TripAdvisorUser tripAdvisorUser = homePagePageParser.parse();
//            System.out.println(tripAdvisorUser.getNickName());
//            System.out.println(tripAdvisorUser.getRegistrationDate());
//
//
//保存
            if (db.saveData(tripAdvisorUser)) {
                System.out.println(tripAdvisorUser.getNickName()+" 用户添加成功\n");
            } else {
                System.out.println(tripAdvisorUser.getNickName()+" 用户添加失败！\n");
            }
        }
    }
}
