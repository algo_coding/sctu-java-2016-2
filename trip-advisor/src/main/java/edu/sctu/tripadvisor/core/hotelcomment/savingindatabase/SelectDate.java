package edu.sctu.tripadvisor.core.hotelcomment.savingindatabase;

import java.io.IOException;

/**
 * Created by 落叶的思恋 on 2016/10/14.
 */
//获取数据方法的继承
interface SelectDate {
    String[] getpeopleNamelist(String url)throws IOException;
    String[] getcommnettime(String url) throws IOException;
    String[] getCommentlist(String url)throws IOException;
    String[] getCommentEquipment(String url)throws IOException;
    String getHotelname(String url) throws IOException;
    String[] getfeel(String url)throws IOException;
    String[] getgrade(String url)throws IOException;
}
