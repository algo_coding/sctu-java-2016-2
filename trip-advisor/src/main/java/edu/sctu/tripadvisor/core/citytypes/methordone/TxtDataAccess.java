package edu.sctu.tripadvisor.core.citytypes.methordone;

import edu.sctu.tripadvisor.core.citytypes.DataAccess;
import edu.sctu.tripadvisor.core.citytypes.ViewPlaces;

import java.io.*;
import java.util.List;

/**
 * Created by Eoly on 2016/10/20.
 */
public class TxtDataAccess implements DataAccess<ViewPlaces,List<Object[]>> {
    private String filename = "d:\\CityInformation.txt";

    @Override
    public void saveInfor(ViewPlaces usedData) {
        File txtFile = new File(filename);
        try {
//            byte[] a = usedData.toString().getBytes();
//            FileOutputStream fileOutputStream = new FileOutputStream(txtFile,true);
//            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
//            bufferedOutputStream.write(a);
//            bufferedOutputStream.write('\n');
////            System.out.println(usedData);
//            bufferedOutputStream.close();
            FileWriter fileWriter = new FileWriter(txtFile,true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.newLine();
            bufferedWriter.write(usedData.toString());
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Object[]> readInfor() {

        DataBaseDataAccess dataBaseDataAccess = new DataBaseDataAccess();
        return dataBaseDataAccess.readInfor();
    }
}
