package edu.sctu.tripadvisor.core.citytypes;

import edu.sctu.tripadvisor.core.citytypes.methordone.Demo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eoly on 2016/10/10.
 */
public class CityTypeDataInformation implements DataInformation<List<String>> {

    @Override
    public List<String> readUrlFromDatabase(String type) {
        String name[]={"type"};
        String value[]={type};
        String tableNmae = "informationInCity";
        List<String> strList = new ArrayList<String>();
        try {
            List<Object[]> objLists = SQLOperate.select(tableNmae, name, value, false);
            for(Object[] objs:objLists){
                strList.add((String)objs[1]);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return strList;
    }

    @Override
    public List<String>readUrlFromWeb(String type){
        List<String> strList = new ArrayList<String>();
        List<List<ViewPlaces>> outofList= Demo.start(false);
        for(List<ViewPlaces> viewList:outofList){
            for(ViewPlaces viewPlaces:viewList){
                if(viewPlaces.getType().equals(type)){
                    strList.add(viewPlaces.getUrl());
                }
            }
        }
        return strList;
    }
}
