package edu.sctu.tripadvisor.core.commentinformation;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by dell on 2016/9/27.
 */
public class LookUp {
    public static void main(String[] args) {
        /* 第一页*/
        try {

            System.out.println("//////////////////////////第一页////////////////////////////");
            Document document= Jsoup.connect("http://www.tripadvisor.cn/Hotel_Review-g297463-d7206943-Reviews-The_Temple_House-Chengdu_Sichuan.html").get();
            document.location();
            Elements NameEle=document.select("div.username");
            System.out.println("第一页点评者昵称：" + NameEle.text());
//            点评者昵称
            Elements CaptionEle=document.select("span.noQuotes");
            System.out.println("第一页标题："+CaptionEle.text());
//            标题
            Elements TimeEle=document.select("span.ratingDate").select(".relativeDate");
            System.out.print("第一页时间：");
            System.out.print(" " + TimeEle.text());
//            Elements TimeEle2=document.select("span.ratingDate").select(".relativeDate");
//            for (Element j:TimeEle2 ){
//                System.out.print(" " + j.attr(""));
//            }
            System.out.println();
            Elements ScoreEle=document.select("img.sprite-rating_s_fill");
            int n=0;
            System.out.print("第一页打分：");
            for (Element j:ScoreEle ){n++;
                if (n>=11)break;
                System.out.print(" " + j.attr("alt"));
            }
            System.out.println("");
//            打分
            Elements YonEle=document.select("a.viaMobile").select(".sprite-grayPhone");

            System.out.println("第一页是否为移动设备："+YonEle.text().trim());

            Elements  ViewEle=document.select("div.entry>p");
            StringBuffer stringBuffer=new StringBuffer();
            int num0=1;
            for(Element li:ViewEle )
            {
                stringBuffer.append("\n"+"\n"+"第"+num0+"位的评价："+li.text().trim());
                num0++;
            }
            System.out.println("第1页评论内容："+ stringBuffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("/////////////////////////////////////////////////////////////");

        /* 第二页*/
        int m=1;
        for (int n=10;n<=80;n=n+10) {
            m++;
            try {
                String ss="http://www.tripadvisor.cn/Hotel_Review-g297463-d7206943-Reviews-or"+n+"-The_Temple_House-Chengdu_Sichuan.html#REVIEWS";
                Document document= Jsoup.connect(ss ).get();
                document.location();
                Elements NameEle=document.select("div.username");
                System.out.println("第" + m + "页点评者昵称：" + NameEle.text());
//            点评者昵称
                Elements CaptionEle=document.select("span.noQuotes");
                System.out.println("第"+ m+"页标题："+CaptionEle.text());
//            标题
                Elements TimeEle=document.select("span.ratingDate");
                System.out.print("第" + m + "页时间：");
                System.out.print(" " + TimeEle.text());
                System.out.println();
                Elements ScoreEle=document.select("img.sprite-rating_s_fill");
                int nmu=0;
                System.out.print("第"+ m+"页打分：");
                for (Element j:ScoreEle ){nmu++;
                    if (n>=11)break;
                    System.out.print(" " + j.attr("alt"));
                }
                System.out.println("");
//            打分
                Elements YonEle=document.select("a.viaMobile").select(".sprite-grayPhone");
                System.out.println("第"+ m+"页是否为移动设备："+YonEle.text());
//            View
                Elements  ViewEle=document.select("div.entry>p");
                StringBuffer stringBuffer=new StringBuffer();
                int num=1;
                for(Element li:ViewEle )
                {
                    stringBuffer.append("\n"+"\n"+"第"+num+"位的评价："+li.text().trim());
                    num++;
                }
                System.out.println("第"+ m+"页评论内容："+ stringBuffer);
            } catch (IOException e) {
                e.printStackTrace();
            }





            String cN=null;
            String Rank=null;
            String Url=null;
            //声明Connection对象
            Connection con;
            //驱动程序名
            String driver = "com.mysql.jdbc.Driver";
            //URL指向要访问的数据库名mydata
            String url = "jdbc:mysql://localhost:3306/test?useUnicode=true&amp;characterEncoding=UTF-8";
            //MySQL配置时的用户名
            String user = "test";
            //MySQL配置时的密码
            String password ="";
            //遍历查询结果集
            try {
                //加载驱动程序
                Class.forName(driver);
                //1.getConnection()方法，连接MySQL数据库！！
                con = DriverManager.getConnection(url, user, password);
                //String sql="create table hotelcomment( hotleName char(100),commentName char(100),grade char(100),conmment char(100), commentEquipment char(100),satisfaction char(100),ommentTimec char(100) )";
                String hotel;
                String sql = "insert into  hotel values('ss','TimeEle.text()','vv',stringBuffer,YonEle.text(),'r','r')";
                java.sql.Statement stmt= con.createStatement();
                stmt.executeUpdate(sql);
                con.close();
            } catch(ClassNotFoundException e) {
                System.out.println("Sorry,can`t find the Driver!");
                e.printStackTrace();
            } catch(SQLException e) {

                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("/////////////////////////////////////////////////////////////");
        }


    }
}
