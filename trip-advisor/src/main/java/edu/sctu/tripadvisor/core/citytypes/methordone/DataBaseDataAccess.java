package edu.sctu.tripadvisor.core.citytypes.methordone;




import edu.sctu.tripadvisor.core.citytypes.DataAccess;
import edu.sctu.tripadvisor.core.citytypes.SQLOperate;
import edu.sctu.tripadvisor.core.citytypes.ViewPlaces;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Eoly on 2016/6/11.
 */
public class DataBaseDataAccess implements DataAccess<ViewPlaces,List<Object[]>> {
    private static String tableName = "informationInCity";
    private static String tableReadName = "tourcity";

    //数据库初始化函数，若数据不存在或对应表不存在则调用数据库函数创建并初始化
    public static void create() throws SQLException {

        SQLOperate.createTable(tableName,ViewPlaces.colunmName);

    }


    public List<Object[]> readInfor(){
        System.out.println("获取数据库信息来源url...");
        List<Object[]> list;
        String name[]={""};
        String value[]={""};
        try {
            list = SQLOperate.select(tableReadName,name,value,false);
            System.out.println("获取url成功");
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveInfor(ViewPlaces viewPlaces) {
        String name[] = {"city_type", "url", "url_belongs", "type", "city_belongs", "quantity", "quantity_view"};
        String value[] = {viewPlaces.getCityBelongs() + viewPlaces.getType(), viewPlaces.getUrl(), viewPlaces.getUrlBelongs(), viewPlaces.getType(), viewPlaces.getCityBelongs(), viewPlaces.getQuantity(), viewPlaces.getQuantityView()};
        try {
            SQLOperate.insert(tableName, name, value, 0);
            System.out.println("写入数据库（" + viewPlaces.getCityBelongs() + viewPlaces.getType() + "）成功");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}