package edu.sctu.tripadvisor.core.citytypes;

/**
 * Created by Eoly on 2016/10/10.
 */
public interface DataInformation<T>{
    public T readUrlFromDatabase(String type);
    public T readUrlFromWeb(String type);
}
