package edu.sctu.tripadvisor.core.usercenter.v2.export;


import edu.sctu.tripadvisor.core.usercenter.v2.TripAdvisorUser;

import java.io.IOException;
import java.util.List;

/**
 * Created by huangsuixin on 2016/10/20.
 */
public interface  UserExporter {
    public void exportToFile(List<TripAdvisorUser> userList, String fileName) throws IOException;
}
