package edu.sctu.tripadvisor.core.hotCity.v1;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by 李帅 on 2016/10/1
 */
public abstract class AbstractParser<T> implements HotCityParser<String> {
    private String url;
    private Document document;
    public AbstractParser(String url){
        this.url = url;
    }
    public String getUrl(){
        return url;
    }
    @Override
    public Document getDocument() {
        try {
            this.document = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return document;
    }

    @Override
    public abstract String parse();


}
