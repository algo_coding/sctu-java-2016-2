package edu.sctu.tripadvisor.core.usercenter.v1;

/**
 * Created by huangsuixn on 2016/10/10.
 */
public interface SaveData {
    public boolean saveData(TripAdvisorUser user);
}
