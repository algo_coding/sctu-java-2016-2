package edu.sctu.tripadvisor.core.hotCity.v2.export;

import edu.sctu.tripadvisor.core.hotCity.v2.CustomerDemand;
import edu.sctu.tripadvisor.core.hotCity.v2.DBOperation;
import edu.sctu.tripadvisor.core.hotCity.v2.PropertiseConstructors;

import java.io.*;
import java.util.List;

/**
 * Created by 李帅 on 2016/10/20
 */
public class exportToTxtIml implements exportToTxt{
    DBOperation dbOperation = new DBOperation();
    @Override
    public void export(String fileName) {
        File file = new File(fileName);
        if(!file.exists()){
            file.mkdirs();
        }

        String f="data.txt";
        File file1 = new File(fileName,f);
        if(!file1.exists()){
            try {
                file1.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file1));
            List<String> list1 = dbOperation.Select();
            for (int i=0;i<list1.size();i++){
                bufferedWriter.write(list1.get(i));
                bufferedWriter.flush();
                bufferedWriter.newLine();
            }
            } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    }



