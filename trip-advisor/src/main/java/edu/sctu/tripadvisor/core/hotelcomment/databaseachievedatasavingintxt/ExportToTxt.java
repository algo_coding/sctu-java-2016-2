package edu.sctu.tripadvisor.core.hotelcomment.databaseachievedatasavingintxt;

import java.io.IOException;

/**
 * Created by dell on 2016/10/24.
 * 输出到文本的方法。
 */
interface  ExportToTxt {
    public void export( String fileName,String table) throws IOException;


}
