package edu.sctu.tripadvisor.core.citytypes.methordone;

import edu.sctu.tripadvisor.core.citytypes.ViewPlaces;
import edu.sctu.tripadvisor.core.pageparser.AbstractPageParser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Eoly on 2016/9/22.
 * alt + enter
 */
public class InformationInCityParser extends AbstractPageParser<List<ViewPlaces>> {
    private String city;
    private boolean isWriteData;

    public InformationInCityParser(String url, String city,boolean isWriteData) {
        super(url);
        this.city=city;
        this.isWriteData = isWriteData;
    }

    @Override
    public List<ViewPlaces> parse() {
        List<ViewPlaces> list = new ArrayList<ViewPlaces>();
        String type="",quantity="",quantityView="",url="";
        try {
            Document document = getDocument();
            Element divElement = document.select("div.navLinks").first();
            Elements elemA = divElement.select("li a");
            for (Element element:elemA) {
                Elements elemSpan = element.select("span");
                for(Element elem:elemSpan){
                    if(elem.className().equals("typeName")){
                        type=elem.text();
                    }else if(elem.className().equals("typeQty")){
                        quantity=elem.text();
                    }else if(elem.className().equals("contentCount")){
                        quantityView=elem.text();
                    }
                }
                url = element.attr("abs:href");

                ViewPlaces viewPlaces = new ViewPlaces(city,getUrl());
                viewPlaces.setType(type == null ? "未定义类型" : type);
                viewPlaces.setQuantity(quantity == null ? "无" : makeGettingNumberOnly(quantity));
                viewPlaces.setQuantityView(quantityView == null ? "无点评" : makeGettingNumberOnly(quantityView));
                viewPlaces.setUrl(url == null ? "无链接地址" : url);

                if(isWriteData) {
//                    DataBaseDataAccess dataBaseProcesser = new DataBaseDataAccess();
//                    dataBaseProcesser.saveInfor(viewPlaces);
                    TxtDataAccess txtProcesser = new TxtDataAccess();
                    txtProcesser.saveInfor(viewPlaces);
                }
                else {
                    list.add(viewPlaces);
                    System.out.println("获取（" + viewPlaces.getCityBelongs() + viewPlaces.getType() + "）成功");
                }
            }
            return list;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public String makeGettingNumberOnly(String data){

        String regEx="[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(data);

        return m.replaceAll("").trim();
    }
}