package edu.sctu.tripadvisor.core.citytypes;


import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 小可爱罗希 on 2016/10/8.
 */
public class WebParser extends AbstractPageParser<List<ViewPlaces>>{
    String city;

    public WebParser(String url, String city) {
        super(url);
        this.city =city;
    }


    @Override
    public List<ViewPlaces> parse() {
        Document document= null;
        List<ViewPlaces> vplace=new ArrayList<ViewPlaces>();
        try {
            document = getDocument();
            Elements types=document.select("div.navLinks").select("li a");//找出没有div的li
            for(Element a: types){
                ViewPlaces information=new ViewPlaces(city,getUrl());
               Elements spans=a.select("span");
                for (Element isnull:spans){                     //通过查找进行判断span的class，
                                                                //进而判断是否存在这个数据。以免select返回值为空，出错
                   String className= isnull.attr("class");
                    if (className.equals("typeQty")){
                        information.setQuantity(isnull.text());
                    }else if (className.equals("contentCount")){
                        information.setQuantityView(isnull.text());
                    }else if(className.equals("typeName")){
                        information.setType(isnull.text());
                    }
                }
//                String quantity=a.select("span.typeQty").first().text();
//                    information.setQuantity(quantity);
//                String quantityView=a.select("span.contentCount").first().text();
//                    information.setQuantityView(quantityView);
//                String type=a.select("span.typeName").first().text();
//                    information.setType(type);
                String link=a.select("a").attr("abs:href");
                    information.setUrl(link);
                vplace.add(information);
            }
         }catch(IOException e) {
            e.printStackTrace();
        }
        return vplace;
    }
}
