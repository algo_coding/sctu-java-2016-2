package edu.sctu.tripadvisor.core.usercenter.v2;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by Administrator on 2016/9/29.
 */
public class HomePagePageParser extends AbstractPageParser<TripAdvisorUser> {

    private String username;

    public HomePagePageParser(String username) {
        this.username = username;
    }

    @Override
    public TripAdvisorUser parse() {
        String url="http://www.tripadvisor.cn/members/"+username;
//    图片评论的网站url2
        String url2="http://www.tripadvisor.cn/members-photos/"+username;
        TripAdvisorUser tripAdvisorUser = new TripAdvisorUser();
//        获得document
        Document document = null;
        Document document1 = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            document = getDocument(url);
//            必须获取document
            tripAdvisorUser.setNickname(document.select(".nameText").text());
            tripAdvisorUser.setRegistrationDate(document.select(".since").text());
            tripAdvisorUser.setHeadPortrait(document.select(".avatarUrl").attr("src"));
            tripAdvisorUser.setHometown(document.select(".hometown").select("p").text());
            tripAdvisorUser.setPoints(document.select("div.points").first().text());
            tripAdvisorUser.setLevels(document.select(".tripcollectiveinfo").select("span").text());

            Elements review = document.select(".cs-filter-bar").select("li");
            for (int i =1;i<review.size();i++){
                stringBuilder.append(review.get(i).text()+" ");
            }
            tripAdvisorUser.setCommentNumber(document.select(".active").select("a").text() + ": " + stringBuilder.toString());

        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("getDocument网页获取失败"+username);
            return tripAdvisorUser;
        }


//获取图片评论条数
        try {
            document1 = getDocument(url2);
            stringBuilder.delete(0,stringBuilder.length());
            Elements picview = document1.select(".cs-filter-bar ul").select("li");
            for (int i =1;i<picview.size();i++){
                stringBuilder.append(picview.get(i).text()+" ");
//            System.out.print(review.get(i).text() + " ");
            }
            tripAdvisorUser.setCommentPicNumber(document1.select(".cs-contribution-bar li").get(1).select("a").text()+": "+stringBuilder.toString());
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("getDocument图片评论页面获取失败！失败原因：此用户尚未有图片评论");
            tripAdvisorUser.setCommentPicNumber(null);
        }

        return tripAdvisorUser;
    }
}
