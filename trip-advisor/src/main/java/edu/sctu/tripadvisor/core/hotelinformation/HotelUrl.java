package edu.sctu.tripadvisor.core.hotelinformation;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by hbl on 2016/10/11.
 */
public class HotelUrl {

    public List<String> hotelUrl() {
        List<String> setUrl = new ArrayList<String>();
        PageForwarding pageForwarding = new PageForwarding();
        List<String> getUrl = pageForwarding.getpageforwarding();
        Iterator<String> Urliterator = getUrl.iterator();
        while (Urliterator.hasNext()) {
            Document doc = null;

            String Urlnext = Urliterator.next();
            try {
                doc = Jsoup.connect(Urlnext).get();
                Elements elements = doc.select(".property_title ");
                String a = elements.attr("href");
                setUrl.add("http://www.tripadvisor.cn/" + a);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return setUrl;
    }

}
