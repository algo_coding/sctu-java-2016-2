package edu.sctu.tripadvisor.core.usercenter.v1;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 黄随心 on 2016/10/11.
 */
public interface GetData {
    public ArrayList<String> getData(String table,String column);

}
