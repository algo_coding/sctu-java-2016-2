package edu.sctu.tripadvisor.core.hotCity.v2.export;

import edu.sctu.tripadvisor.core.hotCity.v2.PropertiseConstructors;

import java.util.List;

/**
 * Created by 李帅 on 2016/10/20
 */
public interface exportToTxt {

    public void export(String fileName);
}
