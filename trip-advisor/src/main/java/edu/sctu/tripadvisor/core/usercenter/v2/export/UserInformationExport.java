package edu.sctu.tripadvisor.core.usercenter.v2.export;


import edu.sctu.tripadvisor.core.usercenter.v2.TripAdvisorUser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by shc on 2016/10/20.
 */
public class UserInformationExport implements UserExporter {

    @Override
    public void exportToFile(List<TripAdvisorUser> userList, String fileName) {

        String url = "D://";
        File file = new File(url + fileName + ".txt"); // /为文件位置
        if (file.exists()) {
            System.out.println("--------------------目标文件存在-----------------------");
        } else {
            System.out.println("正在创建目标文件" + url + fileName);
            try {
                file.createNewFile();
            } catch (IOException e) {
//                    e.printStackTrace();
                System.out.println("创建文件失败");
            }
        }
        BufferedWriter input = null;
        try {
            input = new BufferedWriter(new FileWriter(file));
            for (TripAdvisorUser tripAdvisorUser : userList) {
                input.write("NickName:"+tripAdvisorUser.getNickName() + " RegistrationDate:" + tripAdvisorUser.getRegistrationDate() + " HeadPortrait:" + tripAdvisorUser.getHeadPortrait() + " Hometown:" + tripAdvisorUser.getHometown() + " Points" + tripAdvisorUser.getPoints() + " Levels:" + tripAdvisorUser.getLevels() + " CommentNumber" + tripAdvisorUser.getCommentNumber() + " CommentPicNumber:" + tripAdvisorUser.getCommentPicNumber());
                input.newLine();
            }
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("创建数据输入缓存流错误");
        } finally {
            try {
                input.close();
            } catch (IOException e) {
//                e.printStackTrace();
                System.out.println("BufferedWriter关闭失败");
            }
        }
    }
}
