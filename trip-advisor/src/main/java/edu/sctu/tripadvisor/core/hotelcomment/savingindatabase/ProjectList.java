package edu.sctu.tripadvisor.core.hotelcomment.savingindatabase;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by 落叶的思恋 on 2016/10/10.
 */
 class  ProjectList  implements SelectDate {
    Document document;
    String[] comment = new String[50];
    String[] getpeopleNamelist = new String[50];
    String[] getcommnettime = new String[50];
    String getHotelname;

    String[] getfeel = new String[50];
    String[] grade = new String[50];
    String[] getcommentequipment = new String[50];


    //找到昵称
    @Override
    public String[] getpeopleNamelist(String url) throws IOException {
        document = Jsoup.connect(url).get();
        document.location();
        Elements NameEle = document.select("div.username");
        String ss = NameEle.text();
        getpeopleNamelist = ss.split(" ");

        return getpeopleNamelist;
    }
    //找到评论时间
    @Override
    public String[] getcommnettime(String url) throws IOException {
        document = Jsoup.connect(url).get();
        document.location();
        Elements TimeEle = document.select("span.ratingDate");
        String ss = TimeEle.text();
        getcommnettime = ss.split(" ");

        return getcommnettime;
    }
    //找到评论内容
    @Override
    public String[] getCommentlist(String url) throws IOException {
        document = Jsoup.connect(url).get();
        document.location();
        Elements ViewEle = document.select("div.entry>p");

        int num = 0;
        for (Element getCommentlist : ViewEle) {
            comment[num] = getCommentlist.text().trim();
            num++;
        }
        return comment;
    }

    //找到评论设备
    @Override
    public String [] getCommentEquipment(String url) throws IOException {
        document = Jsoup.connect(url).get();
        document.location();
        Elements commenteq = document.select("div.rating").select(".reviewItemInline");
        String arr[] = new String[10];
        String ar[] ;
        int num = 0;
        String dd = null;
        for (Element gd : commenteq) {
            arr[num] = gd.text();
            String s=arr[num];
            ar=s.split(" ");
            for (int nu = 0; nu < ar.length ; nu++) {

             if(ar[nu].equals("通过移动设备发表"))
             {
                 dd="用移动设备发表";
             }
                else {
                 dd="非移动设备发表";
             }

            }

            getcommentequipment[num]=dd;
            num++;
        }


            return getcommentequipment;
        }
    //找到酒店名
    @Override
    public String getHotelname(String url) throws IOException {
        Document document = Jsoup.connect(url).get();
        document.location();
        Elements TimeEle = document.select("h1.heading_name");
        getHotelname = TimeEle.text().trim();

return getHotelname;
    }
    //找满意程度
    @Override
    public String[] getfeel(String url) throws IOException {
        document = Jsoup.connect(url).get();
        document.location();
        Elements feeling = document.select("span.noQuotes");
        String ss = feeling.text();
        getfeel = ss.split(" ");
    return getfeel;
    }
    //找到打分
    @Override
    public String[] getgrade(String url) throws IOException {
        document = Jsoup.connect(url).get();
        document.location();
        Elements ScoreEle = document.select("img.sprite-rating_s_fill");
        int n = 0;
        for (Element gde : ScoreEle) {
            grade[n] = gde.attr("alt");
            n++;
        }

    return grade;
    }
}



