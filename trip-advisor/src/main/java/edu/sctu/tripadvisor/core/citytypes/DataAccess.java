package edu.sctu.tripadvisor.core.citytypes;

/**
 * Created by Eoly on 2016/10/8.
 */
public interface DataAccess<T,L>{
    public void saveInfor(T usedData);
    public L readInfor();
}
