package edu.sctu.tripadvisor.core.restaurantlist;

import java.sql.*;

/**
 * Created by Administrator on 2016/10/8.
 */
public class DatabaseOperate {


 Connection conn;


 String driver = "com.mysql.jdbc.Driver";

 String url = "jdbc:mysql://172.16.10.37:3306/elite?user=elite&password=elite&useUnicode=true&characterEncoding=UTF8";

 String user = "elite";

 String password = "elite";

 //connect database;
 public Connection connectDatabase() throws SQLException, ClassNotFoundException {

  Class.forName(driver);
  conn = DriverManager.getConnection(url, user, password);

  return conn;
 }


 // create table;
 public void createTable(String TableName, String... field) throws SQLException {
  String sql = "CREATE TABLE if not exists " + TableName + " ( ";
  for (int i = 0; i < field.length; i++) {
   sql = sql + field[i] + (i == field.length - 1 ? ") " : ",");
  }
  Statement statement = conn.createStatement();
  statement.executeUpdate(sql);
  conn.close();
 }

 //insert data to table;
 public void insertDataFiled(String name, String rank, String number)
         throws SQLException, ClassNotFoundException {

  PreparedStatement sql;
  sql = conn.prepareStatement("insert into restaurantinformation (restaurantname,restaurantrank,reviewnumber) " +
          "values (?,?,?)");
  sql.setString(1, name);
  sql.setString(2, rank);
  sql.setString(3, number);



  sql.executeUpdate();

 }

 //insert data to table;
 public void insertData(String[] name, String[] rank, String[] number)
         throws SQLException, ClassNotFoundException {
  int count;
  PreparedStatement sql;



  for (count = 0; count < name.length; count++) {

   sql = conn.prepareStatement("insert into restaurantinformation (restaurantname,restaurantrank,reviewnumber) " +
           "values (?,?,?,?)");
   sql.setString(1, name[count]);
   sql.setString(2, rank[count]);
   sql.setString(3, number[count]);
   sql.executeUpdate();

  }

 }

 //delete table;
 public void delectData() throws SQLException, ClassNotFoundException {

  Statement statement = conn.createStatement();
  String sql = "delete from restaurantinformation ";
  statement.executeUpdate(sql);

 }

 //select data by keyword;
 public void selectDataByKey(String keyword) throws SQLException, ClassNotFoundException {

  Statement statement = conn.createStatement();
  String sql = "select * from restaurantinformation where restaurantname like " + "'%" + keyword + "%'";
  ResultSet resultSet = statement.executeQuery(sql);
  System.out.println("id"+"\t\t"+"餐厅名字"+"\t\t"+"餐厅排名"+"\t\t"+"评论条数");
 int id;
  String name,rank,number;
  while (resultSet.next()){
   id=resultSet.getInt("id");
   name=resultSet.getString("restaurantname");
   rank=resultSet.getString("restaurantrank");
   number=resultSet.getString("reviewnumber");
   System.out.println(id+"\t\t"+name+"\t\t"+rank+"\t\t"+number);
  }

 }

 //select data by fielename;


 public String[] selectDataByFieldName(String fieldName) throws SQLException, ClassNotFoundException {

  Statement statement = conn.createStatement();
  String sql = "select(" + fieldName + ")from Restaurantinformation ";
  ResultSet resultSet = statement.executeQuery(sql);
  String[] result = new String[3292];
  int i = 0;
  while(resultSet.next()) {

   result[i] = resultSet.getString(fieldName);
    i++;
  }


  return result;

}

}

