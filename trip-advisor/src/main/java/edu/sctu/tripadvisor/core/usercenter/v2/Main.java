package edu.sctu.tripadvisor.core.usercenter.v2;

import edu.sctu.tripadvisor.core.usercenter.v2.export.UserInformationExport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Silence on 2016/10/20.
 */
/*
写到文件
 */
public class Main {
    public static void main(String[] args) {
        HomePagePageParser homePagePageParser = new HomePagePageParser("RosieZhang");
        TripAdvisorUser tripAdvisorUser =null;
        tripAdvisorUser=homePagePageParser.parse();

        ArrayList<TripAdvisorUser> userlist = new ArrayList<TripAdvisorUser>();

        userlist.add(tripAdvisorUser);

        UserInformationExport userInformationExport = new UserInformationExport();
        userInformationExport.exportToFile(userlist,"userinformation");
    }
}
