package edu.sctu.tripadvisor.core.citytypes;


/**
 * Created by Eoly on 2016/9/29.
 */
public class ViewPlaces {
    private String url;
    private String urlBelongs;
    private String type;
    private String cityBelongs;
    private String quantity;
    private String quantityView;
    public static String colunmName[] = {
            "city_type VARCHAR(20) primary key",
            "url TEXT",
            "url_belongs TEXT",
            "type VARCHAR(10)",
            "city_belongs VARCHAR(16)",
            "quantity VARCHAR(10)",
            "quantity_view VARCHAR(10)"
    };

    public ViewPlaces(String cityBelongs, String urlBlongs){
        this.urlBelongs=urlBlongs;
        this.cityBelongs=cityBelongs;
    }

    public ViewPlaces(String quantityView, String url, String urlBelongs, String type, String cityBelongs, String quantity) {
        this.quantityView = quantityView;
        this.url = url;
        this.urlBelongs = urlBelongs;
        this.type = type;
        this.cityBelongs = cityBelongs;
        this.quantity = quantity;
    }

    public String getUrlBelongs() {
        return urlBelongs;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setQuantityView(String quantityView) {
        this.quantityView = quantityView;
    }

    public String getUrl() {
        return url;
    }

    public String getType() {
        return type;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getQuantityView() {
        return quantityView;
    }

    public String getCityBelongs() {
        return cityBelongs;
    }

    @Override
    public String toString() {
        return cityBelongs+" "+type+"\t数量："+quantity+"\t点评数量："+quantityView+"\turl："+url;
    }
}