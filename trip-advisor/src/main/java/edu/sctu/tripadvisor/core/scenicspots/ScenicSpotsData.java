package edu.sctu.tripadvisor.core.scenicspots;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mike on 2016/10/15.
 */
public class ScenicSpotsData {


    public static Elements scenictitle(String url) {//返回景点名称
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Element root = document.body();
        Elements links0 = root.getElementsByClass("property_title");
        return links0;

    }


    public static Elements rank(String url) {//返回排名
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Element root = document.body();
        Elements links1 = root.getElementsByClass("popRanking");
        return links1;
    }


    public static List returnurl(String url) {//返回景点url
        List list = new ArrayList<String>();
        String scenicurl;
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements root = document.getElementsByClass("entry");
        for (Element link : root) {
            scenicurl = link.select("a").attr("abs:href");
            list.add(scenicurl);

        }
        return list;
    }

    public static List returncomment(String url) {//返回点评数
        List list = new ArrayList<String>();
        String comment;
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements root = document.getElementsByClass("entry");
        for (Element link : root) {
            if (link.select("span.more").first() == null) {
                comment = "";
                list.add(comment);
            } else {
                comment = link.select("span.more").first().text();
                list.add(comment);
            }
        }
        return list;
    }
}
