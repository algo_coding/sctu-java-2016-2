package edu.sctu.tripadvisor.core.hotCity.v2;

import org.jsoup.nodes.Document;

/**
 * Created by 李帅 on 2016/10/10
 */
public interface HotCityParser<T> {

    public Document getDocument();
    public T parse();
}


