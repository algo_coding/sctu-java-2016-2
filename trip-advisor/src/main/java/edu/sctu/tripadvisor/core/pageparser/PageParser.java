package edu.sctu.tripadvisor.core.pageparser;

import edu.sctu.tripadvisor.core.bean.TripAdvisorUser;
import org.jsoup.nodes.Document;

import javax.print.Doc;
import java.io.IOException;

/**
 * Created by Administrator on 2016/9/29.
 */
public interface PageParser<T> {


    public Document getDocument() throws IOException;

    public T parse();
}
