package edu.sctu.tripadvisor.core.hotCity.v2;

import java.sql.Connection;
import java.sql.Statement;

/**
 *
 */
public class PropertiseConstructors {
    private String cityName;
    private String rank;
    private String URL;


    public PropertiseConstructors(String cityName,String rank,String URL){
        this.cityName=cityName;
        this.rank=rank;
        this.URL=URL;
    }
    public PropertiseConstructors(){}

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
