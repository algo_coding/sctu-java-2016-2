
package edu.sctu.tripadvisor.core.restaurantlist;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;



/**
 * Created by Administrator on 2016/10/8.
 * 余卫梅
 */

public class GetRestaurantData {


    public void getAndInputData() throws SQLException, ClassNotFoundException {

        DatabaseOperate ee = new DatabaseOperate();
        //connect database;
        ee.connectDatabase();

        GetUrl gUrl = new GetUrl();
        ArrayList<String> arrayList1 = gUrl.getUrl();

        for (String http : arrayList1) {
            try {

                Document document = Jsoup.connect(http).get();
                Elements div1 = document.select("div.shortSellDetails");

                for (int i = 0; i < div1.size(); i++) {
                    Element div = document.select("div.shortSellDetails").get(i);
                    //get restaurantname ;
                    Element restName = div.select("h3.title").first();
                    //get restaurantrank ;
                    Element rank = div.select("div.popIndexBlock").first();
                    //get reviewnumber  ;
                    Element reviewNum = div.select(".rating").first();

                    String restaurantname = restName.text();
                    String restaurantrank = rank.text();
                    String reviewnumber = reviewNum.text();
                    //insert data to database;
                    ee.insertDataFiled(restaurantname, restaurantrank, reviewnumber);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }
}




