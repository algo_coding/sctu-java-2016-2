package edu.sctu.tripadvisor.core.citytypes.methordone;


import edu.sctu.tripadvisor.core.citytypes.ViewPlaces;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Eoly on 2016/9/29.
 */
public class AllParser {
    private String province;
    private List<InformationInCityParser> listCity;
    private boolean isOkey = true;

    public AllParser(List<InformationInCityParser> listCity, String province) {
        this.listCity = listCity;
        this.province = province;
    }

    public List<List<ViewPlaces>> parse(){
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(10);
        final List<List<ViewPlaces>> outofList = new ArrayList<List<ViewPlaces>>();
        for(final InformationInCityParser city:listCity){
            scheduledThreadPool.schedule(new Runnable() {
                @Override
                public void run() {
                    outofList.add(city.parse());
                }
            },10, TimeUnit.MILLISECONDS);
        }
        scheduledThreadPool.shutdown();
        while (!scheduledThreadPool.isTerminated());
        return outofList;
    }
}