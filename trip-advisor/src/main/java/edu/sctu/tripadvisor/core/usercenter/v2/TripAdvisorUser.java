package edu.sctu.tripadvisor.core.usercenter.v2;

/**
 * Created by Administrator on 2016/9/29.
 */
public class TripAdvisorUser {

    private String nickName;
    private String registrationDate;
    private String headPortrait;
    private String hometown;
    private String points;
    private String levels;
    private String commentNumber;
    private String commentPicNumber;

    public void setNickname(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getRegistrationDate() {
        return this.registrationDate;
    }

    public void setHeadPortrait(String headPortrait) {
        this.headPortrait = headPortrait;
    }

    public String getHeadPortrait() {
        return this.headPortrait;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getHometown() {
        return this.hometown;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPoints() {
        return this.points;
    }

    public void setLevels(String levels) {
        this.levels = levels;
    }

    public String getLevels() {
        return this.levels;
    }

    public void setCommentNumber(String commentNumber) {
        this.commentNumber = commentNumber;
    }

    public String getCommentNumber() {
        return this.commentNumber;
    }

    public void setCommentPicNumber(String commentPicNumber) {
        this.commentPicNumber = commentPicNumber;
    }

    public String getCommentPicNumber() {
        return this.commentPicNumber;
    }
}
