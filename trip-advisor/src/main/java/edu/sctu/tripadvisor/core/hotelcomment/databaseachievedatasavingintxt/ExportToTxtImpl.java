package edu.sctu.tripadvisor.core.hotelcomment.databaseachievedatasavingintxt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by dell on 2016/10/24.
 * 实现方法。
 */
public class ExportToTxtImpl implements ExportToTxt {
    @Override
    public void export( String fileName,String table) throws IOException {
       HotelData hotelData=new HotelData();
        ArrayList<String> hotelList=new ArrayList<String>();
        File file = new File(fileName);
        if(!file.exists())
        {
            file.createNewFile();
        };
        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file));
            hotelList=hotelData.HotelDataAchieveDatabase(table);
            for (int i=0;i<hotelList.size();i++) {
                writer.write(hotelList.get(i));
                writer.newLine();
                if((i+1)%7==0){
                    writer.write(" ");
                    writer.newLine();
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
