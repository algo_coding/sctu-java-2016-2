package edu.sctu.tripadvisor.core.hotCity.v2;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by 李帅 on 2016/10/11
 */
public class HotCityPageParser extends AbstractParser<String> {
   static int num = 0;
    PropertiseConstructors propertiseConstructors = new PropertiseConstructors();

    public HotCityPageParser(String url) {
        super(url);
    }


    @Override
    public String parse() {

        Elements elements;
        if(num==0){
            Element div =  getDocument().select("div.popularCities").first();
             elements = div.select("a");
            num++;
        }else {
             elements = getDocument().select("a");
        }
        for (Element element:elements){
            propertiseConstructors.setCityName(element.select("span.name").text());
            propertiseConstructors.setRank(element.select("span.rankNum").text());
            propertiseConstructors.setURL("http://www.tripadvisor.cn" + element.attr("href"));

            InsertData insertData = new InsertData();
            insertData.operation();

        }
        return null;
    }
    class InsertData{
        DBOperation dbConnection = new DBOperation();
        String cityName = propertiseConstructors.getCityName();
        String rank = propertiseConstructors.getRank();
        String Url = propertiseConstructors.getURL();
        public void operation(){
            dbConnection.Insert(cityName,rank,Url);
        }
    }
}
