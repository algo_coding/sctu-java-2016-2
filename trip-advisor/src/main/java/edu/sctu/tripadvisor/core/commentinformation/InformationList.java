package edu.sctu.tripadvisor.core.commentinformation;

import edu.sctu.tripadvisor.core.hotelcomment.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by 落叶的思恋 on 2016/10/10.
 */
 class InformationList extends Geturl    {
    Document document ;
    int num0=0;



    ArrayList<String> satisfaction=new ArrayList<String>();
    ArrayList<String> grade=new ArrayList<String>();
    ArrayList<String> commentlist=new ArrayList<String>();
    ArrayList<String> peopleNamelist=new ArrayList<String>();
    ArrayList<String> commentTime=new ArrayList<String>();

//获取评论者姓名
    protected ArrayList<String> peopleNamelist(String url)  {
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.location();
        Elements NameEle=document.select("div.username").select("mo");
        StringBuffer stringBuffer=new StringBuffer();
        String ss=NameEle.text();
        String []arr=ss.split(" ");
        for(int i=0;i<arr.length;i++) {
            peopleNamelist.add(num0,arr[i]);
        }
        System.out.println(peopleNamelist);
        return peopleNamelist;
    }

    //获取评论者分数
    protected  ArrayList<String> grade(String url){
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.location();
        Elements ScoreEle=document.select("img.sprite-rating_s_fill");
        int n=0;

        for (Element j:ScoreEle ){
            n++;
            if (n>=11)break;
            grade.add(num0,j.attr("alt"));
        }
        System.out.println(grade);
        return grade;
    }
    //获取评论者满意程度
    protected  ArrayList<String> feel(String url) throws IOException {
        document = Jsoup.connect(url).get();
        document.location();
        Elements feeling = document.select("span.noQuotes");
        String ss=feeling.text();
        String []arr=ss.split(" ");
        for(int i=0;i<arr.length;i++){
            satisfaction.add(num0,arr[i]);
        }
        System.out.println(satisfaction);
        return satisfaction;
    }
     //获取评论者评论时间
     protected  ArrayList<String> commnettime(String url) throws IOException {
         document = Jsoup.connect(url).get();
         document.location();
        Elements TimeEle=document.select("span.ratingDate");
        String ss= TimeEle.text();
        String []arr=ss.split(" ");
        for(int i=0;i<arr.length;i++) {
          commentTime.add(num0,arr[i]);
        }
        System.out.println(commentTime);
        return commentTime;
    }
    protected ArrayList<String> getCommentlist(String url) throws IOException {
        document = Jsoup.connect(url).get();
        document.location();
        Elements  ViewEle=document.select("div.entry>p");
        StringBuffer stringBuffer=new StringBuffer();
        for(Element li:ViewEle ){
            commentlist.add(num0, li.text().trim());
            num0++;
        }
        System.out.println(commentlist);
        return commentlist;
    }
}











