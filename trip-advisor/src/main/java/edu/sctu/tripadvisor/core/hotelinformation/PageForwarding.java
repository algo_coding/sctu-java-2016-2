package edu.sctu.tripadvisor.core.hotelinformation;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.lang.Integer.parseInt;

/**
 * Created by cc on 2016/10/8.
 */
//李友红
public class PageForwarding {

    public List<String> getpageforwarding() {

        List<String> listUrl = new ArrayList<String>();//创建集合放url
        int pagenumber;    //用来放抓取网页的页数
        GetUrl getUrl = new GetUrl();    //从上一组获得url
        List<String> list = getUrl.connect();
        Iterator<String> li = list.iterator();
        while (li.hasNext()) {
            String hotelUrl = li.next();

            // System.out.println("从上一组获得"+hotelUrl);
            Document doc = null;
            try {
                doc = Jsoup.connect(hotelUrl).get();
                Elements el = doc.select("div.pageNumbers>a");
                for (int i = 0; i < el.size(); i++) {
                    int page = parseInt(el.get(i).text());
                    if (page > 10) {
                        //System.out.println(el.get(i).text());
                        pagenumber = page;
                        for (int j = 1; j <= pagenumber; j++) {
                            if (j == 1) {
                                listUrl.add(hotelUrl);   //酒店首页url
                            } else {
                                Elements sdf = doc.select("[data-page-number=" + j + "]");
                                String href = sdf.attr("href"); //酒店获得部分url

                                //System.out.println("获得部分url" + href);

                                String pageHref = "http://www.tripadvisor.cn/" + href;  //酒店完整url

                                 // System.out.println("完整url:" + pageHref);


                                listUrl.add(pageHref);

                                doc = Jsoup.connect(pageHref).get();
                            }

                        }


                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        return listUrl;
    }
}
