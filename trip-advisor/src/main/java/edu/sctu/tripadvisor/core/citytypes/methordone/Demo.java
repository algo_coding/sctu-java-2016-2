package edu.sctu.tripadvisor.core.citytypes.methordone;

import edu.sctu.tripadvisor.core.citytypes.ViewPlaces;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eoly on 2016/9/29.
 */
public class Demo {
    public static List<List<ViewPlaces>> start(boolean isWriteData) {
        DataBaseDataAccess dataBaseProcesser = new DataBaseDataAccess();
        List<InformationInCityParser> listCity = new ArrayList<InformationInCityParser>();
        try {
            DataBaseDataAccess.create();

            System.out.println("数据解析中...");
            List<Object[]> urlCity = dataBaseProcesser.readInfor();
            for(Object med[]:urlCity){
                InformationInCityParser informationInCity = new InformationInCityParser((String) med[2], (String) med[0],isWriteData);
                listCity.add(informationInCity);
            }
            AllParser allParser = new AllParser(listCity,"四川");
            List<List<ViewPlaces>> outofList = allParser.parse();
            System.out.println("数据导入完成");
            return outofList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}