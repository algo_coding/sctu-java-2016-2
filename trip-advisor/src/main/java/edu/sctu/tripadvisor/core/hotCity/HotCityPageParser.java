package edu.sctu.tripadvisor.core.hotCity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by 李帅 on 2016/10/11
 */
public class HotCityPageParser extends AbstractParser<String> {
    String url;
    public HotCityPageParser(String url) {
        super(url);
        this.url = url;
    }
    AbstractParser abstractParser = new HotCityPageParser(url);
    DBConnection dbConnection = new DBConnection();
    @Override
    public String parse() {

        Element div =  abstractParser.getDocument().select("div.popularCities").first();
        Elements elements = div.select("a");
        for (Element element:elements){
            PropertiseConstructors propertiseConstructors = new PropertiseConstructors(element.select("span.name").text(),element.select("span.rankNum").text(),"http://www.tripadvisor.cn" + element.attr("href"));
            dbConnection.conneciton(propertiseConstructors.getCityName(),propertiseConstructors.getRank(),propertiseConstructors.getURL());
        }
        for (int i = 1; i < 22; i++) {
            abstractParser = new HotCityPageParser("http://www.tripadvisor.cn/TourismChildrenAjax?geo=297462&offset=" + i + "&desktop=true");
            Elements elements1 = abstractParser.getDocument().select("a");
            for (Element element1:elements1){
                PropertiseConstructors propertiseConstructors1 = new PropertiseConstructors(element1.select("span.name").text(),element1.select("span.rankNum").text(),"http://www.tripadvisor.cn" + element1.attr("href"));

                dbConnection.conneciton(propertiseConstructors1.getCityName(),propertiseConstructors1.getRank(),propertiseConstructors1.getURL());
            }
        }

        return null;
    }
}
