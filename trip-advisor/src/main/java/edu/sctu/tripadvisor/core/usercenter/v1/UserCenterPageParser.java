package edu.sctu.tripadvisor.core.usercenter.v1;

import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by Administrator on 2016/9/29.
 */
public interface UserCenterPageParser<T> {


    public Document getDocument(String url) throws IOException;

    public T parse();
}
