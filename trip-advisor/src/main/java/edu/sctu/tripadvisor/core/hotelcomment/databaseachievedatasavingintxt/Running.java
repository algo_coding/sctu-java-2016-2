package edu.sctu.tripadvisor.core.hotelcomment.databaseachievedatasavingintxt;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by on 2016/10/25.
 */
//开始写入数据
public class Running {
    public static void main(String[] args) throws IOException {
        ExportToTxtImpl exportToTxt=new ExportToTxtImpl();
        ImportTxtData importTxtData=new ImportTxtData();
        Scanner scanner=new Scanner(System.in);
        System.out.println("输入你存放信息的位置（默认D:\\hoteldata.txt)");
        String filename="D:\\hoteldata.txt";
        String scfilename=null;
        scfilename=scanner.nextLine();
        System.out.println("你要获取的数据的表名（默认为（hotelcommentlist））");
        String ss=null;
        String table="hotelcommentlist";
        ss=scanner.nextLine();
        if(ss.isEmpty()){
           ss=table;

        }
        if(scfilename.isEmpty()){
            scfilename=filename;
        }
        exportToTxt.export(scfilename, ss);

        System.out.println("输入你要读取的文件路径（默认D:\\hoteldata.txt）");
        String readfilepath="D:\\hoteldata.txt";
        String scanreadfilepath=null;
        scanreadfilepath=scanner.nextLine();
        if (scanreadfilepath.isEmpty())
        {
            scanreadfilepath=readfilepath;
        }
        importTxtData.importTxtData(scanreadfilepath);


    }
}
