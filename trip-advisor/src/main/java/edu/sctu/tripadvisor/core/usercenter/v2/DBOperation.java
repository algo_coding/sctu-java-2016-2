package edu.sctu.tripadvisor.core.usercenter.v2;



import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huagnsuixin on 2016/9/29.
 */
public class DBOperation{
    String url;
    String user;
    String password;
    /*
    构造方法，传入url user password 以连接数据库
     */
    public DBOperation(String url, String user, String password) {
        this.url=url;
        this.user=user;
        this.password=password;
    }
    /*
    获取数据库连接
     */
    public Connection getcon() {
        Connection con = null;
        String driver = "com.mysql.jdbc.Driver";
        try {
            //加载驱动程序
            Class.forName(driver);
           return con = DriverManager.getConnection(url, user, password);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("驱动加载异常！");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("数据库连接失败！");
        }
        return con;
    }

    /*
    增加一条Trip 信息到数据库
     */
    public boolean saveUserInformation(TripAdvisorUser user) {
        Connection con =this.getcon();
        if (con==null) {
            return false;
        }
        Statement st=null;
        try {
            st=con.createStatement();
            String sqlStr=null;
            sqlStr="INSERT INTO usercenter VALUE('"+user.getNickName()+"','"+user.getRegistrationDate()+"','"+user.getHeadPortrait()+"','"
                    +user.getHometown()+"','"+user.getPoints()+"','"+user.getLevels()+"','"+user.getCommentNumber()+"','"+user.getCommentPicNumber()+"')";
//            如果影响的条数为0，则说明updata失败
            if (st.executeUpdate(sqlStr)==0){
                return false;
            }
        } catch (SQLException e) {
//            e.printStackTrace();
            System.out.println("insert 失败！表中有此用户的记录："+user.getNickName());
        } finally {//释放数据库的资源
            try {
                if (st != null)
                    st.close();
                if(con != null && !con.isClosed()){
                    con.close();
                }
            } catch (SQLException e) {
//                e.printStackTrace();
                System.out.println("关闭连接失败");
            }
        }
        return true;
    }

    /*
        从别的表中获取用户名
        table 表名
        column 列名
     */
    public ArrayList<String> getUserNickName(String table,String column) {
        Connection con = this.getcon();
        ResultSet rs = null;
        ArrayList<String> list =new ArrayList<String>();
        Statement st=null;
        try {
            st=con.createStatement();
            rs = st.executeQuery("select "+column+" from "+table);
            try {
                while (rs.next()){
                    list.add(rs.getString(1));
                }
            } catch (SQLException e) {
//                e.printStackTrace();
                System.out.println("ResultSet转化List失败");
            }

        } catch (SQLException e) {
//            e.printStackTrace();
            System.out.println("获取对数据库的操作连接失败！");
        }finally {
            if (st != null)
                try {
                    st.close();
                } catch (SQLException e) {
//                    e.printStackTrace();
                    System.out.println("statement关闭连接失败1");
                }
            try {
                if(con != null && !con.isClosed()){
                    con.close();
                }
            } catch (SQLException e) {
//                e.printStackTrace();
                System.out.println("con关闭失败");
            }
        }
        return list;
    }

    /*
    从数据库中获取所有用户信息
     */
    public List getUserInformation(){
        String table  = "usercenter";
        Connection con = this.getcon();
        ResultSet rs = null;
        ArrayList<TripAdvisorUser> list = new ArrayList<TripAdvisorUser>();
        Statement st=null;
        TripAdvisorUser userInformation = null;
        try {
            st=con.createStatement();
            rs = st.executeQuery("select * "+" from "+table);
            try {
                while (rs.next()){
                    userInformation.setNickname(rs.getString("nickname"));
                    userInformation.setRegistrationDate(rs.getString("registrationdate"));
                    userInformation.setHeadPortrait(rs.getString("headPortrait"));
                    userInformation.setHometown(rs.getString("hometown"));
                    userInformation.setPoints(rs.getString("points"));
                    userInformation.setLevels(rs.getString("levels"));
                    userInformation.setCommentNumber(rs.getString("commentNumber"));
                    userInformation.setCommentPicNumber(rs.getString("commentPicNumber"));
                    list.add(userInformation);
                }
            } catch (SQLException e) {
//                e.printStackTrace();
                System.out.println("ResultSet转化List失败");
            }

        } catch (SQLException e) {
//            e.printStackTrace();
            System.out.println("获取对数据库的操作连接失败！");
        }finally {
            if (st != null)
                try {
                    st.close();
                } catch (SQLException e) {
//                    e.printStackTrace();
                    System.out.println("statement关闭连接失败1");
                }
            try {
                if(con != null && !con.isClosed()){
                    con.close();
                }
            } catch (SQLException e) {
//                e.printStackTrace();
                System.out.println("con关闭失败");
            }
        }
        return list;
    }
}
