package edu.sctu.tripadvisor.core.hotCity.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 李帅 on 2016/10/11
 */
public class Main {
    public static void main(String[] args) {
        List<String>  urls = new ArrayList<String>();
        urls.add("http://www.tripadvisor.cn/Tourism-g297462-Sichuan-Vacations.html");
        for (int i =1;i<22;i++){
            urls.add("http://www.tripadvisor.cn/TourismChildrenAjax?geo=297462&offset=" + i + "&desktop=true");
        }

        for (int j = 0;j<urls.size();j++){
            HotCityPageParser hotCityPageParser = new HotCityPageParser(urls.get(j));
            hotCityPageParser.parse();
        }

    }
}
