package edu.sctu.tripadvisor.core.scenicspots;

import edu.sctu.tripadvisor.core.citytypes.CityTypeDataInformation;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mike on 2016/10/11.
 */
public class InsertDatabase {


    public  static void main(String[] args) {

        int a = 0;
        int i = 0;
        int j = 0;
        int m = 0;
        int n = 0;


        String sql;
        int result = 1;


        Statement stmt = null;
        try {
            stmt = GetConnToDatabase.getConn().createStatement();//获得数据库连接对象conn

            sql = "create table ScenicSpots(id int ,ScenicName char(30), URL char(200),Rank char(60) ,Allassess char(50) ,PRIMARY  key (id)) ";

            result = stmt.executeUpdate(sql);



        } catch (SQLException e) {
            e.printStackTrace();
        }


        CityTypeDataInformation cityTypeDataInfor = new CityTypeDataInformation();
        List<String> list = cityTypeDataInfor.readUrlFromDatabase("景点");//二组传入的url
        for (String str0 : list) {


            GetsumPage getsumPageandNextUrl = new GetsumPage();

            ArrayList<String> arrayList = getsumPageandNextUrl.NextgetUrl(str0);//得到一个市所有的url
            Document document = null;

            for (String str : arrayList) {


                try {

                    if (result != -1) {


                        for (Element link : ScenicSpotsData.scenictitle(str)) {
                            ++i;
                            String linkText = link.text();

                            sql = "insert into  ScenicSpots(id,ScenicName) values(" + i + ",'" + linkText + "')";

                            result = stmt.executeUpdate(sql);

                        }


                        for (Element link0 : ScenicSpotsData.rank(str)) {
                            ++j;

                            String linkText = link0.text();

                            if (linkText.equals("")) {

                                --j;
                                continue;
                            }
                            sql = "update ScenicSpots set Rank='" + linkText + "' where id=" + j + "";

                            result = stmt.executeUpdate(sql);

                        }

                        for (int b = 0; b < ScenicSpotsData.returnurl(str).size(); b++) {

                            ++m;

                            sql = "update ScenicSpots set URL='" + ScenicSpotsData.returnurl(str).get(b) + "' where id=" + m + "";


                            result = stmt.executeUpdate(sql);

                        }


                        for (int c = 0; c < ScenicSpotsData.returncomment(str).size(); c++) {

                            ++n;

                            sql = "update ScenicSpots set Allassess='" + ScenicSpotsData.returncomment(str).get(c) + "' where id=" + n + "";

                            result = stmt.executeUpdate(sql);

                        }
                        a++;
                        System.out.println("第" + a + "页数据插入成功");


                    }





                } catch (SQLException e) {
                    System.out.println("数据库操作错误");
                    e.printStackTrace();
                } finally {

                }
            }


        }
        try {
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
