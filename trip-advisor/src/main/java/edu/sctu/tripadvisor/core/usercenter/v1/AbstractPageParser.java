package edu.sctu.tripadvisor.core.usercenter.v1;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by Administrator on 2016/9/29.
 */
public abstract class AbstractPageParser<T> implements UserCenterPageParser<T> {
    private Document document;

    @Override
    public Document getDocument(String url) throws IOException {
        this.document =  Jsoup.connect(url).get();
        return document;
    }

    @Override
    public abstract T parse();
}
