package edu.sctu.tripadvisor.tasksix;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by yuweimei on 2016/9/22.
 */
public class TaskSix {
    public static void main(String[] args) {
            int j = 0;
            for (int i = 1; i <=97 ; i++) {
                j = 30 * (i - 1);
                String http = "http://www.tripadvisor.cn/RestaurantSearch?Action=PAGE&geo=297463&aja" +
                        "x=1&itags=10591&sortOrder=popularity&o=a" + j + "&availSearchEnabled=false";
                try {
                    Document document = Jsoup.connect(http).get();
                    Element div = document.select("div.shortSellDetails").get(0);
                    //获取店名：
                    Elements restName=div.select("h3.title");
                    System.out.println("店名：" + restName.text());
                    //获取排名：
                    Elements rank=div.select("div.popIndexBlock");
                    System.out.println("排名：" + rank.text());
                    //点评数
                    Elements reviewNum=div.select(".rating");
                    System.out.println("点评数：" + reviewNum.text());
                    //最近的两条评论
                    Elements rs=div.select(".review_stubs");
                    System.out.println("内容：" + rs.text());
                    //价格
                    Elements pr=div.select(".price_range");
                    System.out.println("价格：" + pr.text());
                    //菜系
                    Elements cuisine=div.select(".cuisines");
                    System.out.println("菜系：" + cuisine.text());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }

}

