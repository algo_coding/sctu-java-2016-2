package edu.sctu.tripadvisor.taskeight.Lsongh;

/**
 * Created by 松红 on 2016/10/8.
 */
public class Demo {
    public static void main(String[] args) {
        Test test = new Test();
        /*利用set与get的方式来赋值取值，是最好的方式*/
        test.setAnInt(100);
        test.setcBoolean(true);
        test.setbString("lisongh");
        System.out.println(test.getbString());
        System.out.println(test.getcBoolean());
        System.out.println(test.getAnInt());
       /*利用递归的方式求和*/
        System.out.println(test.sum(5));
    }
}
