package edu.sctu.tripadvisor.taskeight.杨莹;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by yang on 2016/9/22.
 */
public class Test2 {
    public static void main(String args[]) throws IOException {
        try {
            Document document = Jsoup.connect("http://www.tripadvisor.cn/TourismBlog-t4662.html#_tag322831").get();
            Elements div = document.select("div.strategy-info");
            System.out.println(div.text());
            System.out.println("******************");
            Elements rout = document.select("div.content-title");
            Elements note = document.select("div.content-detail");
            Elements before = document.select("div.total-summary");
            for (org.jsoup.nodes.Element element : rout) {
                System.out.println(element.text());
            }
            for (org.jsoup.nodes.Element element : note) {
                System.out.println(element.text());
            }
            for (org.jsoup.nodes.Element element : before) {
                System.out.println(element.text());
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}

