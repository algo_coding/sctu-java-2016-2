package edu.sctu.tripadvisor.taskeight.chenruixiu;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by Administrator on 2016/9/26.
 */
public class Text {
    public static void main(String[] args) {
        try {
            Document document= Jsoup.connect("http://www.sctu.edu.cn/").get();
            Elements div =document.select("div#navs");
            System.out.println(div.text());
            System.out.println("****************************************************************");
//            在后面添加
            System.out.println(div.append("later").text());
            System.out.println("****************************************************************");
//            在前面添加
            System.out.println(div.prepend("forward").text());
            System.out.println("****************************************************************");
//            在前一级加标签
            System.out.println(div.wrap("<p>word</P>").text());
            System.out.println("****************************************************************");
//             所需div处html代码
            System.out.println(div.html());
            System.out.println("****************************************************************");
//               全网页的html代码
            System.out.println(document.html());
            System.out.println("****************************************************************");
//            遍历
            for(Element elements:div){
                System.out.println(elements.text());
                System.out.println("****************************************************************");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
