package edu.sctu.tripadvisor.taskeight.Lsongh;

import javax.swing.*;

/**
 * Created by 松红 on 2016/11/2.
 */
import java.util.Scanner;

public class Lab02 {
    //希腊字母
   /* public static void main (String args[ ])
    {
        int startPosition=0,endPosition=0;
        char cStart='α',cEnd='ω';
         //cStart做int型转换据运算，并将结果赋值给startPosition
        //cEnd做int型转换运算，并将结果赋值给endPosition
        System.out.println("希腊字母\'α\'在unicode表中的顺序位置:"+(int)cStart);
        System.out.println("希腊字母表：");
        for(int i=(int)cStart;i<=(int)cEnd;i++)
        {
            System.out.print(" "+(char)i);
        }
    }*/

    //回文
   /* public static void main(String[] args) {
        int  number=0,d5,d4,d3,d2,d1;
        String str="请输入数字（1-99999）：";
        System .out.print(str);
        Scanner input=new Scanner(System.in );
        number=input.nextInt();
        d1=(int)(number/10000);
        d2=(int)(number/1000)%10;
        d3=(int)(number/100)%10;
        d4=(int)(number/10)%10;
        d5=number%10;
        if(number>=10000&&number<=99999&&d1==d5&&d2==d4){
            System.out.println(number+"是回文！");
        }else if (number>=1000&&number<=9999&&d2==d5&&d3==d4){
            System.out.println(number+"是回文！");
        }else if (number>=100&&number<=999&&d3==d5){
            System.out.println(number+"是回文！");
        }else if (number>=100&&number<=999&&d4==d5){
            System.out.println(number+"是回文");
        }else {
            System.out.println(number+"不是回文");
        }

    }*/
    public static void main(String[] args) {
        int number = 0, num = 0;
        num = (int) (Math.random() * 100 + 1);


        for (int i = 1; i <= num; i++) {
            Scanner input = new Scanner(System.in);
            number = input.nextInt();
            if (num == number) {
                System.out.println("猜对了！恭喜，正确数字是" + number);
                continue;
            } else if (number > num) {
                System.out.println("猜大了，请再次输入：");
                continue;
            } else if (number < num) {
                System.out.println("猜小了，请再次输入：");
                continue;
            }
        }
    }

}

