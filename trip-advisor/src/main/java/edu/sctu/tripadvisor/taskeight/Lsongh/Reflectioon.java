package edu.sctu.tripadvisor.taskeight.Lsongh;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static java.lang.Class.forName;

/**
 * Created by 松红 on 2016/10/27.
 */
public class Reflectioon {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        try {
            /*反射实例化*/
            Class clz = forName(Test.class.getName());
            Test li = (Test) clz.newInstance();
            li.setbString("lisonghhh");
            li.setAnInt(22);
            li.setcBoolean(true);
            System.out.println(li.getAnInt() + "\n" + li.getbString() + "\n" + li.getcBoolean());

            System.out.println("********************");


            /*用反射调用play方法*/
            Method method1 = clz.getDeclaredMethod("play");
            method1.invoke(li);
            System.out.println(method1.invoke(li));

            /*用反射调用sum方法(必须用int.class规定类型)*/
            Method method=clz.getDeclaredMethod("sum",int.class);
            System.out.println(method.invoke(li, 100));
            Method staticmethod=clz.getDeclaredMethod("music",String.class,int.class);
            System.out.println(staticmethod.invoke(li,"李松红",18));

            System.out.println("***********************");
            /* 获取属性名 */
            Field[] fields = clz.getDeclaredFields();
            for (Field field : fields){
            System.out.println(field.getName());
        }

            System.out.println("*********************");
            /*获取方法名*/
            Method[] methods=clz.getMethods();
            for (Method method2 :methods){
                System.out.println(method2.getName());
            }
            /*通过反射获取private变量*/
            System.out.println("**************");
            Field field=clz.getDeclaredField("ansum");
            field.setAccessible(true);//重点（必须）
            field.set(li, 22222222);
            System.out.println(field.get(li));






        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

    }
}
