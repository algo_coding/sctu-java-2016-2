package edu.sctu.tripadvisor.taskone.lishuai.practice;

/**
 * Created by 李帅 on 2016/10/11.
 */
interface IMessage<T>{
    public void print(T t);
}
class MessageImpl implements IMessage<String>{

    @Override
    public void print(String s) {
        System.out.println(s);
    }
}

public class TestDemo2 {
    public static void main(String[] args) {
        IMessage<String> iMessage = new MessageImpl();
        iMessage.print("hello word");
        }
    }

