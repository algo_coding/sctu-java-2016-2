package edu.sctu.tripadvisor.taskone.lishuai.practice;

/**
 * Created by 李帅 on 2016/10/11.
 */
class Message<T>{  //T extends Number 上限
    private T msg;

    public T getMsg() {
        return msg;
    }

    public void setMsg(T msg) {
        this.msg = msg;
    }
}
public class TestDemo1 {
    public static void main(String[] args) {
        Message<String> m = new Message<String>();
        m.setMsg("hello word");
        fun(m);//引用传递
    }
    public static void fun(Message<? super String> temp){//不能设置值，但可以取出 ? super String下限
        System.out.println(temp.getMsg());
    }
}
