package edu.sctu.tripadvisor.taskone.renxinpeng;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * 收集四川著名的目的地数据
 */
public class TaskOne {

    public static void main(String[] args) throws IOException {

        Document document=Jsoup.connect("http://www.tripadvisor.cn/Tourism-g297462-Sichuan-Vacations.html").get();
        Element div=document.select("div.popularCities").first();
        Elements elements=div.select("a");
        for(Element element:elements){

            System.out.println("url is:http://www.tripadvisor.cn"+element.attr("href"));
            System.out.println("内容是："+element.text());

            System.out.println("..............");
        }


        for (int i = 1; i < 22; i++) {

            Document document1 = null;
            try {
                document1 = Jsoup.connect("http://www.tripadvisor.cn/TourismChildrenAjax?geo=297462&offset=" + i + "&desktop=true").get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Element body = document1.select("body").first();
            Elements elements1 = body.select("a");

            for (Element element : elements1) {

                System.out.println("url is:http://www.tripadvisor.cn" + element.attr("href"));
                System.out.println("内容是：" + element.text());

                System.out.println("...........");
            }
        }
    }
}
