package edu.sctu.tripadvisor.taskone.xuhanmei.exporttotxt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2016/10/20.
 */
public class HotCityExportImpl implements HotCityExport {
    @Override
    public void export(List hotCityList, String Filename){


        File file=new File(Filename);
        BufferedWriter writer=null;

        try {
            writer=new BufferedWriter(new FileWriter(file));
            for (int i=0;i<hotCityList.size();i++){
                System.out.println(hotCityList.get(i));
                writer.write((String) hotCityList.get(i));
                writer.newLine();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
