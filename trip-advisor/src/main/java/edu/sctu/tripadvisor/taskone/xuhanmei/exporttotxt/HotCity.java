package edu.sctu.tripadvisor.taskone.xuhanmei.exporttotxt;

import org.omg.CORBA.PUBLIC_MEMBER;

/**
 * Created by Administrator on 2016/10/20.
 */
public class HotCity {
    private String cityName;
    private String rank;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public HotCity(String cityName, String rank, String url) {
        this.cityName = cityName;
        this.rank = rank;
        this.url=url;
    }
    public HotCity(){}
}
