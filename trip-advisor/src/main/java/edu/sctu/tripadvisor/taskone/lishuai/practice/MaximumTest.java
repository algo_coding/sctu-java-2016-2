package edu.sctu.tripadvisor.taskone.lishuai.practice;

/**
 * Created by 李帅 on 2016/10/11.
 */
public class MaximumTest {
    public static <T extends Comparable<T>> T maxmin(T x,T y,T z){
        T max = x;
        if(y.compareTo(max)>0){
            max = y;
        }
        if(z.compareTo(max)>0){
            max = z;
        }
        return max;
    }
    public static void main(String[] args) {

        System.out.println(maxmin(3,4,5));
        System.out.println(maxmin(2.4,4.6,6.7));
        System.out.println(maxmin("apear","apple","eat"));
    }
}
