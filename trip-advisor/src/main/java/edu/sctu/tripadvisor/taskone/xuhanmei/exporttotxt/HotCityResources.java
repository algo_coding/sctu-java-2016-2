package edu.sctu.tripadvisor.taskone.xuhanmei.exporttotxt;

import edu.sctu.tripadvisor.taskone.xuhanmei.exporttotxt.test.HotCityExportTest;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by Administrator on 2016/10/24.
 */
public class HotCityResources {
    public static void main(String[] args) {
        HotCityExportTest hce = new HotCityExportTest();
        try {
            Document document= Jsoup.connect("http://www.tripadvisor.cn/Tourism-g297462-Sichuan-Vacations.html").get();
            Element div=document.select("div.popularCities").first();
            Elements elements=div.select("a");
            for (Element element:elements){
                HotCity hotCity=new HotCity(element.select("span.name").text(),element.select("span.rankNum").text(),element.attr("href"));
//                System.out.println(hotCity.getCityName());
                hce.should_write_hotcity(hotCity.getCityName(),hotCity.getRank(),hotCity.getUrl());
            }

            for (int i=0;i<22;i++){
                Document document1=Jsoup.connect("http://www.tripadvisor.cn/TourismChildrenAjax?geo=297462&offset="
                        + i + "&desktop=true").get();
                Elements elements1=document1.select("a");
                for (Element element:elements1){
                   HotCity hotCity=new HotCity(element.select("span.name").text(),element.select("span.rankNum").text(),element.attr("href"));
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
