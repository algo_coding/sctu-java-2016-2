package edu.sctu.tripadvisor.taskone.lishuai.practice;

/**
 * Created by 李帅 on 2016/10/11.
 */
//此设置的T在Point类定义上只表示一个标记，在使用的时候需要为其设置具体的类型
class Point<T>{  //type = T ,是一个类型
    private T x;  //此属性的类型不知道，由Point类使用时动态决定
    private T y;

    public T getX() {
        return x;
    }

    public void setX(T x) {
        this.x = x;
    }

    public T getY() {
        return y;
    }

    public void setY(T y) {
        this.y = y;
    }
}
public class TestDemo {
    public static void main(String[] args) {
        //第一步：设置数据
        Point<Integer> p = new Point<Integer>();
        p.setX(10);
        p.setY(20);
        //第二步：取出数据
        int x = p.getX();
        int y = p.getX();
        System.out.println("x坐标"+x+",y坐标"+y);

    }
}
