package edu.sctu.tripadvisor.taskone.lishuai.practice;

/**
 * Created by 李帅 on 2016/10/11.
 */
public class GenericMethodTest {
    public static <E> void printArry(E[] inputArry){
    //元组元素
        for (E elemet:inputArry){
            System.out.printf("%s", elemet);
        }
        System.out.println();
    }
    public static void main(String[] args) {
        Integer[] intArray = {1,2,3,4,5};
        Double[] doublesArray = {1.1,1.2,1.3,1.4};
        Character[] charArray = {'E','R','T','F','Y'};
        System.out.println("整数");
        printArry( intArray );
        System.out.println("小数");
        printArry( doublesArray );
        System.out.println("字符串");
        printArry( charArray );

    }
}
