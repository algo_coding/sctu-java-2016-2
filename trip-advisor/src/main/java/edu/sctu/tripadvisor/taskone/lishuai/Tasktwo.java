package edu.sctu.tripadvisor.taskone.lishuai;

import edu.sctu.tripadvisor.taskone.lishuai.Insert;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by 李帅 on 2016/9/22.
 */
public class Tasktwo {

    public static void main(String[] args) {
        Insert a = new Insert();
        System.out.println("四川省热门目的地列表");
        try {
            Document document = Jsoup.connect("http://www.tripadvisor.cn/Tourism-g297462-Sichuan-Vacations.html").get();
            a.f(document);
            for (int i = 1; i < 22; i++) {
                Document document1 = Jsoup.connect("http://www.tripadvisor.cn/TourismChildrenAjax?geo=297462&offset="+i+"&desktop=true").get();
                a.f1(document1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
