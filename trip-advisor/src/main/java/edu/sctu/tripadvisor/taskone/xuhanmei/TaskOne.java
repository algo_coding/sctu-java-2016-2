package edu.sctu.tripadvisor.taskone.xuhanmei;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by Administrator on 2016/9/25.
 */
public class TaskOne {
    public static void main(String[] args) {
        try {
            Document document= Jsoup.connect("http://www.tripadvisor.cn/Tourism-g297462-Sichuan-Vacations.html").get();
            Element div=document.select("div.popularCities").first();
            Elements elements=div.select("a");
            for (Element element:elements){
                System.out.println("链接是："+element.attr("href")+"\n"+"城市是："+element.select("span.name").text()+"\n"
                        +"这个城市"+element.select("span.rankNum").text());
//                Elements citys=elements.select("span.rankNum");
//                for ( Element city:citys){
//                    System.out.println("城市是的排名："+city.text());
//                }
                System.out.println("***********************************************************");
            }
            for (int i=0;i<22;i++){
                Document document1=Jsoup.connect("http://www.tripadvisor.cn/TourismChildrenAjax?geo=297462&offset="
                        + i + "&desktop=true").get();
                Elements elements1=document1.select("a");
                for (Element element:elements1){
                    System.out.println("链接是："+element.attr("href")+"\n"+"城市是"+element.select("span.name").text()+"\n"+
                            "这个城市"+element.select("span.rankNum").text());
                    System.out.println("***********************************************************");
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
