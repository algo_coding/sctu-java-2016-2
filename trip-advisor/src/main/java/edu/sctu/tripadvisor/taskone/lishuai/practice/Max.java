package edu.sctu.tripadvisor.taskone.lishuai.practice;

/**
 * Created by 李帅 on 2016/10/11.
 */
public class Max {
    public static void main(String[] args) {
        Integer[] attr = {1,2,3,4,5,23,35,57,94,77,45};
        System.out.println("The big number is "+getMax(attr));
    }
    public static <T extends Number> T getMax(T arry[]){
        T max = arry[0];
        for (T element:arry){
            max = element.doubleValue() >max.doubleValue()?element:max;
        }
        return max;

    }
}
