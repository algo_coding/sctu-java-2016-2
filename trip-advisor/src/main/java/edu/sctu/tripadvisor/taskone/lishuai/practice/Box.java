package edu.sctu.tripadvisor.taskone.lishuai.practice;

/**
 * Created by 李帅 on 2016/10/11.
 */
public class Box<T> {
    private T t;
    public void add(T t){
        this.t = t;
    }
    public T get(){
        return t;
    }

    public static void main(String[] args) {

        Box<Integer> Integer1 = new Box<Integer>();
        Box<String> str = new Box<String>();

        Integer1.add(new Integer(10));
        str.add(new String("hello word"));
        System.out.println(Integer1.get());
        System.out.println(str.get());
    }
}
