package edu.sctu.tripadvisor.taskone.xuhanmei.exporttotxt;

import java.util.List;

/**
 * Created by Administrator on 2016/10/20.
 */
public interface HotCityExport {
    public void export(List<HotCity> hotCityList, String Filename);
}
