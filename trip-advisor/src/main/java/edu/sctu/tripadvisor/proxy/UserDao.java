package edu.sctu.tripadvisor.proxy;

/**
 * Created by Administrator on 2016/9/27.
 */
public interface UserDao {
    public void saveUser();
    public void updateUser();

}
