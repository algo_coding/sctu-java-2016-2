package edu.sctu.tripadvisor.proxy.dynamic;

import edu.sctu.tripadvisor.proxy.UserDao;
import edu.sctu.tripadvisor.proxy.UserDaoImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Created by Administrator on 2016/9/27.
 */
public class DynamicProxyClient {

    public static void main(String[] args) {


        UserDao target = new UserDaoImpl();
        InvocationHandler handler = new UserDaoInvocationHandler(target);

        UserDao proxy = (UserDao) Proxy.newProxyInstance(
                UserDaoImpl.class.getClassLoader(),
                UserDaoImpl.class.getInterfaces(),
                handler
        );

        proxy.saveUser();

        proxy.updateUser();
    }
}
