package edu.sctu.tripadvisor.proxy.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by Administrator on 2016/9/27.
 */
public class UserDaoInvocationHandler implements InvocationHandler {

    private Object target;

    public UserDaoInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {


        if (method.getName().equals("updateUser")) {
            System.out.println("Before!");

            method.invoke(target, args);

            System.out.println("After!");

        }else{
            method.invoke(target, args);
        }

        return null;
    }
}
