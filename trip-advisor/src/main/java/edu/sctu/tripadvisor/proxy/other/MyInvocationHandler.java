package edu.sctu.tripadvisor.proxy.other;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by Administrator on 2016/9/27.
 */
public class MyInvocationHandler implements InvocationHandler {


    private Object targe;

    public MyInvocationHandler(Object targe) {
        this.targe = targe;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("Before invoke!");

        Object result = method.invoke(targe, args);

        System.out.println("After invoke!");

        return result;

    }
}
