package edu.sctu.tripadvisor.proxy.staticproxy;

import edu.sctu.tripadvisor.proxy.UserDao;
import edu.sctu.tripadvisor.proxy.UserDaoImpl;

/**
 * Created by Administrator on 2016/9/27.
 */
public class StaticProxyClient {

    public static void main(String[] args) {

        UserDao userDao = new UserDaoImpl();

        UserDao userDaoProxy = new UserDaoProxy(userDao);
        userDaoProxy.saveUser();
    }
}
