package edu.sctu.tripadvisor.proxy.staticproxy;

import edu.sctu.tripadvisor.proxy.UserDao;

/**
 * Created by Administrator on 2016/9/27.
 */
public class UserDaoProxy implements UserDao {

    private UserDao userDao;


    public UserDaoProxy(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void saveUser() {
        System.out.println("Before save!");

        userDao.saveUser();

        System.out.println("After save!");
    }

    @Override
    public void updateUser() {

    }
}
