package edu.sctu.tripadvisor.proxy.other;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Created by Administrator on 2016/9/27.
 */
public class ProxyClient {

    public static void main(String[] args) {


        ParrotBird parrot = new ParrotBird();

        InvocationHandler handler = new MyInvocationHandler(parrot);

        BirdActivity activity = (BirdActivity) Proxy.newProxyInstance(ParrotBird.class.getClassLoader(),
                ParrotBird.class.getInterfaces(),
                handler);

        activity.fly();

    }

}
