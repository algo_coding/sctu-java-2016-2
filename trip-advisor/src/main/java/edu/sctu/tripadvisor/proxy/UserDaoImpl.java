package edu.sctu.tripadvisor.proxy;

/**
 * Created by Administrator on 2016/9/27.
 */
public class UserDaoImpl implements UserDao {
    @Override
    public void saveUser() {
        System.out.println("Save user success!");
    }

    @Override
    public void updateUser() {
        System.out.println("update user!");
    }
}
