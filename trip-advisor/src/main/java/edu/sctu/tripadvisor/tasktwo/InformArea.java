package edu.sctu.tripadvisor.tasktwo;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

/**
 * Created by Eoly on 2016/9/22.
 */
public class InformArea {
    public static void main(String[] args) {
        try {
            Document document = Jsoup.connect("http://www.tripadvisor.cn/Tourism-g297463-Chengdu_Sichuan-Vacations.html").get();
            Element divElement = document.select("div.navLinks").first();
            Elements elemA = divElement.select("li a");
            for (Element element:elemA){
                String information[]=new String[]{
                        element.select(".typeName").first().text(),
                        element.select(".typeQty").first().text(),
                        element.select(".contentCount").first().text(),
                        element.attr("abs:href")
                };
                System.out.print(information[0]+"数量："+information[1]+"\t"
                        +information[0]+"点评数量："+information[2]+"\t"
                        +information[0]+"列表链接地址："+information[3]+"\t");
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}