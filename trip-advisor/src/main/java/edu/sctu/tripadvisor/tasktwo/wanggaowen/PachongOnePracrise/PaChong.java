package edu.sctu.tripadvisor.tasktwo.wanggaowen.PachongOnePracrise;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;


/**
 * Created by 15c on 2016/10/10.
 */
public class PaChong {
    Elements elements1 = null;
    int count = 0;


    protected void getData() throws IOException {
        DataParse.getConn();
        String[] sr = {"id int primary key", "rank TEXT", "name TEXT ", "url char(100)"};
        DataParse.creatTable("hotCity", sr);//创建表
        System.out.println("---------------------------------------------");
        for (int i = 0; i < 22; i++) {
            org.jsoup.nodes.Document document1 = Jsoup.connect("http://www.tripadvisor.cn/TourismChildrenAjax?geo=297462&offset="
                    + i + "&desktop=true").get();
            elements1 = document1.select("a");

            for (Element element : elements1) {
                SetDataUrl setDataUrl = new SetDataUrl();
                setDataUrl.setCityRank(element.select("span.rankNum").text());
                setDataUrl.setCityName(element.select("span.name").text());

                setDataUrl.setCityUrl("http://www.tripadvisor.cn" + element.attr("href"));

                DataParse.InsertData("hotCity", ++count, setDataUrl.getCityRank(), setDataUrl.getCityName(), setDataUrl.getCityUrl());//插入数据
                System.out.println("***********************************************************");

            }
        }
    }
}

