package edu.sctu.tripadvisor.tasktwo.wanggaowen.fileInstream;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by 15c on 2016/10/20.
 */
public class Main {
    public static void main(String[] args) {
        GetHotelUrl getHotelUrl=new GetHotelUrl();
        List<Hotels> hotels=new LinkedList<Hotels>();
        hotels.add(new Hotels("酒店名称","地点"));
        hotels.add(new Hotels("龙庭","龙泉"));
        hotels.add(new Hotels("梅州","北京"));
        hotels.add(new Hotels("峨眉大酒店","峨眉"));
        hotels.add(new Hotels("天波大酒店","犍为"));


        try {
            for (int i=0;i<hotels.size();i++) {
                getHotelUrl.expot1(hotels.get(i) );
                System.out.println("已经成功写入一条数据");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                getHotelUrl.writer.close();//关闭流；
                System.out.print("成功关闭流");
            } catch (IOException e) {
                System.out.print("关闭流失败");
                e.printStackTrace();
            }
        }
    }
}
