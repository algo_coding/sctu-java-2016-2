package edu.sctu.tripadvisor.taskfour.liliang;

/**
 * Created by asus on 2016/10/27.
 */
public class Student {
    private String studentName;
    private int num;
    private String ssex;
    private int age;

    public void run(){
        System.out.println("Studnet can run!");
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSsex() {
        return ssex;
    }

    public void setSsex(String ssex) {
        this.ssex = ssex;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
