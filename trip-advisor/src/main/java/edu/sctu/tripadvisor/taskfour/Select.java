package edu.sctu.tripadvisor.taskfour;

        import com.sun.xml.internal.bind.v2.runtime.output.NamespaceContextImpl;
        import org.jsoup.Jsoup;
        import org.jsoup.nodes.Document;
        import org.jsoup.nodes.Element;
        import org.jsoup.select.Elements;
        import java.io.IOException;
        import java.util.Scanner;

/**
 * Created by dell on 2016/9/27.
 */
public class Select {
    public static void main(String[] args) {
        /* 第一页*/
        try {
            System.out.println("//////////////////////////第一页////////////////////////////");
            Document document= Jsoup.connect("http://www.tripadvisor.cn/Hotel_Review-g297463-d4810348-Reviews-The_Ritz_Carlton_Chengdu-Chengdu_Sichuan.html").get();
            document.location();
            Elements NameEle=document.select("div.username");
            System.out.println("第一页点评者昵称：" + NameEle.text());
//            点评者昵称
            Elements CaptionEle=document.select("span.noQuotes");
            System.out.println("第一页标题："+CaptionEle.text());
//            标题
            Elements TimeEle=document.select("span.ratingDate");
            System.out.print("第一页时间：");
            System.out.print(" " + TimeEle.text());
//            Elements TimeEle2=document.select("span.ratingDate").select(".relativeDate");
//            for (Element j:TimeEle2 ){
//                System.out.print(" " + j.attr(""));
//            }
            System.out.println();
            Elements ScoreEle=document.select("img.sprite-rating_s_fill");
            int n=0;
            System.out.print("第一页打分：");
            for (Element j:ScoreEle ){n++;
                if (n>=11)break;
                System.out.print(" " + j.attr("alt"));
            }
            System.out.println("");
//            打分
            Elements YonEle=document.select("a.viaMobile").select(".sprite-grayPhone");

             System.out.println("第一页是否为移动设备："+YonEle.text().trim());

            Elements  ViewEle=document.select("div.entry>p");
            StringBuffer stringBuffer=new StringBuffer();
            int num0=1;
            for(Element li:ViewEle )
            {
                stringBuffer.append("\n"+"\n"+"第"+num0+"位的评价："+li.text().trim());
                num0++;
            }
            System.out.println("第1页评论内容："+ stringBuffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("/////////////////////////////////////////////////////////////");
        /* 第二页*/
        int m=1;
        for (int n=10;n<=80;n=n+10) {
            m++;
            try {
                String ss="http://www.tripadvisor.cn/Hotel_Review-g297463-d4810348-Reviews-or"+n+"-The_Ritz_Carlton_Chengdu-Chengdu_Sichuan.html#REVIEWS";
                Document document= Jsoup.connect(ss ).get();
                document.location();
                Elements NameEle=document.select("div.username");
                System.out.println("第" + m + "页点评者昵称：" + NameEle.text());
//            点评者昵称
                Elements CaptionEle=document.select("span.noQuotes");
                System.out.println("第"+ m+"页标题："+CaptionEle.text());
//            标题
                Elements TimeEle=document.select("span.ratingDate");
                System.out.print("第" + m + "页时间：");
                System.out.print(" " + TimeEle.text());
                System.out.println();
                Elements ScoreEle=document.select("img.sprite-rating_s_fill");
                int nmu=0;
                System.out.print("第"+ m+"页打分：");
                for (Element j:ScoreEle ){nmu++;
                    if (n>=11)break;
                    System.out.print(" " + j.attr("alt"));
                }
                System.out.println("");
//            打分
                Elements YonEle=document.select("a.viaMobile").select(".sprite-grayPhone");
                System.out.println("第"+ m+"页是否为移动设备："+YonEle.text());
//            View
                Elements  ViewEle=document.select("div.entry>p");
                StringBuffer stringBuffer=new StringBuffer();
                int num=1;
                for(Element li:ViewEle )
                {
                    stringBuffer.append("\n"+"\n"+"第"+num+"位的评价："+li.text().trim());
                    num++;
                }
                System.out.println("第"+ m+"页评论内容："+ stringBuffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("/////////////////////////////////////////////////////////////");
        }


    }
}
