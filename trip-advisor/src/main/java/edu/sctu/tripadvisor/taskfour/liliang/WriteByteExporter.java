package edu.sctu.tripadvisor.taskfour.liliang;

import java.util.List;

/**
 * Created by asus on 2016/10/20.
 */
public interface WriteByteExporter {

    public void export(List<WriteByte> StudentNum,String StudentName);
}
