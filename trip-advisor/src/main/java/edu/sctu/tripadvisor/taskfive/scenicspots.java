package edu.sctu.tripadvisor.taskfive;

import java.io.*;
import java.sql.*;

/**
 * Created by lianjin on 2016/10/24.
 *
 *
 */
class ScenicSpots {
    public  void getdata() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn= DriverManager.getConnection("jdbc:mysql://172.16.10.37:3306/elite?user=elite&password=elite&useUnicode=true&characterEncoding=UTF8", "elite", "elite");
        Statement stmt=conn.createStatement();
        String sql="select * from ScenicSpots";
        ResultSet rs=stmt.executeQuery(sql);
        String city_name,provinceBelongs,url,rank,allassess;
        File file=new File("D:\\test.txt");
        BufferedWriter writer=null;


        try {
            writer=new BufferedWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (rs.next()){
            city_name=rs.getString("id");
            provinceBelongs=rs.getString("ScenicName");
            url=rs.getString("URL");
            rank=rs.getString("Rank");
            allassess=rs.getString("Allassess");
            System.out.println(city_name+"\t"+provinceBelongs+"\t"+url+"\t"+rank+"\t"+allassess);
            try {
                writer.write(city_name+"    "+provinceBelongs+"    "+url+"    "+rank+"    "+allassess);
                writer.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        ScenicSpots jj=new ScenicSpots();
        try {
            jj.getdata();
            System.out.println("加载完毕！");
            System.out.println();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
