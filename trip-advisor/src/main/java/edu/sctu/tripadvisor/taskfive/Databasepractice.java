package edu.sctu.tripadvisor.taskfive;
import java.sql.*;

/**
 * Created by 黎子新 on 2016/10/13.
 */
class DatabasePractice {
    public static void main(String[] args) {
        //声明Connection对象
        Connection con;
        //驱动程序名
        String driver = "com.mysql.jdbc.Driver";
        //URL指向要访问的数据库名mydata
        String url = "jdbc:mysql://localhost/mysql";
        //MySQL配置时的用户名
        String user = "root";
        //MySQL配置时的密码
        String password = "";
        try {
            Class.forName(driver);
            con=DriverManager.getConnection(url,user,password);
            String sq="insert into student values ('10')";
            Statement  state=con.createStatement();
            state.executeUpdate(sq);
            con.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        } catch (SQLException e) {

            e.printStackTrace();
        }

    }
}
