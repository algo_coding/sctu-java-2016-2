package edu.sctu.tripadvisor.taskthree.liyouhong.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Administrator on 2016/10/27 0027.
 */
public class Client {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Student stu = new  Student();
        stu.setName("李友红");
        stu.setSex("男");
        stu.getNo(1506101141);

        Class reflect = Class.forName("edu.sctu.tripadvisor.taskthree.liyouhong.reflection.Student");
        Method method = reflect.getDeclaredMethod("eat");
        method.invoke(stu);


//        Field[] fields =reflect.getDeclaredFields();  //获得属性
//        for (Field field : fields){
//                System.out.println("获得的属性："+field.getName());
//        }
//
//        Method[] methods = reflect.getMethods();    //获得方法（包括父类的方法）
//            for (Method method :methods){
//                System.out.println("获得的方法："+method.getName());
//         }



//        Student Li = (Student) reflect.newInstance();
//        Li.setName("td");
//        System.out.println(Li.getName());


    }
}
