package edu.sctu.tripadvisor.taskthree.liyouhong.Experment.Three.ExpermentOne;

/**
 * Created by Administrator on 2016/11/3 0003.
 */
public class Trangle {
    double sideA, sideB, sideC, area, length;
    boolean boo;

    public Trangle(double a, double b, double c) {
        this.sideA = a;
        this.sideB = b;
        this.sideC = c;
        //参数a,b,c分别赋值给sideA,sideB,sideC

        if ((sideA + sideB > sideC && sideA - sideB < sideC) && (sideA + sideC > sideB && sideA - sideC < sideB) && (sideB + sideC > sideA && sideB - sideC < sideA)) //a,b,c构成三角形的条件表达式
        {
            boo = true; //给boo赋值。
        } else {
            boo = false; //给boo赋值。
        }
    }

    double getLength() {
        if (boo) {
            length = sideA + sideB + sideC;

            return length;
        } else {
            System.out.println("不是一个三角形,不能计算周长");
            return 0;
        }
        //方法体，要求计算出length的值并返回
    }

    public double getArea() {
        if (boo) {
            double p = (sideA + sideB + sideC) / 2.0;
            area = Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
            return area;
        } else {
            System.out.println("不是一个三角形,不能计算面积");
            return 0;
        }
    }

    public void setABC(double a, double b, double c) {
        this.sideA = a;
        this.sideB = b;
        this.sideC = c; //参数a,b,c分别赋值给sideA,sideB,sideC
        if ((sideA + sideB > sideC && sideA - sideB < sideC) && (sideA + sideC > sideB && sideA - sideC < sideB) && (sideB + sideC > sideA && sideB - sideC < sideA)) //a,b,c构成三角形的条件表达式
        {
            boo = true; //给boo赋值。
        } else {
            boo = false; //给boo赋值。
        }
    }

    public double getSideA() {

        return sideA;
    }

    public double getSideB() {

        return sideB;
    }

    public double getSideC() {

        return sideC;
    }
}
