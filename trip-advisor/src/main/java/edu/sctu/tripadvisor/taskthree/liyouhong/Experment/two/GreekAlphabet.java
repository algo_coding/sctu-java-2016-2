package edu.sctu.tripadvisor.taskthree.liyouhong.Experment.two;

/**
 * Created by Administrator on 2016/10/27 0027.
 */
public class GreekAlphabet {
    public static void main(String[] args) {
        int sPosition = 0, ePosition = 0;
        char cStart = 'α', cEnd = 'ω';
        sPosition = (char) cStart;
        ePosition = (char) cEnd;

        System.out.println("起始位置" + sPosition);
        System.out.println("结束位置" + ePosition);

        for (int i = sPosition; i <= ePosition; i++) {
            System.out.print((char) i);
        }

    }
}
