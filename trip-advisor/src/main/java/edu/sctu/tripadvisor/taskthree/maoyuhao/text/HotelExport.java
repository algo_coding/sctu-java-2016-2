package edu.sctu.tripadvisor.taskthree.maoyuhao.text;

import edu.sctu.tripadvisor.export.*;

import java.util.List;

/**
 * Created by cc on 2016/10/20.
 */
public interface HotelExport {
    public void export(List<Hotel> hotelList, String fileName);
}
