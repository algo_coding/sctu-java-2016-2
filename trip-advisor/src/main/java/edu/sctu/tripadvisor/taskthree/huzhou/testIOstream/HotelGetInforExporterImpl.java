package edu.sctu.tripadvisor.taskthree.huzhou.testIOstream;

import edu.sctu.tripadvisor.export.Hotel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.Buffer;
import java.util.List;

/**
 * Created by Administrator on 2016/10/20.
 */
public class HotelGetInforExporterImpl implements HotelGetInfor{

    @Override
    public void hotelExpor(List<HotelInfor> hotelInforlist, String fileName)  {
        File file=new File(fileName);
        BufferedWriter writer=null;
        try {
            writer=new BufferedWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(HotelInfor hotel : hotelInforlist){
            try {
                writer.write(hotel.getName()+","+hotel.getPostion()+","+hotel.getRank());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                writer.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }




    }
}
