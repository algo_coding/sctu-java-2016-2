package edu.sctu.tripadvisor.taskthree.huangbaolian.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by hbl on 2016/10/27.
 */
public class ReflectExport {
    public void reflectTest() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

//        测试并列输出
//        List<Student> students = new ArrayList<Student>();
//        students.add(new Student("l",12,"男"));
//        students.add(new Student("h", 13, "女"));
//        for(Student oneByone : students){
//            System.out.println(oneByone.getAge()+"\t"+oneByone.getName()+"\t"+oneByone.getSex());
//        }

//        得到class
        Class laz = Class.forName("edu.sctu.tripadvisor.taskthree.huangbaolian.reflect.Student");


//        输出属性名

        Field[] fields = laz.getDeclaredFields();
        for(Field field:fields) {
            System.out.println(field.getName());
        }


//        生成对象
//        Student student = (Student) laz.newInstance();
//        方法的调用
//        Method method = laz.getDeclaredMethod("study");
//        method.invoke(student);

//        得到方法的名字
        Method[] allmethod = laz.getMethods();
        for(Method thisMethod:allmethod){
            System.out.println(thisMethod.getName());
        }


    }
}
