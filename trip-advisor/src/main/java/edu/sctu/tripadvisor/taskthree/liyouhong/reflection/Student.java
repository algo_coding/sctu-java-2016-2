package edu.sctu.tripadvisor.taskthree.liyouhong.reflection;

/**
 * Created by Administrator on 2016/10/27 0027.
 */
public class Student {
    private String name;
    private String sex;
    private int no;


    public void  eat(){

        System.out.println("all people eat food");

    }

    public int getNo(int i) {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getSex() {

        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
