package edu.sctu.tripadvisor.taskthree.liyouhong.Experment.Three.ExpermentOne;

/**
 * Created by Administrator on 2016/11/3 0003.
 */
public class Circle {
    double radius, area;

    Circle(double r) {
        this.radius = r;
        //方法体
    }

    double getArea() {
        area = 4 / 3.0 * 3.14 * radius * radius;
        return area;//方法体，要求计算出area返回
    }

    double getLength() {
        double length;
        length = 2 * 3.14 * radius;
        return length;
        //getArea方法体的代码,要求计算出length返回
    }

    void setRadius(double newRadius) {
        radius = newRadius;
    }

    double getRadius() {
        return radius;
    }

}
