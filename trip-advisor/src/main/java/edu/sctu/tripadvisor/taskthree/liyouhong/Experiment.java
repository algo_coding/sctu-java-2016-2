package edu.sctu.tripadvisor.taskthree.liyouhong;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

import static java.lang.Integer.parseInt;

/**
 * Created by Administrator on 2016/10/11 0011.
 */
public class Experiment {
    public static void main(String[] args) {
        try {
            Document doc = Jsoup.connect("http://www.tripadvisor.cn/Hotels-g297463-Chengdu_Sichuan-Hotels.html").get();

            Elements el =doc.select("div.pageNumbers>a");
            for(int i=0;i<el.size();i++){
                int page = parseInt(el.get(i).text());
                if(page>10){
                    System.out.println(el.get(i).text());
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
