package edu.sctu.tripadvisor.taskthree.liyouhong.Experment.Three.ExpermentOne;

/**
 * Created by Administrator on 2016/11/3 0003.
 */
public class AreaAndLength {
    public static void main(String[] args) {
        Trangle tr = new Trangle(3, 4, 5);
        Circle cc = new Circle(2);
        Lader ld= new Lader(2,3,5);
        tr.getLength();
        tr.getArea();
        System.out.println("三角形周长为：" + tr.getLength());
        System.out.println("三角形面积为：" + tr.getArea());
        System.out.println("三角形的三边为："+tr.getSideA()+","+tr.getSideB()+","+tr.getSideC());

        System.out.println("修改三角形边长");
        tr.setABC(34, 12, 1);
        tr.getLength();
        tr.getArea();
        System.out.println("三角形周长为：" + tr.getLength());
        System.out.println("三角形面积为：" + tr.getArea());

        cc.getArea();
        System.out.println("圆的面积为：" + cc.getArea());
        cc.getLength();
        System.out.println("圆的周长为：" + cc.getLength());

        System.out.println("修改圆的半径：");
        cc.setRadius(5);
        cc.getArea();
        System.out.println("修改后圆的面积为：" + cc.getArea());
        cc.getLength();
        System.out.println("修改后圆的周长为：" + cc.getLength());

        ld.getArea();
        System.out.println("梯形的面积为："+ld.getArea());

    }
}
