package edu.sctu.tripadvisor.taskthree.huangbaolian.reflect;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by hbl on 2016/10/27.
 */
public class Test_Student {
    @Test
    public void test() {
        ReflectExport reflectExport = new ReflectExport();
        try {
            try {
                reflectExport.reflectTest();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}
