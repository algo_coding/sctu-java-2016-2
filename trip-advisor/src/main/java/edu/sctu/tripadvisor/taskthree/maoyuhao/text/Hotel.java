package edu.sctu.tripadvisor.taskthree.maoyuhao.text;

/**
 * Created by cc on 2016/10/20.
 */
public class Hotel {
    private String Rank;
    private String Comment;
    private String Hotel;

    public Hotel(String Rank,String Comment,String Hotel){
        this.Rank=Rank;
        this.Comment=Comment;
        this.Hotel=Hotel;
    }

    public String getRank() {
        return Rank;
    }
    public void setRank(String rank) {Rank = rank;
    }
    public String getComment() {
        return Comment;
    }
    public void setComment(String comment) {Comment = comment;
    }

    public String getHotel() {
        return Hotel;
    }
    public void setHotel(String hotel) {
        Hotel = hotel;
    }



}
