package edu.sctu.tripadvisor.taskthree.liyouhong.Experment.Eight.Three;

import java.awt.*;

/**
 * Created by Administrator on 2016/12/15 0015.
 */
public class ImageCanvas extends Canvas {
    private static final long serialVersionUID = 1L;
    Image image = null;

    public ImageCanvas() {
        setSize(200, 200);
    }

    public void paint(Graphics g) {
        if (image != null) g.drawImage(image, 0, 0, this);
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
