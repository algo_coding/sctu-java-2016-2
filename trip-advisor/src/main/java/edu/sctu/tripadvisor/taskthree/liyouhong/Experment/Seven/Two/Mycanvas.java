package edu.sctu.tripadvisor.taskthree.liyouhong.Experment.Seven.Two;

import java.awt.*;

/**
 * Created by Administrator on 2016/12/8 0008.
 */
public class Mycanvas extends Canvas {
    int r;
    Color c;
    public void setColor(Color c)
    { this.c=c;
    }
    public void setR(int r)
    {  this.r=r;
    }
    public void paint(Graphics g)
    { g.setColor(c);
        g.fillOval(0,0,2*r,2*r);
    }
    public int getR()
    {  return r;
    }

}
