package edu.sctu.tripadvisor.taskthree.liyouhong;

import java.io.*;

/**
 * Created by Administrator on 2016/10/25 0025.
 */
public class ReadFromFileByChar {
    public void readFileByChar(String fileName){
        File file = new File(fileName);
        Reader reader= null;


        try {
            System.out.println("以字符为单位读取文件内容，一次读一个字节:");
            reader = new InputStreamReader(new FileInputStream(file));
            int tempchar;



            try {
                while((tempchar=reader.read())!=-1){
                    if(((char)tempchar) != '\r'){
                        System.out.println((char)tempchar);
                    }


                }
            } catch (IOException e) {
                e.printStackTrace();
            }

                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        ReadFromFileByChar rfbc = new ReadFromFileByChar();
        rfbc.readFileByChar("e:\\data.txt");

    }

}
