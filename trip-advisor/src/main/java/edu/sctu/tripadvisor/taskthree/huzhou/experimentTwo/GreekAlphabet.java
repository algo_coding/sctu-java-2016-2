package edu.sctu.tripadvisor.taskthree.huzhou.experimentTwo;

/**
 * Created by huhzou on 2016/11/3.
 */
public class GreekAlphabet {
    public static void main(String[] args) {
        char a = 'α';
        char b = 'ω';
        System.out.println("希腊字母α在unicode码中的位置:" + (int) a);
        System.out.println("希腊字母ω在unicode码中的位置:" + (int) b);
        System.out.println("输出希腊字母");
        for (char i = 'α'; i <= 'ω'; i++) {
            System.out.print(i + " ");
        }
    }

}

