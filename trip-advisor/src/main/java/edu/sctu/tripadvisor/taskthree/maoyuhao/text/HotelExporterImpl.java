package edu.sctu.tripadvisor.taskthree.maoyuhao.text;

import edu.sctu.tripadvisor.export.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by cc on 2016/10/24.
 */
public class HotelExporterImpl implements HotelExport {
    @Override
    public void export(List<Hotel> hotelList, String fileName) {
        File file = new File(fileName);

        BufferedWriter writer = null;

        try {
            writer = new BufferedWriter(new FileWriter(file));

//            writer.write("hello,world!");

            for (Hotel hotel : hotelList) {
                // name, position
                writer.write(hotel.getRank() + " , " + hotel.getComment() + " , " +hotel.getHotel());
                writer.newLine();      //换行
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}

