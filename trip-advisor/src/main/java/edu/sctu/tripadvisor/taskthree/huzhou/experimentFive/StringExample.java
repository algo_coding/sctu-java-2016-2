package edu.sctu.tripadvisor.taskthree.huzhou.experimentFive;

/**
 * Created by Administrator on 2016/11/17.
 */
public class StringExample {
    public static void main(String[] args) {
        String s1=new String("you are a student"), s2=new String("how are you");
        if(s1.equals(s2)){
            System.out.println("s1和s2相同！");
        }else {
            System.out.println("s1和s2不同！");
        }

        String s3=new String("22030219851022024");
        if(s3.substring(0,6).equals("220302")){
            System.out.println("吉林省的身份证");
        }

        String s4=new String("你"),s5=new String("我");
        if(s4.compareTo(s5)<0){
            System.out.println("按字典序s4大于s5");
        }else{
            System.out.println("按字典序s4小于s5");
        }

        String s6=new String("100"), s7=new String("123.678");
        int n1=Integer.parseInt(s6); //将s6转化成int型数据。
        double n2=Double.parseDouble(s7);  //将s7转化成double型数据。
        double m=n1+n2;
        System.out.println(m);

        int position;
        String s8=String.valueOf(m); //String调用valuOf(int n)方法将m转化为字符串对象
        position=s8.indexOf(".");
        String temp=s8.substring(position+1);
        System.out.println("数字"+m+"有"+temp.length()+"位小数") ;
        String s9=new String("ABCDEF");
        char a[]= s9.toCharArray();   //将s8存放到数组a中。
        for(int i=a.length-1;i>=0;i--)
        {
            System.out.print(" "+a[i]);
        }

    }

}
