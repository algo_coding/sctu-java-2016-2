package edu.sctu.tripadvisor.taskthree.huzhou;

import edu.sctu.tripadvisor.core.hotelinformation.PageForwarding;

import java.util.Iterator;
import java.util.List;

/**
 * Created by huzhou on 2016/10/13.
 * 用于获取每个小的页面的url
 */
public class GetSUrl {
    String smallurlname;
    PageForwarding getSmallUrl=new PageForwarding();//new一个PageForwarding
    List<String> smallurl=getSmallUrl.getpageforwarding();//将获得的PageForwarding对象存到smallurl
    //该方法用于遍历smallurl这个数组并返回一个字符串
    public String traverseSmallUrl(){
        Iterator<String> li=smallurl.iterator();
        while (li.hasNext()){
           smallurlname=li.next();
        }
       return smallurlname;
    }
}
