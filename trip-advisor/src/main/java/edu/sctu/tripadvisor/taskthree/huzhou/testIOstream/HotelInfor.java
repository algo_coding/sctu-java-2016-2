package edu.sctu.tripadvisor.taskthree.huzhou.testIOstream;

/**
 * Created by Administrator on 2016/10/20.
 */
public class HotelInfor {
    private String name;
    private  String postion;
    private  String rank;

    public HotelInfor(String name, String postion, String rank) {
        this.name=name;
        this.postion=postion;
        this.rank=rank;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostion() {
        return postion;
    }

    public void setPostion(String postion) {
        this.postion = postion;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }
}
