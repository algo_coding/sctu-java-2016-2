package edu.sctu.tripadvisor.taskthree.huzhou.experimentFour;

/**
 * Created by Administrator on 2016/11/14.
 */
public class Sort_Demo {
    int i, j, k, swap;

    Sort_Demo() {
        i = j = k = swap = 0;
    }

    void sort(int t1, int t2[]) {                   //父类中的方法用来实现升序
        //用选择法按升序排列
        for (i = 0; i < t1 - 1; i++) {
            k = i;
            for (j = i + 1; j < t1; j++)
                if (t2[j] < t2[k]) k = j;
            if (k != i) {
                swap = t2[i];
                t2[i] = t2[k];
                t2[k] = swap;
            }
        }
    }
}
