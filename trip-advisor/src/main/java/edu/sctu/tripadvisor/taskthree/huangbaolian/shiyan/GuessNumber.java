package edu.sctu.tripadvisor.taskthree.huangbaolian.shiyan;

import java.util.Scanner;

/**
 * Created by hbl on 2016/10/27.
 */
public class GuessNumber {
    public void gess(){
        System.out.println("给你一个1至100之间的整数,请猜测这个数");
        int realNumber= (int)(Math.random()*100)+1 ;
        System.out.println("输入您的猜测:");
        Scanner input  = new Scanner(System.in);
        int number = input.nextInt();
        while (number!=realNumber){
            if(number > realNumber){
                System.out.println("猜大了,再输入你的猜测:");
                number= input.nextInt();
            }else{
                System.out.println("猜小了,再输入你的猜测:");
                number= input.nextInt();
            }
        }
        System.out.println("猜对了!");
    }

}
