package edu.sctu.tripadvisor.taskthree.liyouhong.Experment.Three.ExpermentTwo;

/**
 * Created by Administrator on 2016/11/3 0003.
 */
public class Example {
    public static void main(String[] args) {
        Compare compare = new Compare();
        compare.b = 100;           //通过类名操作类变量b,并赋值100
        compare.inputB();            //通过类名调用方法inputB()
        Compare cat = new Compare();
        Compare dog = new Compare();
        cat.setA(200);    //cat象调用方法setA(int a)将cat的成员a的值设置为200
        cat.setB(400);    //cat调用方法setB(int b)将cat的成员b的值设置为400
        cat.inputA();   //cat调用inputA()。
        cat.inputB();   //cat调用inputB()。


        dog.setA(150);    //dog象调用方法setA(int a)将dog的成员a的值设置为150
        dog.setB(300);   //dog调用方法setB(int b)将dog的成员b的值设置为300
        dog.inputA();  //dog调用inputA()。
        dog.inputB();  //dog调用inputB()。

    }
}
