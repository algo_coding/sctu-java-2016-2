package edu.sctu.tripadvisor.taskthree.liyouhong;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

import static java.lang.Integer.parseInt;

/**
 * Created by Administrator on 2016/10/18 0018.
 */
public class JumpPage {
    public static void main(String[] args) {
        int pagenumber;
        ConnectionSql CS = new ConnectionSql();
        String url = "http://www.tripadvisor.cn/Hotels-g297463-Chengdu_Sichuan-Hotels.html";
        try {
            Document doc = Jsoup.connect(url).get();

            Elements el =doc.select("div.pageNumbers>a");
            for(int i=0;i<el.size();i++) {
                int page = parseInt(el.get(i).text());
                if (page > 10) {
                    pagenumber = page;
                    for (int j = 1; j <= pagenumber; j++) {
                        if (j == 1) {
                            CS.connectionInsert(url);//酒店首页url
                        } else {
                            Elements sdf = doc.select("[data-page-number=" + j + "]");
                            String href = sdf.attr("href"); //酒店获得部分url

                            //System.out.println("获得部分url" + href);

                            String pageHref = "http://www.tripadvisor.cn/" + href;  //酒店完整url

                            CS.connectionInsert(pageHref);
                            doc = Jsoup.connect(pageHref).get();
                             System.out.println("完整url:" + pageHref);

                        }
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
