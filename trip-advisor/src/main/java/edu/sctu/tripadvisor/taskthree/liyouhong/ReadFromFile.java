package edu.sctu.tripadvisor.taskthree.liyouhong;

import java.io.*;

/**
 * Created by Administrator on 2016/10/25 0025.
 */
public class ReadFromFile {
public void readFromFileByBates(String fileName){
        File file = new File(fileName);
        InputStream in = null;


    try {
        in=new FileInputStream(file);
        int tempbyte;
        try {
            while ((tempbyte = in.read()) != -1) {
                System.out.write(tempbyte);
                System.out.println("");
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    } catch (FileNotFoundException e) {
        e.printStackTrace();
    }
    try {
        System.out.println("以字节为单位读取文件内容，一次读多个字节：");
        // 一次读多个字节
        byte[] tempbytes = new byte[100];
        int byteread = 0;
        in = new FileInputStream(file);
        ReadFromFile.showAvailableBytes(in);
        // 读入多个字节到字节数组中，byteread为一次读入的字节数
        while ((byteread = in.read(tempbytes)) != -1) {
            System.out.write(tempbytes, 0, byteread);
        }
    } catch (Exception e1) {
        e1.printStackTrace();
    } finally {
        if (in != null) {
            try {
                in.close();
            } catch (IOException e1) {
            }
        }
    }
}

    private static void showAvailableBytes(InputStream in) {
        try {
            System.out.println("当前字节输入流中的字节数为:" + in.available());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {
        ReadFromFile rff = new ReadFromFile();
        rff.readFromFileByBates("e:\\data.txt");
    }
}
