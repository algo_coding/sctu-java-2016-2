package edu.sctu.tripadvisor.taskthree.huzhou.experimentThree;

/**
 * Created by Administrator on 2016/11/7.
 */
public class Circle {
    double radius,area;
    double Pi=3.1415926;
    public Circle(double r){
        this.radius=r;
    }
    double getArea(){
        area=Pi*(radius*radius);
        return area;
    }
    double getLength(){
        double length=2*Pi*radius;
        return length;
    }
    public void setRadius(double newRadius){
        radius=newRadius;
    }
    double getRadius(){
        return radius;
    }
}
