package edu.sctu.tripadvisor.taskthree.liyouhong.Experment.Five.three;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by Administrator on 2016/11/14 0014.
 */
public class BigIntegerExample {
    public static void main(String args[]) {
        BigInteger n1 = new BigInteger("987654321987654321987654321"),
                n2 = new BigInteger("123456789123456789123456789"),
                result = null;
        result = n1.add(n2);//n1和n2做加法运算
        System.out.println("和:" + result.toString());
        result = n1.subtract(n2);//n1和n2做减法运算
        System.out.println("差:" + result.toString());
        result = n1.multiply(n2);//n1和n2做乘法运算
        System.out.println("积:" + result.toString());
        result = n1.divide(n2);//n1和n2做除法运算
        System.out.println("商:" + result.toString());
        BigInteger m = new BigInteger("1968957"),
                COUNT = new BigInteger("0"),
                ONE = new BigInteger("1"),
                TWO = new BigInteger("2");
        System.out.println(m.toString() + "的因子有:");
        for (BigInteger i = TWO; i.compareTo(m) < 0; i = i.add(ONE)) {
            if ((n1.remainder(i).compareTo(BigInteger.ZERO)) == 0) {
                COUNT = COUNT.add(ONE);
                System.out.print("  " + i.toString());
            }
        }
        System.out.println("");
        System.out.println(m.toString() + "一共有" + COUNT.toString() + "个因子");
        Scanner cin=new Scanner(System.in);
        System.out.println("请输入：");
        int x=cin.nextInt();
        BigInteger a=BigInteger.valueOf(x);   //相当于寄存器输入的一个输入，试验里面有
        BigInteger b=BigInteger.ONE;          //代表一个数，值为1
        BigInteger s=BigInteger.ONE;          //同上
        for(BigInteger i=BigInteger.ONE;i.compareTo(a)<=0;i=i.add(b)){
            s=s.multiply(i);  //multiply相当与乘法
        }
        System.out.println(s);
//        int a=1;
//        int sum=0;
//        int max=999999999;
//        for(int i=1;i<max;i++){
//
//            sum=sum+a;
//            a++;
//        }
//        System.out.println("1+2+3…的前999999999项的和为："+sum);
    }
}
