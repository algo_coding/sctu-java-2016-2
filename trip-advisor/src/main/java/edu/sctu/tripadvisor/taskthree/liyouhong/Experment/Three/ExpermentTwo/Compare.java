package edu.sctu.tripadvisor.taskthree.liyouhong.Experment.Three.ExpermentTwo;

/**
 * Created by Administrator on 2016/11/3 0003.
 */
public class Compare {

    float a;             //声明一个float型实例变量a
    static float b;             //声明一个float型类变量b,即static变量b

    void setA(float a) {
        this.a = a;       //将参数a的值赋值给成员变量a
    }

    void setB(float b) {
        this.b = b;       //将参数b的值赋值给成员变量b
    }

    float getA() {
        return a;
    }

    float getB() {
        return b;
    }

    void inputA() {
        System.out.println(a+b);
    }

    static void inputB() {
        System.out.println(b);
    }


}
