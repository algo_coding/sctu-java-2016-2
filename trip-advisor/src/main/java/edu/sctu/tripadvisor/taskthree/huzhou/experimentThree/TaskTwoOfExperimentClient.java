package edu.sctu.tripadvisor.taskthree.huzhou.experimentThree;

/**
 * Created by Administrator on 2016/11/8.
 */
public class TaskTwoOfExperimentClient {
    public static void main(String[] args) {
        TaskTwoOfExperiment task=new TaskTwoOfExperiment();
        task.setB(100);
        task.inputB();
        TaskTwoOfExperiment cat=new TaskTwoOfExperiment();
        TaskTwoOfExperiment dog=new TaskTwoOfExperiment();
        cat.setA(200);
        cat.setB(400);
        dog.setA(150);
        dog.setB(300);
        cat.inputA();
        cat.inputB();
        dog.inputA();
        dog.inputB();
    }
}
