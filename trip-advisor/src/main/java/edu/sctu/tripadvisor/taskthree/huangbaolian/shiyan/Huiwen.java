package edu.sctu.tripadvisor.taskthree.huangbaolian.shiyan;

import java.util.Scanner;

/**
 * Created by hbl on 2016/10/27.
 */
public class Huiwen {
    public void criter(){
        System.out.print("输入一个数:");
        Scanner input  = new Scanner(System.in);
        int number = input.nextInt();
        if(number<99999||number>0){
            if(number>9999){
                System.out.println(number+"是5位数");
                if(number%10 == number/10000){
                    System.out.println(number+"是回文数");
                }
                else {
                    System.out.println(number+"不是回文数");
                }
            }else if(number>999){
                System.out.println(number+"是4位数");
                if(number%10 == number/1000){
                    System.out.println(number+"是回文数");
                }
                else {
                    System.out.println(number+"不是回文数");
                }
            }else if(number>99){
                System.out.println(number+"是3位数");
                if(number%10 == number/100){
                    System.out.println(number+"是回文数");
                }
                else {
                    System.out.println(number+"不是回文数");
                }
            }else if(number>9){
                System.out.println(number+"是2位数");
                if(number%10 == number/10){
                    System.out.println(number+"是回文数");
                }
                else {
                    System.out.println(number+"不是回文数");
                }

            }
        }else {
            System.out.println(number+"不在1至99999之间");
        }

    }
}
