package edu.sctu.tripadvisor.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Administrator on 2016/10/27.
 */
public class Client {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

//        Student chen = new Student();
//        chen.setName("chengongsuo");
//        chen.setAge(10);
//        chen.study();
        Class clz = Class.forName("edu.sctu.tripadvisor.reflection.Student");
//        Method method = clz.getDeclaredMethod("study");
//        method.invoke(chen);

//
//
//        Field[] fields = clz.getDeclaredFields();
//        for (Field field : fields){
//            System.out.println(field.getName());
//        }
//
//        Method[] methods = clz.getMethods();
//        for (Method method :methods){
//            System.out.println(method.getName());
//        }

//        chen.getName();

//        Class clz = Class.forName("edu.sctu.tripadvisor.reflection.Student");
        Student chen = (Student) clz.newInstance();
        chen.setName("chegongsuo");
        System.out.println(chen.getName());
    }
}
