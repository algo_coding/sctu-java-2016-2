package edu.sctu.tripadvisor.reflection;




/**
 * Created by Administrator on 2016/10/27.
 */
public class Student {
    private String name;
    private int age;

    private int no;


    public void study(){

        System.out.println("study hard!!!");
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
