package liliang.export;

import edu.sctu.tripadvisor.taskfour.liliang.WriteByte;
import edu.sctu.tripadvisor.taskfour.liliang.WriteByteExporter;
import edu.sctu.tripadvisor.taskfour.liliang.WriteByteExporterImpl;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus on 2016/10/20.
 */
public class WriteByteExportTest {


    @Test
    public void shou_print_Hello_World(){
        System.out.println("Hello,World!");

    }

    @Test
    public void shou_write_student_infomation(){

        List<WriteByte> writeBytes = new ArrayList<WriteByte>();

        writeBytes.add(new WriteByte("22","liliang"));
        writeBytes.add(new WriteByte("23","liliang"));

       WriteByteExporterImpl writeByteExporter = new WriteByteExporterImpl();

        writeByteExporter.export(writeBytes,"H:\\data.txt");

    }
}
