package edusctu.tripadvisor.export.citytypes;

import edu.sctu.tripadvisor.core.citytypes.ViewPlaces;
import edu.sctu.tripadvisor.core.citytypes.methordtwo.TextExport;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 小可爱罗希 on 2016/10/27.
 */
public class XiguaTest {
    @Test
    public void should_print_method_name() throws InvocationTargetException, IllegalAccessException {
        List<ViewPlaces> list = new ArrayList<ViewPlaces>();
        ViewPlaces viewPlaces = new ViewPlaces("34","www.xiaoheixiaohei.cn","www.palerock.cn","景点","成都","54");
        list.add(viewPlaces);

        TextExport textExport = new TextExport<List<ViewPlaces>>();
        textExport.export(list,null);

    }
}
