package edusctu.tripadvisor.export.citytypes;

import edu.sctu.tripadvisor.core.citytypes.ViewPlaces;
import edu.sctu.tripadvisor.core.citytypes.methordone.TxtDataAccess;
import org.junit.Test;

/**
 * Created by Eoly on 2016/10/20.
 */
public class TxtExportTest {
    @Test
    public void should_write_content_to_txt(){
        ViewPlaces viewPlaces = new ViewPlaces("151","www.baidu.com","www.edu.sctu.cn","搜索引擎","百度","5");

        TxtDataAccess txtDataAccess = new TxtDataAccess();
        txtDataAccess.saveInfor(viewPlaces);
    }
}
