package edu.sctu.java.lab_05;

import javax.swing.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2016/11/10.
 */
public class DateExample {
    public static void main(String args[ ])
    {
        String str= JOptionPane.showInputDialog("输入第一个日期的年份:");
        int yearOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份:");
        int monthOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期:");
        int dayOne=Integer.parseInt(str);
//        str=JOptionPane.showInputDialog("输入该日的小时:");
//        int hourOne=Integer.parseInt(str);
//        str=JOptionPane.showInputDialog("输入该小时的分钟:");
//        int minuteOne=Integer.parseInt(str);
//        str=JOptionPane.showInputDialog("输入该分钟的秒:");
//        int secondOne=Integer.parseInt(str);


        str=JOptionPane.showInputDialog("输入第二个日期的年份:");
        int yearTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份:");
        int monthTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期:");
        int dayTwo=Integer.parseInt(str);
//        str=JOptionPane.showInputDialog("输入该日的小时:");
//        int hourTwo=Integer.parseInt(str);
//        str=JOptionPane.showInputDialog("输入该小时的分钟:");
//        int minuteTwo=Integer.parseInt(str);
//        str=JOptionPane.showInputDialog("输入该分钟的秒:");
//        int secondTwo=Integer.parseInt(str);

        Calendar calendar=Calendar.getInstance();  //初始化日历对象
        // 将calendar的时间设置为yearOne年monthOne月dayOne日
        calendar.set(Calendar.YEAR,yearOne);
        calendar.set(Calendar.DATE,monthOne);
        calendar.set(Calendar.DATE,dayOne);
//        calendar.set(Calendar.HOUR,hourOne);
//        calendar.set(Calendar.MINUTE,minuteOne);
//        calendar.set(Calendar.SECOND,secondOne);
        long timeOne=calendar.getTimeInMillis();     //calendar表示的时间转换成毫秒




        //将calendar的时间设置为yearTwo年monthTwo月dayTwo日
        calendar.set(Calendar.YEAR,yearTwo);
        calendar.set(Calendar.DATE,monthTwo);
        calendar.set(Calendar.DATE,dayTwo);
//        calendar.set(Calendar.HOUR,hourTwo);
//        calendar.set(Calendar.MINUTE,minuteTwo);
//        calendar.set(Calendar.SECOND,secondTwo);
        long timeTwo=calendar.getTimeInMillis();    //calendar表示的时间转换成毫秒。




        Date date1=new Date(timeOne);      // 用timeOne做参数构造date1
        Date date2=new Date(timeTwo);     // 用timeTwo做参数构造date2
        if(date2.equals(date1))
        {
            System.out.println("两个日期的年、月、日完全相同");
        }
        else if(date2.after(date1))
        {
            System.out.println("您输入的第二个日期大于第一个日期");
        }
        else if(date2.before(date1))
        {
            System.out.println("您输入的第二个日期小于第一个日期");
        }
        long days=timeTwo-timeOne/(long)(24*60*60);//计算两个日期相隔天数

        System.out.println(yearOne+"年"+monthOne+"月"+dayOne+"日和"
                +yearTwo+"年"+monthTwo+"月"+dayTwo+"相隔"+days+"天");


        String s= JOptionPane.showInputDialog("输入您存入的本金:");
        int money=Integer.parseInt(s);
        double rate=0.000035;
        System.out.println("您的利息是："+money*rate);

    }

}
