package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * Created by Administrator on 2016/9/9.
 */
public class TestSwitch {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入您的成绩，以便我们帮您评定：");
        int n=scanner.nextInt();
        switch (n){
            case 1:
                System.out.println("不及格");break;
            case 2:
                System.out.println("及格");break;
            case 3:
                System.out.println("中等");break;
            case 4:
                System.out.println("良好");break;
            case 5:
                System.out.println("优秀");break;
            default:
                System.out.println("您输入的成绩有误，请重新输入");
        }
    }
}
