package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * Created by Administrator on 2016/9/9.
 */
public class TestWhile {
    public static void main(String[] args) {
        int sum=0;
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入您想求和的数:");
        int n=scanner.nextInt();
        int m=1;
        while (m<=n){
            sum+=m;
            m++;
        }
        System.out.println(sum);
    }
}
