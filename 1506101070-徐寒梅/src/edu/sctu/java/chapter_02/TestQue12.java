package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * Created by Administrator on 2016/9/9.
 */
public class TestQue12 {
    public static void main(String[] args) {
        int sum=0;
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入您想求和的数:");
        int n=scanner.nextInt();
        for (int i=1;i<=n;i++){
            sum+=i;
        }
        System.out.println(sum);
    }
}
