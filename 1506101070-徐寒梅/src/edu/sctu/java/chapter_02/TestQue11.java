package edu.sctu.java.chapter_02;

/**
 * Created by Administrator on 2016/9/20.
 */
public class TestQue11 {
    public static void main(String[] args) {
        // 打印的层数
        int n = 4;
        String space = " ";
        char c = 'A';
//分成上下两部分，分开打印
        // 打印上半部分
        for (int i = 1; i <= n; i++) {
            for (int j = n - i; j > 0; j--)
                System.out.print(space);
            for (int k = 1; k <= i; k++) {
                System.out.print(c + space);
                c++;
            }
            System.out.println();
        }
        // 打印下半部分
        for (int i = n; i > 0; i--) {
            for (int j = 0; j < n - i; j++)
                System.out.print(space);
            for (int k = i; k > 0; k--) {
                System.out.print(c + space);
                c++;
            }
            System.out.println();
        }

    }

}
