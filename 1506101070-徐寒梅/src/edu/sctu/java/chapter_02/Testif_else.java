package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * Created by Administrator on 2016/9/9.
 */
public class Testif_else {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入您的成绩，以便我们帮您评定等级:");
        int n=scanner.nextInt();
        if (n==1){
            System.out.println("不及格");
        }else if (n==2){
            System.out.println("及格");
        }else if (n==3){
            System.out.println("中等");
        }else if (n==4){
            System.out.println("良好");
        }else if(n==5){
            System.out.println("优秀");
        }
    }
}
