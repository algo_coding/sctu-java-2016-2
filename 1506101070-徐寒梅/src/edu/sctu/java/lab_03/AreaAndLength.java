package edu.sctu.java.lab_03;

/**
 * Created by Administrator on 2016/11/3.
 */
public class AreaAndLength {
    public static void main(String args[])
    {
        double length,area;
        Circle circle=null;
        Trangle trangle;
        Lader lader;
         //创建对象circle
        circle=new Circle(4);
        //创建对象trangle。
        trangle=new Trangle(3,4,5);
        //创建对象lader
        lader=new Lader(2,5,3);


         // circle调用方法返回周长并赋值给length
        length=circle.getLength();
        System.out.println("圆的周长:"+length);

         // circle调用方法返回面积并赋值给area
        area=circle.getArea();
        System.out.println("圆的面积:"+area);
        circle.setRadius(10);
        System.out.println("修改后的半径为：" + circle.getRadius());
        System.out.println("修改后的面积为："+circle.getArea());





         // trangle调用方法返回周长并赋值给length
        length=trangle.getLength();
        System.out.println("三角形的周长:"+length);



         // trangle调用方法返回面积并赋值给area
        area=trangle.getArea();
        System.out.println("三角形的面积:" + area);


         // lader调用方法返回面积并赋值给area
        area=lader.getArea();
        System.out.println("梯形的面积:"+area);


         // trangle调用方法设置三个边，要求将三个边修改为12,34,1。
        trangle.setABC(12, 34, 1);
        // trangle调用方法返回面积并赋值给area
        area=trangle.getArea();
        System.out.println("三角形的面积:"+area);


         // trangle调用方法返回周长并赋值给length
        length=trangle.getLength();
        System.out.println("三角形的周长:"+length);
    }
}
