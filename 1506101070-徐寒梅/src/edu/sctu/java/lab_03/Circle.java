package edu.sctu.java.lab_03;

/**
 * Created by Administrator on 2016/11/3.
 */
public class Circle {
    double radius,area;
    Circle(double r)
    {
        //方法体
        radius=r;
    }
    double getArea()
    {
         //方法体，要求计算出area返回
        area=3.14*radius*radius;
        return area;
    }
    double getLength()
    {
         //getLength方法体的代码,要求计算出length返回
        return 2*3.14*radius;
    }
    void setRadius(double newRadius)
    {
        radius=newRadius;
    }
    double getRadius()
    {
        return radius;
    }
}
