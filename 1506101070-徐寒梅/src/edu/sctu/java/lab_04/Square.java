package edu.sctu.java.lab_04;

/**
 * Created by Administrator on 2016/11/17.
 */
public class Square extends Rectangle implements EqualDiagonal{
         int side;

    public Square(int side) {
        super(side, side);
        this.side=side;
    }

    @Override
    public void getDiagonal() {
        System.out.println("正方形的对角线是："+Math.sqrt(2*side*side));
    }
}
