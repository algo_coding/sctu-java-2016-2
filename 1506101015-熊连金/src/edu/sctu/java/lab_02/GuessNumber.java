package edu.sctu.java.lab_02;

import javax.swing.*;

/**
 * Created by lianjin on 2016/11/1.
 */
public class GuessNumber
{
    public static void main (String args[ ])
    {
        System.out.println("给你一个1至100之间的整数,请猜测这个数");
        int realNumber=(int)(Math.random()*100)+1;
        int yourGuess=0;
        String str= JOptionPane.showInputDialog("输入您的猜测:");
        yourGuess=Integer.parseInt(str);
        while(realNumber!=yourGuess) //循环条件
        {
            if(realNumber<yourGuess) //条件代码
            {
                str=JOptionPane.showInputDialog("猜大了,再输入你的猜测:");
                yourGuess=Integer.parseInt(str);
            }
            else if(yourGuess<realNumber) //条件代码
            {
                str=JOptionPane.showInputDialog("猜小了,再输入你的猜测:");
                yourGuess=Integer.parseInt(str);
            }
        }
        System.out.println("猜对了!");
    }
}
