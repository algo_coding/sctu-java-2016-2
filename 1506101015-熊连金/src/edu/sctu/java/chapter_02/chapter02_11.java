package edu.sctu.java.chapter_02;

/**
 * Created by lianjin on 2016/9/13.
 * 请编写程序实现如下效果图。
 *1111111A
 *11111B   C
 *111D   E   F
 *1G   H   I   J
 *1K   L   M   N
 *111O   P   Q
 *11111R   S
 *1111111T
 */
public class chapter02_11 {
    public static void main(String[] args) {
        int i,j=1,m=6,x,y;
        char ch='A';
        for(i=1;i<=4;i++,m=m-2,j++){
            for(x=1;x<=m;x++){
                System.out.print(" ");
            }
            for(y=1;y<=i;y++){
                System.out.print(" " + ch + "  ");
            }
            System.out.println();
        }
    }
}
