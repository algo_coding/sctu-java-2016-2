package edu.sctu.java.chapter_02;

import java.util.Scanner;

/**
 * Created by lianjin on 2016/9/13.
 * 分别利用for语句、while语句以及do while语句编写一个求和程序（即sum=1+2+3+....+n）。
 */
public class chapter02_12_1 {
    public static void main(String[] args) {
        System.out.println("请输入要加到的数：");
        Scanner scan =new Scanner(System.in);
        int n=scan.nextInt();
        int i=1,sum=0;
        for(i=1;i<=n;i++){
            sum=sum+i;
        }
        System.out.println("1到"+n+"的和为："+sum);
    }
}
