package edu.sctu.java.chapter_02;

/**
 * Created by lianjin on 2016/9/13.
 *请编写输出乘法口诀表的程序。
 * 乘法口诀表的部分内容如下。
 *
 * 1*1=1
 * 1*2=2 2*2=4
 * 1*3=3 2*3=6 3*3=9
 * 。。。。。。
 */
public class chapter02_10 {
    public static void main(String[] args) {
        int i=1,j=1;
        for(i=1;i<=9;i++){
            for(j=1;j<=i;j++){
                System.out.print(j+"*"+i+"="+i*j+"  ");
            }
            System.out.println();
        }
    }
}
