package edu.sctu.java.chapter_02;

/**
 * Created by lianjin on 2016/9/18.
 * 复习break 和contiune语句，请调试本章设计这两个语句的程序。
 */
public class chapter02_13_1 {
    public static void main(String[] args) {

        outer:
        for( int i=0;i<5;i++){
            for( int j=0;j<5;j++){
                System.out.println(i+"   "+j);
                if(j==1)
                    break outer;
            }
        }
    }
}
