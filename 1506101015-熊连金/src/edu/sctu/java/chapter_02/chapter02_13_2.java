package edu.sctu.java.chapter_02;

/**
 * Created by lianjin on 2016/9/18.
 * 复习break 和contiune语句，请调试本章设计这两个语句的程序。
 */
public class chapter02_13_2 {
    public static void main(String[] args) {
        int sum=0;
        for( int i=1;i<10;i++){
            if(i%3!=0){
                continue;
            }else{
            }
            sum+=i;
        }
        System.out.println("sum="+sum);
    }
}
