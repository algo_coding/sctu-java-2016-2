package edu.sctu.java.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by lianjin on 2016/10/27.
 */
public class Client {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class lianjin=Class.forName("edu.sctu.java.reflection.Student");
        Student lian=(Student)lianjin.newInstance();
        lian.setno("12346");
        lian.setname("jkfk");
        lian.setage(20);
        System.out.println(lian.getage());
        System.out.println(lian.getname());
        System.out.println(lian.getno());


//获得类中的属性

        Field[] fields=lianjin.getDeclaredFields();
        for(Field field:fields){
            System.out.println(field.getName());
        }


//获得类中的方法

        Method[] methods=lianjin.getMethods();
        for(Method method:methods){
            System.out.println(method.getName());
        }
    }
}
