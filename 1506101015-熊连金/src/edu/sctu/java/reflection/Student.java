package edu.sctu.java.reflection;

/**
 * Created by lianjin on 2016/10/27.
 */
public class Student {
    private String name;
    private String no;
    private int age;
    public void study(){
        System.out.println("I can study!And i can eat things!");
    }
    public int getage() {
        return age;
    }
    public void setage(int age){
        this.age=age;
    }
    public String getname(){
        return name;
    }
    public void setname(String name){
        this.name=name;
    }
    public String getno(){
        return no;
    }
    public void setno(String no){
        this.no=no;
    }
}
