package edu.sctu.java.lab_05;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by lianjin on 2016/11/30.
 */
public class Jiecheng {
    public static void main(String[] args) {
        System.out.println("请输入数字：");
        Scanner scan=new Scanner(System.in);
        int m=scan.nextInt();
        BigInteger a=BigInteger.valueOf(m);
        BigInteger b=BigInteger.ONE;
        BigInteger s=BigInteger.ONE;
        for(BigInteger i=BigInteger.ONE;i.compareTo(a)<=0;i.add(b)){

            s=s.multiply(i);
        }
        System.out.println(m+"的阶乘是："+s);
    }
}
