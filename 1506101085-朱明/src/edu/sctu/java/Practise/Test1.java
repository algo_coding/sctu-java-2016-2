package Practise;
import java.util.Random;
import java.util.Scanner;
/**
 * Created by Administrator on 2016/11/15.
 */
public class Test1 {
        public static void main(String[] args) {
            System.out.println("是否机选一组双色球号码？(Y/N)");
            Scanner scanner = new Scanner(System.in);
            char a = scanner.next().charAt(0); // 输入一个char字符，0位即是第一位

            if (a == 'Y' || a == 'y') {
                Random suiji = new Random(); // 创建suiji方法
                int blue = suiji.nextInt(16);//从0到16中选一个数字个蓝球
                while(blue==0){
                    blue=suiji.nextInt(16);
                }//如果选到了0，再选一次给blue
                int red[] = new int[6];// 用一个6个元素的数组装红球
                for (int i = 0; i < 6; i++) {
                    red[i] = suiji.nextInt(33); // 随机分配0到33的整数
                    if (red[i] == 0) {i--;}
                    if (i == -1) {i = 0;}
                    for (int j = 0; j < i; j++) {
                        while (red[i] == red[j]) {i--;}// 发现相同的从新回去生成一个
                    }
                }
                System.out.print("红球：");
                for (int k = 0; k < red.length; k++) {
                    System.out.print(red[k] + " ");// 输出数组red
                }
                System.out.print("蓝球："+blue);
            } else
                System.out.println("........~");
        }
    }
