package src.edu.sctu.java.lab_06.lab_06_1;

import java.io.*;
import java.util.StringTokenizer;

/**可以将一个由汉字字符组成的文本文件读入到程序中；
 （2）单击名为“下一个汉字”的按钮，可以在一个标签中显示程序读入的一个汉字；
 （3）单击名为“发音”的按钮，可以听到标签上显示的汉字的读音。
 （4）用户可以使用文本编辑器编辑程序中用到的3个由汉字字符组成的文本文件：training1.txt、training2.txt和training.txt，这些文本文件中的汉字需要用空格、逗号或回车分隔。
 （5）需要自己制作相应的声音文件，比如：training1.txt文件包含汉字“你”，那么在当前应用程序的运行目录中需要有“你.wav”格式的声音文件。
 （6）用户选择“帮助”菜单，可以查看软件的帮助信息。

 * Created by Administrator on 2016/11/24.
 */
public class ChineseCharacters {
    public StringBuffer getChinesecharacters(File file){
        StringBuffer hanzi = new StringBuffer();
        FileReader  inOne= null;
        try {
            inOne = new FileReader(file);  //创建指向文件file的inOne 的对象
            BufferedReader inTwo=new BufferedReader(inOne);//创建指向文件inOne的inTwo的对象
            String s=null;
            int i=0;
            while((s=inTwo.readLine())!=null){//inTwo读取一行
                StringTokenizer tokenizer=new StringTokenizer(s,",'\n'");//StringTokenizer是一个用来分隔String的应用类，相当于VB的split函数.
                // 第一个参数就是要分隔的String，第二个是分隔字符集合，第三个参数表示隔符号是否作为标记返回，如果不指定分隔字符，默认的是：”\t\n\r\f”
                while(tokenizer.hasMoreElements()){//返回是否还有分隔符。
                    hanzi.append(tokenizer.nextToken());// String nextToken()：返回从当前位置到下一个分隔符的字符串。
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hanzi;
    }
}
