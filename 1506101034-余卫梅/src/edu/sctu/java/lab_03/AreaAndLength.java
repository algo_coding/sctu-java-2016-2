package src.edu.sctu.java.lab_03;

/**总共的主类,对于实验1
 * Created by Administrator on 2016/11/3.
 */
public class AreaAndLength {
    public static void main(String[] args) {  //主方法调用
        double length,area,side1,side2,side3;
        Lader lader=new Lader(5,6,7);
        Circle circle=new Circle(3.14);
        Trangle trangle= new Trangle(2,3,4);
        length=circle.getLength();
        System.out.println("圆的周长："+length);
        area=circle.getArea();
        System.out.println("圆的面积："+area);
        area=lader.getArea();
        System.out.println("梯形的面积："+area);
        trangle.setABC(12, 34, 1);
        side1=trangle.getSideA();
        side2=trangle.getSideB();
        side3=trangle.getSideC();
        System.out.println("三边分别为："+side1+" "+side2+" "+side3);
        area=trangle.getArea();
        System.out.println("三角形的面积："+area);
        length=trangle.getLength();
        System.out.println("三角形的周长为："+length);
        circle.setRadius(4);
        length=circle.getLength();
        System.out.println("半径改变后的圆周长为："+length);
        area=circle.getArea();
        System.out.println("半径改变后的圆面积为："+area);
    }
}
