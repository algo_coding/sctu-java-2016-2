package src.edu.sctu.java.lab_03;

/**对于实验2总共的主类。，就是静态类的引用
 * Created by Administrator on 2016/11/7.
 */
public class Example {
    public static void main(String[] args) {
      A.b=100;  // 通过类名操作类变量b，并赋值100
      A.inputB();  //通过类名调用方法inputB()
        A cat =new A();
        A dog = new A();
        cat.setA(200);
        cat.setB(400);
        dog.setA(150);
        dog.setB(200);
        cat.inputA();
        cat.inputB();
        dog.inputA();
        dog.inputB();
    }
}
