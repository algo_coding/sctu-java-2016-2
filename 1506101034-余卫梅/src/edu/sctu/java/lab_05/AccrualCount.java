package src.edu.sctu.java.lab_05;

import javax.swing.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by yuweimei on 2016/11/21.
 */
public class AccrualCount {
    public static void main(String[] args) {
        String str = JOptionPane.showInputDialog("请输入存款数目：（元）");
        int money = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入存款开始年份：");
        int year = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入存款开始月份：");
        int month = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入存款开始日期：");
        int day = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入存款结束年份：");
        int year2 = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入存款结束月份：");
        int month2 = Integer.parseInt(str);
        str = JOptionPane.showInputDialog("请输入存款结束日期：");
        int day2 = Integer.parseInt(str);
        str = JOptionPane.showInputDialog( "请输入一天的利率：");
        Double rate = Double.parseDouble(str);
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        long timeOne = calendar.getTimeInMillis();
        calendar.set(year2, month2, day2);
        long timeTwo = calendar.getTimeInMillis();
        Date date1 = new Date(timeOne);
        Date date2 = new Date(timeTwo);
        long days = (timeTwo - timeOne) / (1000 * 60 * 60 * 24);
        System.out.println( "经计算" + days + "天后" + "您可得到本金加利息共" + (money * days * rate + money) + "元");

    }
}
