package src.edu.sctu.java.lab_05;

import java.math.BigInteger;

/**
 * Created by yuweimei on 2016/11/21.
 */
public class AddCount {
    public static void main(String[] args) {
        BigInteger a = new BigInteger("1"),
                b = new BigInteger("999999999"),
                result = new BigInteger("1");
        for (BigInteger i = a; i.compareTo(b) < 0; i = i.add(a)) {
            result = result.add(i);
        }
        System.out.println("1+2+3…的前999999999项的和:" + result);
    }
}