package src.edu.sctu.java.lab_04;

/**
 * Created by yuweimei on 2016/11/14.
 */
public class A {
    int a = 4, b = 5, c = 6;

    A() {
        System.out.println("参数：a=" + a + "\t" + "b=" + b + "\t" + "c=" + c);
    }

    A(String d) {
        System.out.println(d);
    }
}

