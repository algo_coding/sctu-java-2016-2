package src.edu.sctu.java.lab_04;

/**
 * Created by yuweimei on 2016/11/14.
 */
class SubSortDemo extends SortDemo {
    void sort(int t1, int t2[]) {            //子类中的方法实现降序
        //用选择法实现降序排列
        for (i = 0; i < t1 - 1; i++) {
            k = i;
            for (j = i + 1; j < t1; j++)
                if (t2[j] > t2[k]) k = j;
            if (k != i) {
                swap = t2[i];
                t2[i] = t2[k];
                t2[k] = swap;
            }
        }
    }

}
