package src.edu.sctu.java.lab_04;

/**
 * Created by yuweimei on 2016/11/14.
 */
public class Rect {
    float a;
    float b;

    void setAb(float a, float b) {
        this.a = a;
        this.b = b;
    }

    double getArea() {
        return a * b;
    }
}
