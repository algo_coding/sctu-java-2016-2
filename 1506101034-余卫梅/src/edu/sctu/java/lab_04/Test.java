package src.edu.sctu.java.lab_04;

/**
 * Created by yuweimei on 2016/11/14.
 */
public class Test {
    public static void main(String[] args) {
        Rect rect = new Rect();
        rect.setAb(4, 5);
        double area = rect.getArea();
        System.out.print("矩形的长为：" + rect.a + "\n" + "矩形的宽为：" + rect.b + "\n" + "矩形的面积为:" + area+"\n");
        Square square = new Square();
        square.setA(12);
        double squareArea = square.getArea();
        double squareDiag = square.getDiagonal();
        System.out.print("正方形的边长为:" + square.a + "\n" + "正方形的对角线长为：" + squareDiag + "\n" + "正方形的面积为：" + squareArea);

    }
}
