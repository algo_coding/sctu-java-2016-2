package src.edu.sctu.java.lab_04;

import java.util.Scanner;

/**
 * Created by yuweimei on 2016/11/14.
 */
class Result implements Sum, Sub {
    public static void main(String[] args) {
        Result sum = new Result();
        sum.sum();
        sum.sub();
    }

    @Override
    public void sum() {
        System.out.println("请输入第一个数：");
        Scanner number1 = new Scanner(System.in);
        double a = number1.nextDouble();
        System.out.println("请输入第二个数：");
        Scanner number2 = new Scanner(System.in);
        double b = number2.nextDouble();
        double sum = a + b;
        System.out.println("这两个数的和为：" + sum);
    }

    @Override
    public void sub() {
        System.out.println("请输入第一个数：");
        Scanner number3 = new Scanner(System.in);
        double c = number3.nextDouble();
        System.out.println("请输入第二个数：");
        Scanner number4 = new Scanner(System.in);
        double d = number4.nextDouble();
        double sub = c - d;
        System.out.println("这两个数的差为：" + sub);

    }
}
