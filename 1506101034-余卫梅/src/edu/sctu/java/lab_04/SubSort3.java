package src.edu.sctu.java.lab_04;

/**
 * Created by yuweimei on 2016/11/10.
 */
class SubSort3 extends Sort3 {
    void subsort(double i, double j, double k) {
        max1 = i;
        max2 = j;
        max3 = k;
        sort();  //调用父类中的方法sort()
    }
}
