package src.edu.sctu.java.lab_07.lab_07_1;

import java.awt.*;

/**
 * Created by yuweimei on 2016/12/22.
 */
public class WordThread extends Thread {
    char word;
    int k = 19968;
    Label com;
    int time = 6000;

    WordThread(Label com) {
        this.com = com;
    }

    public void setTime(int n) {
        time = n;
    }

    public void run() {
        k = 19968;
        while (true) {
            word = (char) k;
            com.setText("" + word);
            try {
                Thread.sleep(time);  //调用sleep方法使得线程中断6000豪秒
            } catch (InterruptedException e) {
            }
            k++;
            if (k >= 29968) k = 19968;
        }
    }
}
