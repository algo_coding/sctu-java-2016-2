package sctu.java.lab_4;

/**
 * Created by Administrator on 2016/11/23 0023.
 */
public class Rect{
    public double height=0;
    public double width=0;
    public Rect(double height,double width){
        this.height=height;
        this.width=width;
    }
    public double getArea(){
        return height*width;
    }
}
interface EqualDiagonal{
    double getDiagonal();
}
class Square extends Rect implements EqualDiagonal{
    public double side=0;
    public Square(double s){
        super(s,s);
        side=s;
    }
    public double getDiagonal(){
        return Math.sqrt(side*side+side*side);
    }
    public static void main(String[] args) {
        Rect rect=new Rect(5.0,8.0);
        Square squ=new Square(10.0);
        System.out.println("矩形面积为:"+String.valueOf(rect.getArea()));
        System.out.println("正方形面积为:"+String.valueOf(squ.getArea()));
        System.out.println("正方形对角线为:"+String.valueOf(squ.getDiagonal()));
    }
}
