package sctu.java.lab_5;

import javax.swing.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2016/11/16 0016.
 */
    public class MoneyExample {
    public static void main(String[] args){
        String str= JOptionPane.showInputDialog("输入存入的年份:");
        int yearOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入存入的月份:");
        int monthOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入存入月份的日期:");
        int dayOne=Integer.parseInt(str);

        str=JOptionPane.showInputDialog("输入取出存款的年份:");
        int yearTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入取出存款的月份:");
        int monthTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入取出存款的日期:");
        int dayTwo=Integer.parseInt(str);

        Calendar calendar=Calendar.getInstance();
        str=JOptionPane.showInputDialog("输入需要存的钱数：");
        int money=Integer.parseInt(str);
        calendar.set(yearOne,monthOne,dayOne);
        long timeOne=calendar.getTimeInMillis();
        calendar.set(yearTwo,monthTwo,dayTwo);
        long timeTwo=calendar.getTimeInMillis();
        Date date1=new Date(timeOne);
        Date date2=new Date(timeTwo);
        long lixi= Math.abs(timeTwo-timeOne)/(money*10);
        System.out.println(yearOne+"年"+monthOne+"月"+dayOne+"日到"
                +yearTwo+"年"+monthTwo+"月"+dayTwo+"日的利息为："+lixi);
}
    }

