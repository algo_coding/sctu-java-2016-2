package sctu.java.lab_5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Administrator on 2016/11/16 0016.
 */
public class BigShuExample {
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("请输入一个数:");
        int one = Integer.parseInt(in.readLine());//强制转换为需要的类型
        int sum=1;
        for(int i=one;i>0;i--){
            sum*=i;
        }
        System.out.println(one+"!"+"="+sum);
    }
    }


