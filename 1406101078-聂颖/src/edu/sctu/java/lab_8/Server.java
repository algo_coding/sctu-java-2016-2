package sctu.java.lab_8;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Administrator on 2016/12/22 0022.
 */
public class Server
{  public static void main(String args[])
{  ServerSocket server=null;
    ServerThread thread;
    Socket you=null;
    while(true)
    {  try{  server=new ServerSocket(4331);//创建在端口4331上负责监听的 ServerSocket对象
    }
    catch(IOException e1)
    {  System.out.println("正在监听");
    }
        try{  you=server.accept();// server返回和客户端相连接的Socket对象
            System.out.println("客户的地址:"+you.getInetAddress());
        }
        catch (IOException e)
        {  System.out.println("正在等待客户");
        }
        if(you!=null)
        {  new ServerThread(you).start();
        }
        else{  continue;
        }
    }
}
}
class ServerThread extends Thread
{  Socket socket;
    ObjectInputStream in=null;
    ObjectOutputStream out=null;
    String s=null;
    ServerThread(Socket t)
    {  socket=t;
        try  { out=new ObjectOutputStream(socket.getOutputStream()); //socket返回输出流。
            in=new ObjectInputStream(socket.getInputStream()); //socket返回输入流。
        }
        catch (IOException e)
        {}
    }
    public void run()
    {  TextArea text=new TextArea("你好,我是服务器",12,12);
        try{  out.writeObject(text);
        }
        catch (IOException e)
        {   System.out.println("客户离开");
        }
    }
}

