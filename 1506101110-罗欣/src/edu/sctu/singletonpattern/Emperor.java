package src.edu.sctu.singletonpattern;

/**
 * Created by Administrator on 2016/10/8.
 */
public class Emperor {
    private static Emperor emperor = null;  //声明一个Emperor类的引用
    private Emperor(){     //将构造方法私有

        }
    public static Emperor getInstance(){
        if(emperor==null){
            emperor=new Emperor();
        }

        return emperor;
    }
    public void getName(){       //使用普通方法输出皇帝
        System.out.println("我是皇帝：明日科技");
    }
}
