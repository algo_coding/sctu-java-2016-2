package src.edu.sctu.lab_05.lab_05_2;



import javax.swing.*;
import java.util.Calendar;
import java.util.Date;

/**编写一个Java应用程序，用户从输入对话框输入了两个日期，程序将判断两个日期的大小关系，以及两个日期之间的间隔天数。
 * Created by Administrator on 2016/11/17.
 */
public class DataExample {
    public static void main(String[] args) {
        String str= JOptionPane.showInputDialog("输入第一个日期的年份：");
        int yearOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份：");
        int monthOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期：");
        int dayOne=Integer.parseInt(str);
        str= JOptionPane.showInputDialog("输入该日期的时分：");
        int hourOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该日期的分钟：");
        int minuteOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该日期的秒分：");
        int secondOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入第二个日期的年份：");
        int yearTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该年的月份：");
        int monthTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该月份的日期：");
        int dayTwo=Integer.parseInt(str);
        str= JOptionPane.showInputDialog("输入该日期的时分：");
        int hourTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该日期的分钟：");
        int minuteTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入该日期的秒分：");
        int secondTwo=Integer.parseInt(str);
        Calendar calendar=Calendar.getInstance();//初始化日历对象
       calendar.set(yearOne,monthOne,dayOne,hourOne,minuteOne,secondOne);//将calender时间设置为年月日
        Long timeOne=calendar.getTimeInMillis();//将calender时间设置为毫秒
        calendar.set(yearTwo,monthTwo,dayTwo,hourTwo,minuteTwo,secondTwo);
        long timeTwo=calendar.getTimeInMillis();
        Date data1=new Date(timeOne);//用timeOne作参数构造date1
        Date data2=new Date(timeTwo);//用timeTwo作参数构造date2
        if(data2.equals(data1)){
            System.out.println("两个日期的年、月、日完全相同");
        }
        else if(data2.after(data1)){//判断,如果时间在data1时间后,就执行后面操作
            System.out.println("您输入的第二个日期大于第一个日期");
        }
        else if(data2.before(data1)){
            System.out.println("您输入的第二个日期小于第一个日期");
        }
        Long days=(timeTwo - timeOne) / (long)(24*60*60*1000);//计算两日期相隔天数
        System.out.println(yearOne+"年"+monthOne+"月"+dayOne+"日"+hourOne+"时"+minuteOne+"分"+
                secondOne+"秒"+"和" +yearTwo+"年"+monthTwo+"月"+dayTwo+hourTwo+"时"+minuteTwo+"分"+secondTwo+"秒"+"相隔"+days+"天");
    }
}
