package src.edu.sctu.lab_05.lab_05_2;

import javax.swing.*;
import java.util.Calendar;

/**根据本程序中的一些知识，编写一个计算利息（按天计算）的程序。从输入对话框输入存款的数目和起止时间
 * 息计算公式：本金×利率×时间＝利息
 * Created by Administrator on 2016/11/17.
 */
public class Interest {
    public static void main(String[] args) {
        String str= JOptionPane.showInputDialog("请输入存款金额：");
        int money=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("请输入存款的年份：");
        int yearOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("请输入存款该年的月份：");
        int monthOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入存款该月份的日期：");
        int dayOne=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("请输入取款的年份：");
        int yearTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("请输入取款该年的月份：");
        int monthTwo=Integer.parseInt(str);
        str=JOptionPane.showInputDialog("输入取款该月份的日期：");
        int dayTwo=Integer.parseInt(str);
        Calendar calendar=Calendar.getInstance();
        calendar.set(yearOne,monthOne,dayOne);
        Long timeOne=calendar.getTimeInMillis();
        calendar.set(yearTwo,monthTwo,dayTwo);
        Long timeTwo=calendar.getTimeInMillis();
        Long days=(timeTwo - timeOne) / (long)(24*60*60*1000);
        Long month=days/30;

        double deposit=money*month*0.3;
        System.out.println("利息为："+deposit);
    }
}
