package src.edu.sctu.lab_05.lab_05_3;

import javax.swing.*;
import java.math.BigInteger;
import java.util.Scanner;

/**编写程序，计算大整数的阶乘。
 * Created by Administrator on 2016/11/17.
 */
public class Factorial {
    public static void main(String[] args) {
        /*String str= JOptionPane.showInputDialog("输入一个大整数：");
        String result=null;
        String i= JOptionPane.showInputDialog("请输入零：");
        String j=null;
        j=new BigInteger(i).toString();
        result =new BigInteger(str).toString();
        System.out.println(result);


        //result=new BigInteger(n1).multiply(new BigInteger(n2)).toString();*/
        Scanner cin=new Scanner(System.in);
        int x=cin.nextInt();
        BigInteger a=BigInteger.valueOf(x);
        BigInteger b=BigInteger.ONE;
        BigInteger s=BigInteger.ONE;
        for(BigInteger i=BigInteger.ONE;i.compareTo(a)<=0;i=i.add(b)){
            s=s.multiply(i);
        }
        System.out.println(s);
        cin.close();

    }
}
