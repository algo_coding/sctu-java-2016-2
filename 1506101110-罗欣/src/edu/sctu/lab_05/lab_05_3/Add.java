package src.edu.sctu.lab_05.lab_05_3;

import java.math.BigInteger;

/**编写程序，计算1+2+3…的前999999999项的和
 * Created by Administrator on 2016/11/17.
 */
public class Add {
    public static void main(String[] args) {
        BigInteger m=new BigInteger("999999999"),
        COUNT=new BigInteger("0"),
        ONE=new BigInteger("1");
        for(BigInteger i=ONE;i.compareTo(m)<0;i=i.add(ONE)){

            COUNT=COUNT.add(i);
        }
        System.out.println("1+2+3…的前999999999项的和:"+COUNT);

    }
}
