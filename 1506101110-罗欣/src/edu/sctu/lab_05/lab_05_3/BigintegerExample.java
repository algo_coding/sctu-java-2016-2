package src.edu.sctu.lab_05.lab_05_3;

import java.math.BigInteger;

/**编写一个Java应用程序，计算两个大整数的和、差、积和商，并计算一个大整数的因子个数（因子中不包括1和大整数本身）
 * Created by Administrator on 2016/11/17.
 */
public class BigintegerExample {
    public static void main(String[] args) {
        String n1=new String("987654321987654321987654321");
        String n2=new String("123456789123456789123456789");
        String result=null;
        result =new BigInteger(n1).add(new BigInteger(n2)).toString() ;
        System.out.println("和:"+result);
        result=new BigInteger(n1).subtract(new BigInteger(n2)).toString();
        System.out.println("差："+result);
        result=new BigInteger(n1).multiply(new BigInteger(n2)).toString();
        System.out.println("积："+result);
        result=new BigInteger(n1).divide(new BigInteger(n2)).toString();
        System.out.println("商："+result);

        BigInteger m=new BigInteger("1968957"),
                COUNT=new BigInteger("0"),
                ONE=new BigInteger("1"),
                TWO=new BigInteger("2");
        System.out.println(m.toString()+"的因子有:");
        for(BigInteger i=TWO;i.compareTo(m)<0;i=i.add(ONE))
            {
                if((i.remainder(i).compareTo(BigInteger.ZERO))==0)
                {
                    COUNT=COUNT.add(ONE);
                }
            }
        System.out.println("");
        System.out.println(m.toString()+"一共有"+COUNT.toString()+"个因子");

    }
}
