package src.edu.sctu.lab_05.lab_05_1;

/**String类的常用方法
 * Created by Administrator on 2016/11/17.
 */
public class StringExample {
    public static void main(String[] args) {
        String s1= new String("you are a student");
        String s2= "how are you";
        if(s1.equals(s2)){
            System.out.println("s1与s2相同");
        }
        else{
            System.out.println("s1与s2不相同");
        }
        String s3= new String("22030219851022024");
        if(s3.startsWith("220302")){
            System.out.println("吉林省省份证");
        }
        String s4=new String("你");
        String s5=new String("我");
        int size1=s4.compareTo(s5);//按字典序比较两个数的大小，当s4>s5时，size1是1，当s4<s5时，值为-1.
        if(size1==1){
            System.out.println("按字典序s4大于s5");
        }
        else{
            System.out.println("按字典序s4小于s5");
        }
        int position=0;
        String path="D:\\ADSafe\\adb";
     //   position=path.indexOf("a");  从0开始数出现的位数
        position=path.lastIndexOf("\\");//获取path中最后出现目录分隔符号的位置
        System.out.println("D:\\ADSafe\\adb中最后出现\\的位置是："+position);
        String s6= new String("１００");
        String s7="123.678";
        int n1=Integer.parseInt(s6);//将s6转化成int型数据
        double n2=Double.parseDouble(s7);//将s7转化成double型数据
        double m=n1+n2;
        System.out.println(m);
        String s8=String.valueOf(m);    //将double型变量m转化为String型
        position=s8.indexOf(".");   //获取字符串S8中.的索引位置
        String temp = s8.substring(position+1); //获取.后面的字符串
        System.out.println("数字"+m+"有"+temp.length()+"位小数");
        System.out.println("数字"+m+"整数部分为"+s8.substring(0,3));
        System.out.println("数字"+m+"小数部分为"+s8.substring(4));
        String s9="ABCDEF";
        char a[]=s9.toCharArray();//将字符串放到数组a中
        String ch=new String(a);  // 将数组a转化为字符串
        System.out.println(ch.substring(0,3));//输出前三个字符串
        System.out.println(path.substring(4,7));//获取字符串“DSa”
        for(int i=a.length-1;i>=0;i--){
            System.out.println(" "+a[i]);
        }
        String str1=new String("ABCABC"),
                str2=null,
                str3=null,
                str4=null;
        str2=str1.replaceAll("A","First");
        str3=str2.replaceAll("B","Second");
        str4=str3.replaceAll("C","Third");
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
        System.out.println(str4);
        Long i=new Long(12345);
        System.out.println("12345的二进制表示："+i.toBinaryString(i));
        System.out.println("12345的八进制表示："+i.toOctalString(i));
        System.out.println("12345的十六进制表示："+i.toHexString(i));
        System.out.println(i+"的二进制表示"+Long.toString(i,2));
        System.out.println(i+"的八进制表示"+Long.toString(i,8));
        System.out.println(i+"的十六进制表示"+Long.toString(i,16));
    }
}
