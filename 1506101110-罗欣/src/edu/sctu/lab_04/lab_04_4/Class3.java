package src.edu.sctu.lab_04.lab_04_4;

/**
 * Created by Administrator on 2016/11/10.
 */
public class Class3 {
    public static void main (String args[]){
        int a[]={34,12,8,67,88,23,98,101,119,56};
       Demo m1=new Demo();
        Sub_demo m2=new Sub_demo();
        System.out.println("排序前的数据为：");
        for (int i=0;i<10;i++)
            System.out.print("  "+a[i]);
        //调用父类中的sotr()方法
        m1.sort(a.length ,a);
        System.out.println("\n按升序排列的数据为：");
        for (int i=0;i<10;i++)
            System.out.print("  "+a[i]);
        //调用子类中的sort()方法,该方法实现了对父类中方法的覆盖
        m2.sort(a.length ,a);
        System.out.println("\n按降序排列的数据为：");
        for (int i=0;i<10;i++)
            System.out.print("  "+a[i]);               }

}
