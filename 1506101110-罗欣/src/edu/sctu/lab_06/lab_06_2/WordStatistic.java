package src.edu.sctu.lab_06.lab_06_2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Vector;

/**使用RandomAccessFile流统计一篇英文中的单词，要求如下：
 （1）一共出现了多少个单词；
 （2）有多少个互不相同的单词；
 （3）给出每个单词出现的频率，并将这些单词按频率大小顺序显示在一个TextArea中。

 * Created by Administrator on 2016/11/30.
 */
public class WordStatistic {
    Vector allWorsd,noSameWord;
    WordStatistic()
    {  allWorsd=new Vector();
        noSameWord=new Vector();
    }

    public void wordStatistic(File file){

        try {
            RandomAccessFile inOne = new  RandomAccessFile(file,"rw"); //创建指向文件file的inOne 的对象
            RandomAccessFile inTwo = new RandomAccessFile(file,"rw");   //创建指向文件file的inTwo 的对象
            long wordStarPosition=0,wordEndPosition=0;
            long length = inOne.length();
            int flag=1;
            int c=-1;
            for(int k=0;k<=length;k++){
                c=inOne.read();   // inOne调用read()方法
                boolean boo=(c<='Z'&&c>='A')||(c<='z'&&c>='a');
                if(boo){
                    if(flag==1){
                    wordEndPosition=inOne.getFilePointer()-1;
                    flag=0;}
                }else{
                    if(flag==0){
                        if(c==-1)
                            wordEndPosition=inOne.getFilePointer();
                        else
                            wordEndPosition=inOne.getFilePointer()-1;
                            inTwo.seek(wordEndPosition);// inTwo调用seek方法将读写位置移动到wordStarPostion
                            byte cc[]=new byte[(int)wordEndPosition-(int)wordStarPosition];
                            inTwo.readFully(cc);//inTwo调用readFully(byte a)方法，向a传递cc
                            String word = new String(cc);
                            allWorsd.add(word);
                            if(!(noSameWord.contains(word)))
                                noSameWord.add(word);
                    }
                    flag=1;
                }
            }
            inOne.close();
            inTwo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Vector getAllWorsd(){
        return allWorsd;
    }
    public Vector getNoSameWord(){
        return noSameWord;
    }
}
