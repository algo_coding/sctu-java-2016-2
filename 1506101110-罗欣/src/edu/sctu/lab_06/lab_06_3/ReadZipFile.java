package src.edu.sctu.lab_06.lab_06_3;

import com.sun.nio.zipfs.ZipInfo;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**读取book.zip，并将book.zip中含有的文件重新存放到当前目录中的book文件夹中，即将book.zip的内容解压到book文件夹中。
 * Created by Administrator on 2016/12/1.
 */
public class ReadZipFile {
    public static void main(String[] args) {
        File f=new File("F:\\ReadMe.rar");
        File dir=new File("F:\\Book");
        byte b[]=new byte[100];
        dir.mkdir();
        try {
            ZipInputStream in = new ZipInputStream(new FileInputStream(f));
            ZipEntry zipEntry=null;
            while((zipEntry=in.getNextEntry())!=null){
                File file =new File(dir,zipEntry.getName());
                FileOutputStream out=new FileOutputStream(file);
                int n=-1;
                System.out.println(file.getAbsoluteFile()+"的内容：");
                while((n=in.read(b,0,100))!=-1){
                    String str=new String(b,0,n);
                    System.out.println(str);
                    out.write(b,0,n);
                }
                out.close();
            }
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
