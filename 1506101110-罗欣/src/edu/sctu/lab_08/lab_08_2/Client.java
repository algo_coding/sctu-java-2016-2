package src.edu.sctu.lab_08.lab_08_2;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Administrator on 2016/12/25.
 */
public class Client extends Frame implements Runnable,ActionListener {
    Button connection;
    Socket socket=null;
    ObjectInputStream in=null;
    ObjectOutputStream out=null;
    Thread thread;
    public void  Client(){
        socket = new Socket();
        connection=new Button("连接服务器，读取文本对象");
        add(connection,BorderLayout.NORTH);
        connection.addActionListener(this);
        thread=new Thread(this);
        setBounds(100,100,360,310);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
    public void  run(){
        while (true){
            TextArea text= null;
            try {
                text = (TextArea)in.readObject();
                add(text,BorderLayout.CENTER);
                validate();
            } catch (IOException e) {
                break;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==connection){
            if(socket.isConnected()){}
            else {
                InetAddress address= null;
                try {
                    address = InetAddress.getByName("127.0.0.1");
                } catch (UnknownHostException e1) {
                    e1.printStackTrace();
                }
                InetSocketAddress socketAddress=new InetSocketAddress(address,4331);//创建端口为4331、地址为address的socketAddress
                try {
                    socket.connect(socketAddress);  //socket建立和socketAddress的连接呼叫。
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                try {
                    in=new ObjectInputStream(socket.getInputStream());//socket返回输入流
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                try {
                    out=new ObjectOutputStream(socket.getOutputStream());//socket返回输出流
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                thread.start();
            }
        }
    }

    public static void main(String[] args) {
        Client win=new Client();
    }
}
