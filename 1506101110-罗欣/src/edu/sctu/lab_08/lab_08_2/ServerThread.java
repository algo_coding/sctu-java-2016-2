package src.edu.sctu.lab_08.lab_08_2;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ServerThread extends Thread {
   Socket socket;
   ObjectOutputStream out=null;
   ObjectInputStream in=null;
   String s=null;
   public ServerThread(Socket t) {
       socket=t;
       try {
           out=new ObjectOutputStream(socket.getOutputStream());//socket返回输出流。
       } catch (IOException e) {
           e.printStackTrace();
       }
       try {
           in=new ObjectInputStream(socket.getInputStream());//socket返回输入流。
       } catch (IOException e) {
           e.printStackTrace();
       }
   }
   public void  run(){
       TextArea text=new TextArea("你好，我是服务器",12,12);
       try {
           out.writeObject(text);
       } catch (IOException e) {
           System.out.println("客户离开");
       }
   }
}
