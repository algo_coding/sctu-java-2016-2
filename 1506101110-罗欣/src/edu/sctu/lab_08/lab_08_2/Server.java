package src.edu.sctu.lab_08.lab_08_2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Administrator on 2016/12/25.
 */
public class Server {
    public static void main(String[] args) {
        ServerSocket server=null;
        ServerThread thread;
        Socket you=null;
        while (true){
            try {
                server=new ServerSocket(4331);
            } catch (IOException e) {
                System.out.println("我正在监听");
            }
            try {
                you=server.accept();// server返回和客户端相连接的Socket对象
            } catch (IOException e) {
                System.out.println("正在等待客户");
            }
            System.out.println("客户的地址"+you.getInetAddress());
            if(you!=null){
                new ServerThread(you).start();
            }
            else {continue;}
        }
    }
}

