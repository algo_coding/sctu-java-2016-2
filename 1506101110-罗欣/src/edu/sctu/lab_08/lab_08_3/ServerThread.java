package src.edu.sctu.lab_08.lab_08_3;

import org.omg.CORBA.DataOutputStream;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * Created by Administrator on 2016/12/25.
 */
public class ServerThread extends  Thread {
    InetAddress address;
    DataOutputStream out=null;
    DataInputStream in=null;
    String s=null;
    ServerThread(InetAddress address) {
        this.address = address;
    }
    public void run(){
        FileInputStream in;
        byte b[]=new  byte[8192];
        try {
            in=new FileInputStream("a.jpg");
            int n=-1;
            while ((n=in.read(b))!=-1){
                DatagramPacket data=new DatagramPacket(b,b.length,address,5678);//创建数据包，目标地址和端口分别是address和5678，其中的数据为数组b中的前n个字节
                DatagramSocket mailSend=new DatagramSocket(); //创建发送数据的DatagramSocket对象
                mailSend.send(data);// mailSend发送数据data
            }
            in.close();
            byte end[]="end".getBytes();
            DatagramPacket data=new DatagramPacket(end,end.length,address,5678);//创建数据包，目标地址和端口分别是address和5678，其中的数据为数组end中的全部字节
            DatagramSocket mailSend=new DatagramSocket();//创建负责发送数据的DatagramSocket对象
            mailSend.send(data);// mailSend发送数据data
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
