package src.edu.sctu.lab_08.lab_08_3;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.*;

/**
 * Created by Administrator on 2016/12/25.
 */
public class Client extends Frame implements Runnable,ActionListener {
    Button b=new Button("获取图像");
    InetAddress address=null;
    ImageCanvas canvas;
    Client(){
        super("I am a client");
        setSize(320, 200);
        setVisible(true);
        b.addActionListener(this);
        add(b, BorderLayout.NORTH);
        canvas=new ImageCanvas();
        add(canvas,BorderLayout.CENTER);
        Thread thread=new Thread(this);
        validate();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        thread.start();
    }
    public void actionPerformed(ActionEvent event){
        byte b[]="请发图像".trim().getBytes();
        try {
            InetAddress address=InetAddress.getByName("127.0.0.1");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        DatagramPacket data=new DatagramPacket(b,b.length,address,1234); //创建数据包，该数据包的目标地址和端口分别是address和1234，其中的数据为数组b中的全部字节。
        DatagramSocket mailSend= null; //创建负责发送数据的DatagramSocket对象。
        try {
            mailSend = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        try {
            mailSend.send(data);                  // mailSend发送数据data。
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void run(){
        DatagramPacket pack=null;
        DatagramSocket mailReceive=null;
        byte b[]=new  byte[8192];
        ByteArrayOutputStream out=new ByteArrayOutputStream();
        pack=new DatagramPacket(b,b.length);
        try {
            DatagramSocket datagramSocket=new DatagramSocket();  //创建在端口5678负责收取数据包的DatagramSocket对象。
        } catch (SocketException e) {
            e.printStackTrace();
        }

        while (true){
            try {
                mailReceive.receive(pack);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String message=new String(pack.getData(),0,pack.getLength());
            if(message.startsWith("end")){
                break;
            }
            out.write(pack.getData(),0,pack.getLength());
        }
        byte imagebyte[]=out.toByteArray();
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toolkit tool=getToolkit();
        Image image=tool.createImage(imagebyte);
        canvas.setImage(image);
        canvas.repaint();
        validate();
    }

    public static void main(String[] args) {
        new Client();
    }

}
