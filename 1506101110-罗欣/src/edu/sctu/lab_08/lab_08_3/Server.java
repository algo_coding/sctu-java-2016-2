package src.edu.sctu.lab_08.lab_08_3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * Created by Administrator on 2016/12/25.
 */
public class Server {
    public static void main(String[] args) {
        DatagramPacket pack=null;
        DatagramSocket mailReceiv=null;
        ServerThread thread;
        byte b[]=new byte[8192];
        InetAddress address=null;
        pack=new DatagramPacket(b,b.length);
        while (true){
            try {
                mailReceiv=new DatagramSocket(1234);//创建在端口1234负责收取数据包的DatagramSocket对象。
            } catch (SocketException e) {
                System.out.println("正在等待");
            }
            try {
                mailReceiv.receive(pack);
                address=pack.getAddress();//pack返回InetAddress对象。
                System.out.println("客户的地址"+address);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(address!=null){
                new  ServerThread(address).start();
            }
            else {
                continue;
            }
        }
    }
}
