package src.edu.sctu.lab_08.lab_08_3;

import java.awt.*;

/**
 * Created by Administrator on 2016/12/25.
 */

public  class ImageCanvas extends Canvas{
    Image image=null;
    public ImageCanvas(){
        setSize(200, 200);
    }
    public void paint(Graphics g){
        if(image!=null)
            g.drawImage(image,0,0,this);
    }
    public void setImage(Image image){
        this.image=image;
    }

}
