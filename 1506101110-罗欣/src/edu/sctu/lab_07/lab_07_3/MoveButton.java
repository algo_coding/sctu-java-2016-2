package src.edu.sctu.lab_07.lab_07_3;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Administrator on 2016/12/25.
 */
public class MoveButton extends Frame implements  Runnable,ActionListener {
    Thread first,second;
    Button redButton,greenButton,startButton;
    int distance=10;
    public void MoveButton(){
        first=new Thread(this);//创建first线程，当前窗口做为该线程的目标对象
        second=new Thread(this);//创建second线程，当前窗口做为该线程的目标对象
        redButton=new Button();
        greenButton=new Button();
        redButton.setBackground(Color.red);
        greenButton.setBackground(Color.green);
        startButton =new Button("start");
        startButton.addActionListener(this);
        setLayout(null);
        add(redButton);
        redButton.setBounds(10, 60, 15, 15);
        add(greenButton);
        greenButton.setBounds(100, 60, 15, 15);
        add(startButton);
        startButton.setBounds(10, 100, 30, 30);
        setBounds(0, 0, 300, 200);
        setVisible(true);
        validate();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
    public void actionPerformed(ActionEvent e){
        first.start();
        second.start();
    }
    public void run(){
        while (true){
            if(Thread.currentThread()==first){
                try {
                    moveComponent(redButton);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(Thread.currentThread()==second){
                try {
                    moveComponent(greenButton);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public synchronized void  moveComponent(Component b) throws InterruptedException {
        if(Thread.currentThread()==first){
            while (distance>100&&distance<=200){
                wait();
                distance=distance+1;
                b.setLocation(distance,60);
                if(distance>=100){
                    b.setLocation(10,60);
                    notifyAll();
                }
            }
            if(Thread.currentThread()==second){
                while (distance>=10&&distance<100){
                    wait();
                    distance=distance+1;
                    b.setLocation(distance,60);
                    if(distance>200){
                        distance=10;
                        b.setLocation(100,60);
                        notifyAll();

                    }
                }
            }
        }
    }
}
