package src.edu.sctu.lab_07.lab_07_2;

import java.awt.*;

/**
 * Created by Administrator on 2016/12/22.
 */
public class Mycanvas extends Canvas {
    int r;
    Color c;
    public void serColor(Color c){
        this.c=c;
    }
    public void setR(int r){
        this.r=r;
    }
    public void paint(Graphics g){
        g.setColor(c);
        g.fillOval(0,0,2*r,2*r);
    }
    public int getR(){
        return r;
    }
}
