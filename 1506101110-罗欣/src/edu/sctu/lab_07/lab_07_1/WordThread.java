package src.edu.sctu.lab_07.lab_07_1;

import java.awt.*;

/**编写一个Java应用程序，在主线程中再创建一个Frame类型的
 * 窗口，在该窗口中再创建1个线程giveWord。线程giveWord每隔2
 * 秒钟给出一个汉字，用户
 * 使用一种汉字输入法将该汉字输入到文本框中。
 * Created by Administrator on 2016/12/1.
 */
public class WordThread extends Thread {
    char word;
    int k = 19968;
    Label com;
    int time = 6000;

    WordThread(Label com) {
        this.com = com;
    }

    public void setTime(int n) {
        time = n;
    }

    public void run() {
        k = 19968;
        while (true) {
            word = (char) k;
            com.setText("" + word);
            try {
                Thread.sleep(time);  //调用sleep方法使得线程中断6000豪秒
            } catch (InterruptedException e) {
            }
            k++;
            if (k >= 29968) k = 19968;
        }
    }
}
