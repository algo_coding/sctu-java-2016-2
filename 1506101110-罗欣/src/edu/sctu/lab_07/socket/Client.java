package src.edu.sctu.lab_07.socket;

import java.io.*;
import java.net.Socket;

/**
 * Created by Administrator on 2016/12/8.
 */
public class Client {
    public static void main(String[] args) throws IOException{
        Socket socket=new Socket("Localhost",10086);//客户端怎么访问服务器端
        OutputStream os=socket.getOutputStream();//首先客户端会写入访问 //字节输出流
        PrintWriter pw=new PrintWriter(os);
        pw.write("用户名：admin；密码：123");
        pw.flush();
        socket.shutdownInput();

        //中间时间,客户端发送访问到服务器端


        //服务器端返回客户端，客户端读取数据
        InputStream is=socket.getInputStream();
        InputStreamReader isr= new InputStreamReader(is);
        BufferedReader br =new BufferedReader(isr);
        String info=null;
        while ((info=br.readLine())!=null){
            System.out.println("我是客户端，服务器端说"+info);
        }
        //从下到上一次关闭流
        br.close();
        isr.close();
        is.close();
        pw.close();
        os.close();
        socket.close();

    }
}
