package src.edu.sctu.lab_07.socket;

import sun.java2d.pipe.BufferedRenderPipe;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.Buffer;

/**
 * Created by Administrator on 2016/12/8.
 */
public class Server {
    public static void main(String[] args) throws IOException{
        ServerSocket serverSocket=new ServerSocket(10086);//1024-65535的某个端口
        Socket socket=new Socket();
        socket=serverSocket.accept();//接受客户端请求，下面是怎么接受客户端请求
        InputStream is= socket.getInputStream();//读入输入流，读取
        InputStreamReader isr=new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String info=null;
        while ((info=br.readLine())!=null){
            System.out.println("我是客户端"+info);
        }
        socket.shutdownInput();//关闭输入流

        OutputStream os=socket.getOutputStream();//写入输入流，返回给客户端的信息
        PrintWriter pw=new PrintWriter(os);
        pw.write("欢迎您！");
        pw.flush();
        //用完之后要依次从下到上关闭流
        pw.close();
        os.close();
        br.close();
        isr.close();
        is.close();
        socket.close();
        serverSocket.close();
    }
}
