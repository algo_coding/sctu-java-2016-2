package src.edu.sctu.lab_02;

import javax.swing.*;

/**
 * Created by Administrator on 2016/10/27.
 */
public class Number {
    public static void main(String[] args) {
        int number =0,d5,d4,d3,d2,d1;
        String str = JOptionPane.showInputDialog("输入一个1至99999之间的数");
        number = Integer.parseInt(str);
        if(number>=1&&number<=99999){
            d5=number/10000;
            d4=number%10000/1000;
            d3=number/100%10;
            d2=number%100/10;
            d1=number%10;
            if(number>=10000&&number<=99999){
                System.out.println(number+"是5位数");
                if(d1==d5&&d2==d4){
                    System.out.println(number +"是回文数");
                }
                else{
                    System.out.println(number +"不是回文数");
                }
            }
           else if(number>=1000&&number<=9999){
                System.out.println(number+"是4位数");
                if(d1==d4&&d2==d3){
                    System.out.println(number +"是回文数");
                }
                else{
                    System.out.println(number +"不是回文数");
                }
            }
            else if (number>=100&&number<=999){
                System.out.println(number+"是3位数");
                if(d1==d3){
                    System.out.println(number +"是回文数");
                }
                else{
                    System.out.println(number +"不是回文数");
                }
            }
            else if(d2!=0){
                System.out.println(number+"是2位数");
                if(d1==d2){
                    System.out.println(number +"是回文数");
                }
                else{
                    System.out.println(number +"不是回文数");
                }
            }
            else if(d1!=0){
                System.out.println(number+"是1位数");
                    System.out.println(number +"是回文数");
            }
        }
        else{
            System.out.println("\n%d不在1-99999之间"+number);
        }
    }
}
