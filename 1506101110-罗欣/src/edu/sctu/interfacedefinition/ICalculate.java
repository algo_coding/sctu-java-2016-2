package src.edu.sctu.interfacedefinition;

/**
 * Created by Administrator on 2016/10/8.
 */
public interface ICalculate {
    final float PI=3.14159f;      //定义用于表示圆周率的常量PI
    float getArea(float r);      //定义一个用于计算面积的方法 getArea()
    float getCircumference(float r); //定义一个用于计算周长的方法getCircumference

}
