package src.edu.sctu.interfacedefinition;

import java.io.IOException;

/**
 * Created by Administrator on 2016/10/11.
 */
public interface Restaurant <T>{
    public T getRestaurantname() throws IOException;  //定义一个用于获取餐厅名称的方法

    public T getRestaurantrank() throws IOException;  //定义一个用于获取餐厅排名的方法

    public T getRestaurantcomment() throws IOException;  //定义一个用于获取餐厅评论的方法

    public T getMapphoto() throws IOException;       //定义一个用于获取餐厅地图，游客照片

    public T getChaincuisines() throws IOException;  //定义一个用于获取餐厅菜系的方法
}
