package src.edu.sctu.interfacedefinition;

/**
 * Created by Administrator on 2016/10/8.
 */
public   class Cire implements ICalculate{
    //定义计算圆面积的方法
    public float getArea(float r){  //计算圆面积并赋值给变量 area
        float area= PI*r*r;
        return area;               //返回计算后的圆面积
    }
    //定义计算圆周长的方法
    public float getCircumference(float r){
        float circumference = 2*PI*r;   //计算圆周长并赋值给变量circumference
        return circumference;  //返回计算后的圆周长
    }
}
