package src.edu.sctu.reflection;

/**
 * Created by Administrator on 2016/10/27.
 */
public class Student {
   private String name;
    private int age;
    private int no;
    public  void study(){
        System.out.println("hand study!");
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getNo() {
        return no;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setNo(int no) {
        this.no = no;
    }
}
