package src.edu.sctu.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**反射机制
 * Created by Administrator on 2016/10/27.
 */
public class Client {


    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {

       // Student p = new Student();

        /*
        p.setAge(10);
        p.setName("xiaoming");
        System.out.println(p.getName() + p.getAge());
    */

        Class clz = Class.forName("Student");
        /*利用反射机制获得属性
        Field[] fields = clz.getDeclaredFields();
        for(Field field:fields) {
            System.out.println(field.getName());
        }*/
        /*获得方法
        Method[] methods = clz.getMethods();
        for(Method method:methods){
            System.out.println(method.getName());
        }*/
       // p.study();

       /*获取单个方法
        Method method=clz.getDeclaredMethod("study");

        method .invoke(p);*/


        /*另一种实例化
        Student chen=(Student) clz.newInstance();
        chen.setAge(10);
        System.out.println(chen.getAge());*/
    }
}
