package src.edu.sctu.lab_03;

/**计算圆的面积和周长
 * Created by Administrator on 2016/11/3.
 */
public class Circle {
    double radius,area,length;
    final double  π=3.14;
   public  Circle(double r){
       radius=r;
   }


    public double getArea(){
        area= π*radius*radius;
        return area;
    }

    public  double getLength(){
        length=2*π*radius;
        return length;
    }

    public void setRadius(double newRadius){
    radius=newRadius;
    }
    public double getRadius(){
        return radius;
    }
}
