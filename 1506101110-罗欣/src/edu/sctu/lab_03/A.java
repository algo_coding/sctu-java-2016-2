package src.edu.sctu.lab_03;

/**
 * Created by Administrator on 2016/11/7.
 */
public class A {
    float a;
    static   float b;  //定义了static修饰，只能保存一次数据，并且是最后一次复制的数据才能保存下来。
    public  void setA(float A){//新建方法
        a=A;
    }
    public void setB(float B){
        b=B;
    }


    public float getA() {
        return a;
    }

    public static float getB() {
        return b;
    }

    public void inputA(){
        System.out.println(a+b);
    }
    static void inputB(){
        System.out.println(+b);
    }
}
